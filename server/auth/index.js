const bodyParser = require('body-parser');
const FacebookTokenStrategy = require('passport-facebook-token');
const GoogleTokenStrategy = require('passport-google-id-token');
const LocalStrategy = require('passport-local').Strategy;
const passport = require('passport');
const path = require('path');
const session = require('express-session');
// const _ = require('lodash');

const request = require('../utils/request');

const config = require(path.resolve(process.cwd(), 'package.json')).config;
const apiUrl = process.env.API_URL || config.api_url;

module.exports = function auth(app) {
  /* ----- Setup ----- */

  app.use(bodyParser());
  app.use(session({ secret: config.session_secret }));
  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser((user, done) => done(null, user));
  passport.deserializeUser((user, done) => done(null, user));

  const checkUser = (provider, { username, password, authorization }, done) => {
    request.post(`${apiUrl}/auth`, {
      data: {
        auth: { email: username, password },
      },
    }).then((user) => {
      done(null, user);
    }).catch((error) => {
      done(null, false, { message: `Login failed! ${error}` });
    });
  };

  passport.use(new LocalStrategy(
    (username, password, done) => {
      if (username && password) {
        checkUser('local', { username, password }, done);
      } else {
        done(null, false, { message: 'Login failed!' });
      }
    }
  ));

  passport.use(new FacebookTokenStrategy({
    clientID: process.env.FBClientId || config.fb_client_id,
    clientSecret: process.env.FBClientSecret || config.fb_client_secret,
  }, (accessToken, refreshToken, profile, done) => {
    checkUser('facebook', { facebook_id: profile.id }, done);
  }));

  passport.use(new GoogleTokenStrategy({
    clientID: process.env.GoogleClientId || config.google_client_id,
  }, (parsedToken, googleId, done) => {
    checkUser('google', { google_id: googleId }, done);
  }));

  /* ----- END Setup ----- */

  /* ----- Authentication Routes ----- */

  app.get('/auth', (req, res) => {
    if (req.user) {
      res.send(req.user);
    } else {
      res.status(401).send({ message: 'No user logged in!' });
    }
  });

  app.post('/register', function(req, res, done) {
    const { user } = req.body;
    request.post(`${apiUrl}/register`, { data: { user } })
    .then((registeredUser) => {
      req.login(registeredUser, function(err) {
        if (err) {
          console.log('error: ', err);
        }
        res.send(registeredUser);
      });
    }).catch((error) => {
      done(null, false, { message: `registration failed! ${error}` });
    });
  });

  function checkAuth(req, res, next) {
    return (err, user, info) => {
      if (err) {
        next(err);
      } else if (!user) {
        res.status(401).json({ error: info });
      } else {
        req.login(user, (loginErr) => {
          if (loginErr) {
            next(loginErr);
          } else {
            res.send(user);
          }
        });
      }
    };
  }

  app.post('/auth/local',
    (req, res, next) => { passport.authenticate('local', checkAuth(req, res, next))(req, res, next); }
  );

  app.post('/auth/facebook',
    (req, res, next) => { passport.authenticate('facebook-token', checkAuth(req, res, next))(req, res, next); }
  );

  app.post('/auth/google',
    (req, res, next) => { passport.authenticate('google-id-token', checkAuth(req, res, next))(req, res, next); }
  );

  app.post('/logout', (req, res) => {
    req.logout();
    res.send({ message: 'Logout successful' });
  });

  /* ----- END Authentication Routes ----- */
};
