const fetch = require('node-fetch');

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  return response.json();
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(`${response.statusText}`);
  error.response = response;
  throw error;
}

function getQueryString(params) {
  const esc = encodeURIComponent;
  return Object.keys(params)
    .map((k) => `${esc(k)}=${esc(params[k])}`)
    .join('&');
}

function request(url, params) {
  const method = params.method || 'GET';
  let qs = '';
  let body;
  const credentials = 'include';
  const headers = params.headers || {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
  if (['GET', 'DELETE'].indexOf(method) > -1) {
    qs = `?${getQueryString(params.data)}`;
  } else { // POST or PUT
    body = JSON.stringify(params.data);
  }

  return fetch(`${url}${qs}`, { method, headers, body, credentials })
    .then(checkStatus)
    .then(parseJSON);
}

module.exports = {
  get: (url, params) => request(url, Object.assign({ method: 'GET' }, params)),
  post: (url, params) => request(url, Object.assign({ method: 'POST' }, params)),
  put: (url, params) => request(url, Object.assign({ method: 'PUT' }, params)),
  delete: (url, params) => request(url, Object.assign({ method: 'DELETE' }, params)),
};
