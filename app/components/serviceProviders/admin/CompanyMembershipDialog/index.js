
/**
*
* CompanyMembershipDialog
*
*/

import React, { PropTypes } from 'react';
import AutoComplete from 'material-ui/AutoComplete';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import LinkIcon from 'material-ui/svg-icons/content/link';
import MenuItem from 'material-ui/MenuItem';
import PersonOutline from 'material-ui/svg-icons/social/person-outline';
import { Tabs, Tab } from 'material-ui/Tabs';
import { fromJS } from 'immutable';
import { FormattedMessage } from 'react-intl';

import CenteredLoadingSpinner from 'components/common/CenteredLoadingSpinner';
import { ID as COMPANY_ID, NAME as COMPANY_NAME } from 'entities/schemas/companies/constants';
import {
  CAN_EDIT as COMPANY_MEMBERSHIP_CAN_EDIT,
  COMPANY_ID as COMPANY_MEMBERSHIP_COMPANY_ID,
  ID as COMPANY_MEMBERSHIP_ID,
  ROLE_ID as COMPANY_MEMBERSHIP_ROLE_ID,
  USER_ID as COMPANY_MEMBERSHIP_USER_ID,
} from 'entities/schemas/companyMemberships/constants';
import { ID as ROLE_ID, DISPLAY_NAME } from 'entities/schemas/roles/constants';
import { ID as USER_ID, FIRST_NAME, LAST_NAME, EMAIL } from 'entities/schemas/users/constants';

import messages from './messages';

const DELETED = 'deleted';
const UPDATED = 'updated';

class CompanyMembershipDialog extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = { companyMemberships: fromJS([]), searchText: '' };

    this.autoCompleteDataSource = this.autoCompleteDataSource.bind(this);
    this.getStateOfCompanyMemberships = this.getStateOfCompanyMemberships.bind(this);
    this.handleAutoCompleteNewRequest = this.handleAutoCompleteNewRequest.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleRemoveMembership = this.handleRemoveMembership.bind(this);
    this.handleUpdateInput = this.handleUpdateInput.bind(this);
    this.renderTabs = this.renderTabs.bind(this);
    this.renderUserAutoComplete = this.renderUserAutoComplete.bind(this);
  }

  /* ----- Lifecycle ----- */

  componentWillReceiveProps(nextProps) {
    const { companyMemberships, loading, open } = this.props;
    const { company, companyMemberships: nextCompanyMemberships, open: nextOpen } = nextProps;
    if (open && !nextOpen) {
      this.setState({ companyMemberships: fromJS([]), searchText: '' });
    }
    if (
      !loading &&
      (company && company.get(COMPANY_ID)) &&
      ((!open && nextOpen) ||
      (!companyMemberships && nextCompanyMemberships) ||
      (companyMemberships && !companyMemberships.equals(nextCompanyMemberships)))
    ) {
      this.setState({
        companyMemberships: nextCompanyMemberships.filter((m) =>
          m.get(COMPANY_MEMBERSHIP_COMPANY_ID) === company.get(COMPANY_ID)),
      });
    }
  }

  /* ----- END Lifecycle ----- */

  /* ----- Helper Methods ----- */

  getStateOfCompanyMemberships() {
    const { companyMemberships } = this.state;
    const created = companyMemberships.filter((m) => !m.get(COMPANY_MEMBERSHIP_ID));
    const updated = companyMemberships
      .filter((m) => m.get(UPDATED))
      .map((m) => m.delete(DELETED).delete(UPDATED));
    const deleted = companyMemberships
      .filter((m) => m.get(DELETED))
      .map((m) => m.delete(DELETED).delete(UPDATED));
    return { created, updated, deleted };
  }

  /* ----- END Helper Methods ----- */

  /* ----- Event Binding ----- */

  handleAutoCompleteNewRequest(roleId) {
    const { company } = this.props;
    return (chosenRequest) => {
      const { companyMemberships: initialCompanyMemberships } = this.props;
      const { companyMemberships } = this.state;
      const matchMembership = (m) =>
        m.get(COMPANY_MEMBERSHIP_COMPANY_ID) === company.get(COMPANY_ID) &&
        m.get(COMPANY_MEMBERSHIP_USER_ID) === chosenRequest[COMPANY_MEMBERSHIP_USER_ID];
      const existingMembershipEntry = companyMemberships.findEntry(matchMembership);
      const existingInitialMembershipEntry = initialCompanyMemberships.findEntry(matchMembership);
      if (existingMembershipEntry) {
        const [existingMembershipIndex, existingMembership] = existingMembershipEntry;
        this.setState({
          companyMemberships: companyMemberships
            .set(existingMembershipIndex, existingMembership
              .set(COMPANY_MEMBERSHIP_CAN_EDIT, true)
              .set(COMPANY_MEMBERSHIP_ROLE_ID, roleId)
              .set(DELETED, false)
              .set(UPDATED, existingInitialMembershipEntry &&
                existingInitialMembershipEntry[1].get(COMPANY_MEMBERSHIP_ROLE_ID) !== roleId)
            ),
          searchText: '',
        });
      } else {
        this.setState({
          companyMemberships: companyMemberships
            .push(fromJS({
              [COMPANY_MEMBERSHIP_CAN_EDIT]: true,
              [COMPANY_MEMBERSHIP_COMPANY_ID]: company.get(COMPANY_ID),
              [COMPANY_MEMBERSHIP_ROLE_ID]: roleId,
              [COMPANY_MEMBERSHIP_USER_ID]: chosenRequest[COMPANY_MEMBERSHIP_USER_ID],
            })
          ),
          searchText: '',
        });
      }
      setTimeout(() => {
        this.userAutoComplete.focus();
      }, 5);
    };
  }

  handleRemoveMembership(membership) {
    return () => {
      const { companyMemberships } = this.state;
      const [index, matchingMembership] = companyMemberships.findEntry((m) =>
        m.get(COMPANY_MEMBERSHIP_COMPANY_ID) === membership.get(COMPANY_MEMBERSHIP_COMPANY_ID) &&
        m.get(COMPANY_MEMBERSHIP_USER_ID) === membership.get(COMPANY_MEMBERSHIP_USER_ID)
      );
      const updatedCompanyMemberships = membership.get(COMPANY_MEMBERSHIP_ID) ?
        companyMemberships.set(index, matchingMembership.set(DELETED, true)) :
        companyMemberships.delete(index);
      this.setState({
        companyMemberships: updatedCompanyMemberships,
      });
    };
  }

  handleSave() {
    const { onSave } = this.props;
    onSave(this.getStateOfCompanyMemberships());
  }

  handleUpdateInput(searchText) {
    this.setState({ searchText });
  }

  /* ----- END Event Binding ----- */

  /* ----- Helpers ----- */

  autoCompleteDataSource(roleId) {
    const { company, roles, users } = this.props;
    const { companyMemberships } = this.state;
    function getMembershipRole(membership) {
      if (!membership || membership.get(DELETED)) { return ''; }
      return `(Currently ${roles
        .filter((r) => r.get(ROLE_ID) === membership.get(COMPANY_MEMBERSHIP_ROLE_ID))
        .getIn([0, DISPLAY_NAME])})`;
    }
    return users.reduce((memo, user) => {
      const membershipMatch = (m) =>
        m.get(COMPANY_MEMBERSHIP_USER_ID) === user.get(USER_ID) &&
        m.get(COMPANY_MEMBERSHIP_COMPANY_ID) === company.get(COMPANY_ID);
      const existingMembership = companyMemberships.filter(membershipMatch).first();
      return existingMembership &&
        existingMembership.get(COMPANY_MEMBERSHIP_ROLE_ID) === roleId &&
        !existingMembership.get(DELETED) ?
          memo : memo.concat({
            value: (<MenuItem
              leftIcon={<PersonOutline />}
              primaryText={`${user.get(FIRST_NAME)} ${user.get(LAST_NAME)} (${user.get(EMAIL)}) ${getMembershipRole(existingMembership)}`}
            />),
            text: `${user.get(FIRST_NAME)} ${user.get(LAST_NAME)} (${user.get(EMAIL)})`,
            [COMPANY_MEMBERSHIP_ID]: existingMembership ? existingMembership.get(COMPANY_MEMBERSHIP_ID) : null,
            [COMPANY_MEMBERSHIP_USER_ID]: user.get(USER_ID),
          });
    }, fromJS([]));
  }

  /* ----- END Helpers ----- */

  /* ----- Rendering ----- */

  renderUserAutoComplete(roleId) {
    const { searchText } = this.state;
    const filter = (st, key = '') =>
      st !== '' && key.trim().toLowerCase().indexOf(st.trim().toLowerCase()) !== -1;
    return (<AutoComplete
      dataSource={this.autoCompleteDataSource(roleId).toJS()}
      filter={filter}
      fullWidth
      hintText={'Add User'}
      onNewRequest={this.handleAutoCompleteNewRequest(roleId)}
      onUpdateInput={this.handleUpdateInput}
      ref={(input) => { this.userAutoComplete = input; }}
      searchText={searchText}
      style={{ padding: '0 10px' }}
    />);
  }

  renderTabs() {
    const { roles, users } = this.props;
    const { companyMemberships } = this.state;
    return roles.map((role) => {
      const filteredCompanyMemberships = companyMemberships
        .filter((m) => m.get(COMPANY_MEMBERSHIP_ROLE_ID) === role.get(ROLE_ID) && !m.get(DELETED));
      const membershipCount = filteredCompanyMemberships.size;
      return (<Tab
        key={`role_${role.get(ROLE_ID)}_tab`}
        label={`${role.get(DISPLAY_NAME)} ${membershipCount ? `(${membershipCount})` : ''}`}
        style={{ height: '100%' }}
      >
        {this.renderUserAutoComplete(role.get(ROLE_ID))}
        <div style={{ marginTop: 15 }}>
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {
              filteredCompanyMemberships
                .map((membership) => {
                  const user = users
                    .filter((u) => u.get(USER_ID) === membership.get(COMPANY_MEMBERSHIP_USER_ID))
                    .first() || fromJS({});
                  const onRequestDelete = membership.get(COMPANY_MEMBERSHIP_CAN_EDIT) ?
                    this.handleRemoveMembership(membership) : null;
                  return (<Chip
                    key={`membership_chip_${membership.get(COMPANY_MEMBERSHIP_USER_ID)}`}
                    onRequestDelete={onRequestDelete}
                    style={{ margin: 4 }}
                  >
                    <Avatar size={40} icon={<PersonOutline />} /> {`${user.get(FIRST_NAME)} ${user.get(LAST_NAME)} (${user.get(EMAIL)})`}
                  </Chip>);
                })
              }
          </div>
        </div>
      </Tab>);
    });
  }

  render() {
    const { company, contentStyle, loading, modal, onCancel, open } = this.props;
    const noChanges = () => {
      const { created, updated, deleted } = this.getStateOfCompanyMemberships();
      return !created.size && !updated.size && !deleted.size;
    };
    const actions = [
      <FlatButton
        key="company_memberships_modal_cancel_btn"
        label={<FormattedMessage {...messages.cancel} />}
        onTouchTap={onCancel}
        secondary
      />,
      <FlatButton
        disabled={loading || noChanges()}
        key="company_memberships_modal_save_btn"
        label={<FormattedMessage {...messages.save} />}
        onTouchTap={this.handleSave}
        primary
      />,
    ];

    return (<Dialog
      actions={actions}
      contentStyle={contentStyle}
      title={<h3 style={{ padding: '10px 24px' }}>
        <LinkIcon style={{ width: 40, height: 40, marginRight: 8 }} />
        <b>
          <FormattedMessage {...messages.companyMemberships} values={{ companyName: company.get(COMPANY_NAME) }} />
        </b>
      </h3>}
      modal={modal}
      open={!!open}
    >
      {loading ? <CenteredLoadingSpinner /> : <Tabs>{this.renderTabs()}</Tabs>}
    </Dialog>);
  }

  /* ----- END Rendering ----- */
}

CompanyMembershipDialog.defaultProps = {
  company: fromJS({}),
  contentStyle: { maxWidth: 'none', width: '80%' },
  loading: false,
  companyMemberships: fromJS([]),
  modal: true,
  open: false,
  roles: fromJS([]),
  users: fromJS([]),
};

CompanyMembershipDialog.propTypes = {
  company: PropTypes.object,
  contentStyle: PropTypes.object,
  companyMemberships: PropTypes.object,
  loading: PropTypes.bool,
  modal: PropTypes.bool,
  onCancel: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  open: PropTypes.bool,
  roles: PropTypes.object,
  users: PropTypes.object,
};

export default CompanyMembershipDialog;
