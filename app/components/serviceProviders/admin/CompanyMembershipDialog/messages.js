/*
 * CompanyMembershipDialog Messages
 *
 * This contains all the text for the CompanyMembershipDialog component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  cancel: {
    id: 'cancel',
    defaultMessage: 'Cancel',
  },
  companyMemberships: {
    id: 'app.components.serviceProviders.admin.CompanyMembershipDialog.companyMemberships',
    defaultMessage: 'Company Memberships - {companyName}',
  },
  save: {
    id: 'save',
    defaultMessage: 'Save',
  },
});
