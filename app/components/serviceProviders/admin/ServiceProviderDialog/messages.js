/*
 * ServiceProviderDialog Messages
 *
 * This contains all the text for the ServiceProviderDialog component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  cancel: {
    id: 'cancel',
    defaultMessage: 'Cancel',
  },
  name: {
    id: 'name',
    defaultMessage: 'Name',
  },
  parentCompany: {
    id: 'parentCompany',
    defaultMessage: 'Parent Company',
  },
  save: {
    id: 'save',
    defaultMessage: 'Save',
  },
  serviceProvider: {
    id: 'serviceProvider',
    defaultMessage: 'Service Provider',
  },
  serviceProviderType: {
    id: 'serviceProviderType',
    defaultMessage: 'Service Provider Type',
  },
});
