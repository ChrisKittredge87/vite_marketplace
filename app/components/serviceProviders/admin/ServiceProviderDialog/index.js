/**
*
* ServiceProviderDialog
*
*/

import React, { PropTypes } from 'react';
import CircularProgress from 'material-ui/CircularProgress';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import { fromJS } from 'immutable';
import { FormattedMessage } from 'react-intl';
import _ from 'lodash';

import {
  ID as ID_COMPANY,
  NAME as COMPANY_NAME,
} from 'entities/schemas/companies/constants';
import {
  COMPANY_ID,
  NAME as SERVICE_PROVIDER_NAME,
  SERVICE_PROVIDER_TYPE_ID,
} from 'entities/schemas/serviceProviders/constants';
import {
  ID as ID_SERVICE_PROVIDER_TYPE,
  DISPLAY_NAME as DISPLAY_NAME_SERVICE_PROVIDER_TYPE,
} from 'entities/schemas/serviceProviders/types/constants';

import {
  FORM_COMPANY_ID,
  FORM_SERVICE_PROVIDER_NAME,
  FORM_SERVICE_PROVIDER_TYPE_ID,
} from './constants';
import messages from './messages';

class ServiceProviderDialog extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = {
      [FORM_COMPANY_ID]: null,
      [FORM_SERVICE_PROVIDER_NAME]: '',
      [FORM_SERVICE_PROVIDER_TYPE_ID]: null,
    };

    this.handleChangeFormData = this.handleChangeFormData.bind(this);
    this.handleSaveServiceProvider = this.handleSaveServiceProvider.bind(this);
  }

  /* ----- Lifecycle ----- */

  componentWillReceiveProps(nextProps) {
    const { open } = this.props;
    const { open: nextOpen, serviceProvider: nextServiceProvider } = nextProps;
    if (!open && nextOpen) {
      this.setState({
        [FORM_COMPANY_ID]: nextServiceProvider.get(COMPANY_ID),
        [FORM_SERVICE_PROVIDER_NAME]: nextServiceProvider.get(SERVICE_PROVIDER_NAME) || '',
        [FORM_SERVICE_PROVIDER_TYPE_ID]: nextServiceProvider.get(SERVICE_PROVIDER_TYPE_ID),
      });
    }
  }

  /* ----- END Lifecycle ----- */

  /* ----- Event Binding ----- */

  handleChangeFormData(key) {
    return ({ target: { value } }, isToggled, newVal) => {
      let val = value;
      if (_.isBoolean(isToggled)) {
        val = isToggled;
      } else if (_.isNumber(isToggled)) {
        val = newVal;
      }
      this.setState({ [key]: val });
    };
  }

  handleSaveServiceProvider() {
    const { onSaveServiceProvider, serviceProvider } = this.props;
    const { formServiceProviderName, formServiceProviderTypeId } = this.state;
    onSaveServiceProvider(
      serviceProvider
        .set(SERVICE_PROVIDER_NAME, formServiceProviderName)
        .set(SERVICE_PROVIDER_TYPE_ID, formServiceProviderTypeId)
    );
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  render() {
    const { companies, loading, onCloseDialog, open } = this.props;
    const serviceProvider = this.props.serviceProvider || fromJS({});
    const serviceProviderTypes = this.props.serviceProviderTypes || fromJS([]);
    const {
      formCompanyId,
      formServiceProviderName,
      formServiceProviderTypeId,
    } = this.state;
    const actions = [
      <FlatButton
        disabled={loading}
        key="service_provider_dialog_cancel_btn"
        label={<FormattedMessage {...messages.cancel} />}
        onTouchTap={onCloseDialog}
        secondary
      />,
      <FlatButton
        disabled={loading}
        key="service_provider_dialog_save_btn"
        label={<FormattedMessage {...messages.save} />}
        onTouchTap={this.handleSaveServiceProvider}
        primary
      />,
    ];
    const serviceProviderTypesMenuItems = serviceProviderTypes.map((spType) =>
      (<MenuItem
        key={spType.get(ID_SERVICE_PROVIDER_TYPE)}
        value={spType.get(ID_SERVICE_PROVIDER_TYPE)}
        primaryText={spType.get(DISPLAY_NAME_SERVICE_PROVIDER_TYPE)}
      />));
    const companyMenuItems = companies.reduce((memo, company) =>
      memo.concat(<MenuItem
        key={company.get(ID_COMPANY)}
        value={company.get(ID_COMPANY)}
        primaryText={company.get(COMPANY_NAME)}
      />), fromJS([]));
    return (<Dialog
      actions={actions}
      modal
      open={open}
      title={<h3>
        <FormattedMessage
          {...messages.serviceProvider}
          values={{ name: serviceProvider.get(SERVICE_PROVIDER_NAME) }}
        />
      </h3>}
    >
      {
        loading ?
          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <CircularProgress size={150} thickness={5} />
          </div> :
          <div>
            <div style={{ display: 'flex', alignItems: 'flex-start', marginBottom: 20 }}>
              <TextField
                floatingLabelText={<FormattedMessage {...messages.name} />}
                onChange={this.handleChangeFormData(FORM_SERVICE_PROVIDER_NAME)}
                style={{ marginRight: 20, width: '50%' }}
                value={formServiceProviderName}
              />
              <SelectField
                floatingLabelText={<FormattedMessage {...messages.serviceProviderType} />}
                onChange={this.handleChangeFormData(FORM_SERVICE_PROVIDER_TYPE_ID)}
                style={{ width: '50%' }}
                value={formServiceProviderTypeId}
              >
                {serviceProviderTypesMenuItems}
              </SelectField>
            </div>
            <div style={{ display: 'flex', alignItems: 'flex-start', marginBottom: 20 }}>
              <SelectField
                disabled={companyMenuItems.length < 2}
                floatingLabelText={<FormattedMessage {...messages.parentCompany} />}
                onChange={this.handleChangeFormData(FORM_COMPANY_ID)}
                style={{ width: '100%' }}
                value={formCompanyId}
              >
                {companyMenuItems}
              </SelectField>
            </div>
          </div>
      }
    </Dialog>);
  }

  /* ----- END Rendering ----- */
}

ServiceProviderDialog.defaultProps = {
  companies: fromJS([]),
  loading: false,
  onCloseDialog: () => false,
  onSaveServiceProvider: () => false,
  open: false,
  serviceProvider: fromJS({}),
  serviceProviderTypes: fromJS([]),
};

ServiceProviderDialog.propTypes = {
  companies: PropTypes.object,
  loading: PropTypes.bool,
  onCloseDialog: PropTypes.func,
  onSaveServiceProvider: PropTypes.func,
  open: PropTypes.bool,
  serviceProvider: PropTypes.object,
  serviceProviderTypes: PropTypes.object,
};

export default ServiceProviderDialog;
