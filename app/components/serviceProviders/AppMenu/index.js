/**
*
* ServiceProviderAppMenu
*
*/

import React, { PropTypes } from 'react';

import Badge from 'material-ui/Badge';
import Divider from 'material-ui/Divider';
import MessagesIcon from 'material-ui/svg-icons/communication/email';
import EventsIcon from 'material-ui/svg-icons/action/event';
import { FormattedMessage } from 'react-intl';
import FlatButton from 'material-ui/FlatButton';
import HomeIcon from 'material-ui/svg-icons/action/home';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import { Link } from 'react-router';
import LogoutIcon from 'material-ui/svg-icons/action/exit-to-app';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications';
import SearchIcon from 'material-ui/svg-icons/action/search';
import SettingsIcon from 'material-ui/svg-icons/action/settings';

import { path as adminPath } from 'containers/Admin/routes';
import { path as eventsPath } from 'containers/serviceProviders/Events/routes';
import { path as homePath } from 'containers/Home/routes';

import messages from './messages';

export class ServiceProviderAppMenu extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.renderIconMenu = this.renderIconMenu.bind(this);
  }

  /* ----- Rendering ----- */

  renderIconMenu() {
    const { isAdmin, onLogoutUser } = this.props;
    return (<IconMenu
      anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
      iconButtonElement={<IconButton iconStyle={{ color: 'white' }}><MoreVertIcon /></IconButton>}
      targetOrigin={{ horizontal: 'right', vertical: 'top' }}
    >
      <MenuItem
        containerElement={<Link to={homePath}>.</Link>}
        primaryText={<FormattedMessage {...messages.home} />}
        rightIcon={<HomeIcon />}
        style={{ minWidth: 200 }}
      />
      <Divider />
      <MenuItem
        containerElement={<Link to={eventsPath}>.</Link>}
        primaryText={<FormattedMessage {...messages.myEvents} />}
        rightIcon={<EventsIcon />}
        style={{ minWidth: 200 }}
      />
      <Divider />
      { isAdmin ?
        <MenuItem
          key="user_menu_item"
          containerElement={<Link to={`${adminPath}/users`}>.</Link>}
          primaryText={<FormattedMessage {...messages.admin} />}
          rightIcon={<SettingsIcon />}
          style={{ minWidth: 200 }}
        /> : null
      }
      { isAdmin ? <Divider /> : null }
      <MenuItem
        onClick={onLogoutUser}
        primaryText={<FormattedMessage {...messages.logOut} />}
        rightIcon={<LogoutIcon />}
        style={{ minWidth: 200 }}
      />
    </IconMenu>);
  }

  renderMessagesIcon() {
    const messages = 10;
    const messagesIconButton = (<IconButton
      iconStyle={{ color: 'white' }}
    >
        <MessagesIcon />
    </IconButton>);
    return messages > 0 ? <Badge
      badgeContent={messages}
      badgeStyle={{ top: 0, right: -12, background: 'rgba(255, 255, 255, 0.2)' }}
      primary
      style={{ padding: 0 }}
    >
      {messagesIconButton}
    </Badge> : messagesIconButton;
  }

  renderNotificationsIcon() {
    const notifications = 10;
    const notificationsIcon = (<IconButton
      iconStyle={{ color: 'white' }}
    >
        <NotificationsIcon />
    </IconButton>);
    return notifications > 0 ? <Badge
      badgeContent={notifications}
      badgeStyle={{ top: 0, right: -12, background: 'rgba(255, 255, 255, 0.2)' }}
      primary
      style={{ padding: 0 }}
    >
      {notificationsIcon}
    </Badge> : notificationsIcon;
  }

  render() {
    return (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <div style={{ marginRight: 10 }}>
          {this.renderNotificationsIcon()}
        </div>
        <div style={{ marginRight: 10 }}>
          {this.renderMessagesIcon()}
        </div>
        {this.renderIconMenu()}
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

ServiceProviderAppMenu.propTypes = {
  isAdmin: PropTypes.boolean,
  onLogoutUser: PropTypes.func,
};

ServiceProviderAppMenu.defaultProps = {
  onLogoutUser: () => true,
};

export default ServiceProviderAppMenu;
