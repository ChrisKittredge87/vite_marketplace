/**
*
* EventList
*
*/

import React, { PropTypes } from 'react';
import { fromJS } from 'immutable';
import { AutoSizer, List } from 'react-virtualized';
import styled from 'styled-components';
import _ from 'lodash';

import EventCard from 'components/EventCard';
import { RespDiv } from 'styled/common';
import { LG, XL } from 'globalConstants';

export const Container = styled.div`
  width: 100%;
  height: calc(100vh - 4px);
  overflow: auto;
`;

class EventList extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.numberOfCols = this.numberOfCols.bind(this);
    this.rowCalc = this.rowCalc.bind(this);
    this.renderRow = this.renderRow.bind(this);
  }

  numberOfCols() {
    switch (true) {
      case window.innerWidth < LG:
        return 1;
      case window.innerWidth < XL:
        return 2;
      default:
        return 3;
    }
  }

  rowCalc(row, max) {
    const cols = this.numberOfCols();
    const baseCols = _.range(cols);
    const colsByRow = _.map(baseCols, (col) => (cols * row) + col);
    return _.filter(colsByRow, (idx) => idx < max);
  }

  renderRow({ index, style }) {
    const { events } = this.props;
    return (
      <div
        key={`sp_landing_page_event_${index}`}
        style={style}
      >
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          {_.map(this.rowCalc(index, events.size), (idx) =>
            (<RespDiv cols={this.numberOfCols()} key={`sp_landing_page_event_${idx}`}>
              <EventCard event={fromJS({})} />
            </RespDiv>))
          }
        </div>
      </div>);
  }

  render() {
    const { events } = this.props;
    return (
      <Container>
        <AutoSizer>
          {({ height, width }) => (
            <List
              height={height}
              rowCount={_.ceil(events.size / this.numberOfCols())}
              rowHeight={206}
              rowRenderer={this.renderRow}
              width={width}
            />)}
        </AutoSizer>
      </Container>
    );
  }
}

EventList.defaultProps = {
  events: fromJS([]),
};

EventList.propTypes = {
  events: PropTypes.object,
};

export default EventList;
