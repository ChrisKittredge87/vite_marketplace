/*
 * RangeDropdown Messages
 *
 * This contains all the text for the RangeDropdown component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  range: {
    id: 'range',
    defaultMessage: 'Range',
  },
});
