/**
*
* RangeDropdown
*
*/

import React, { PropTypes } from 'react';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import { FormattedMessage } from 'react-intl';
import _ from 'lodash';

import messages from './messages';

class RangeDropdown extends React.Component { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { hintText, onChange, ranges, style, value } = this.props;
    return (
      <div style={_.assign({}, { display: 'flex' }, style)}>
        <SelectField
          hintText={hintText}
          onChange={onChange}
          style={{ minWidth: 250 }}
          value={value}
        >
          {_.map(ranges, ({ value: rangeValue, label, primaryText }, idx) =>
            <MenuItem
              disabled={false}
              key={`time_range_dropdown_${label}_${idx}`}
              label={label}
              primaryText={primaryText}
              style={{ minWidth: 255 }}
              value={rangeValue}
            />
          )}
        </SelectField>
      </div>
    );
  }
}

RangeDropdown.defaultProps = {
  hintText: <FormattedMessage {...messages.range} />,
  onChange: () => false,
  ranges: [],
  style: {},
  value: null,
};

RangeDropdown.propTypes = {
  hintText: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  onChange: PropTypes.func,
  ranges: PropTypes.array,
  style: PropTypes.object,
  value: PropTypes.object,
};

export default RangeDropdown;
