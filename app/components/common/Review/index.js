/**
*
* Review
*
*/

import React, { PropTypes } from 'react';
import Highlighter from 'react-highlight-words';
import Rater from 'react-rater';
import { fromJS } from 'immutable';

import {
  DATE,
  RATING,
  TEXT,
} from 'entities/schemas/serviceProviders/reviews/constants';
import { FIRST_NAME, LAST_NAME } from 'entities/schemas/users/constants';

import { FormattedDate, FormattedMessage } from 'react-intl';
import messages from './messages';
import {
  TruncateWrapper,
  Truncate,
} from './styled';

class Review extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
    };

    this.toggleExpanded = this.toggleExpanded.bind(this);
  }

  toggleExpanded(event) {
    event.preventDefault();

    this.setState({
      expanded: !this.state.expanded,
    });
  }

  render() {
    const { expanded } = this.state;
    const { highlightWords, review, user } = this.props;
    return (
      <div>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          <h3>
            <FormattedMessage
              {...messages.reviewHeader}
              values={{
                date: <FormattedDate value={review.get(DATE)} />,
                userFullName: `${user.get(FIRST_NAME)} ${user.get(LAST_NAME)}`,
              }}
            />
          </h3>
          <Rater total={5} rating={review.get(RATING)} interactive={false} />
        </div>
        <TruncateWrapper>
          <Truncate expanded={expanded}>
            <Highlighter
              searchWords={highlightWords}
              textToHighlight={review.get(TEXT)}
            />
          </Truncate>
        </TruncateWrapper>
        <div>
          <a
            href="#"
            onClick={this.toggleExpanded}
            style={{ color: 'blue', cursor: 'pointer', textDecoration: 'underline' }}
          >
            Show {expanded ? 'less' : 'more'}...
          </a>
        </div>
      </div>
    );
  }
}

Review.propTypes = {
  highlightWords: PropTypes.array,
  review: PropTypes.object,
  user: PropTypes.object,
};

Review.defaultProps = {
  highlightWords: [],
  review: fromJS({}),
  user: fromJS({}),
};

export default Review;
