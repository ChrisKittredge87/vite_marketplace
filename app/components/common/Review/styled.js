import styled from 'styled-components';

export const TruncateWrapper = styled.div`
  width: 100%;
  overflow: hidden;
`;

export const Truncate = styled.div`
  display: -webkit-box;
  -webkit-line-clamp: ${({ expanded }) => expanded ? false : 3};
  -webkit-box-orient: vertical;
  transition: -webkit-line-clamp 0.5s;
`;
