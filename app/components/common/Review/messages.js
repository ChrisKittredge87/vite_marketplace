/*
 * Review Messages
 *
 * This contains all the text for the Review component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  reviewHeader: {
    id: 'app.components.serviceProviders.Review.header',
    defaultMessage: 'By {userFullName} on {date}',
  },
});
