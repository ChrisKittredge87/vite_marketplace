/*
 * GuestsRangeDropdown Messages
 *
 * This contains all the text for the GuestsRangeDropdown component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  '10to100Range': {
    id: 'app.components.common.GuestsRangeDropdown.10to100Range',
    defaultMessage: '10 - 100 Guests',
  },
  '100to250Range': {
    id: 'app.components.common.GuestsRangeDropdown.100to250Range',
    defaultMessage: '100 - 250 Guests',
  },
  '250to500Range': {
    id: 'app.components.common.GuestsRangeDropdown.250to500Range',
    defaultMessage: '250 - 500 Guests',
  },
  '500to1000Range': {
    id: 'app.components.common.GuestsRangeDropdown.500to1000Range',
    defaultMessage: '500 - 1000 Guests',
  },
  '1000PlusRange': {
    id: 'app.components.common.GuestsRangeDropdown.1000PlusRange',
    defaultMessage: '1000+ Guests',
  },
  guests: {
    id: 'guests',
    defaultMessage: 'Guests',
  },
});
