/**
*
* GuestsRangeDropdown
*
*/

import React, { PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';

import RangeDropdown from 'components/common/RangeDropdown';

import messages from './messages';

class GuestsRangeDropdown extends React.Component { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { hintText, onChange, style, timeRanges, value } = this.props;
    return (
      <RangeDropdown
        hintText={hintText}
        onChange={onChange}
        style={style}
        ranges={timeRanges}
        value={value}
      />
    );
  }
}

GuestsRangeDropdown.defaultProps = {
  hintText: <FormattedMessage {...messages.guests} />,
  onChange: () => false,
  timeRanges: [
    {
      value: { start: 10, end: 100 },
      label: <FormattedMessage {...messages['10to100Range']} />,
      primaryText: <FormattedMessage {...messages['10to100Range']} />,
    },
    {
      value: { start: 100, end: 250 },
      label: <FormattedMessage {...messages['100to250Range']} />,
      primaryText: <FormattedMessage {...messages['100to250Range']} />,
    },
    {
      value: { start: 250, end: 500 },
      label: <FormattedMessage {...messages['250to500Range']} />,
      primaryText: <FormattedMessage {...messages['250to500Range']} />,
    },
    {
      value: { start: 500, end: 1000 },
      label: <FormattedMessage {...messages['500to1000Range']} />,
      primaryText: <FormattedMessage {...messages['500to1000Range']} />,
    },
    {
      value: { start: 1000 },
      label: <FormattedMessage {...messages['1000PlusRange']} />,
      primaryText: <FormattedMessage {...messages['1000PlusRange']} />,
    },
  ],
  value: null,
};

GuestsRangeDropdown.propTypes = {
  hintText: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  onChange: PropTypes.func,
  style: PropTypes.object,
  timeRanges: PropTypes.array,
  value: PropTypes.object,
};

export default GuestsRangeDropdown;
