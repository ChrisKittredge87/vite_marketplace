/**
*
* CenteredLoadingSpinner
*
*/

import React, { PropTypes } from 'react';
import styled from 'styled-components';
import CircularProgress from 'material-ui/CircularProgress';

const SpinnerContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: ${({ height }) => height};
  width: ${({ width }) => width};
`;

function CenteredLoadingSpinner({ containerHeight = '100%', containerWidth = '100%', size = 150, thickness = 8 }) {
  return (<SpinnerContainer height={containerHeight} width={containerWidth}>
    <div style={{ width: '100%' }}>
      <div style={{ textAlign: 'center' }}>
        <CircularProgress size={size} thickness={thickness} />
      </div>
    </div>
  </SpinnerContainer>);
}

CenteredLoadingSpinner.propTypes = {
  containerHeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  containerWidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  size: PropTypes.number,
  thickness: PropTypes.number,
};

export default CenteredLoadingSpinner;
