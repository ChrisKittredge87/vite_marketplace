/**
*
* FlexContainer
*
*/

import React, { PropTypes } from 'react';
import styled from 'styled-components';

const FlexDiv = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

function FlexContainer({ children }) {
  return (
    <FlexDiv>
      {children}
    </FlexDiv>
  );
}

const { arrayOf, node, oneOfType } = PropTypes;

FlexContainer.propTypes = {
  children: oneOfType([node, arrayOf(node)]),
};

export default FlexContainer;
