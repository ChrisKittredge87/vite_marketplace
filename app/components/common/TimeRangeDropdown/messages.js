/*
 * TimeDropdown Messages
 *
 * This contains all the text for the TimeDropdown component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  afternoon: {
    id: 'afternoon',
    defaultMessage: 'Afternoon',
  },
  afternoonRange: {
    id: 'app.components.common.TimeDropDown.afternoonRange',
    defaultMessage: '12pm - 6pm',
  },
  evening: {
    id: 'evening',
    defaultMessage: 'Evening',
  },
  eveningRange: {
    id: 'app.components.common.TimeDropDown.eveningRange',
    defaultMessage: '6pm - 12am',
  },
  morning: {
    id: 'morning',
    defaultMessage: 'Morning',
  },
  morningRange: {
    id: 'app.components.common.TimeDropDown.morningRange',
    defaultMessage: '6am - 12pm',
  },
  night: {
    id: 'night',
    defaultMessage: 'Night',
  },
  nightRange: {
    id: 'app.components.common.TimeDropDown.nightRange',
    defaultMessage: '12am - 6am',
  },
  time: {
    id: 'time',
    defaultMessage: 'Time',
  },
});
