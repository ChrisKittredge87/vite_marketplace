/**
*
* TimeRangeDropdown
*
*/

import React, { PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';

import RangeDropdown from 'components/common/RangeDropdown';

import messages from './messages';

class TimeRangeDropdown extends React.Component { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { hintText, onChange, style, timeRanges, value } = this.props;
    return (
      <RangeDropdown
        hintText={hintText}
        onChange={onChange}
        style={style}
        ranges={timeRanges}
        value={value}
      />
    );
  }
}

TimeRangeDropdown.defaultProps = {
  hintText: <FormattedMessage {...messages.time} />,
  onChange: () => false,
  style: {},
  timeRanges: [
    {
      value: { start: 5, end: 12 },
      label: <FormattedMessage {...messages.morningRange} />,
      primaryText: <FormattedMessage {...messages.morning} />,
    },
    {
      value: { start: 12, end: 17 },
      label: <FormattedMessage {...messages.afternoonRange} />,
      primaryText: <FormattedMessage {...messages.afternoon} />,
    },
    {
      value: { start: 17, end: 21 },
      label: <FormattedMessage {...messages.eveningRange} />,
      primaryText: <FormattedMessage {...messages.evening} />,
    },
    {
      value: { start: 21, end: 5 },
      label: <FormattedMessage {...messages.nightRange} />,
      primaryText: <FormattedMessage {...messages.night} />,
    },
  ],
  value: null,
};

TimeRangeDropdown.propTypes = {
  hintText: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  onChange: PropTypes.func,
  style: PropTypes.object,
  timeRanges: PropTypes.array,
  value: PropTypes.object,
};

export default TimeRangeDropdown;
