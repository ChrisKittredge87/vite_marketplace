/**
*
* MaskedTextField
*
*/

import React, { PropTypes } from 'react';
import VMasker from 'vanilla-masker';
import { TextField } from 'material-ui';

function applyMask(mask, value) {
  if (!value) {
    return { masked: '', unmasked: '' };
  }
  const _mask = (mask || '').trim().toLowerCase();
  let masked = value;
  let unmasked = masked;
  if (_mask === 'currency') {
    masked = VMasker.toMoney(masked, { unit: '$', delimiter: ',', separator: '.', precision: 0 })
    unmasked = VMasker.toNumber(masked);
  } else if (_mask === 'number') {
    masked = VMasker.toMoney(masked, { suffixUnit: null, delimiter: ',', separator: '.', precision: 0 })
    unmasked = VMasker.toNumber(masked);
  } else {
    masked = VMasker.toPattern(masked, _mask);
  }
  return { masked, unmasked };
}

class MaskedTextField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    const { mask, defaultValue, value } = props;
    const { masked } = applyMask(mask, defaultValue || value);
    this.state = { value: masked }; // set initial value from default value in props
  }

  onChange(mask, e) {
    const { onChange } = this.props;
    const { masked, unmasked } = applyMask(mask, e.target.value);
    this.setState({ value: masked });
    if (onChange) {
      onChange(unmasked);
    }
  }

  render() {
    const { mask, ...other } = this.props;
    delete other.defaultValue; // remove default value from TextField input (see link below)
    other.onChange = this.onChange.bind(this, mask);
    other.value = this.state.value;
    return (<TextField {...other} />);
  }
}

MaskedTextField.propTypes = {
  mask: PropTypes.string,
  precision: PropTypes.number,
};

MaskedTextField.defaultProps = {
  precision: 2,
}

export default MaskedTextField;
