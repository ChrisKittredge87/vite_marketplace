/**
*
* CurrencyField
*
*/

import React, { PropTypes } from 'react';
import MaskedTextField from 'components/common/MaskedTextField';

class CurrencyField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (<MaskedTextField
      {...this.props}
      mask={'currency'}
    />);
  }
}

CurrencyField.propTypes = {

};

export default CurrencyField;
