/**
*
* NumberField
*
*/

import React, { PropTypes } from 'react';
import MaskedTextField from 'components/common/MaskedTextField';

class NumberField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (<MaskedTextField
      {...this.props}
      mask={'number'}
    />);
  }
}

NumberField.propTypes = {

};

export default NumberField;
