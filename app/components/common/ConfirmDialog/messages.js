/*
 * ConfirmationDialog Messages
 *
 * This contains all the text for the ConfirmationDialog component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  cancel: {
    id: 'cancel',
    defaultMessage: 'Cancel',
  },
  confirm: {
    id: 'confirm',
    defaultMessage: 'Confirm',
  },
  message: {
    id: 'app.components.common.ConfirmDialog.message',
    defaultMessage: 'Are you sure?',
  },
  submit: {
    id: 'submit',
    defaultMessage: 'Submit',
  },
});
