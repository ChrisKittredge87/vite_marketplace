/**
*
* ConfirmDialog
*
*/

import React, { PropTypes } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class ConfirmDialog extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { confirmBtnText, message, modal, onCancel, onConfirm, open, title } = this.props;
    const actions = [
      <FlatButton
        key="confirm_modal_cancel_btn"
        label={<FormattedMessage {...messages.cancel} />}
        onTouchTap={onCancel}
        secondary
      />,
      <FlatButton
        key="confirm_modal_confirm_btn"
        label={confirmBtnText}
        onTouchTap={onConfirm}
        primary
      />,
    ];

    return (<Dialog
      title={<h3>{title}</h3>}
      actions={actions}
      modal={modal}
      open={!!open}
    >
      <div>{message}</div>
    </Dialog>);
  }
}

ConfirmDialog.defaultProps = {
  confirmBtnText: <FormattedMessage {...messages.submit} />,
  message: <FormattedMessage {...messages.message} />,
  modal: true,
  open: false,
  title: <FormattedMessage {...messages.confirm} />,
};

ConfirmDialog.propTypes = {
  confirmBtnText: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  modal: PropTypes.bool,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  open: PropTypes.bool,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
};

export default ConfirmDialog;
