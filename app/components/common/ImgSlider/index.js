/**
*
* ImgSlider
*
*/

import React, { PropTypes } from 'react';
import { fromJS } from 'immutable';
import Slider from 'react-slick';

import { Crop, DynamicImg } from 'styled/common';

class ImgSlider extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    imgSources: PropTypes.object,
  };

  static defaultProps = {
    imgSources: fromJS([]),
  };

  /* ----- Rendering ----- */

  render() {
    const { height, imgSources } = this.props;
    const settings = {
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: false,
      lazyLoad: true,
      dots: true,
    };
    return imgSources.size > 0 ? (<Slider {...settings}>
      {imgSources.map((imgSrc, idx) =>
        (<div key={`cropped_search_rslt_img_${idx}`}>
          <Crop height={height}>
            <DynamicImg src={imgSrc} />
          </Crop>
        </div>))}
    </Slider>) : null;
  }

  /* ----- END Rendering ----- */
}

export default ImgSlider;
