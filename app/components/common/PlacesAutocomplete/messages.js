/*
 * PlacesAutocomplete Messages
 *
 * This contains all the text for the PlacesAutocomplete component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  enterALocation: {
    id: 'enterALocation',
    defaultMessage: 'Enter a Location',
  },
});
