/**
*
* PlacesAutocomplete
*
*/

import React, { PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';
import TextField from 'material-ui/TextField';

import messages from './messages';


class PlacesAutocomplete extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.bindAutocomplete = this.bindAutocomplete.bind(this);
  }

  /* ----- Event Binding ----- */

  handlePlaceChange() {

  }

  bindAutocomplete(props) {
    const { id, map, onLocationChange } = props;
    const defaultBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(-33.8902, 151.1759),
      new google.maps.LatLng(-33.8474, 151.2631)
    );

    const options = { types: [], componentRestrictions: { country: 'us' } };

    const input = document.getElementById(id);
    input.placeholder = '';
    const autocomplete = new google.maps.places.Autocomplete(input, options);

    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    var marker = new google.maps.Marker({
      setMap: map,
      anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();
      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      onLocationChange(place);
      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
        // map.setZoom(8);
      } else {
        map.setCenter(place.geometry.location);
        // map.setZoom(8);
      }
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      // var address = '';
      // if (place.address_components) {
      //   address = [
      //     (place.address_components[0] && place.address_components[0].short_name || ''),
      //     (place.address_components[1] && place.address_components[1].short_name || ''),
      //     (place.address_components[2] && place.address_components[2].short_name || '')
      //   ].join(' ');
      // }

      // infowindowContent.children['place-icon'].src = place.icon;
      // infowindowContent.children['place-name'].textContent = place.name;
      // infowindowContent.children['place-address'].textContent = address;
      infowindow.open(map, marker);
    });
  }

  /* ----- END Event Binding ----- */

  /* ----- Lifecycle ----- */

  componentDidMount() {
    if (!!this.props.map) {
      this.bindAutocomplete(this.props);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.map && !!nextProps.map) {
      this.bindAutocomplete(nextProps);
    }
  }

  /* ----- END Lifecycle ----- */

  /* ----- Rendering ----- */

  render() {
    const _props = _.pickBy(this.props, (value, key) => ['map'].indexOf(key) === -1);
    return (<TextField {..._props} />);
  }

  /* ----- END Rendering ----- */
}

PlacesAutocomplete.propTypes = {
  id: PropTypes.string,
  fullWidth: PropTypes.bool,
  map: PropTypes.object,
  // callbacks
  onLocationChange: PropTypes.func,
};

PlacesAutocomplete.defaultProps = {
  id: 'places_autocomplete',
  fullWidth: false,
};

export default PlacesAutocomplete;
