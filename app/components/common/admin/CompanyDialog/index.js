/**
*
* CompanyDialog
*
*/

import React, { PropTypes } from 'react';
import CircularProgress from 'material-ui/CircularProgress';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import { fromJS } from 'immutable';
import { FormattedMessage } from 'react-intl';
import _ from 'lodash';

import { ID, COMPANY_TYPE_ID, NAME, PARENT_ID } from 'entities/schemas/companies/constants';
import { ID as ID_COMPANY_TYPE, DISPLAY_NAME as COMPANY_TYPE_DISPLAY_NAME } from 'entities/schemas/companies/types/constants';

import messages from './messages';

class CompanyDialog extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = { formCompanyName: '', formCompanyTypeId: null, formParentCompanyId: null };

    this.handleChangeFormData = this.handleChangeFormData.bind(this);
    this.handleSaveCompany = this.handleSaveCompany.bind(this);
  }

  /* ----- Lifecycle ----- */

  componentWillReceiveProps(nextProps) {
    const { open, selectedCompany } = this.props;
    const { open: nextOpen, selectedCompany: nextSelectedCompany } = nextProps;
    if (nextSelectedCompany && !nextSelectedCompany.equals(selectedCompany)) {
      this.setState({
        formCompanyName: nextSelectedCompany.get(NAME),
        formCompanyTypeId: nextSelectedCompany.get(COMPANY_TYPE_ID),
        formParentCompanyId: nextSelectedCompany.get(PARENT_ID),
      });
    }
    if (open && !nextOpen) {
      this.setState({ formCompanyName: '', formCompanyTypeId: null, formParentCompanyId: null });
    }
  }

  /* ----- END Lifecycle ----- */

  /* ----- Event Binding ----- */

  handleChangeFormData(key) {
    return ({ target: { value } }, isToggled, newVal) => {
      let val = value;
      if (_.isBoolean(isToggled)) {
        val = isToggled;
      } else if (_.isNumber(isToggled)) {
        val = newVal;
      }
      this.setState({ [key]: val });
    };
  }

  handleSaveCompany() {
    const { onSaveCompany, selectedCompany } = this.props;
    const { formCompanyName, formCompanyTypeId, formParentCompanyId } = this.state;
    onSaveCompany(
      selectedCompany
        .set(NAME, formCompanyName)
        .set(COMPANY_TYPE_ID, formCompanyTypeId)
        .set(PARENT_ID, formParentCompanyId)
    );
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  render() {
    const { companies, companyTypes, loading, onCloseDialog, open } = this.props;
    const { formCompanyName, formCompanyTypeId, formParentCompanyId } = this.state;
    const selectedCompany = this.props.selectedCompany || fromJS({});
    const actions = [
      <FlatButton
        disabled={loading}
        key="company_dialog_cancel_btn"
        label={<FormattedMessage {...messages.cancel} />}
        onTouchTap={onCloseDialog}
        secondary
      />,
      <FlatButton
        disabled={loading}
        key="company_dialog_save_btn"
        label={<FormattedMessage {...messages.save} />}
        onTouchTap={this.handleSaveCompany}
        primary
      />,
    ];

    const companyTypesMenuItems = companyTypes.map((companyType) =>
      (<MenuItem
        key={companyType.get(ID_COMPANY_TYPE)}
        value={companyType.get(ID_COMPANY_TYPE)}
        primaryText={companyType.get(COMPANY_TYPE_DISPLAY_NAME)}
      />));

    const parentCompanyMenuItems = companies.reduce((memo, company) =>
      company.get(ID) !== selectedCompany.get(ID) && company.get(PARENT_ID) !== selectedCompany.get(ID) ?
        memo.concat(<MenuItem key={company.get(ID)} value={company.get(ID)} primaryText={company.get(NAME)} />) :
        memo
    , [<MenuItem key={'no_parent_company'} value={null} secondaryText={'- - -'} />]);
    return (<Dialog
      actions={actions}
      title={<h3><FormattedMessage {...messages.company} /></h3>}
      modal
      open={!!open}
    >
      {
        loading ?
          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <CircularProgress size={150} thickness={5} />
          </div> :
          <div>
            <div style={{ display: 'flex', alignItems: 'flex-start', marginBottom: 20 }}>
              <TextField
                floatingLabelText={<FormattedMessage {...messages.companyName} />}
                onChange={this.handleChangeFormData('formCompanyName')}
                style={{ marginRight: 20, width: '50%' }}
                value={formCompanyName}
              />
              <SelectField
                floatingLabelText={<FormattedMessage {...messages.companyType} />}
                onChange={this.handleChangeFormData('formCompanyTypeId')}
                style={{ width: '50%' }}
                value={formCompanyTypeId}
              >
                {companyTypesMenuItems}
              </SelectField>
            </div>
            <div style={{ display: 'flex', alignItems: 'flex-start', marginBottom: 20 }}>
              <SelectField
                disabled={parentCompanyMenuItems.length < 2}
                floatingLabelText={<FormattedMessage {...messages.parentCompany} />}
                onChange={this.handleChangeFormData('formParentCompanyId')}
                style={{ width: '100%' }}
                value={formParentCompanyId}
              >
                {parentCompanyMenuItems}
              </SelectField>
            </div>
          </div>
      }
    </Dialog>);
  }

  /* ----- END Rendering ----- */
}

CompanyDialog.defaultProps = {
  companies: fromJS([]),
  loading: false,
  open: false,
  selectedCompany: fromJS({}),
};

CompanyDialog.propTypes = {
  companies: PropTypes.object,
  companyTypes: PropTypes.object,
  loading: PropTypes.bool,
  onCloseDialog: PropTypes.func,
  onSaveCompany: PropTypes.func,
  open: PropTypes.bool,
  selectedCompany: PropTypes.object,
};

export default CompanyDialog;
