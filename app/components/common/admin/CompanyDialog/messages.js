/*
 * CompanyDialog Messages
 *
 * This contains all the text for the CompanyDialog component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  cancel: {
    id: 'cancel',
    defaultMessage: 'Cancel',
  },
  company: {
    id: 'company',
    defaultMessage: 'Company',
  },
  companyName: {
    id: 'companyName',
    defaultMessage: 'Company Name',
  },
  companyType: {
    id: 'companyType',
    defaultMessage: 'Company Type',
  },
  parentCompany: {
    id: 'parentCompany',
    defaultMessage: 'Parent Company',
  },
  save: {
    id: 'save',
    defaultMessage: 'Save',
  },
});
