/**
*
* CompanyList
*
*/

import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import Avatar from 'material-ui/Avatar';
import AddIcon from 'material-ui/svg-icons/content/add';
import CompanyIcon from 'material-ui/svg-icons/communication/business';
import EditIcon from 'material-ui/svg-icons/image/edit';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import IconButton from 'material-ui/IconButton';
import LinkIcon from 'material-ui/svg-icons/content/link';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import Store from 'material-ui/svg-icons/action/store';
import Subheader from 'material-ui/Subheader';
import ViewIcon from 'material-ui/svg-icons/action/visibility';
import { fromJS } from 'immutable';
import { FormattedMessage } from 'react-intl';
import { black, blue500 } from 'material-ui/styles/colors';

import { path as serviceProviderProfilePath } from 'containers/serviceProviders/Profile/routes';
import { ID, COMPANY_TYPE, NAME, PARENT_ID } from 'entities/schemas/companies/constants';
import { NAME as COMPANY_TYPE_NAME } from 'entities/schemas/companies/types/constants';
import {
  COMPANY_ID,
  ID as SERVICE_PROVIDER_ID,
  NAME as SERVICE_PROVIDER_NAME,
} from 'entities/schemas/serviceProviders/constants';

import { COMPANY, SERVICE_PROVIDER, SERVICE_PROVIDERS, SUBSIDIARIES } from './constants';
import messages from './messages';

class CompanyList extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.handleAddSubsidiaryIconClick = this.handleAddSubsidiaryIconClick.bind(this);
    this.handleCompanyMembershipIconClick = this.handleCompanyMembershipIconClick.bind(this);
    this.handleDeleteIconClick = this.handleDeleteIconClick.bind(this);
    this.handleEditIconClick = this.handleEditIconClick.bind(this);
    this.handleServiceProviderIconClick = this.handleServiceProviderIconClick.bind(this);
    this.renderCompanyListItems = this.renderCompanyListItems.bind(this);
    this.renderCompanyRightIconButtons = this.renderCompanyRightIconButtons.bind(this);
  }

  /* ----- Event Binding ----- */

  handleAddSubsidiaryIconClick(company) {
    const { onAddSubsidiaryIconClick } = this.props;
    return () => onAddSubsidiaryIconClick(company);
  }

  handleAddServiceProviderIconClick(company) {
    const { onAddServiceProviderIconClick } = this.props;
    return () => onAddServiceProviderIconClick(company);
  }

  handleCompanyMembershipIconClick(company) {
    const { onCompanyMembershipIconClick } = this.props;
    return () => onCompanyMembershipIconClick(company);
  }

  handleDeleteIconClick(entity, type) {
    const { onDeleteIconClick } = this.props;
    return () => onDeleteIconClick(entity, type);
  }

  handleEditIconClick(type, entity) {
    const { onEditIconClick } = this.props;
    return () => onEditIconClick(type, entity);
  }

  handleServiceProviderIconClick(company) {
    const { onServiceProviderIconClick } = this.props;
    return () => onServiceProviderIconClick(company);
  }

  /* ----- END Event Binding ----- */

  /* ----- Helpers ----- */

  isAgency(company) {
    return (company.getIn([COMPANY_TYPE, COMPANY_TYPE_NAME]) || '').toLowerCase().indexOf('agency') > -1;
  }

  /* ----- END Helpers ----- */

  /* ----- Rendering ----- */

  renderCompanyListItems(companies) {
    const subsidiaryCompanyItems = (c, nestLevel = 0) => {
      const subsidiaryNestLevel = nestLevel + 1;
      const subsidiaries = c.get(SUBSIDIARIES, fromJS([]));
      return subsidiaries.size ?
        [<Subheader
          key={`company_${c.get(ID)}_subsidiaries_subheader`}
          style={{ marginLeft: 20 * subsidiaryNestLevel }}
        >
          <FormattedMessage {...messages.subsidiaries} />
        </Subheader>]
        .concat(subsidiaries.map((subsidiary) =>
          (<ListItem
            initiallyOpen
            key={subsidiary.get(ID)}
            leftAvatar={<Avatar size={40} color={black} icon={<CompanyIcon />} />}
            nestedItems={
              this.isAgency(subsidiary) ?
                subsidiaryCompanyItems(subsidiary, subsidiaryNestLevel) :
                serviceProviderItems(subsidiary, subsidiaryNestLevel)
            }
            primaryText={subsidiary.get(NAME)}
            primaryTogglesNestedList
            rightIconButton={this.renderCompanyRightIconButtons(subsidiary)}
          />)
        ).toJS()) : [];
    };
    const serviceProviderItems = (c, nestLevel = 0) => {
      const serviceProviderNestLevel = nestLevel + 1;
      const serviceProviders = c.get(SERVICE_PROVIDERS, fromJS([]));
      return serviceProviders.size ?
        [<Subheader
          key={`company_${c.get(ID)}_service_providers_subheader`}
          style={{ marginLeft: 20 * serviceProviderNestLevel }}
        >
          <FormattedMessage {...messages.serviceProviders} />
        </Subheader>]
        .concat(serviceProviders.map((sp) =>
          (<ListItem
            initiallyOpen
            key={sp.get(SERVICE_PROVIDER_ID)}
            leftAvatar={<Avatar size={40} color={blue500} icon={<Store />} />}
            primaryText={sp.get(SERVICE_PROVIDER_NAME)}
            primaryTogglesNestedList
            rightIconButton={this.renderServiceProviderRightIconButtons(sp)}
          />)
        ).toJS()) : [];
    };
    return companies.map((company) => (<ListItem
      initiallyOpen
      key={`company_list_item_${company.get(ID)}`}
      leftAvatar={<Avatar size={40} color={black} icon={<CompanyIcon />} />}
      nestedItems={this.isAgency(company) ? subsidiaryCompanyItems(company) : serviceProviderItems(company)}
      primaryText={company.get(NAME)}
      primaryTogglesNestedList
      rightIconButton={this.renderCompanyRightIconButtons(company)}
    />));
  }

  renderCompanyRightIconButtons(company) {
    const addSubsidiaryButton = (c) =>
      (<IconButton
        onClick={this.handleAddSubsidiaryIconClick(c)}
        tooltip={<FormattedMessage {...messages.addSubsidiary} />}
        tooltipPosition="top-left"
      >
        <AddIcon />
      </IconButton>);
    const addServiceProviderButton = (c) =>
      (<IconButton
        onClick={this.handleAddServiceProviderIconClick(c)}
        tooltip={<FormattedMessage {...messages.addServiceProvider} />}
        tooltipPosition="top-left"
      >
        <AddIcon />
      </IconButton>);
    return (<div>
      <IconButton
        onClick={this.handleCompanyMembershipIconClick(company)}
        tooltip={<FormattedMessage {...messages.manageMemberships} />}
        tooltipPosition="top-left"
      >
        <LinkIcon />
      </IconButton>
      {this.isAgency(company) ? addSubsidiaryButton(company) : addServiceProviderButton(company)}
      <IconButton
        onClick={this.handleEditIconClick(COMPANY, company)}
        tooltip={<FormattedMessage {...messages.edit} />}
        tooltipPosition="top-left"
      >
        <EditIcon />
      </IconButton>
      <IconButton
        disabled={company.get(SUBSIDIARIES).size > 0 || company.get(SERVICE_PROVIDERS).size > 0}
        onClick={this.handleDeleteIconClick(company)}
        tooltip={<FormattedMessage {...messages.delete} />}
        tooltipPosition="top-left"
      >
        <DeleteIcon />
      </IconButton>
    </div>);
  }

  renderServiceProviderRightIconButtons(serviceProvider) {
    return (<div>
      <Link
        to={`/${serviceProviderProfilePath}/${serviceProvider.get(COMPANY_ID)}?serviceProviderId=${serviceProvider.get(SERVICE_PROVIDER_ID)}`}
      >
        <IconButton
          tooltip={<FormattedMessage {...messages.view} />}
          tooltipPosition="top-left"
        >
          <ViewIcon />
        </IconButton>
      </Link>
      <IconButton
        onClick={this.handleEditIconClick(SERVICE_PROVIDER, serviceProvider)}
        tooltip={<FormattedMessage {...messages.edit} />}
        tooltipPosition="top-left"
      >
        <EditIcon />
      </IconButton>
      <IconButton
        onClick={this.handleDeleteIconClick(serviceProvider)}
        tooltip={<FormattedMessage {...messages.delete} />}
        tooltipPosition="top-left"
      >
        <DeleteIcon />
      </IconButton>
    </div>);
  }

  render() {
    const { companies, serviceProviders } = this.props;
    function buildCompanyHierarchy(company) {
      return company.merge(fromJS({
        [SUBSIDIARIES]: companies.reduce((memo, c) =>
          c.get(PARENT_ID) === company.get(ID) ?
            memo.push(buildCompanyHierarchy(c)) : memo, fromJS([])),
        [SERVICE_PROVIDERS]: serviceProviders.reduce((memo, sp) =>
          sp.get(COMPANY_ID) === company.get(ID) ?
            memo.push(sp) : memo, fromJS([])),
      }));
    }
    const topLevelCompanies = companies.filter((c) => c.get(PARENT_ID) === null);
    const companiesHierarchy = topLevelCompanies.map(buildCompanyHierarchy);
    return (
      <div>
        <List>
          {this.renderCompanyListItems(companiesHierarchy)}
        </List>
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

CompanyList.defaultProps = {
  companies: fromJS({}),
  serviceProviders: fromJS({}),
  onAddServiceProviderIconClick: () => false,
  onAddSubsidiaryIconClick: () => false,
  onEditIconClick: () => false,
  onDeleteIconClick: () => false,
  onCompanyMembershipIconClick: () => false,
  onServiceProviderIconClick: () => false,
};

CompanyList.propTypes = {
  companies: PropTypes.object,
  serviceProviders: PropTypes.object,
  onAddServiceProviderIconClick: PropTypes.func,
  onAddSubsidiaryIconClick: PropTypes.func,
  onEditIconClick: PropTypes.func,
  onDeleteIconClick: PropTypes.func,
  onCompanyMembershipIconClick: PropTypes.func,
  onServiceProviderIconClick: PropTypes.func,
};

export default CompanyList;

export { COMPANY, SERVICE_PROVIDER };
