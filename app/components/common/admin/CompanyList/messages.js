/*
 * CompanyList Messages
 *
 * This contains all the text for the CompanyList component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  addServiceProvider: {
    id: 'addServiceProvider',
    defaultMessage: 'Add Service Provider',
  },
  addSubsidiary: {
    id: 'addSubsidiary',
    defaultMessage: 'Add Subsidiary',
  },
  delete: {
    id: 'delete',
    defaultMessage: 'Delete',
  },
  edit: {
    id: 'edit',
    defaultMessage: 'Edit',
  },
  manageMemberships: {
    id: 'manageMemberships',
    defaultMessage: 'Manage Memberships',
  },
  manageServiceProviders: {
    id: 'manageServiceProviders',
    defaultMessage: 'Manage Service Providers',
  },
  serviceProviders: {
    id: 'serviceProviders',
    defaultMessage: 'Service Providers',
  },
  subsidiaries: {
    id: 'subsidiaries',
    defaultMessage: 'Subsidiaries',
  },
  view: {
    id: 'view',
    defaultMessage: 'View',
  },
});
