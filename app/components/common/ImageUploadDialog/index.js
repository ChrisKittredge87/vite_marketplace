/**
*
* ImageUploadDialog
*
*/

import React, { PropTypes } from 'react';
import AddIcon from 'material-ui/svg-icons/content/add';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import _ from 'lodash';

import { FormattedMessage } from 'react-intl';
import IconButton from 'material-ui/IconButton';
import { Crop, DynamicImg, SpaceBetweenFlex } from 'styled/common';

import messages from './messages';

class ImageUploadDialog extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      photosToUpload: [],
    };

    this.handleFileSelected = this.handleFileSelected.bind(this);
    this.handleUploadPhotos = this.handleUploadPhotos.bind(this);
    this.toggleDialogOpen = this.toggleDialogOpen.bind(this);
    this.renderDialog = this.renderDialog.bind(this);
  }

  /* ------ Lifecycle ------ */

  componentWillReceiveProps(nextProps) {
    if (!nextProps.photosUploading && this.props.photosUploading !== nextProps.photosUploading) {
      this.setState({ open: false });
    }
  }

  /* ------ END Lifecycle ------ */

  /* ------ Event Binding ------ */

  handleFileSelected(e) {
    e.preventDefault();

    const fileList = e.target.files;
    _.each(fileList, (file) => {
      const reader = new FileReader();
      reader.onloadend = () => {
        this.setState({
          photosToUpload: [{ src: reader.result, file }].concat(this.state.photosToUpload),
        });
      };
      reader.readAsDataURL(file);
    });
  }

  handleUploadPhotos() {
    this.props.onUploadPhotos(this.state.photosToUpload);
  }

  toggleDialogOpen() {
    this.setState({ open: !this.state.open, photosToUpload: [] });
  }

  /* ------ END Event Binding ------ */

  /* ------ Rendering ------ */

  renderDialog() {
    const { open } = this.state;
    const dialogHeader = (
      <SpaceBetweenFlex style={{ padding: '5px 20px' }}>
        <h3 style={{ margin: 0 }}><FormattedMessage {...messages.uploadPhotos} /></h3>
        <IconButton onClick={this.toggleDialogOpen}><CloseIcon /></IconButton>
      </SpaceBetweenFlex>
    );
    const actions = [
      <FlatButton
        label={<FormattedMessage {...messages.cancel} />}
        onClick={this.toggleDialogOpen}
      />,
      <FlatButton
        disabled={!this.state.photosToUpload.length}
        primary
        label={<FormattedMessage {...messages.upload} />}
        onClick={this.handleUploadPhotos}
      />,
    ];
    return (<Dialog
      actions={actions}
      modal={false}
      onRequestClose={this.toggleDialogOpen}
      open={!!open}
      title={dialogHeader}
    >
      <div style={{ overflowX: 'auto' }}>
        <div style={{ display: 'inline-flex', justifyContent: 'space-between', paddingTop: 20 }}>
          {_.map(this.state.photosToUpload, (img, idx) =>
            (<div style={{ marginRight: 10 }} key={`cropped_upload_img_${idx}`}>
              <Crop height={183} style={{ width: 183 }}>
                <DynamicImg src={img.src} />
              </Crop>
            </div>))}
          <form>
            <div style={{ marginRight: 10 }}>
              <label htmlFor="add_photo">
                <div style={{ padding: 50, border: '2px solid darkgray', cursor: 'pointer' }}>
                  <input
                    accept=".jpg,.png"
                    id="add_photo"
                    type="file"
                    style={{ display: 'none' }}
                    onChange={this.handleFileSelected}
                    name="photos"
                    multiple
                  />
                  <AddIcon style={{ height: 80, width: 80 }} hoverColor={'lightgray'} />
                </div>
              </label>
            </div>
          </form>
        </div>
      </div>
    </Dialog>);
  }

  render() {
    return (
      <div>
        <RaisedButton
          label={<FormattedMessage {...messages.uploadPhotos} />}
          onClick={this.toggleDialogOpen}
          style={this.props.buttonStyle}
        />
        {this.renderDialog()}
      </div>
    );
  }

  /* ------ END Rendering ------ */
}

ImageUploadDialog.propTypes = {
  buttonStyle: PropTypes.object,
  onUploadPhotos: PropTypes.func,
  photosUploading: PropTypes.bool,
};

export default ImageUploadDialog;
