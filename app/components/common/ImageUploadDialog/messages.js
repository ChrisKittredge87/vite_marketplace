/*
 * ImageUploadDialog Messages
 *
 * This contains all the text for the ImageUploadDialog component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  addPhoto: {
    id: 'addPhoto',
    defaultMessage: 'Add Photo',
  },
  cancel: {
    id: 'cancel',
    defaultMessage: 'Cancel',
  },
  upload: {
    id: 'upload',
    defaultMessage: 'Upload',
  },
  uploadPhotos: {
    id: 'uploadPhotos',
    defaultMessage: 'Upload Photos',
  },
});
