/**
*
* DateTimePicker
*
*/

import React, { PropTypes } from 'react';

import CalendarIcon from 'material-ui/svg-icons/action/event';
import ClockIcon from 'material-ui/svg-icons/device/access-time';
import DatePickerDialog from 'material-ui/DatePicker/DatePickerDialog';
import TimePickerDialog from 'material-ui/TimePicker/TimePickerDialog';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class DateTimePicker extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <CalendarIcon
          onClick={this.handleOpenStartDateDialog}
          style={{ position: 'absolute', right: 26, top: 10, height: 30, cursor: 'pointer', zIndex: 3 }}
        />
        <ClockIcon
          onClick={this.handleOpenStartTimeDialog}
          style={{ position: 'absolute', right: 0, top: 10, height: 30, cursor: 'pointer', zIndex: 3 }}
        />
        <TextField
          style={{ width: 300 }}
          onClick={this.handleOpenStartDateDialog}
          value={formatDate(startDateTime)}
        />
        <DatePickerDialog
          autoOk
          container={'inline'}
          firstDayOfWeek={0}
          floatingLabelText={<FormattedMessage {...messages.startDate} />}
          onAccept={this.handleAcceptStartDate}
          value={startDateTime}
        />
        <TimePickerDialog
          format={'ampm'}
          initialTime={startDateTime}
          minutesStep={1}
        />
      </div>
    );
  }
}

DateTimePicker.propTypes = {

};

export default DateTimePicker;
