/*
 * DateTimePicker Messages
 *
 * This contains all the text for the DateTimePicker component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.DateTimePicker.header',
    defaultMessage: 'This is the DateTimePicker component !',
  },
});
