/**
*
* EventCard
*
*/

import React, { PropTypes } from 'react';
import { fromJS } from 'immutable';
import ClockIcon from 'material-ui/svg-icons/device/access-time';
import Paper from 'material-ui/Paper';
import PeopleIcon from 'material-ui/svg-icons/social/people';
import PlaceIcon from 'material-ui/svg-icons/maps/place';
import RestaurantIcon from 'material-ui/svg-icons/maps/restaurant';

class EventCard extends React.Component { // eslint-disable-line react/prefer-stateless-function


  render() {
    return (
      <Paper style={{ marginTop: 10, minWidth: 300 }} zDepth={2}>
        <div style={{ width: '100%', height: 50, backgroundColor: 'gray', padding: 10 }}>
          <h3 style={{ alignItems: 'center', color: 'white', display: 'flex', justifyContent: 'space-between', margin: 0 }}>
            <div>
              <RestaurantIcon style={{ color: 'lightgray' }} />
              <span style={{ marginLeft: 3 }}>Network Under 40 | Catering</span>
            </div>
            <div>
              <span style={{ color: 'white' }}>$2,000 - $5,000</span>
            </div>
          </h3>
        </div>
        <div style={{ padding: '2px 15px' }}>
          <div>
            <p><ClockIcon /> Wed Oct 10, 2017 7:00 pm - 11:00 pm</p>
            <p><PlaceIcon /> Within 25 Miles of Atlanta</p>
            <p><PeopleIcon /> 300 - 500 Guests</p>
          </div>
        </div>
      </Paper>
    );
  }
}

EventCard.defaultProps = {
  event: fromJS({}),
};

EventCard.propTypes = {
  event: PropTypes.object,
};

export default EventCard;
