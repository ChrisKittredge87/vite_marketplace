/*
 * EventTemplateList Messages
 *
 * This contains all the text for the EventTemplateList component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.EventTemplateList.header',
    defaultMessage: 'This is the EventTemplateList component !',
  },
});
