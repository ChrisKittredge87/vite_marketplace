/**
*
* EventTemplateList
*
*/

import React, { PropTypes } from 'react';
import moment from 'moment';
import { fromJS } from 'immutable';
import { AutoSizer, List } from 'react-virtualized';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import _ from 'lodash';

import EventSummaryCard from 'components/eventPlanners/EventSummaryCard';
import {
  ADDRESS,
  DONT_SET_BUDGET,
  END_DATE,
  INCLUDE_IN_HOUSE_VENDORS,
  INCLUDE_OUTSIDE_VENDORS,
  NAME,
  NEEDS_VENUE,
  NUMBER_OF_EXPECTED_GUESTS,
  OVERALL_BUDGET,
  RADIUS,
  START_DATE,
  VENDOR_BUDGET,
  VENUE_BUDGET,
} from 'entities/schemas/eventTemplates/constants';
import { XXS, XS, SM, MD, LG, XL } from 'globalConstants';
import { RespDiv } from 'styled/common';

import messages from './messages';

export const Container = styled.div`
  margin: 10px 0;
  width: 100%;
  height: calc(100vh - 132px);
  overflow: auto;
`;

class EventTemplateList extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.rowCalc = this.rowCalc.bind(this);
    this.renderRow = this.renderRow.bind(this);
  }

  /* ----- Util Methods ----- */

  numberOfCols() {
    switch (true) {
      case window.innerWidth < LG:
        return 1;
      case window.innerWidth < XL:
        return 2;
      default:
        return 3;
    }
  }

  rowCalc(row, max) {
    const cols = this.numberOfCols();
    const baseCols = _.range(cols);
    const colsByRow = _.map(baseCols, (col) => (cols * row) + col);
    return _.filter(colsByRow, (idx) => idx < max);
  }

  rowHeightCalc() {
    switch (true) {
      case window.innerWidth <= XS:
        return 290;
      case window.innerWidth <= XL:
        return 345;
      default:
        return 345;
    }
  }

  textSizeCalc() {
    switch (true) {
      case window.innerWidth < LG:
        return 10;
      case window.innerWidth < XL:
        return 12;
      default:
        return 12;
    }
  }

  /* ----- END Util Methods ----- */

  renderRow({ index, style }) {
    const { eventTemplates } = this.props;
    return (
      <div
        key={`event_template_list_items_row_${index}`}
        style={style}
      >
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          {
            _.map(this.rowCalc(index, eventTemplates.size), (idx) => {
              const template = eventTemplates.get(idx);
              return (<RespDiv cols={this.numberOfCols()} key={`event_template_list_item_${idx}`}>
                <EventSummaryCard
                  estimatedSpend={template.get(VENUE_BUDGET) }
                  hideBudget={template.get(DONT_SET_BUDGET)}
                  startDateTime={moment(template.get(START_DATE))}
                  endDateTime={moment(template.get(END_DATE))}
                  location={template.get(ADDRESS)}
                  name={template.get(NAME)}
                  numberOfExpectedGuests={template.get(NUMBER_OF_EXPECTED_GUESTS)}
                  numberOfMatchingVendors={57}
                  numberOfMatchingVenues={121}
                  overallBudget={_.toNumber(template.get(OVERALL_BUDGET))}
                  radius={template.get(RADIUS)}
                  showMatchingVendors
                  showMatchingVenues
                  mobile={window.innerWidth <= XS}
                  onClickEditEventTemplate={() => true}
                  onClickCopyEventTemplate={() => true}
                  onClickDeleteEventTemplate={() => true}
                  onClickPublishEventTemplate={() => true}
                />
              </RespDiv>)
            })
          }
        </div>
      </div>
    );
  }

  render() {
    const { eventTemplates } = this.props;
    return (
      <Container>
        <AutoSizer>
          {({ height, width }) => (
            <List
              height={height}
              rowCount={_.ceil(eventTemplates.size / this.numberOfCols())}
              rowHeight={this.rowHeightCalc()}
              rowRenderer={this.renderRow}
              width={width}
            />)}
        </AutoSizer>
      </Container>
    );
  }
}

EventTemplateList.propTypes = {
  eventTemplates: PropTypes.object,
};

EventTemplateList.defaultProps = {
  eventTemplates: fromJS([]),
};

export default EventTemplateList;
