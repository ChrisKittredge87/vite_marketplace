/**
*
* EventSummaryCard
*
*/

import React, { PropTypes } from 'react';
import ClockIcon from 'material-ui/svg-icons/device/access-time';
import CopyIcon from 'material-ui/svg-icons/content/content-copy';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import EditIcon from 'material-ui/svg-icons/image/edit';
import EventIcon from 'material-ui/svg-icons/action/event';
import RaisedButton from 'material-ui/RaisedButton';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import IconButton from 'material-ui/IconButton';
import LinearProgress from 'material-ui/LinearProgress';
import PeopleIcon from 'material-ui/svg-icons/social/people';
import PlaceIcon from 'material-ui/svg-icons/maps/place';
import PublishIcon from 'material-ui/svg-icons/editor/publish';
import moment from 'moment';
import RestaurantIcon from 'material-ui/svg-icons/maps/restaurant';

import messages from './messages';

class EventSummaryCard extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.formatEventTimeline = this.formatEventTimeline.bind(this);
    this.renderEventTemplateCallbackButtons = this.renderEventTemplateCallbackButtons.bind(this);
  }

  /* ----- Helpers ----- */

  formatEventTimeline() {
    const { endDateTime, startDateTime, mobile } = this.props;
    const dateFormat = mobile ? 'MM/DD/YY' : 'MMMM Do, YYYY';
    if (!endDateTime || !startDateTime) {
      return '';
    }
    if (startDateTime.date() === endDateTime.date()) {
      return `${startDateTime.format(`${dateFormat} | h:mm a`)} - ${endDateTime.format('h:mm a')}`;
    }
    return `${startDateTime.format(`${dateFormat}, h:mm a`)} - ${endDateTime.format(`${dateFormat}, h:mm a`)}`;
  }

  /* ----- END Helpers ----- */

  /* ----- Rendering ----- */

  renderEventTemplateCallbackButtons() {
    const {
      mobile,
      onClickEditEventTemplate,
      onClickCopyEventTemplate,
      onClickDeleteEventTemplate,
      onClickPublishEventTemplate,
    } = this.props;
    return mobile ?
      (<div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <div>
          {
            !!onClickEditEventTemplate ?
              <IconButton
                iconStyle={{ width: 20, height: 20, margin: -10 }}
                style={{ width: 20, height: 20 }}>
                  <EditIcon />
              </IconButton> : null
          }
          {
            !!onClickCopyEventTemplate ?
              <IconButton
                iconStyle={{ width: 20, height: 20, margin: -10 }}
                style={{ width: 20, height: 20 }}>
                  <CopyIcon />
              </IconButton> : null
          }
          {
            !!onClickDeleteEventTemplate ?
              <IconButton
                iconStyle={{ width: 20, height: 20, margin: -10 }}
                style={{ width: 20, height: 20 }}>
                  <DeleteIcon />
              </IconButton> : null
          }
        </div>
        <div>
          {
            !!onClickPublishEventTemplate ?
              <IconButton
                iconStyle={{ width: 20, height: 20, margin: -10 }}
                style={{ width: 20, height: 20 }}>
                  <PublishIcon />
              </IconButton> : null
          }
        </div>
      </div>) :
      (<div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <div>
          {
            !!onClickEditEventTemplate ?
              <RaisedButton
                icon={<EditIcon />}
                label={<FormattedMessage {...messages.edit} />}
                style={{ marginRight: 8 }}
              /> : null
          }
          {
            !!onClickCopyEventTemplate ?
              <RaisedButton
                icon={<CopyIcon />}
                label={<FormattedMessage {...messages.copy} />}
                primary
                style={{ marginRight: 8 }}
              /> : null
          }
          {
            !!onClickDeleteEventTemplate ?
              <RaisedButton
                icon={<DeleteIcon />}
                label={<FormattedMessage {...messages.delete} />}
                secondary
              /> : null
          }
        </div>
        <div>
          {
            !!onClickPublishEventTemplate ?
              <RaisedButton
                icon={<PublishIcon />}
                label={<FormattedMessage {...messages.publish} />}
                primary
              /> : null
          }
        </div>
      </div>);
  }

  render() {
    const {
      estimatedSpend,
      hideBudget,
      location,
      name,
      numberOfExpectedGuests,
      numberOfMatchingVenues,
      numberOfMatchingVendors,
      overallBudget,
      radius,
      showMatchingVenues,
      showMatchingVendors,
      mobile,
      // callbacks
      onClickEditBudget,
      onClickEditEventDate,
      onClickEditNumberOfGuests,
      onClickEditVendors,
      onClickEditVenue,
    } = this.props;
    const spend = estimatedSpend > 0 ? <FormattedNumber value={estimatedSpend} /> : '0';
    const budget = overallBudget > 0 ? (<FormattedNumber value={overallBudget} />) : '0';
    const percentOfBudgetSpent = (!estimatedSpend || !overallBudget) ? 0 : (estimatedSpend / overallBudget) * 100;
    const leftIconStyle = {
      height: mobile ? 18 : 24,
      marginRight: 7,
      width: mobile ? 18 : 24,
    };
    const headerStyle = {
      alignItems: 'center',
      color: 'white',
      display: 'flex',
      fontSize: mobile ? 16 : 24,
      margin: 0,
    };
    const matchingSPStyle = {
      margin: 0,
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
    };
    return (
      <div style={{ border: '1px solid black', width: '100%' }}>
        <div style={{ background: '#1278BB', padding: '5px 20px' }}>
          <h2
            style={headerStyle}
          >
            <EventIcon
              style={_.assign({}, leftIconStyle, { color: 'white' } )}
            />
            <div style={{ marginRight: 8 }}>
              {
                !!name ?
                  <span>{name}</span> :
                <FormattedMessage {...messages.eventSummary} />
              }
            </div>
          </h2>
        </div>
        <div style={{ padding: '6px 20px', fontSize: mobile ? 14 : 18 }}>
          <p style={{ margin: '10px 0', display: 'flex', alignItems: 'center' }}>
            <ClockIcon style={leftIconStyle} />
            <span style={{ marginRight: 8 }}>
              {this.formatEventTimeline()}
            </span>
            {
              !!onClickEditEventDate ?
                <IconButton
                  iconStyle={{ width: 20, height: 20, margin: -10 }}
                  onClick={onClickEditEventDate}
                  style={{ width: 20, height: 20 }}>
                    <EditIcon />
                </IconButton> : null
            }
          </p>
          <p style={{ margin: '10px 0', display: 'flex' }}>
            <PeopleIcon style={leftIconStyle} />
            <span style={{ marginRight: 8}}>
              <b style={{ marginRight: 4 }}>{numberOfExpectedGuests > 0 ? <FormattedNumber value={numberOfExpectedGuests} /> : '--'}</b>
              <FormattedMessage {...messages.expectedGuests} />
            </span>
            {
              !!onClickEditNumberOfGuests ?
                <IconButton
                  iconStyle={{ width: 20, height: 20, margin: -10 }}
                  onClick={onClickEditNumberOfGuests}
                  style={{ width: 20, height: 20 }}>
                    <EditIcon />
                </IconButton> : null
            }
          </p>
          {
            showMatchingVenues ?
            <div style={{ margin: '10px 0', display: 'flex', alignItems: 'center' }}>
              <PlaceIcon style={leftIconStyle} />
              <span style={{ display: 'flex', alignItems: 'center' }}>
                <b style={{ marginRight: 8}}>{
                  numberOfMatchingVenues > 0 ?
                    <FormattedNumber value={numberOfMatchingVenues} /> :null
                }</b>
                <p style={matchingSPStyle}>
                  <FormattedMessage
                    {...messages[`${mobile ? 'mobileM' : 'm'}atchingVenuesWithinRadius`]}
                    values={{ radius, location }}
                  />
                </p>
              </span>
              {
                !!onClickEditVenue ?
                  <IconButton
                    iconStyle={{ width: 20, height: 20, margin: -10 }}
                    onClick={onClickEditVenue}
                    style={{ width: 20, height: 20 }}>
                      <EditIcon />
                  </IconButton> : null
              }
            </div> : null
          }
          {
            showMatchingVendors ?
              <div style={{ margin: '10px 0', display: 'flex', alignItems: 'center' }}>
                <RestaurantIcon style={leftIconStyle} />
                <span style={{ display: 'flex', alignItems: 'center' }}>
                  <b style={{ marginRight: 8}}>{
                    numberOfMatchingVenues > 0 ?
                    <FormattedNumber value={numberOfMatchingVendors} /> : null
                  }</b>
                  <p style={matchingSPStyle}>
                    <FormattedMessage {...messages[`${mobile ? 'mobileM' : 'm'}atchingVendorsWithinRadius`]} values={{ radius, location }} />
                  </p>
                </span>
                  {
                    !!onClickEditVendors ?
                      <IconButton
                        iconStyle={{ width: 20, height: 20, margin: -10 }}
                        onClick={onClickEditVendors}
                        style={{ width: 20, height: 20 }}>
                          <EditIcon />
                      </IconButton> : null
                  }
              </div> : null
          }
          {
            hideBudget ? null :
            <div>
              <LinearProgress
                mode="determinate"
                value={percentOfBudgetSpent}
                style={{ margin: '10px 0', height: 24 }}
              />
              <div style={{ margin: '6px 0', display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                <span style={{ display: 'flex', alignItems: 'center' }}>
                  <b>
                    <FormattedMessage {...messages.budgetBreakdown} values={{ spend, budget }} />
                  </b>
                  {
                    !!onClickEditBudget ?
                      <IconButton
                        iconStyle={{ width: 20, height: 20, margin: -10 }}
                        onClick={onClickEditBudget}
                        style={{ width: 20, height: 20, marginLeft: 8 }}>
                          <EditIcon />
                      </IconButton> : null
                  }
                </span>
              </div>
              <div style={{ margin: '10px 0' }}>
                {this.renderEventTemplateCallbackButtons()}
              </div>
            </div>
          }
        </div>
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

EventSummaryCard.propTypes = {
  hideBudget: PropTypes.bool,
  endDateTime: PropTypes.object,
  estimatedSpend: PropTypes.number,
  location: PropTypes.string,
  name: PropTypes.string,
  numberOfExpectedGuests: PropTypes.number,
  numberOfMatchingVenues: PropTypes.number,
  numberOfMatchingVendors: PropTypes.number,
  overallBudget: PropTypes.number,
  radius: PropTypes.number,
  showMatchingVendors: PropTypes.bool,
  showMatchingVenues: PropTypes.bool,
  startDateTime: PropTypes.object,
  // formatting
  mobile: PropTypes.bool,
  // callbacks
  onClickEditBudget: PropTypes.func,
  onClickEditEventDate: PropTypes.func,
  onClickEditNumberOfGuests: PropTypes.func,
  onClickEditVendors: PropTypes.func,
  onClickEditVenue: PropTypes.func,
  onClickEditEventTemplate: PropTypes.func,
  onClickCopyEventTemplate: PropTypes.func,
  onClickDeleteEventTemplate: PropTypes.func,
  onClickPublishEventTemplate: PropTypes.func,
};

export default EventSummaryCard;
