/*
 * EventSummaryCard Messages
 *
 * This contains all the text for the EventSummaryCard component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  budgetBreakdown: {
    id: 'app.components.eventPlanners.EventSummaryCard.budgetBreakdown',
    defaultMessage: '${ spend } of your ${ budget } Budget',
  },
  copy: {
    id: 'copy',
    defaultMessage: 'Copy',
  },
  delete: {
    id: 'delete',
    defaultMessage: 'Delete',
  },
  edit: {
    id: 'edit',
    defaultMessage: 'Edit',
  },
  eventSummary: {
    id: 'eventSummary',
    defaultMessage: 'Event Summary',
  },
  expectedGuests: {
    id: 'expectedGuests',
    defaultMessage: 'Expected Guests',
  },
  matchingVendorsWithinRadius: {
    id: 'matchingVendorsWithinRadius',
    defaultMessage: 'Vendors | { radius } miles of { location }',
  },
  matchingVenuesWithinRadius: {
    id: 'matchingVenuesWithinRadius',
    defaultMessage: 'Venues | { radius } miles of { location }',
  },
  mobileMatchingVendorsWithinRadius: {
    id: 'mobileMatchingVendorsWithinRadius',
    defaultMessage: 'Vendors | { radius } mi. of { location }',
  },
  mobileMatchingVenuesWithinRadius: {
    id: 'mobileMatchingVenuesWithinRadius',
    defaultMessage: 'Venues | { radius } mi. of { location }',
  },
  publish: {
    id: 'publish',
    defaultMessage: 'Publish',
  },
});
