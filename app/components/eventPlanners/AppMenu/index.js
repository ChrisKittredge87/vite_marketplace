/**
*
* EventPlannerAppMenu
*
*/

import React, { PropTypes } from 'react';

import Badge from 'material-ui/Badge';
import Divider from 'material-ui/Divider';
import EventsIcon from 'material-ui/svg-icons/action/event';
import { FormattedMessage } from 'react-intl';
import FlatButton from 'material-ui/FlatButton';
import HomeIcon from 'material-ui/svg-icons/action/home';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import { Link } from 'react-router';
import LogoutIcon from 'material-ui/svg-icons/action/exit-to-app';
import MenuItem from 'material-ui/MenuItem';
import MessagesIcon from 'material-ui/svg-icons/communication/email';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications';
import SearchIcon from 'material-ui/svg-icons/action/search';
import SettingsIcon from 'material-ui/svg-icons/action/settings';

import { XXS, XS, SM, MD, LG, XL } from 'globalConstants';
import { path as adminPath } from 'containers/Admin/routes';
import { path as createEventPath } from 'containers/eventPlanners/Events/Create/routes';
import { path as eventsPath } from 'containers/eventPlanners/Events/routes';
import { path as homePath } from 'containers/Home/routes';
import { path as searchPath } from 'containers/serviceProviders/Search/routes';

import messages from './messages';

export class EventPlannerAppMenu extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.renderIconMenu = this.renderIconMenu.bind(this);
  }

  /* ----- Rendering ----- */

  renderIconMenu() {
    const { isAdmin, onLogoutUser } = this.props;
    return (<IconMenu
      anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
      iconButtonElement={<IconButton iconStyle={{ color: 'white' }}><MoreVertIcon/></IconButton>}
      targetOrigin={{ horizontal: 'right', vertical: 'top' }}
      style={{ color: 'white' }}
    >
      <MenuItem
        containerElement={<Link to={homePath}>.</Link>}
        primaryText={<FormattedMessage {...messages.home} />}
        rightIcon={<HomeIcon />}
        style={{ minWidth: 200 }}
      />
      <MenuItem
        containerElement={<Link to={searchPath}>.</Link>}
        primaryText={<FormattedMessage {...messages.search} />}
        rightIcon={<SearchIcon />}
        style={{ minWidth: 200 }}
      />
      <Divider />
      <MenuItem
        key="user_events"
        containerElement={<Link to={eventsPath}>.</Link>}
        primaryText={<FormattedMessage {...messages.myEvents} />}
        rightIcon={<EventsIcon />}
        style={{ minWidth: 200 }}
      />
      <Divider />
      { isAdmin ?
        <MenuItem
          key="user_menu_item"
          containerElement={<Link to={`${adminPath}/users`}>.</Link>}
          primaryText={<FormattedMessage {...messages.admin} />}
          rightIcon={<SettingsIcon />}
          style={{ minWidth: 200 }}
        /> : null
      }
      { isAdmin ? <Divider /> : null }
      <MenuItem
        onClick={onLogoutUser}
        primaryText={<FormattedMessage {...messages.logOut} />}
        rightIcon={<LogoutIcon />}
        style={{ minWidth: 200 }}
      />
    </IconMenu>);
  }

  renderMessagesIcon() {
    const messages = 10;
    const messagesIconButton = (<IconButton
      iconStyle={{ color: 'white' }}
    >
        <MessagesIcon />
    </IconButton>);
    return messages > 0 ? <Badge
      badgeContent={messages}
      badgeStyle={{ top: 0, right: -12, background: 'rgba(255, 255, 255, 0.2)' }}
      primary
      style={{ padding: 0 }}
    >
      {messagesIconButton}
    </Badge> : messagesIconButton;
  }

  renderNotificationsIcon() {
    const notifications = 10;
    const notificationsIcon = (<IconButton
      iconStyle={{ color: 'white' }}
    >
        <NotificationsIcon />
    </IconButton>);
    return notifications > 0 ? <Badge
      badgeContent={notifications}
      badgeStyle={{ top: 0, right: -12, background: 'rgba(255, 255, 255, 0.2)' }}
      primary
      style={{ padding: 0 }}
    >
      {notificationsIcon}
    </Badge> : notificationsIcon;
  }

  render() {
    const mobile = window.innerWidth < SM;
    return (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        {
          !mobile ?
            <div style={{ marginRight: 10 }}>
              {this.renderNotificationsIcon()}
            </div> : null
        }
        {
          !mobile ?
            <div style={{ marginRight: 10 }}>
              {this.renderMessagesIcon()}
            </div> : null
        }
        {this.renderIconMenu()}
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

EventPlannerAppMenu.propTypes = {
  isAdmin: PropTypes.bool,
  onLogoutUser: PropTypes.func,
};

EventPlannerAppMenu.defaultProps = {
  onLogoutUser: () => true,
};

export default EventPlannerAppMenu;
