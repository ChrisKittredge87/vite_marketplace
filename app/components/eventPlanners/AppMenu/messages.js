/*
 * EventPlannerAppMenu Messages
 *
 * This contains all the text for the AppMenu component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  admin: {
    id: 'admin',
    defaultMessage: 'Admin',
  },
  home: {
    id: 'home',
    defaultMessage: 'Home',
  },
  logOut: {
    id: 'logOut',
    defaultMessage: 'Log Out',
  },
  myEvents: {
    id: 'myEvents',
    defaultMessage: 'My Events',
  },
  search: {
    id: 'search',
    defaultMessage: 'Search',
  },
});
