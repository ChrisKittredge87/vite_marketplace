/**
*
* DateBudgetGuestsStep
*
*/

import React, { PropTypes } from 'react';

import CalendarIcon from 'material-ui/svg-icons/action/event';
import ClockIcon from 'material-ui/svg-icons/device/access-time';
import PeopleIcon from 'material-ui/svg-icons/social/people';
import MoneyIcon from 'material-ui/svg-icons/editor/attach-money';
import Checkbox from 'material-ui/Checkbox';
import TextField from 'material-ui/TextField';
import DatePickerDialog from 'material-ui/DatePicker/DatePickerDialog';
import TimePickerDialog from 'material-ui/TimePicker/TimePickerDialog';
import moment from 'moment';

import NumberField from 'components/common/MaskedTextField/NumberField';
import CurrencyField from 'components/common/MaskedTextField/CurrencyField';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class DateBudgetGuestsPanel extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    const { startDateTime, endDateTime, numberOfGuests, overallBudget, hasSetBudget } = props;
    this.state = { startDateTime, endDateTime, numberOfGuests, overallBudget, hasSetBudget };

    this.handleAcceptStartDate = this.handleAcceptStartDate.bind(this);
    this.handleAcceptEndDate = this.handleAcceptEndDate.bind(this);
    this.handleOpenEndDateDialog = this.handleOpenEndDateDialog.bind(this);
    this.handleOpenEndTimeDialog = this.handleOpenEndTimeDialog.bind(this);
    this.handleOpenStartDateDialog = this.handleOpenStartDateDialog.bind(this);
    this.handleOpenStartTimeDialog = this.handleOpenStartTimeDialog.bind(this);
  }

  /* ----- Lifecycle ----- */

  componentWillMount() {
    const {
      startDateTime, endDateTime,
      numberOfExpectedGuests,
      overallBudget,
      dontSetBudget,
      /* callbacks */
      onValidate,
    } = this.props;
    const isValid = !!startDateTime && !!endDateTime &&
                    endDateTime.isAfter(startDateTime) &&
                    !!numberOfExpectedGuests && (dontSetBudget || !!overallBudget);
    onValidate(isValid);
  }

  componentWillReceiveProps(nextProps) {
    const {
      startDateTime, endDateTime,
      numberOfExpectedGuests,
      overallBudget,
      dontSetBudget,
      /* callbacks */
      onValidate,
    } = this.props;
    const {
      startDateTime: nextStartDateTime,
      endDateTime: nextEndDateTime,
      numberOfExpectedGuests: nextNumberOfExpectedGuests,
      overallBudget: nextOverallBudget,
      dontSetBudget: nextDontSetBudget,
    } = nextProps;
    if (startDateTime !== nextStartDateTime || endDateTime !== nextEndDateTime ||
      numberOfExpectedGuests !== nextNumberOfExpectedGuests ||
      overallBudget !== nextOverallBudget ||
      dontSetBudget !== nextDontSetBudget
    ) {
      const isValid = !!nextStartDateTime && !!nextEndDateTime &&
                      nextEndDateTime.isAfter(nextStartDateTime) &&
                      !!nextNumberOfExpectedGuests && (nextDontSetBudget || !!nextOverallBudget);
      onValidate(isValid);
    }
  }

  /* ----- END Lifecycle ----- */

  /* ----- Event Binding ----- */

  handleAcceptEndDate(newDate) {
    const { onEndDateTimeChange } = this.props;
    onEndDateTimeChange(newDate);
    this.endTimeDialog.show();
  }

  handleAcceptStartDate(newDate) {
    const { onStartDateTimeChange } = this.props;
    onStartDateTimeChange(newDate);
    this.startTimeDialog.show();
  }

  handleOpenEndDateDialog() {
    this.endDateDialog.show();
  }

  handleOpenEndTimeDialog() {
    this.endTimeDialog.show();
  }

  handleOpenStartDateDialog() {
    this.startDateDialog.show();
  }

  handleOpenStartTimeDialog() {
    this.startTimeDialog.show();
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  render() {
    function formatDate(date) {
      return moment(date).format('M/D/YYYY h:mm a');
    }
    const {
      startDateTime,
      endDateTime,
      numberOfExpectedGuests,
      overallBudget,
      dontSetBudget,
      /* callbacks */
      onNumberOfExpectedGuestsChange,
      onOverallBudgetChange,
      onStartDateTimeChange,
      onEndDateTimeChange,
      onDontSetBudgetChange,
    } = this.props;
    return (
      <div style={{ padding: '0 0 10px 0'}}>
        <p style={{ textAlign: 'center' }}>
          <FormattedMessage {...messages.description} />
        </p>
        <div style={{ display: 'flex', alignItems: 'baseline', justifyContent: 'space-between' }}>
          <div>
            <div style={{ position: 'relative', display: 'inline-block' }}>
              <CalendarIcon
                onClick={this.handleOpenStartDateDialog}
                style={{ position: 'absolute', right: 26, top: 30, height: 30, cursor: 'pointer', zIndex: 3 }}
              />
              <ClockIcon
                onClick={this.handleOpenStartTimeDialog}
                style={{ position: 'absolute', right: 0, top: 30, height: 30, cursor: 'pointer', zIndex: 3 }}
              />
              <TextField
                onClick={this.handleOpenStartDateDialog}
                floatingLabelText={<FormattedMessage {...messages.startDate} />}
                style={{ width: 300 }}
                value={formatDate(startDateTime)}
              />
              <DatePickerDialog
                autoOk
                container={'inline'}
                firstDayOfWeek={0}
                onAccept={this.handleAcceptStartDate}
                ref={(c) => { this.startDateDialog = c; }}
                value={startDateTime}
              />
              <TimePickerDialog
                format={'ampm'}
                initialTime={startDateTime.toDate()}
                minutesStep={1}
                onAccept={this.handleAcceptStartDate}
                ref={(c) => { this.startTimeDialog = c; }}
              />
            </div>
          </div>
          <p style={{ fontSize: 20 }}>to</p>
          <div style={{ position: 'relative', display: 'inline-block' }}>
            <CalendarIcon
              onClick={this.handleOpenEndDateDialog}
              style={{ position: 'absolute', right: 26, top: 30, height: 30, cursor: 'pointer', zIndex: 3 }}
            />
            <ClockIcon
              onClick={this.handleOpenEndTimeDialog}
              style={{ position: 'absolute', right: 0, top: 30, height: 30, cursor: 'pointer', zIndex: 3 }}
            />
            <TextField
              floatingLabelText={<FormattedMessage {...messages.startDate} />}
              onClick={this.handleOpenEndDateDialog}
              style={{ width: 300 }}
              value={formatDate(endDateTime)}
            />
            <DatePickerDialog
              autoOk
              container={'inline'}
              firstDayOfWeek={0}
              onAccept={this.handleAcceptEndDate}
              ref={(c) => { this.endDateDialog = c; }}
              value={endDateTime}
            />
            <TimePickerDialog
              format={'ampm'}
              initialTime={endDateTime.toDate()}
              minutesStep={1}
              onAccept={this.handleAcceptEndDate}
              ref={(c) => { this.endTimeDialog = c; }}
            />
          </div>
        </div>
        <div style={{ display: 'flex', alignItems: 'baseline', justifyContent: 'space-between' }} >
          <div style={{ position: 'relative', display: 'inline-block', width: '100%' }}>
            <PeopleIcon style={{ position: 'absolute', right: 5, top: 35, width: 20, height: 25 }} />
            <NumberField
              fullWidth
              floatingLabelText={<FormattedMessage {...messages.numberOfExpectedGuests} />}
              maxLength="6"
              onChange={onNumberOfExpectedGuestsChange}
              precision={0}
              value={numberOfExpectedGuests}
            />
          </div>
        </div>
        <div style={{ display: 'flex', alignItems: 'baseline', justifyContent: 'space-between' }}>
          <div style={{ position: 'relative', display: 'inline-block', width: '100%' }}>
            <MoneyIcon style={{ position: 'absolute', right: 5, top: 35, width: 20, height: 25 }} />
            <CurrencyField
              disabled={dontSetBudget}
              fullWidth
              floatingLabelText={<FormattedMessage {...messages.budget} />}
              maxLength="8"
              onChange={onOverallBudgetChange}
              precision={0}
              value={overallBudget}
            />
          </div>
        </div>
        <div style={{ padding: '15px 0 0 0' }}>
          <Checkbox
            label={<FormattedMessage {...messages.dontSetBudget} />}
            checked={dontSetBudget}
            onCheck={onDontSetBudgetChange}
          />
        </div>
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

DateBudgetGuestsPanel.propTypes = {
  startDateTime: PropTypes.object,
  endDateTime: PropTypes.object,
  numberOfExpectedGuests: PropTypes.number,
  overallBudget: PropTypes.number,
  dontSetBudget: PropTypes.bool,
  /* callbacks */
  onStartDateTimeChange: PropTypes.func,
  onEndDateTimeChange: PropTypes.func,
  onNumberOfExpectedGuestsChange: PropTypes.func,
  onOverallBudgetChange: PropTypes.func,
  onDontSetBudgetChange: PropTypes.func,
  onValidate: PropTypes.func,
};

DateBudgetGuestsPanel.defaultProps = {
  onStartDateTimeChange: () => false,
  onEndDateTimeChange: () => false,
  onNumberOfExpectedGuestsChange: () => false,
  onOverallBudgetChange: () => false,
  onDontSetBudgetChange: () => false,
  onValidate: () => false,
};

export default DateBudgetGuestsPanel;
