/*
 * DateBudgetGuestsStep Messages
 *
 * This contains all the text for the DateBudgetGuestsStep component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  budget: {
    id: 'budget',
    defaultMessage: "Budget",
  },
  description: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.DateBudgetGuestsPanel.description',
    defaultMessage: "Let's setup the when, who and how much of your event!",
  },
  dontSetBudget: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.DateBudgetGuestsPanel.dontSetBudget',
    defaultMessage: "Don't Set Budget",
  },
  endDate: {
    id: 'endDate',
    defaultMessage: 'End Date',
  },
  numberOfExpectedGuests: {
    id: 'numberOfExpectedGuests',
    defaultMessage: 'Number of Expected Guests',
  },
  startDate: {
    id: 'startDate',
    defaultMessage: 'Start Date',
  },
});
