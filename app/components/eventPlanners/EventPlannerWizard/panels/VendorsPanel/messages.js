/*
 * FoodAndBeveragesPanel Messages
 *
 * This contains all the text for the FoodAndBeveragesPanel component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  budgetPerCaterer: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VendorsPanel.budgetPerCaterer',
    defaultMessage: 'Budget per Caterer',
  },
  budgetPerFoodTruck: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VendorsPanel.budgetPerFoodTruck',
    defaultMessage: 'Budget per Food Truck',
  },
  caterers: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VendorsPanel.caterers',
    defaultMessage: 'Caterer(s)',
  },
  directions: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VendorsPanel.directions',
    defaultMessage: 'What will you need for food & beverages at your event?',
  },
  emptyPillContainerText: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VendorsPanel.emptyPillContainerText',
    defaultMessage: 'Go ahead, spice things up...',
  },
  vendorBudget: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VendorsPanel.vendorBudget',
    defaultMessage: 'Food & Beverage Budget',
  },
  foodTrucks: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VendorsPanel.foodTrucks',
    defaultMessage: 'Food Trucks(s)',
  },
  guests: {
    id: 'guests',
    defaultMessage: 'Guests',
  },
  host: {
    id: 'host',
    defaultMessage: 'Host',
  },
  paidForBy: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VendorsPanel.paidForBy',
    defaultMessage: 'Paid for by:',
  },
  searchForCuisineTypesToAdd: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VendorsPanel.searchForCuisineTypesToAdd',
    defaultMessage: 'Search for Cuisine Types to add',
  },
});
