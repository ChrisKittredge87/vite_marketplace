VendorsPanel/**
*
* VendorsPanel
*
*/

import React, { PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';
import AddIcon from 'material-ui/svg-icons/content/add';
import MoneyIcon from 'material-ui/svg-icons/editor/attach-money';
import FoodTruckIcon from 'material-ui/svg-icons/maps/local-shipping';
import CatererIcon from 'material-ui/svg-icons/maps/restaurant';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import AutoComplete from 'material-ui/AutoComplete';
import Chip from 'material-ui/Chip';
import { fromJS } from 'immutable';
import _ from 'lodash';

import CurrencyField from 'components/common/MaskedTextField/CurrencyField';
import NumberField from 'components/common/MaskedTextField/NumberField';
import { ID, DISPLAY_NAME } from 'entities/schemas/serviceProviders/cuisineTypes/constants';

import messages from './messages';

class VendorsPanel extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = { cuisineTypeSearchText: '' };

    this.handleAddCuisineTypeChip = this.handleAddCuisineTypeChip.bind(this);
    this.handleRemoveCuisineTypeChip = this.handleRemoveCuisineTypeChip.bind(this);
    this.handleUpdateCuisineTypeSearchText = this.handleUpdateCuisineTypeSearchText.bind(this);
    this.renderAutoComplete = this.renderAutoComplete.bind(this);
    this.renderCatererConfig = this.renderCatererConfig.bind(this);
    this.renderFoodTruckConfig = this.renderFoodTruckConfig.bind(this);
    this.renderCuisineTypeChipContainer = this.renderCuisineTypeChipContainer.bind(this);
  }

  /* ----- Lifecycle ----- */

  componentWillMount() {
    const {
      vendorBudget,
      includeOutsideVendors,
      selectedCuisineTypes,
      onValidate,
    } = this.props;
    let isValid = selectedCuisineTypes.size > 0 && vendorBudget > 0;
    onValidate(isValid);
  }

  componentWillReceiveProps(nextProps) {
    const {
      cuisineTypeOptions,
      vendorBudget,
      selectedCuisineTypes,
      onValidate,
    } = this.props;
    const {
      cuisineTypeOptions: nextCuisineTypeOptions,
      vendorBudget: nextVendorBudget,
      selectedCuisineTypes: nextSelectedCuisineTypes,
    } = nextProps;
    if (!cuisineTypeOptions.equals(nextCuisineTypeOptions) || vendorBudget !== nextVendorBudget ||
        !selectedCuisineTypes.equals(nextSelectedCuisineTypes)) {
        const isValid = nextSelectedCuisineTypes.size > 0 && nextVendorBudget > 0;
        onValidate(isValid);
    }
  }

  /* ----- END Lifecycle ----- */

  /* ----- Event Binding ----- */

  handleAddCuisineTypeChip({ id }) {
    const { onAddCuisineType } = this.props;
    onAddCuisineType(id);
    this.setState({ cuisineTypeSearchText: '' });
    this.cuisineTypeAutoComplete.focus();
  }

  handleRemoveCuisineTypeChip(id) {
    const { onRemoveCuisineType } = this.props;
    return () => onRemoveCuisineType(id);
  }

  handleUpdateCuisineTypeSearchText(cuisineTypeSearchText) {
    this.setState({ cuisineTypeSearchText });
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  renderAutoComplete() {
    const { cuisineTypeSearchText } = this.state;
    const { cuisineTypeOptions, selectedCuisineTypes } = this.props;
    const filteredCuisineTypes = cuisineTypeOptions.filter((ct) => !selectedCuisineTypes.contains(ct.get('id')));
    const dataSourceConfig = { text: DISPLAY_NAME, value: ID };
    return (
      <div style={{ position: 'relative', display: 'inline-block', width: '100%' }}>
        <AddIcon
          style={{ position: 'absolute', right: 3, top: 8, height: 30, cursor: 'pointer', zIndex: 3 }}
        />
        <AutoComplete
          dataSource={filteredCuisineTypes.toJS()}
          dataSourceConfig={dataSourceConfig}
          filter={AutoComplete.fuzzyFilter}
          hintText={<FormattedMessage {...messages.searchForCuisineTypesToAdd} />}
          onNewRequest={this.handleAddCuisineTypeChip}
          onUpdateInput={this.handleUpdateCuisineTypeSearchText}
          ref={(input) => { this.cuisineTypeAutoComplete = input; }}
          searchText={cuisineTypeSearchText}
          style={{ width: '100%' }}
          textFieldStyle={{ width: '100%' }}
        />
      </div>
    );
  }

  renderCuisineTypeChipContainer() {
    const { cuisineTypeOptions, selectedCuisineTypes, onRemoveCuisineType } = this.props;
    return (
      <div style={{ border: '1px solid black', padding: 10, display: 'flex', minHeight: 100 }}>
        {
          selectedCuisineTypes.size === 0 ?
            <p style={{ margin: '5px 0' }}>
              <FormattedMessage {...messages.emptyPillContainerText} />
            </p> : null
        }
        {
          selectedCuisineTypes.map((ctId) => {
            const selectedCT = cuisineTypeOptions.find((ct) => ct.get('id') === ctId);
            return (<Chip
              onRequestDelete={this.handleRemoveCuisineTypeChip(ctId)}
              style={{ height: 32, margin: 4 }}
            >
              {selectedCT.get(DISPLAY_NAME)}
            </Chip>);
          })
        }
      </div>
    );
  }

  renderCatererConfig() {
    const {
      budgetPerCaterer,
      catererQuantity,
      onChangeBudgetPerCaterer,
      onChangeCatererQuantity,
    } = this.props;
    return (
      <div style={{
        border: '1px solid black',
        display: 'flex',
        flexDirection: 'row',
        height: 75,
      }}>
        <div style={{
          alignItems: 'center',
          borderRight: '1px solid black',
          background: 'lightgray',
          display: 'flex',
          height: '99%',
          justifyContent: 'space-around',
          margin: '1px 0',
          width: '15%',
        }}>
          <CatererIcon
            style={{ height: 60, width: 60 }}
          />
        </div>
        <div style={{ margin: '0 7px', fontSize: 22, display: 'flex', alignItems: 'center', width: '84%', justifyContent: 'space-between' }}>
          <div style={{ display: 'flex', alignItems: 'baseline' }}>
            <NumberField
              inputStyle={{ textAlign: 'center', fontSize: 22 }}
              maxLength="3"
              onChange={onChangeCatererQuantity}
              style={{ width: 40, marginRight: 5 }}
              value={catererQuantity}
            /> <FormattedMessage {...messages.caterers} />
          </div>
          <CurrencyField
            floatingLabelShrinkStyle={{ fontSize: 12 }}
            floatingLabelStyle={{ fontSize: 14 }}
            floatingLabelText={<FormattedMessage {...messages.budgetPerCaterer} />}
            maxLength="8"
            onChange={onChangeBudgetPerCaterer}
            style={{ width: 160, marginBottom: 18 }}
            value={budgetPerCaterer}
          />
        </div>
      </div>
    );
  }

  renderFoodTruckConfig() {
    const {
      budgetPerFoodTruck,
      foodTruckPaidForByHost,
      foodTruckQuantity,
      onChangeBudgetPerFoodTruck,
      onChangeFoodTruckPaidForByHost,
      onChangeFoodTruckQuantity,
    } = this.props;
    return (
      <div
        style={{
          border: '1px solid black',
          display: 'flex',
          flexDirection: 'row',
          height: 75,
        }}>
        <div style={{
          alignItems: 'center',
          borderRight: '1px solid black',
          background: 'lightgray',
          display: 'flex',
          height: '100%',
          justifyContent: 'space-around',
          width: '15%',
        }}>
          <FoodTruckIcon style={{ width: 60, height: 60 }} />
        </div>
        <div style={{ fontSize: 22, margin: '0 7px', display: 'flex', alignItems: 'center', justifyContent: 'space-between', width: '84%' }}>
          <div style={{ display: 'flex', alignItems: 'baseline' }}>
            <NumberField
              inputStyle={{ textAlign: 'center', fontSize: 22 }}
              maxLength="3"
              onChange={onChangeFoodTruckQuantity}
              style={{ width: 40, marginRight: 5 }}
              value={foodTruckQuantity}
            /> <FormattedMessage {...messages.foodTrucks} />
          </div>
          <div style={{ margin: '0 7px', padding: '7px 10px', border: '1px solid gray', display: 'flex', flexDirection: 'column' }}>
            <p style={{ fontSize: 12, margin: 0 }}><FormattedMessage {...messages.paidForBy} /></p>
            <RadioButtonGroup
              name="food_trucks_paid_for_by"
              onChange={onChangeFoodTruckPaidForByHost}
              style={{ display: 'flex', fontSize: 18, padding: '0 10px' }}
              valueSelected={foodTruckPaidForByHost}
            >
              <RadioButton
                value={true}
                iconStyle={{ marginRight: 4 }}
                label={<FormattedMessage {...messages.host} />}
              />
              <RadioButton
                value={false}
                iconStyle={{ marginRight: 4 }}
                label={<FormattedMessage {...messages.guests} />}
              />
            </RadioButtonGroup>
          </div>
          <CurrencyField
            floatingLabelShrinkStyle={{ fontSize: 12 }}
            floatingLabelStyle={{ fontSize: 14 }}
            floatingLabelText={<FormattedMessage {...messages.budgetPerFoodTruck} />}
            maxLength="8"
            onChange={onChangeBudgetPerFoodTruck}
            style={{ width: 160, marginBottom: 18 }}
            value={budgetPerFoodTruck}
          />
        </div>
      </div>
    );
  }

  render() {
    const { vendorBudget, includeOutsideVendors, onChangeVendorBudget } = this.props;
    return (
      <div>
        <p style={{ textAlign: 'center' }}>
          <FormattedMessage {...messages.directions} />
        </p>
        <div style={{ position: 'relative', display: 'inline-block', width: '100%' }}>
          <MoneyIcon style={{ position: 'absolute', right: 5, top: 35, width: 20, height: 25 }} />
          <CurrencyField
            floatingLabelText={<FormattedMessage {...messages.vendorBudget} />}
            fullWidth
            maxLength="8"
            onChange={onChangeVendorBudget}
            precision={0}
            value={vendorBudget}
          />
        </div>
        <div>
          {this.renderAutoComplete()}
          {this.renderCuisineTypeChipContainer()}
        </div>
        {
          includeOutsideVendors ?
            <div>
              <div style={{ margin: '12px 0 0 0' }}>
                {this.renderFoodTruckConfig()}
              </div>
              <div style={{ margin: '12px 0 0 0' }}>
                {this.renderCatererConfig()}
              </div>
            </div> : null
        }
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

VendorsPanel.propTypes = {
  budgetPerCaterer: PropTypes.number,
  budgetPerFoodTruck: PropTypes.number,
  catererQuantity: PropTypes.number,
  cuisineTypeOptions: PropTypes.object,
  vendorBudget: PropTypes.number,
  foodTruckQuantity: PropTypes.number,
  foodTruckPaidForByHost: PropTypes.bool,
  includeOutsideVendors: PropTypes.bool,
  selectedCuisineTypes: PropTypes.object,
  // Callbacks
  onAddCuisineType: PropTypes.func,
  onChangeBudgetPerCaterer: PropTypes.func,
  onChangeBudgetPerFoodTruck: PropTypes.func,
  onChangeCatererQuantity: PropTypes.func,
  onChangeVendorBudget: PropTypes.func,
  onChangeFoodTruckPaidForByHost: PropTypes.func,
  onChangeFoodTruckQuantity: PropTypes.func,
  onRemoveCuisineType: PropTypes.func,
  onValidate: PropTypes.func,
};

VendorsPanel.defaultProps = {
  cuisineTypeOptions: fromJS([]),
  selectedCuisineTypes: fromJS([]),
  // Callbacks
  onAddCuisineType: () => true,
  onChangeBudgetPerCaterer: () => true,
  onChangeBudgetPerFoodTruck: () => true,
  onChangeCatererQuantity: () => true,
  onChangeVendorBudget: () => true,
  onChangeFoodTruckPaidForByHost: () => true,
  onChangeFoodTruckQuantity: () => true,
  onRemoveCuisineType: () => true,
  onValidate: () => true,
};

export default VendorsPanel;
