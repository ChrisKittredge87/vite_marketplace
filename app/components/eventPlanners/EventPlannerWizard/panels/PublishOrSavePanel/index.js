/**
*
* PublishOrSavePanel
*
*/

import React, { PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';
import TextField from 'material-ui/TextField';

import messages from './messages';

class PublishOrSavePanel extends React.Component { // eslint-disable-line react/prefer-stateless-function
  /* ----- Lifecycle ----- */

  componentWillMount() {
    const { description, name, onValidate } = this.props;
    const isValid = !!description && !!name;
    onValidate(isValid);
  }

  componentWillReceiveProps(nextProps) {
    const { description, name, onValidate } = this.props;
    const { description: nextDescription, name: nextName } = nextProps;
    if (description !== nextDescription || name !== nextName) {
      const isValid = !!nextDescription && !!nextName;
      onValidate(isValid);
    }
  }
  /* ----- END Lifecycle ----- */

  /* ----- Rendering ----- */

  render() {
    const { description, name, onChangeName, onChangeDescription } = this.props;
    return (
      <div>
        <p style={{ textAlign: 'center' }}>
          <FormattedMessage {...messages.directions} />
        </p>
        <TextField
          floatingLabelText={'Event Name'}
          fullWidth
          maxLength="50"
          onChange={onChangeName}
          value={name}
        />
        <TextField
          floatingLabelText={'Event Description'}
          fullWidth
          value={description}
          multiLine
          onChange={onChangeDescription}
        />
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

PublishOrSavePanel.propTypes = {
  description: PropTypes.string,
  name: PropTypes.string,
  // callbacks
  onChangeDescription: PropTypes.func,
  onChangeName: PropTypes.func,
  onValidate: PropTypes.func,
};

PublishOrSavePanel.defaultProps = {
  onChangeDescription: () => true,
  onChangeName: () => true,
  onValidate: () => true,
};

export default PublishOrSavePanel;
