/*
 * PublishOrSavePanel Messages
 *
 * This contains all the text for the PublishOrSavePanel component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  directions: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.PublishOrSavePanel.directions',
    defaultMessage: 'Save your event template to publish to the marketplace or use or a future event!',
  },
});
