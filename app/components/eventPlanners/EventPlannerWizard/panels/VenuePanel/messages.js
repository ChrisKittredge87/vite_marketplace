/*
 * VenuePanel Messages
 *
 * This contains all the text for the VenuePanel component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  enterTheVenueAddress: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VenuePanel.enterTheVenueAddress',
    defaultMessage: "Enter the venue address",
  },
  locationDirections: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VenuePanel.locationDirections',
    defaultMessage: "Where will your event be hosted?",
  },
  location: {
    id: 'location',
    defaultMessage: 'Location',
  },
  noVenueNeededLabel: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VenuePanel.noVenueNeededLabel',
    defaultMessage: 'I already have a venue ready to host my event',
  },
  radius: {
    id: 'radius',
    defaultMessage: 'Radius (Miles)',
  },
  venueBudget: {
    id: 'venueBudget',
    defaultMessage: 'Venue Budget',
  },
  venueExperience: {
    id: 'venueExperience',
    defaultMessage: 'Venue Experience',
  },
  venueNeededLabel: {
    id: 'app.components.eventPlanners.EventPlannerWizard.panels.VenuePanel.venueNeededLabel',
    defaultMessage: 'I need a venue to host my event',
  },
});
