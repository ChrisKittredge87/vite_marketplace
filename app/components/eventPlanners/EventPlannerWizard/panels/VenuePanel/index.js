/**
*
* VenuePanel
*
*/

import React, { PropTypes } from 'react';

import AutoComplete from 'material-ui/AutoComplete';
import Checkbox from 'material-ui/Checkbox';
import { Circle, GoogleMap, Marker, withGoogleMap } from 'react-google-maps';
import { FormattedMessage } from 'react-intl';
import { fromJS } from 'immutable';
import MoneyIcon from 'material-ui/svg-icons/editor/attach-money';
import PlaceIcon from 'material-ui/svg-icons/maps/place';
import RadiusIcon from 'material-ui/svg-icons/device/gps-fixed';
import RaisedButton from 'material-ui/RaisedButton';
import RestaurantIcon from 'material-ui/svg-icons/maps/restaurant';
import TextField from 'material-ui/TextField';

import CurrencyField from 'components/common/MaskedTextField/CurrencyField';
import NumberField from 'components/common/MaskedTextField/NumberField';
import PlacesAutocomplete from 'components/common/PlacesAutocomplete';
import { ID, DISPLAY_NAME } from 'entities/schemas/serviceProviders/experiences/constants';

import messages from './messages';

const LocationMap = withGoogleMap((props) => {
  const center = { lat: props.lat || 33.7490, lng: props.lng || -84.3880 };
  return (
    <GoogleMap
      ref={props.onMapLoad}
      defaultZoom={8}
      defaultCenter={center}
      onClick={props.onMapClick}
    >
      {
        props.showRadius ? (<Circle
          center={center}
          radius={(props.radius || 50) * 1609.34}
        />) : null
      }
      {
        props.showMarker ? (<Marker position={center} />) : null
      }
    </GoogleMap>
  );
});

class VenuePanel extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = {
      selectedVenueExperience: !!props.venueExperience ? props.venueExperience[DISPLAY_NAME] : '',
    };

    this.handleChangeFilterText = this.handleChangeFilterText.bind(this);
    this.handleFilterVenueExperiences = this.handleFilterVenueExperiences.bind(this);
    this.handleNewExperienceRequest = this.handleNewExperienceRequest.bind(this);
    this.handleVenueNeedsClick = this.handleVenueNeedsClick.bind(this);
    this.handleMapLoaded = this.handleMapLoaded.bind(this);
  }

  /* ----- Lifecycle ----- */

  componentWillMount() {
    const {
      hasSelectedVenueRequirements,
      location,
      needsVenue,
      radius,
      venueBudget,
      venueExperience,
      onValidate,
    } = this.props;
    let isValid;
    if (hasSelectedVenueRequirements && needsVenue) {
      isValid = hasSelectedVenueRequirements && !!location &&
                !!radius && !!venueBudget && venueExperience;
    } else {
      isValid = !!location;
    }
    onValidate(isValid);
  }

  componentWillReceiveProps(nextProps) {
    const {
      hasSelectedVenueRequirements,
      location,
      needsVenue,
      radius,
      venueBudget,
      venueExperience,
      onValidate,
    } = this.props;
    const {
      hasSelectedVenueRequirements: nextHasSelectedVenueRequirements,
      location: nextLocation,
      needsVenue: nextNeedsVenue,
      radius: nextRadius,
      venueBudget: nextVenueBudget,
      venueExperience: nextVenueExperience,
    } = nextProps;
    if (!!nextVenueExperience && venueExperience !== nextVenueExperience) {
      this.setState({ selectedVenueExperience: nextVenueExperience[DISPLAY_NAME] });
    }
    if (hasSelectedVenueRequirements !== nextHasSelectedVenueRequirements ||
        location !== nextLocation || needsVenue !== nextNeedsVenue || radius !== nextRadius ||
        venueBudget !== nextVenueBudget || venueExperience !== nextVenueExperience
    ) {
      let isValid;
      if (nextHasSelectedVenueRequirements && nextNeedsVenue) {
        isValid = nextHasSelectedVenueRequirements && !!nextLocation &&
                  !!nextRadius && !!nextVenueBudget && nextVenueExperience;
      } else {
        isValid = !!nextLocation;
      }
      onValidate(isValid);
    }
  }

  /* ----- END Lifecycle ----- */

  /* ----- Event Binding ----- */

  handleChangeFilterText(searchText) {
    if (searchText !== this.state.selectedVenueExperience) {
      this.setState({ selectedVenueExperience: searchText });
    }
  }

  handleFilterVenueExperiences(searchText, dataSource) {
    return AutoComplete.fuzzyFilter(searchText, dataSource);
  }

  handleMapLoaded(map) {
    this.map = map;
    this.forceUpdate();
  }

  handleNewExperienceRequest(venueExperience) {
    const { onVenueExperienceChange } = this.props;
    onVenueExperienceChange(venueExperience);
    this.venueExperienceAutocomplete.focus();
    this.venueExperienceAutocomplete.close();
  }

  handleVenueNeedsClick(needsVenue) {
    const { onNeedsVenueChange } = this.props;
    return () => onNeedsVenueChange(needsVenue);
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  renderVenueRequired() {
    const {
      venueExperienceOptions,
      // form values
      hasSelectedVenueRequirements,
      includeInHouse,
      includeOutsideVendors,
      location,
      needsVenue,
      radius,
      venueExperience,
      venueBudget,
      // callbacks
      onIncludeInHouseChange,
      onIncludeOutsideVendorsChange,
      onLocationChange,
      onRadiusChange,
      onVenueBudgetChange,
    } = this.props;
    const { selectedVenueExperience } = this.state;
    const venueExperienceDataSourceConfig = { text: DISPLAY_NAME, value: ID };
    return (<div style={{ width: '100%' }}>
      <div style={{ display: 'flex', placeContent: 'center', justifyContent: 'space-between' }}>
        <div style={{ position: 'relative', display: 'inline-block', width: '49%' }}>
          <PlaceIcon style={{ position: 'absolute', right: 5, top: 35, width: 20, height: 25 }} />
          <PlacesAutocomplete
            defaultValue={location.address}
            id={'venue_needed_places_autocomplete'}
            floatingLabelText={<FormattedMessage {...messages.location} />}
            map={this.map}
            onLocationChange={onLocationChange}
            style={{ width: '100%' }}
          />
        </div>
        <div style={{ position: 'relative', display: 'inline-block', width: '49%' }}>
          <RadiusIcon style={{ position: 'absolute', right: 5, top: 35, width: 20, height: 25 }} />
          <NumberField
            floatingLabelText={<FormattedMessage {...messages.radius} />}
            fullWidth
            maxLength="3"
            onChange={onRadiusChange}
            precision={0}
            value={radius}
          />
        </div>
      </div>
      <div style={{ display: 'flex', placeContent: 'center', justifyContent: 'space-between' }}>
        <div style={{ position: 'relative', display: 'inline-block', width: '49%' }}>
          <RestaurantIcon style={{ position: 'absolute', right: 5, top: 35, width: 20, height: 25 }} />
          <AutoComplete
            dataSource={venueExperienceOptions.toJS()}
            dataSourceConfig={venueExperienceDataSourceConfig}
            filter={this.handleFilterVenueExperiences}
            floatingLabelText={<FormattedMessage {...messages.venueExperience} />}
            onNewRequest={this.handleNewExperienceRequest}
            onUpdateInput={this.handleChangeFilterText}
            openOnFocus
            ref={(input) => { this.venueExperienceAutocomplete = input; }}
            searchText={selectedVenueExperience}
            style={{ width: '100%' }}
            textFieldStyle={{ width: '100%' }}
          />
        </div>
        <div style={{ position: 'relative', display: 'inline-block', width: '49%' }}>
          <MoneyIcon style={{ position: 'absolute', right: 5, top: 35, width: 20, height: 25 }} />
          <CurrencyField
            floatingLabelText={<FormattedMessage {...messages.venueBudget} />}
            fullWidth
            maxLength="8"
            onChange={onVenueBudgetChange}
            precision={0}
            value={venueBudget}
          />
        </div>
      </div>
      <div style={{ display: 'flex', placeContent: 'center', paddingTop: 10 }}>
        <span style={{ paddingRight: 12 }}>
          <b>Include Venues that:</b>
        </span>
        <Checkbox
          checked={includeInHouse}
          label={'Have in-house food & beverage'}
          labelStyle={{ fontSize: 12 }}
          onCheck={onIncludeInHouseChange}
          style={{ width: '35%' }}
        />
        <Checkbox
          checked={includeOutsideVendors}
          label={'Allow outside vendors'}
          labelStyle={{ fontSize: 12 }}
          onCheck={onIncludeOutsideVendorsChange}
          style={{ width: '35%' }}
        />
      </div>
      <div style={{ height: 350, margin: '20px 0'}}>
        <LocationMap
          containerElement={
            <div style={{ height: '100%' }} />
          }
          mapElement={
            <div style={{ height: '100%' }} />
          }
          onMapLoad={this.handleMapLoaded}
          onMapClick={_.noop}
          lat={location.lat}
          lng={location.lng}
          radius={radius}
          showRadius
        />
      </div>
      <RaisedButton
        fullWidth
        label={<FormattedMessage {...messages.noVenueNeededLabel} />}
        onClick={this.handleVenueNeedsClick(false)}
        primary
      />
    </div>);
  }

  renderHasVenue() {
    const { location, onLocationChange } = this.props;
    return (<div style={{ width: '100%'}}>
      <PlacesAutocomplete
        defaultValue={!!location ? location.address : ''}
        id={'has_venue_places_autocomplete'}
        floatingLabelText={<FormattedMessage {...messages.enterTheVenueAddress} />}
        fullWidth
        map={this.map}
        onLocationChange={onLocationChange}
      />
      <div style={{ height: 350, margin: '20px 0'}}>
        <LocationMap
          containerElement={<div style={{ height: '100%' }} />}
          lat={!!location ? location.lat : null}
          lng={!!location ? location.lng : null}
          mapElement={<div style={{ height: '100%' }} />}
          onMapLoad={this.handleMapLoaded}
          onMapClick={_.noop}
          showMarker={!!location}
        />
      </div>
      <RaisedButton
        fullWidth
        label={<FormattedMessage {...messages.venueNeededLabel} />}
        onClick={this.handleVenueNeedsClick(true)}
        primary
      />
    </div>);
  }

  render() {
    const { hasSelectedVenueRequirements, needsVenue } = this.props;
    if (hasSelectedVenueRequirements) {
      return needsVenue ? this.renderVenueRequired() : this.renderHasVenue();
    } else {
      return (<div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <p><FormattedMessage {...messages.locationDirections} /></p>
        <RaisedButton
          label={<FormattedMessage {...messages.venueNeededLabel} />}
          onClick={this.handleVenueNeedsClick(true)}
          primary
          style={{ height: 50, width: '80%', margin: '10px 0' }}
        />
        <RaisedButton
          label={<FormattedMessage {...messages.noVenueNeededLabel} />}
          onClick={this.handleVenueNeedsClick(false)}
          primary
          style={{ height: 50, width: '80%', margin: '10px 0' }}
        />
      </div>);
    }
  }

  /* ----- END Rendering ----- */
}

VenuePanel.propTypes = {
  venueExperienceOptions: PropTypes.object,
  // form values
  hasSelectedVenueRequirements: PropTypes.bool,
  includeInHouse: PropTypes.bool,
  includeOutsideVendors: PropTypes.bool,
  location: PropTypes.object,
  needsVenue: PropTypes.bool,
  radius: PropTypes.number,
  venueBudget: PropTypes.number,
  venueExperience: PropTypes.string,
  // callbacks
  onIncludeInHouseChange: PropTypes.func,
  onIncludeOutsideVendorsChange: PropTypes.func,
  onLocationChange: PropTypes.func,
  onNeedsVenueChange: PropTypes.func,
  onRadiusChange: PropTypes.func,
  onVenueExperienceChange: PropTypes.func,
  onVenueBudgetChange: PropTypes.func,
  // Validation
  onValidate: PropTypes.func,
};

VenuePanel.defaultProps = {
  venueExperienceOptions: fromJS([]),
  onIncludeInHouseChange: () => true,
  onIncludeOutsideVendorsChange: () => true,
  onLocationChange: () => true,
  onNeedsVenueChange: () => true,
  onRadiusChange: () => true,
  onVenueExperienceChange: () => true,
  onVenueBudgetChange: () => true,
  onValidate: () => true,
};

export default VenuePanel;
