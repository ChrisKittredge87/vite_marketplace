/*
 * Rfpwizard Messages
 *
 * This contains all the text for the RfpStepper component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  addressLine1: {
    id: 'addressLine1',
    defaultMessage: 'Address Line 1',
  },
  addressLine2: {
    id: 'addressLine2',
    defaultMessage: 'Address Line 2',
  },
  back: {
    id: 'back',
    defaultMessage: 'Back',
  },
  budget: {
    id: 'budget',
    defaultMessage: 'Budget',
  },
  dateBudgetAndGuests: {
    id: 'dateBudgetAndGuests',
    defaultMessage: 'Date, Budget, & Guests',
  },
  city: {
    id: 'city',
    defaultMessage: 'City',
  },
  distance: {
    id: 'distance',
    defaultMessage: 'Distance (Miles)',
  },
  enterEventAddress: {
    id: 'enterEventAddress',
    defaultMessage: 'Enter the address of your event',
  },
  foodAndBeverages: {
    id: 'foodAndBeverages',
    defaultMessage: 'Food & Beverages',
  },
  location: {
    id: 'location',
    defaultMessage: 'Location',
  },
  postalCode: {
    id: 'postalCode',
    defaultMessage: 'Postal Code',
  },
  publishOrSave: {
    id: 'publishOrSave',
    defaultMessage: 'Publish/Save',
  },
  publishToMarketplace: {
    id: 'publishToMarketplace',
    defaultMessage: 'Publish To Marketplace',
  },
  next: {
    id: 'next',
    defaultMessage: 'Next',
  },
  planYourEvent: {
    id: 'planYourEvent',
    defaultMessage: 'Plan Your Event',
  },
  save: {
    id: 'save',
    defaultMessage: 'Save',
  },
  serviceProviders: {
    id: 'serviceProviders',
    defaultMessage: 'Service Providers',
  },
  state: {
    id: 'state',
    defaultMessage: 'State',
  },
  summary: {
    id: 'summary',
    defaultMessage: 'Summary',
  },
  venue: {
    id: 'venue',
    defaultMessage: 'Venue',
  },
  venueAddress: {
    id: 'venueAddress',
    defaultMessage: 'Venue Address',
  },
});
