/**
*
* RfpStepper
*
*/

import React, { PropTypes } from 'react';
import ArrowForwardIcon from 'material-ui/svg-icons/navigation/arrow-forward';
import ExpandTransition from 'material-ui/internal/ExpandTransition';
import FlatButton from 'material-ui/FlatButton';
import { FormattedMessage } from 'react-intl';
import { fromJS, Set as set } from 'immutable';
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import PublishIcon from 'material-ui/svg-icons/editor/publish';
import RaisedButton from 'material-ui/RaisedButton';
import SaveIcon from 'material-ui/svg-icons/content/save';
import { Step, Stepper, StepLabel } from 'material-ui/Stepper';
import moment from 'moment';

import CenteredLoadingSpinner from 'components/common/CenteredLoadingSpinner';
import {
  DateBudgetGuestsPanel,
  VenuePanel,
  VendorsPanel,
  PublishOrSavePanel,
} from 'components/eventPlanners/EventPlannerWizard/panels';
import EventSummaryCard from 'components/eventPlanners/EventSummaryCard';

import messages from './messages';

const STEP_INDEX_DICT = {
  '0': 'dateBudgetGuestsPanel',
  '1': 'venuePanel',
  '2': 'vendorsPanel',
  '3': 'publishOrSavePanel',
};

class RfpStepper extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = {
      dateBudgetGuestsPanel: {
        dontSetBudget: false,
        endDateTime: moment().startOf('day').add(1, 'day').add(20, 'hours'),
        numberOfExpectedGuests: null,
        overallBudget: null,
        startDateTime: moment().startOf('day').add(1, 'day').add(17, 'hours'),
        valid: false,
      },
      venuePanel: {
        hasSelectedVenueRequirements: false,
        includeInHouse: true,
        includeOutsideVendors: true,
        needsVenue: null,
        radius: 50,
        valid: false,
        venueExperience: null,
        venueBudget: null,
      },
      vendorsPanel: {
        budgetPerCaterer: null,
        budgetPerFoodTruck: null,
        catererQuantity: 1,
        foodTruckQuantity: 1,
        foodTruckPaidForByHost: true,
        selectedCuisineTypes: set(),
        valid: false,
        vendorBudget: null,
      },
      publishOrSavePanel: {
        description: '',
        name: '',
        valid: false,
      },
      loading: false,
      stepIndex: 0,
    };

    this.handleGoToStep = this.handleGoToStep.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handlePrev = this.handlePrev.bind(this);
    this.handlePublish = this.handlePublish.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleValidateStep = this.handleValidateStep.bind(this);
    this.nextButtonDisabled = this.nextButtonDisabled.bind(this);
    this.renderEventSummary = this.renderEventSummary.bind(this);
    this.renderStepperButtons = this.renderStepperButtons.bind(this);
    this.renderSelectedPanel = this.renderSelectedPanel.bind(this);

    // DateBudgetGuestsPanel
    this.updateDateBudgetGuestsPanelState = this.updatePanelState.bind(this, STEP_INDEX_DICT[0]);
    this.handleEndDateTimeChange = this.handleEndDateTimeChange.bind(this);
    this.handleStartDateTimeChange = this.handleStartDateTimeChange.bind(this);
    this.handleNumberOfGuestsChange = this.handleNumberOfGuestsChange.bind(this);
    this.handleOverallBudgetChange = this.handleOverallBudgetChange.bind(this);
    this.handleDontSetBudgetChange = this.handleDontSetBudgetChange.bind(this);

    // VenuePanel
    this.updateVenuePanelState = this.updatePanelState.bind(this, STEP_INDEX_DICT[1]);
    this.handleNeedsVenueChange = this.handleNeedsVenueChange.bind(this);
    this.handleIncludeInHouseChange = this.handleIncludeInHouseChange.bind(this);
    this.handleIncludeOutsideVendorsChange = this.handleIncludeOutsideVendorsChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleRadiusChange = this.handleRadiusChange.bind(this);
    this.handleVenueBudgetChange = this.handleVenueBudgetChange.bind(this);
    this.handleVenueExperienceChange = this.handleVenueExperienceChange.bind(this);

    // VendorsPanel
    this.updateVendorsPanelState = this.updatePanelState.bind(this, STEP_INDEX_DICT[2]);
    this.handleAddCuisineType = this.handleAddCuisineType.bind(this);
    this.handleChangeBudgetPerCaterer = this.handleChangeBudgetPerCaterer.bind(this);
    this.handleChangeBudgetPerFoodTruck = this.handleChangeBudgetPerFoodTruck.bind(this);
    this.handleChangeCatererQuantity = this.handleChangeCatererQuantity.bind(this);
    this.handleChangeVendorBudget = this.handleChangeVendorBudget.bind(this);
    this.handleChangeFoodTruckPaidForByHost = this.handleChangeFoodTruckPaidForByHost.bind(this);
    this.handleChangeFoodTruckQuantity = this.handleChangeFoodTruckQuantity.bind(this);
    this.handleRemoveCuisineTypeChip = this.handleRemoveCuisineTypeChip.bind(this);

    // PublishOrSavePanel
    this.updatePublishOrSavePanelState = this.updatePanelState.bind(this, STEP_INDEX_DICT[3]);
    this.handleChangeDescription = this.handleChangeDescription.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
  }

  /* ----- Helper Methods ----- */

  dummyAsync = (cb) => {
    this.setState({ loading: true }, () => {
      this.asyncTimer = setTimeout(cb, 500);
    });
  };

  nextButtonDisabled() {
    const { stepIndex } = this.state;
    const step = STEP_INDEX_DICT[stepIndex];
    return !!step ? !this.state[step].valid : true;
  }

  /* ----- END Helper Methods ----- */

  /* ----- Event Binding ----- */



  handleGoToStep(stepIndex) {
    const { loading } = this.state;
    return () => {
      if (!loading && stepIndex < 4) {
        this.dummyAsync(() => this.setState({ loading: false, stepIndex }));
      }
    }
  }

  handleNext() {
    const { loading, stepIndex } = this.state;
    if (!loading && stepIndex < 4) {
      this.dummyAsync(() => this.setState({ loading: false, stepIndex: stepIndex + 1 }));
    }
  }

  handlePrev() {
    const { loading, stepIndex } = this.state;
    if (!loading && stepIndex > 0) {
      this.dummyAsync(() => this.setState({ loading: false, stepIndex: stepIndex - 1 }));
    }
  }

  handlePublish() {
    const { onPublish } = this.props;
  }

  handleSave() {
    const { onSave } = this.props;
    const {
      dateBudgetGuestsPanel: {
        endDateTime, numberOfExpectedGuests, overallBudget, dontSetBudget, startDateTime,
      },
      venuePanel: {
        includeInHouse, includeOutsideVendors, location, needsVenue, radius, venueBudget, venueExperience
      },
      vendorsPanel: {
        selectedCuisineTypes, vendorBudget, catererQuantity, budgetPerCaterer, foodTruckQuantity, budgetPerFoodTruck, foodTruckPaidForByHost,
      },
      publishOrSavePanel: { name, description },
    } = this.state;
    const eventTemplate = {
      userId: 1,
      companyId: 1,
      dontSetBudget,
      startDate: startDateTime.toDate(),
      endDate: endDateTime.toDate(),
      numberOfExpectedGuests,
      overallBudget,
      lon: location.lng,
      lat: location.lat,
      address: location.address,
      includeInHouseVendors: includeInHouse,
      cuisineTypeIds: selectedCuisineTypes.toJS(),
      includeOutsideVendors,
      needsVenue,
      radius,
      venueBudget,
      venueExperienceId: venueExperience.id,
      vendorBudget,
      catererQuantity, budgetPerCaterer,
      foodTruckQuantity, budgetPerFoodTruck,
      foodTruckPaidForByHost,
      name, description,
    };
    onSave(eventTemplate);
  };

  updatePanelState(panelKey, mergeState) {
    const panelState = _.assign({},
      this.state[panelKey], mergeState
    );
    this.setState({ [panelKey]: panelState });
  }

  /* ----- DateBudgetGuestsPanel Binding ----- */

  handleNumberOfGuestsChange(newNumberOfExpectedGuests) {
    const numberOfExpectedGuests = _.toNumber(newNumberOfExpectedGuests);
    this.updateDateBudgetGuestsPanelState({ numberOfExpectedGuests });
  }

  handleOverallBudgetChange(newOverallBudget) {
    const overallBudget = _.toNumber(newOverallBudget);
    this.updateDateBudgetGuestsPanelState({ overallBudget });
  }

  handleStartDateTimeChange(newStartDateTime) {
    const startDateTime = moment(newStartDateTime);
    this.updateDateBudgetGuestsPanelState({ startDateTime });
  }

  handleEndDateTimeChange(newEndDateTime) {
    const endDateTime = moment(newEndDateTime);
    this.updateDateBudgetGuestsPanelState({ endDateTime });
  }

  handleDontSetBudgetChange(evt, dontSetBudget) {
    this.updateDateBudgetGuestsPanelState({ dontSetBudget });
  }

  /* ----- END DateBudgetGuestsPanel Binding ----- */

  /* ----- VenuePanel Binding ----- */

  handleIncludeInHouseChange(evt, includeInHouse) {
    this.updateVenuePanelState({ includeInHouse });
  }

  handleIncludeOutsideVendorsChange(evt, includeOutsideVendors) {
    this.updateVenuePanelState({ includeOutsideVendors });
  }

  handleLocationChange({ formatted_address, geometry: { location: { lat, lng }}}) {
    const location = { address: formatted_address, lat: lat(), lng: lng() };
    this.updateVenuePanelState({ location });
  }

  handleNeedsVenueChange(needsVenue) {
    this.updateVenuePanelState({
      hasSelectedVenueRequirements: true,
      hasSelectedVenueRequirements: true,
      includeInHouse: true,
      includeOutsideVendors: true,
      location: needsVenue ? {
        lat: 33.753746,
        lng: -84.386330,
        address: 'Atlanta, GA, United States',
      } : null,
      needsVenue,
      radius: 50,
      venueExperience: null,
      venueBudget: null,
    });
  }

  handleRadiusChange(radius) {
    this.updateVenuePanelState({ radius });
  }

  handleVenueBudgetChange(newVenueBudget) {
    const venueBudget = _.toNumber(newVenueBudget);
    this.updateVenuePanelState({ venueBudget });
  }

  handleVenueExperienceChange(venueExperience) {
    this.updateVenuePanelState({ venueExperience });
  }

  /* ----- END VenuePanel Binding ----- */

  /* ----- VendorsPanel Binding ----- */

  handleAddCuisineType(id) {
    const selectedCuisineTypes = this.state.vendorsPanel.selectedCuisineTypes.add(id);
    this.updateVendorsPanelState({ selectedCuisineTypes });
  }

  handleChangeBudgetPerCaterer(newBudgetPerCaterer) {
    const budgetPerCaterer = _.toNumber(newBudgetPerCaterer);
    this.updateVendorsPanelState({ budgetPerCaterer });
  }

  handleChangeBudgetPerFoodTruck(newBudgetPerFoodTruck) {
    const budgetPerFoodTruck = _.toNumber(newBudgetPerFoodTruck);
    this.updateVendorsPanelState({ budgetPerFoodTruck });
  }

  handleChangeCatererQuantity(newCatererQuantity) {
    const catererQuantity = _.toNumber(newCatererQuantity);
    this.updateVendorsPanelState({ catererQuantity });
  }

  handleChangeVendorBudget(newVendorBudget) {
    const vendorBudget = _.toNumber(newVendorBudget);
    this.updateVendorsPanelState({ vendorBudget });
  }

  handleChangeFoodTruckPaidForByHost(evt, foodTruckPaidForByHost) {
    this.updateVendorsPanelState({ foodTruckPaidForByHost });
  }

  handleChangeFoodTruckQuantity(newFoodTruckQuantity) {
    const foodTruckQuantity = _.toNumber(newFoodTruckQuantity);
    this.updateVendorsPanelState({ foodTruckQuantity });
  }

  handleRemoveCuisineTypeChip(id) {
    const selectedCuisineTypes = this.state.vendorsPanel.selectedCuisineTypes.remove(id);
    this.updateVendorsPanelState({ selectedCuisineTypes });
  }

  /* ----- END VendorsPanel Binding ----- */

  /* ----- PublishOrSavePanel Binding ----- */

  handleChangeDescription(evt, description) {
    this.updatePublishOrSavePanelState({ description });
  }

  handleChangeName(evt, name) {
    this.updatePublishOrSavePanelState({ name });
  }

  /* ----- END PublishOrSavePanel Binding ----- */

  handleValidateStep(panel) {
    return (isValid) => {
      const newPanelState = _.assign({}, this.state[`${panel}`], { valid: isValid });
      this.setState({ [`${panel}`]: newPanelState });
    };
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  renderEventSummary() {
    const { stepIndex } = this.state;
    const {
      dateBudgetGuestsPanel: {
        endDateTime, numberOfExpectedGuests, overallBudget, dontSetBudget, startDateTime,
      },
      venuePanel: { location , radius, venueBudget, valid: venueValid },
      vendorsPanel: { vendorBudget, valid: vendorsValid },
      publishOrSavePanel: { name },
    } = this.state;
    const estimatedSpend = venueBudget + vendorBudget;
    return (<div style={{ maxWidth: 700, margin: '20px auto 30px auto' }}>
      <EventSummaryCard
        endDateTime={endDateTime}
        estimatedSpend={estimatedSpend}
        hideBudget={dontSetBudget}
        location={!!location ? location.address : null}
        name={name}
        numberOfExpectedGuests={numberOfExpectedGuests}
        numberOfMatchingVenues={57}
        numberOfMatchingVendors={34}
        onClickEditBudget={this.handleGoToStep(0)}
        onClickEditEventDate={this.handleGoToStep(0)}
        onClickEditNumberOfGuests={this.handleGoToStep(0)}
        onClickEditVendors={this.handleGoToStep(2)}
        onClickEditVenue={this.handleGoToStep(1)}
        overallBudget={overallBudget}
        radius={radius}
        startDateTime={startDateTime}
        showMatchingVendors={vendorsValid}
        showMatchingVenues={venueValid}
      />
    </div>);
  }

  renderStepperButtons() {
    const { hasSelectedVenueRequirements, stepIndex } = this.state;
    return (<div style={{ marginTop: 24, marginBottom: 12, textAlign: 'right' }}>
      {
        stepIndex > 0 ?
        <FlatButton
          label={<FormattedMessage {...messages.back} />}
          disabled={stepIndex === 0}
          onClick={this.handlePrev}
          style={{ marginRight: 12 }}
        /> : null
      }
      {
        stepIndex < 3 ?
          <RaisedButton
            disabled={this.nextButtonDisabled()}
            label={<FormattedMessage {...messages.next}/>}
            labelPosition="before"
            primary
            onClick={this.handleNext}
            style={{ marginRight: 12 }}
          /> :
          <RaisedButton
            disabled={this.nextButtonDisabled()}
            icon={<SaveIcon />}
            label={<FormattedMessage {...messages.save} />}
            labelPosition="before"
            primary
            onClick={this.handleSave}
            style={{ marginRight: 12 }}
          />
      }
      {
        stepIndex === 3 ?
          <RaisedButton
            disabled={this.nextButtonDisabled()}
            label={<FormattedMessage {...messages.publishToMarketplace} />}
            labelPosition="before"
            onClick={this.handlePublish}
            icon={<PublishIcon />}
          /> : null
      }
    </div>);
  }

  renderSelectedPanel() {
    const {
      stepIndex,
      dateBudgetGuestsPanel,
      venuePanel,
      vendorsPanel,
    } = this.state;
    const {
      cuisineTypeOptions,
      venueExperienceOptions,
    } = this.props;
    let selectedPanel;
    const onValidate = this.handleValidateStep(STEP_INDEX_DICT[stepIndex]);
    switch(stepIndex) {
      case 0:
        selectedPanel = (
          <DateBudgetGuestsPanel
            {...dateBudgetGuestsPanel}
            onNumberOfExpectedGuestsChange={this.handleNumberOfGuestsChange}
            onOverallBudgetChange={this.handleOverallBudgetChange}
            onStartDateTimeChange={this.handleStartDateTimeChange}
            onEndDateTimeChange={this.handleEndDateTimeChange}
            onDontSetBudgetChange={this.handleDontSetBudgetChange}
            onValidate={onValidate}
          />
        );
        break;
      case 1:
        selectedPanel = (
          <VenuePanel
            {...venuePanel}
            venueExperienceOptions={venueExperienceOptions}
            onIncludeInHouseChange={this.handleIncludeInHouseChange}
            onIncludeOutsideVendorsChange={this.handleIncludeOutsideVendorsChange}
            onLocationChange={this.handleLocationChange}
            onNeedsVenueChange={this.handleNeedsVenueChange}
            onRadiusChange={this.handleRadiusChange}
            onVenueBudgetChange={this.handleVenueBudgetChange}
            onVenueExperienceChange={this.handleVenueExperienceChange}
            onValidate={onValidate}
          />
        );
        break;
      case 2:
        selectedPanel = (
          <VendorsPanel
            {...vendorsPanel}
            cuisineTypeOptions={cuisineTypeOptions}
            includeOutsideVendors={venuePanel.includeOutsideVendors}
            onAddCuisineType={this.handleAddCuisineType}
            onChangeBudgetPerCaterer={this.handleChangeBudgetPerCaterer}
            onChangeBudgetPerFoodTruck={this.handleChangeBudgetPerFoodTruck}
            onChangeCatererQuantity={this.handleChangeCatererQuantity}
            onChangeVendorBudget={this.handleChangeVendorBudget}
            onChangeFoodTruckPaidForByHost={this.handleChangeFoodTruckPaidForByHost}
            onChangeFoodTruckQuantity={this.handleChangeFoodTruckQuantity}
            onRemoveCuisineType={this.handleRemoveCuisineTypeChip}
            onValidate={onValidate}
          />
        );
        break;
      case 3:
        const { publishOrSavePanel: { description, name }} = this.state;
        selectedPanel = (
          <PublishOrSavePanel
            description={description}
            name={name}
            onChangeDescription={this.handleChangeDescription}
            onChangeName={this.handleChangeName}
            onValidate={onValidate}
          />
        );
        break;
      default:
        selectedPanel = (
          <DateBudgetGuestsPanel
            {...dateBudgetGuestsPanel}
            onNumberOfExpectedGuestsChange={this.handleNumberOfGuestsChange}
            onOverallBudgetChange={this.handleOverallBudgetChange}
            onStartDateTimeChange={this.handleStartDateTimeChange}
            onEndDateTimeChange={this.handleEndDateTimeChange}
            onDontSetBudgetChange={this.handleDontSetBudgetChange}
            onValidate={onValidate}
          />
        );
    }
    return (<div style={{ display: 'flex', placeContent: 'center' }}>
      <div style={{ width: '100%' }}>
        {selectedPanel}
      </div>
    </div>)
  }

  render() {
    const {
      dateBudgetGuestsPanel,
      venuePanel,
      loading,
      stepIndex,
    } = this.state;
    return (
      <div>
        <div style={{ maxWidth: 700, margin: 'auto' }}>
          <h1 style={{ textAlign: 'center', margin: '15px 0 0 0' }}><FormattedMessage {...messages.planYourEvent} /></h1>
          <Stepper activeStep={stepIndex} connector={<ArrowForwardIcon />}>
            <Step>
              <StepLabel>
                <FormattedMessage {...messages.dateBudgetAndGuests} />
              </StepLabel>
            </Step>
            <Step>
              <StepLabel>
                <FormattedMessage {...messages.venue} />
              </StepLabel>
            </Step>
            <Step>
              <StepLabel>
                <FormattedMessage {...messages.foodAndBeverages} />
              </StepLabel>
            </Step>
            <Step>
              <StepLabel>
                <FormattedMessage {...messages.publishOrSave} />
              </StepLabel>
            </Step>
          </Stepper>
          <ExpandTransition loading={loading} open={true}>
            {this.renderSelectedPanel()}
          </ExpandTransition>
          {this.renderStepperButtons()}
        </div>
        {this.renderEventSummary()}
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

RfpStepper.propTypes = {
  cuisineTypes: PropTypes.object,
  venueExperienceOptions: PropTypes.object,
  // callbacks
  onPublish: PropTypes.func,
  onSave: PropTypes.func,
};

RfpStepper.defaultProps = {
  cuisineTypeOptions: fromJS([]),
  venueExperienceOptions: fromJS([]),
  // callbacks
  onPublish: () => true,
  onSave: () => true,
};

export default RfpStepper;
