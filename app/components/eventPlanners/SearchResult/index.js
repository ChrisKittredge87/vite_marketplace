/**
*
* SearchResult
*
*/

import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { fromJS } from 'immutable';
import styled from 'styled-components';
import ImgSlider from 'components/common/ImgSlider';
import Rater from 'react-rater';

import { path as serviceProviderProfilePath } from 'containers/serviceProviders/Profile/routes';
import { ADDRESS, AVG_RATING, COMPANY_ID, LOCALITY, ID, IMAGES, NAME, REGION } from 'entities/schemas/serviceProviders/constants';
import { XS } from 'globalConstants';

const FloatingDiv = styled.div`
  box-shadow: ${({ highlight }) => highlight ? '9px 9px 5px lightblue' : '6px 5px 5px darkgray'};
  height: 300px;
  @media(max-width: ${XS}px) {
    height: 200px;
  }
`;

const SpaceBetweenFlex = styled.div`
  display: flex;
  justify-content: space-between;
`;

class SearchResult extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    serviceProvider: PropTypes.object,
    highlight: PropTypes.bool,
  };

  static defaultProps = {
    serviceProvider: fromJS({}),
  };

  render() {
    const { highlight, serviceProvider } = this.props;
    return (
      <div>
        <FloatingDiv highlight={highlight}>
          <Link
            to={`/${serviceProviderProfilePath}/${serviceProvider.get(COMPANY_ID)}?serviceProviderId=${serviceProvider.get(ID)}`}
          >
            <ImgSlider
              height={window.innerWidth > XS ? '300px' : '200px'}
              imgSources={serviceProvider.get(IMAGES)}
            />
          </Link>
        </FloatingDiv>
        <SpaceBetweenFlex>
          <h3>{serviceProvider.get(NAME)}</h3>
          <div style={{ display: 'flex', alignItems: 'baseline' }}>
            <h3 style={{ marginRight: 15 }}>
              {`${serviceProvider.getIn([ADDRESS, LOCALITY])}, ${serviceProvider.getIn([ADDRESS, REGION])}`}
            </h3>
            <Rater style={{ paddingRight: 7 }} total={5} rating={serviceProvider.get(AVG_RATING)} interactive={false} />
          </div>
        </SpaceBetweenFlex>
      </div>
    );
  }
}

export default SearchResult;
