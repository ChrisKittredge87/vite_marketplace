/*
 * SearchResult Messages
 *
 * This contains all the text for the SearchResult component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({});
