/* ----- Responsive Breakpoints ----- */
export const XXS = 320;
export const XS = 480;
export const SM = 768;
export const MD = 1024;
export const LG = 1224;
export const XL = 1824;

/* ----- Styling Colors ----- */
export const FACEBOOK_BLUE = '#3B5998';
export const FACEBOOK_BLUE_HOVER = '#4e69a2';

/* ----- Facebook Login ----- */
export const FB_LOGIN_STATUS_CONNECTED = 'connected';

/* ----- OAUTH ----- */
export const OAUTH_ACCESS_TOKEN = 'oauthAccessToken';
