/*
 *
 * ServiceProviderMessageDialog
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { SpaceBetweenFlex } from 'styled/common';

import { changeDialogOpen } from './actions';
import { makeSelectDialogOpen } from './selectors';
import messages from './messages';

export class ServiceProviderMessageDialog extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    dialogOpen: PropTypes.bool,
    dialogWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onChangeDialogOpen: PropTypes.func,
  };

  static defaultProps = {
    dialogWidth: '75%',
  };

  constructor(props) {
    super(props);

    this.toggleDialogOpen = this.toggleDialogOpen.bind(this);
  }

  /* ----- Event Binding ----- */

  toggleDialogOpen() {
    const { dialogOpen, onChangeDialogOpen } = this.props;
    onChangeDialogOpen(!dialogOpen);
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  renderDialogContent() {
    return (<div style={{ padding: '20px 0' }}>
      <TextField
        autoFocus
        hintText={<FormattedMessage {...messages.startADialogElipses} />}
        fullWidth
        multiLine
        rows={10}
        rowsMax={15}
      />
    </div>);
  }

  render() {
    const { dialogOpen, dialogWidth } = this.props;
    const dialogHeader = (
      <SpaceBetweenFlex style={{ padding: '5px 20px' }}>
        <h3 style={{ margin: 0 }}><FormattedMessage {...messages.message} /></h3>
        <IconButton onClick={this.toggleDialogOpen}><CloseIcon /></IconButton>
      </SpaceBetweenFlex>
    );
    const actions = [
      <FlatButton
        label={<FormattedMessage {...messages.cancel} />}
        onClick={this.toggleDialogOpen}
      />,
      <FlatButton
        primary
        label={<FormattedMessage {...messages.send} />}
        onClick={this.toggleDialogOpen}
      />,
    ];
    return (
      <div>
        <RaisedButton
          label={<FormattedMessage {...messages.sendAMessage} />}
          onClick={this.toggleDialogOpen}
          primary
        />
        <Dialog
          actions={actions}
          autoScrollBodyContent
          contentStyle={{ maxWidth: 'none', width: dialogWidth }}
          modal
          open={dialogOpen}
          onRequestClose={this.toggleDialogOpen}
          title={dialogHeader}
        >
          {this.renderDialogContent()}
        </Dialog>
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

const mapStateToProps = createStructuredSelector({
  dialogOpen: makeSelectDialogOpen(),
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeDialogOpen: (val) => dispatch(changeDialogOpen(val)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceProviderMessageDialog);
