/*
 * ServiceProviderMessageDialog Messages
 *
 * This contains all the text for the ServiceProviderMessageDialog component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  cancel: {
    id: 'cancel',
    defaultMessage: 'Cancel',
  },
  message: {
    id: 'message',
    defaultMessage: 'Message',
  },
  send: {
    id: 'send',
    defaultMessage: 'Send',
  },
  sendAMessage: {
    id: 'sendAMessage',
    defaultMessage: 'Send a message',
  },
  startADialogElipses: {
    id: 'startADialogElipses',
    defaultMessage: 'Start a dialog...',
  },
});
