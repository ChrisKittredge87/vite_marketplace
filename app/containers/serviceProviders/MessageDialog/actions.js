/*
 *
 * ServiceProviderMessageDialog actions
 *
 */

import {
  CHANGE_DIALOG_OPEN,
} from './constants';

export function changeDialogOpen(val) {
  return {
    type: CHANGE_DIALOG_OPEN,
    val,
  };
}
