import { createSelector } from 'reselect';

import { DOMAIN } from './reducer';
import {
  DIALOG_OPEN,
} from './constants';

/**
 * Direct selector to the serviceProviderMessageDialog state domain
 */
const selectMessageDialogDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by ServiceProviderMessageDialog
 */

const makeSelectMessageDialog = () => createSelector(
  selectMessageDialogDomain(),
  (substate) => substate.toJS()
);

const makeSelectDialogOpen = () => createSelector(
  selectMessageDialogDomain(),
  (substate) => substate.get(DIALOG_OPEN)
);

export default makeSelectMessageDialog;
export {
  selectMessageDialogDomain,
  makeSelectDialogOpen,
};
