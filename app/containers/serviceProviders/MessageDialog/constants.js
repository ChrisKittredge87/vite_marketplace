/*
 *
 * ServiceProviderMessageDialog constants
 *
 */

/* Actions */
export const CHANGE_DIALOG_OPEN = 'app/containers/promoterPortal/serviceProviders/MessageDialog/CHANGE_DIALOG_OPEN';

/* State Keys */
export const DIALOG_OPEN = 'dialogOpen';
