
import { fromJS } from 'immutable';
import serviceProviderMessageDialogReducer from '../reducer';

describe('serviceProviderMessageDialogReducer', () => {
  it('returns the initial state', () => {
    expect(serviceProviderMessageDialogReducer(undefined, {})).toEqual(fromJS({}));
  });
});
