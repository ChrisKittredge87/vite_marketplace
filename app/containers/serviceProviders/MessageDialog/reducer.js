/*
 *
 * ServiceProviderMessageDialog reducer
 *
 */

import { fromJS } from 'immutable';
import {
  /* Actions */
  CHANGE_DIALOG_OPEN,
  /* State Keys */
  DIALOG_OPEN,
} from './constants';

const initialState = fromJS({
  [DIALOG_OPEN]: false,
});

function messageDialogReducer(state = initialState, action) {
  const { val } = action;
  switch (action.type) {
    case CHANGE_DIALOG_OPEN:
      return state.set(DIALOG_OPEN, val);
    default:
      return state;
  }
}

export default messageDialogReducer;

export const DOMAIN = 'serviceProviderMessageDialog';
