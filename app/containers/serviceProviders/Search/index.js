/*
 *
 * Search
 *
 */

import React, { PropTypes } from 'react';
import { fromJS } from 'immutable';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Helmet from 'react-helmet';
import Measure from 'react-measure';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import FilterIcon from 'material-ui/svg-icons/content/filter-list';
import RaisedButton from 'material-ui/RaisedButton';
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import { AutoSizer, List } from 'react-virtualized';
import _ from 'lodash';

import { XS, SM, MD, XL } from 'globalConstants';
import { path as searchPath } from 'containers/serviceProviders/Search/routes';
import SearchResult from 'components/eventPlanners/SearchResult';
import { ADDRESS, LAT, LON, NAME } from 'entities/schemas/serviceProviders/constants';
import {
  changeFilterDrawerOpen,
  changeHighlightedServiceProvider,
  changeSearchText,
  loadServiceProviders,
} from './actions';
import {
  makeSelectFilterDrawerOpen,
  makeSelectHighlightedServiceProvider,
  makeSelectSearchText,
  makeSelectServiceProviders,
} from './selectors';
import messages from './messages';
import { MapContainer, RespDiv, SearchResultsContainer } from './styled';
import FilterDrawer from './FilterDrawer';

const TheGoogleMap = withGoogleMap((props) => (
  <GoogleMap
    ref={props.onMapLoad}
    defaultZoom={12}
    defaultCenter={{ lat: 33.7490, lng: -84.3880 }}
    onClick={props.onMapClick}
  >
    {props.markers.map((marker, idx) => (
      <Marker
        key={`marker_${idx}`}
        {...marker}
        onRightClick={() => props.onMarkerRightClick(marker)}
        onMouseOver={() => props.onMarkerMouseOver(idx)}
        onFocus={() => props.onMarkerMouseOver(idx)}
        onMouseOut={() => props.onMarkerMouseOut(null)}
        onBlur={() => props.onMarkerMouseOut(null)}
      />
    ))}
  </GoogleMap>
));

export class Search extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.handleApplyFilters = this.handleApplyFilters.bind(this);
    this.handleChangeFilterDrawerOpen = this.handleChangeFilterDrawerOpen.bind(this);
    this.handleChangeSearchText = this.handleChangeSearchText.bind(this);
    this.handleMapLoaded = this.handleMapLoaded.bind(this);
    this.renderFilterDrawer = this.renderFilterDrawer.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.setListRef = this.setListRef.bind(this);
  }

  /* ----- React Lifecycle ----- */

  componentWillMount() {
    const { dispatchLoadServiceProviders } = this.props;
    dispatchLoadServiceProviders();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.children) {
      return;
    }
    const {
      dispatchLoadServiceProviders,
      highlightedServiceProvider: prevHighlightedServiceProvider,
      searchText: prevSearchText,
      serviceProviders: prevServiceProviders,
    } = this.props;
    const { highlightedServiceProvider, searchText, serviceProviders } = nextProps;
    if (searchText !== prevSearchText) {
      dispatchLoadServiceProviders();
    }
    if ((!prevServiceProviders.equals(serviceProviders) || prevHighlightedServiceProvider !== highlightedServiceProvider) && this.searchResultList) {
      this.searchResultList.forceUpdateGrid();
    }
  }

  /* ----- END React Lifecycle ----- */

  /* ----- Helpers ----- */

  setListRef(list) {
    if (!this.searchResultList) {
      this.searchResultList = list;
    }
  }

  rowCalc(row, max) {
    const cols = this.numberOfCols();
    const baseCols = _.range(cols);
    const colsByRow = _.map(baseCols, (col) => (cols * row) + col);
    return _.filter(colsByRow, (idx) => idx < max);
  }

  numberOfCols() {
    switch (true) {
      case window.innerWidth < XS:
        return 1;
      case window.innerWidth < SM:
        return 2;
      case window.innerWidth < XL:
        return 2;
      default:
        return 3;
    }
  }

  /* ----- END Helpers ----- */

  /* ----- Event Binding ------ */

  handleApplyFilters() {
    const { dispatchLoadServiceProviders } = this.props;
    if (window.innerWidth <= SM) {
      this.handleChangeFilterDrawerOpen();
    }
    dispatchLoadServiceProviders();
  }

  handleChangeFilterDrawerOpen() {
    const { filterDrawerOpen, onChangeFilterDrawerOpen } = this.props;
    onChangeFilterDrawerOpen(!filterDrawerOpen);
  }

  handleChangeSearchText(evt) {
    this.props.onChangeSearchText(evt.target.value);
  }

  handleMapLoaded(map) {
    this.map = map;
  }

  /* ----- END Event Binding ------ */

  /* ----- Rendering ----- */

  renderFilterDrawer() {
    const { filterDrawerOpen } = this.props;
    const getWidth = () => {
      switch (true) {
        case window.innerWidth <= XS:
          return '100%';
        case window.innerWidth <= SM:
          return '50%';
        default:
          return '35%';
      }
    };
    return (<FilterDrawer
      queryParams={this.props.location.query}
      onApplyFilters={this.handleApplyFilters}
      onCloseButtonTouchTap={this.handleChangeFilterDrawerOpen}
      open={filterDrawerOpen}
      width={filterDrawerOpen ? getWidth() : 0}
    />);
  }

  renderMap() {
    const { serviceProviders, onChangeHighlightedServiceProvider } = this.props;
    const markers = serviceProviders.map((serviceProvider) => ({
      position: {
        lat: parseFloat(serviceProvider.getIn([ADDRESS, LAT])),
        lng: parseFloat(serviceProvider.getIn([ADDRESS, LON])),
      },
      key: serviceProvider.get(NAME),
      defaultAnimation: 2,
    })).toArray();
    return (<MapContainer>
      <TheGoogleMap
        containerElement={
          <div style={{ height: '100%' }} />
        }
        mapElement={
          <div style={{ height: '100%' }} />
        }
        onMapLoad={this.handleMapLoaded}
        onMapClick={_.noop}
        markers={markers}
        onMarkerRightClick={_.noop}
        onMarkerMouseOver={onChangeHighlightedServiceProvider}
        onMarkerMouseOut={onChangeHighlightedServiceProvider}
      />
    </MapContainer>);
  }

  renderRow({ index, style }) {
    const { serviceProviders, highlightedServiceProvider } = this.props;
    return (
      <div
        key={`service_provider_sr_${index}`}
        style={style}
      >
        <div style={{ display: 'flex' }}>
          {_.map(this.rowCalc(index, serviceProviders.size), (idx) =>
            (<RespDiv cols={this.numberOfCols()} key={`search_result_${idx}`}>
              <SearchResult
                serviceProvider={serviceProviders.get(idx)}
                highlight={highlightedServiceProvider === idx}
              />
            </RespDiv>))
          }
        </div>
      </div>);
  }

  renderSearchBar() {
    const searchBarStyle = {
      alignItems: 'flex-end',
      border: '1px solid lightgray',
      display: 'flex',
      justifyContent: 'center',
      margin: '10px 10px',
      paddingBottom: 10,
    };
    return (<div style={searchBarStyle}>
      <TextField
        floatingLabelText={<FormattedMessage {...messages.where} />}
        value="Atlanta, GA" disabled style={{ margin: '0 10px 0 0' }}
      />
      <DatePicker
        autoOk
        locale="en-US"
        floatingLabelText={<FormattedMessage {...messages.when} />}
        textFieldStyle={{ margin: '0 10px' }}
      />
      <TextField
        floatingLabelText={<FormattedMessage {...messages.guests} />}
        style={{ margin: '0 10px' }}
      />
      <Link to={searchPath}>
        <RaisedButton label="Search" primary style={{ margin: '10px 0' }} />
      </Link>
      <IconButton
        style={{
          position: 'absolute',
          right: window.innerWidth > SM ? 20 : 10,
          top: 100,
        }}
        onClick={this.handleChangeFilterDrawerOpen}
      >
        <FilterIcon style={{ height: 20 }} />
      </IconButton>
    </div>);
  }

  render() {
    const { children, serviceProviders } = this.props;
    return children || (
      <Measure>
        {() =>
          <div>
            <Helmet
              title="Search"
              meta={[
                { name: 'description', content: 'Description of Search' },
              ]}
            />
            <div style={{ marginTop: 15, height: '100%' }}>
              {this.renderSearchBar()}
              <div style={{ display: 'flex' }}>
                <SearchResultsContainer>
                  <AutoSizer>
                    {({ height, width }) => (
                      <List
                        height={height}
                        ref={this.setListRef}
                        rowCount={_.ceil(serviceProviders.size / this.numberOfCols())}
                        rowHeight={window.innerWidth >= XS ? 360 : 300}
                        rowRenderer={this.renderRow}
                        width={width}
                      />)}
                  </AutoSizer>
                </SearchResultsContainer>
                {window.innerWidth >= MD ? this.renderMap() : null}
              </div>
            </div>
            {this.renderFilterDrawer()}
          </div>
        }
      </Measure>
    );
  }

  /* ----- END Rendering ----- */
}

Search.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]),
  dispatchLoadServiceProviders: PropTypes.func,
  filterDrawerOpen: PropTypes.bool,
  highlightedServiceProvider: PropTypes.number,
  location: PropTypes.object,
  onChangeFilterDrawerOpen: PropTypes.func,
  onChangeHighlightedServiceProvider: PropTypes.func,
  onChangeSearchText: PropTypes.func,
  searchText: PropTypes.string,
  serviceProviders: PropTypes.object,
};

Search.defaultProps = {
  serviceProviders: fromJS({}),
};

const mapStateToProps = createStructuredSelector({
  filterDrawerOpen: makeSelectFilterDrawerOpen(),
  highlightedServiceProvider: makeSelectHighlightedServiceProvider(),
  searchText: makeSelectSearchText(),
  serviceProviders: makeSelectServiceProviders(),
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeFilterDrawerOpen: (open) => dispatch(changeFilterDrawerOpen(open)),
    onChangeHighlightedServiceProvider: (index) => dispatch(changeHighlightedServiceProvider(index)),
    onChangeSearchText: (searchText) => dispatch(changeSearchText(searchText)),
    dispatchLoadServiceProviders: _.debounce(() => dispatch(loadServiceProviders()), 150),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
