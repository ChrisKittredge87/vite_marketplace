/*
 *
 * Search actions
 *
 */

import { createSagaActions } from 'utils/sagas';
import { list as schema } from 'entities/schemas/serviceProviders';
import {
  CHANGE_FILTER_DRAWER_OPEN,
  CHANGE_HIGHLIGHTED_SERVICE_PROVIDER,
  CHANGE_SEARCH_TEXT,
  LOAD_SERVICE_PROVIDERS,
  SERVICE_PROVIDERS_LOADED,
  SERVICE_PROVIDERS_LOADING_ERROR,
} from './constants';

export function changeFilterDrawerOpen(val) {
  return {
    type: CHANGE_FILTER_DRAWER_OPEN,
    val,
  };
}

export function changeHighlightedServiceProvider(val) {
  return {
    type: CHANGE_HIGHLIGHTED_SERVICE_PROVIDER,
    val,
  };
}

export function changeSearchText(val = '') {
  return {
    type: CHANGE_SEARCH_TEXT,
    val,
  };
}

export const {
  loadEntity: loadServiceProviders,
  entityLoaded: serviceProvidersLoaded,
  entityLoadingError: serviceProvidersLoadingError,
} = createSagaActions({
  loadAction: LOAD_SERVICE_PROVIDERS,
  entityLoadedAction: SERVICE_PROVIDERS_LOADED,
  entityLoadingErrorAction: SERVICE_PROVIDERS_LOADING_ERROR,
  schema,
});
