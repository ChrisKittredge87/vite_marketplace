
import {
  changeSearchText,
} from '../actions';
import {
  CHANGE_SEARCH_TEXT,
} from '../constants';

describe('Search actions', () => {
  describe('CHANGE_SEARCH_TEXT Action', () => {
    it('has a type of CHANGE_SEARCH_TEXT', () => {
      const expected = {
        type: CHANGE_SEARCH_TEXT,
      };
      expect(changeSearchText()).toEqual(expected);
    });
  });
});
