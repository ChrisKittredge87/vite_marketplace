/*
 * Search Messages
 *
 * This contains all the text for the Search component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  guests: {
    id: 'guests',
    defaultMessage: 'Guests',
  },
  search: {
    id: 'search',
    defaultMessage: 'Search',
  },
  when: {
    id: 'when',
    defaultMessage: 'When',
  },
  where: {
    id: 'where',
    defaultMessage: 'Where',
  },
});
