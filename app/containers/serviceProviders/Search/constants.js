/*
 *
 * Search constants
 *
 */

/* Actions */
export const CHANGE_FILTER_DRAWER_OPEN = 'app/promoterPortal/serviceProviders/Search/CHANGE_FILTER_DRAWER_OPEN';
export const CHANGE_HIGHLIGHTED_SERVICE_PROVIDER = 'app/promoterPortal/serviceProviders/Search/CHANGE_HIGHLIGHTED_SERVICE_PROVIDER';
export const CHANGE_SEARCH_TEXT = 'app/promoterPortal/serviceProviders/Search/CHANGE_SEARCH_TEXT';
export const LOAD_SERVICE_PROVIDERS = 'app/promoterPortal/serviceProviders/Search/LOAD_SERVICE_PROVIDERS';
export const SERVICE_PROVIDERS_LOADED = 'app/promoterPortal/serviceProviders/Search/SERVICE_PROVIDERS_LOADED';
export const SERVICE_PROVIDERS_LOADING_ERROR = 'app/promoterPortal/serviceProviders/Search/SERVICE_PROVIDERS_LOADING_ERROR';

/* State Keys */
export const ERROR = 'error';
export const FILTER_DRAWER_OPEN = 'filterDrawerOpen';
export const HIGHLIGHTED_SERVICE_PROVIDER = 'highlightedServiceProvider';
export const LOADING = 'loading';
export const SEARCH_TEXT = 'searchText';
export const SERVICE_PROVIDER_IDS = 'serviceProviderIds';
