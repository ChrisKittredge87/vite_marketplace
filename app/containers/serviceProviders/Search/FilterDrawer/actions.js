/*
 *
 * SearchFilterDrawer actions
 *
 */
import { list as styleListSchema } from 'entities/schemas/serviceProviders/styles';
import { list as typeListSchema } from 'entities/schemas/serviceProviders/types';
import { createSagaActions } from 'utils/sagas';

import {
  ADD_SERVICE_PROVIDER_STYLE_ID,
  ADD_SERVICE_PROVIDER_TYPE_ID,
  REMOVE_SERVICE_PROVIDER_STYLE_ID,
  REMOVE_SERVICE_PROVIDER_TYPE_ID,
  CHANGE_APPLY_FILTERS,
  CHANGE_BUDGET_MAX,
  CHANGE_BUDGET_MIN,
  CHANGE_SERVING_CAPACITY_MAX,
  CHANGE_SERVING_CAPACITY_MIN,
  LOAD_STYLES,
  STYLES_LOADED,
  STYLES_LOADING_ERROR,
  LOAD_TYPES,
  TYPES_LOADED,
  TYPES_LOADING_ERROR,
  SET_KEY_VAL,
} from './constants';

export function setKeyVal(key, val) {
  return { type: SET_KEY_VAL, key, val };
}

const changeVal = (type) => (val) => ({ type, val });

export const addServiceProviderStyleId = changeVal(ADD_SERVICE_PROVIDER_STYLE_ID);
export const addServiceProviderTypeId = changeVal(ADD_SERVICE_PROVIDER_TYPE_ID);
export const removeServiceProviderStyleId = changeVal(REMOVE_SERVICE_PROVIDER_STYLE_ID);
export const removeServiceProviderTypeId = changeVal(REMOVE_SERVICE_PROVIDER_TYPE_ID);
export const changeApplyFilters = changeVal(CHANGE_APPLY_FILTERS);
export const changeBudgetMax = changeVal(CHANGE_BUDGET_MAX);
export const changeBudgetMin = changeVal(CHANGE_BUDGET_MIN);
export const changeServingCapacityMax = changeVal(CHANGE_SERVING_CAPACITY_MAX);
export const changeServingCapacityMin = changeVal(CHANGE_SERVING_CAPACITY_MIN);

export const {
  loadEntity: loadStyles,
  entityLoaded: stylesLoaded,
  entityLoadingError: stylesLoadingError,
} = createSagaActions({
  loadAction: LOAD_STYLES,
  entityLoadedAction: STYLES_LOADED,
  entityLoadingErrorAction: STYLES_LOADING_ERROR,
  schema: styleListSchema,
});

export const {
  loadEntity: loadTypes,
  entityLoaded: typesLoaded,
  entityLoadingError: typesLoadingError,
} = createSagaActions({
  loadAction: LOAD_TYPES,
  entityLoadedAction: TYPES_LOADED,
  entityLoadingErrorAction: TYPES_LOADING_ERROR,
  schema: typeListSchema,
});
