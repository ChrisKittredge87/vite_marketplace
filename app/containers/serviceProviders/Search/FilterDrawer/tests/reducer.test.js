
import { fromJS } from 'immutable';
import searchFilterDrawerReducer from '../reducer';

describe('searchFilterDrawerReducer', () => {
  it('returns the initial state', () => {
    expect(searchFilterDrawerReducer(undefined, {})).toEqual(fromJS({}));
  });
});
