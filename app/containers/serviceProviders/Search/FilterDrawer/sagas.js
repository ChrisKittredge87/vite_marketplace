import { getEntitiesSaga as createSPTypeGetSagas } from 'entities/schemas/serviceProviders/types/sagas';
import {
  stylesLoaded, stylesLoadingError,
  typesLoaded, typesLoadingError,
} from './actions';
import { LOAD_STYLES, LOAD_TYPES } from './constants';

// Individual exports for testing

const serviceProviderStylesData = createSPTypeGetSagas({
  actionKey: LOAD_STYLES,
  successAction: stylesLoaded,
  errorAction: stylesLoadingError,
});

const serviceProviderTypesData = createSPTypeGetSagas({
  actionKey: LOAD_TYPES,
  successAction: typesLoaded,
  errorAction: typesLoadingError,
});

// All sagas to be loaded
export default [
  serviceProviderStylesData,
  serviceProviderTypesData,
];

export const NAME = 'spSearchFilterDrawerSagas';
