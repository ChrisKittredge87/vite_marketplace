/*
 *
 * SearchFilterDrawer
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import AppBar from 'material-ui/AppBar';
import Checkbox from 'material-ui/Checkbox';
import DatePicker from 'material-ui/DatePicker';
import Drawer from 'material-ui/Drawer';
import IconButton from 'material-ui/IconButton';
import InputRange from 'react-input-range';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import TimePicker from 'material-ui/TimePicker';
import { createStructuredSelector } from 'reselect';
import { fromJS } from 'immutable';
import _ from 'lodash';

import * as STYLE_CONSTANTS from 'entities/schemas/serviceProviders/styles/constants';
import * as TYPE_CONSTANTS from 'entities/schemas/serviceProviders/types/constants';
import {
  setKeyVal,
  addServiceProviderStyleId, addServiceProviderTypeId,
  removeServiceProviderStyleId, removeServiceProviderTypeId,
  changeApplyFilters, changeBudgetMax, changeBudgetMin,
  changeServingCapacityMax, changeServingCapacityMin,
  loadStyles, loadTypes,
} from './actions';
import { DATE, START_TIME, END_TIME, SERVING_CAPACITY_MIN, SERVING_CAPACITY_MAX } from './constants';
import messages from './messages';
import makeSelectSearchFilterDrawer, {
  makeSelectBudgetMax, makeSelectBudgetMin,
  makeSelectServingCapacityMax, makeSelectServingCapacityMin,
  makeSelectServiceProviderStyles, makeSelectServiceProviderTypes,
  makeSelectStyles, makeSelectTypes,
} from './selectors';

export class SearchFilterDrawer extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.handleApplyFilters = this.handleApplyFilters.bind(this);
    this.handleChangeMinMaxBudget = this.handleChangeMinMaxBudget.bind(this);
    this.handleChangeMinMaxServingCapacity = this.handleChangeMinMaxServingCapacity.bind(this);
    this.handleSetKeyVal = this.handleSetKeyVal.bind(this);
  }

  /* ----- Component Lifecycle ----- */

  componentWillMount() {
    const { dispatchLoadStyles, dispatchLoadTypes } = this.props;
    dispatchLoadStyles();
    dispatchLoadTypes();

    const { onSetKeyVal, queryParams } = this.props;
    if (queryParams) {
      const { date, minGuests, maxGuests } = queryParams;
      if (date) {
        onSetKeyVal(DATE, new Date(date));
      }
      if (minGuests) {
        onSetKeyVal(SERVING_CAPACITY_MIN, minGuests);
      }
      if (maxGuests) {
        onSetKeyVal(SERVING_CAPACITY_MAX, maxGuests);
      }
    }
  }

  /* ----- Event Handlers ----- */

  handleApplyFilters(applyFilters) {
    return () => {
      const { onApplyFilters, onChangeApplyFilters } = this.props;
      onChangeApplyFilters(applyFilters);
      onApplyFilters();
    };
  }

  handleSetKeyVal(key) {
    const { onSetKeyVal } = this.props;
    return (val) => {
      onSetKeyVal(key, val);
    };
  }

  handleSetDateTime(key) {
    return (evt, val) => this.handleSetKeyVal(key)(val);
  }

  handleStyleCheckboxChange(id) {
    const { onAddServiceProviderStyleId, onRemoveServiceProviderStyleId } = this.props;
    return (evt, isInputChecked) => {
      if (isInputChecked) {
        onAddServiceProviderStyleId(id);
      } else {
        onRemoveServiceProviderStyleId(id);
      }
    };
  }

  handleTypeCheckboxChange(id) {
    const { onAddServiceProviderTypeId, onRemoveServiceProviderTypeId } = this.props;
    return (evt, isInputChecked) => {
      if (isInputChecked) {
        onAddServiceProviderTypeId(id);
      } else {
        onRemoveServiceProviderTypeId(id);
      }
    };
  }

  handleChangeMinMaxBudget({ min, max }) {
    const { budgetMax, budgetMin, onChangeBudgetMax, onChangeBudgetMin } = this.props;
    if (budgetMax !== max) {
      onChangeBudgetMax(max);
    }
    if (budgetMin !== min) {
      onChangeBudgetMin(min);
    }
  }

  handleChangeMinMaxServingCapacity({ min, max }) {
    const { onChangeServingCapacityMax, onChangeServingCapacityMin, servingCapacityMax, servingCapacityMin } = this.props;
    if (max !== servingCapacityMax) {
      onChangeServingCapacityMax(max);
    }
    if (min !== servingCapacityMin) {
      onChangeServingCapacityMin(min);
    }
  }

  /* ----- END Event Handlers ----- */

  renderDateRow() {
    const { filterDrawerState } = this.props;
    return (<div style={{ display: 'flex', alignItems: 'baseline', margin: '10px 0 10px 0' }}>
      <div style={{ width: '20%', textAlign: 'right', marginRight: 20 }}>
        <span><b><FormattedMessage {...messages.date} /></b></span>
      </div>
      <div style={{ width: '70%' }}>
        <DatePicker
          locale="en-US"
          onChange={this.handleSetDateTime(DATE)}
          value={filterDrawerState.get(DATE)}
        />
      </div>
    </div>);
  }

  renderTimeRow() {
    const { filterDrawerState } = this.props;
    return (<div>
      <div style={{ display: 'flex', alignItems: 'baseline', margin: '10px 0 10px 0' }}>
        <div style={{ width: '20%', textAlign: 'right', marginRight: 20 }}>
          <span><b><FormattedMessage {...messages.startTime} /></b></span>
        </div>
        <div style={{ width: '70%' }}>
          <TimePicker
            onChange={this.handleSetDateTime(START_TIME)}
            value={filterDrawerState.get(START_TIME)}
          />
        </div>
      </div>
      <div style={{ display: 'flex', alignItems: 'baseline', margin: '10px 0 10px 0' }}>
        <div style={{ width: '20%', textAlign: 'right', marginRight: 20 }}>
          <span><b><FormattedMessage {...messages.endTime} /></b></span>
        </div>
        <div style={{ width: '70%' }}>
          <TimePicker
            onChange={this.handleSetDateTime(END_TIME)}
            value={filterDrawerState.get(END_TIME)}
          />
        </div>
      </div>
    </div>);
  }

  renderBudgetRow() {
    const { budgetMax, budgetMin } = this.props;
    return (<div style={{ display: 'flex', alignItems: 'baseline', margin: '50px 0 10px 0' }}>
      <div style={{ width: '20%', textAlign: 'right', marginRight: 20 }}>
        <span><b><FormattedMessage {...messages.budget} /></b></span>
      </div>
      <div style={{ width: '70%' }}>
        <div style={{ width: '70%' }}>
          <InputRange
            formatLabel={(value) => `$${value}`}
            maxValue={10000}
            minValue={0}
            value={{ min: budgetMin, max: budgetMax || 10000 }}
            onChange={this.handleChangeMinMaxBudget}
          />
        </div>
      </div>
    </div>);
  }

  renderServingCapacityRow() {
    const { servingCapacityMax, servingCapacityMin } = this.props;
    return (<div style={{ display: 'flex', alignItems: 'baseline', margin: '50px 0 10px 0' }}>
      <div style={{ width: '20%', textAlign: 'right', marginRight: 20 }}>
        <span style={{ paddingRight: 10 }}>
          <b><FormattedMessage {...messages.guests} /></b>
        </span>
      </div>
      <div style={{ width: '70%' }}>
        <div style={{ width: '70%' }}>
          <InputRange
            maxValue={500}
            minValue={0}
            value={{ min: servingCapacityMin, max: servingCapacityMax || 500 }}
            onChange={this.handleChangeMinMaxServingCapacity}
          />
        </div>
      </div>
    </div>);
  }

  renderStyleRow() {
    const { serviceProviderStyles, styles } = this.props;
    const { ID, NAME } = STYLE_CONSTANTS;
    return (<div style={{ display: 'flex', paddingTop: 25 }}>
      <div style={{ width: '20%', textAlign: 'right', marginRight: 15 }}>
        <span style={{ paddingRight: 10 }}><b><FormattedMessage {...messages.style} /></b></span>
      </div>
      <div style={{ width: '70%' }}>
        {styles.map((style, idx) =>
          (<Checkbox
            checked={serviceProviderStyles.indexOf(style.get(ID)) > -1}
            key={`style_${idx}`}
            label={style.get(NAME)}
            onCheck={this.handleStyleCheckboxChange(style.get(ID))}
          />))}
      </div>
    </div>);
  }

  renderTypeRow() {
    const { serviceProviderTypes, types } = this.props;
    const { ID, NAME } = TYPE_CONSTANTS;
    return (<div style={{ display: 'flex', paddingTop: 25 }}>
      <div style={{ width: '20%', textAlign: 'right', marginRight: 15 }}>
        <span style={{ paddingRight: 10 }}><b><FormattedMessage {...messages.type} /></b></span>
      </div>
      <div style={{ width: '70%' }}>
        {types.map((type, idx) =>
          (<Checkbox
            checked={serviceProviderTypes.indexOf(type.get(ID)) > -1}
            key={`style_${idx}`}
            label={type.get(NAME)}
            onCheck={this.handleTypeCheckboxChange(type.get(ID))}
          />))}
      </div>
    </div>);
  }

  /* ----- Rendering ----- */

  render() {
    const {
      onCloseButtonTouchTap,
      open,
      width,
    } = this.props;
    return (<Drawer open={!!open} openSecondary width={width}>
      <AppBar
        iconElementLeft={<IconButton><NavigationClose /></IconButton>}
        onLeftIconButtonTouchTap={onCloseButtonTouchTap}
        title={<FormattedMessage {...messages.filters} />}
      />
      <div style={{ padding: 15, textAlign: 'left' }}>
        <Paper style={{ height: 'calc(100vh - 150px)', overflow: 'auto' }}>
          {this.renderDateRow()}
          {this.renderTimeRow()}
          {this.renderBudgetRow()}
          {this.renderServingCapacityRow()}
          {this.renderStyleRow()}
          {this.renderTypeRow()}
        </Paper>
      </div>
      <div style={{ display: 'flex', position: 'absolute', bottom: 20, right: 25 }}>
        <RaisedButton label={<FormattedMessage {...messages.clear} />} onClick={this.handleApplyFilters(false)} />
        <RaisedButton primary label={<FormattedMessage {...messages.apply} />} onClick={this.handleApplyFilters(true)} />
      </div>
    </Drawer>);
  }

  /* ----- END Rendering ----- */

}

SearchFilterDrawer.propTypes = {
  budgetMax: PropTypes.string,
  budgetMin: PropTypes.string,
  dispatchLoadStyles: PropTypes.func,
  dispatchLoadTypes: PropTypes.func,
  filterDrawerState: PropTypes.object,
  onAddServiceProviderStyleId: PropTypes.func,
  onAddServiceProviderTypeId: PropTypes.func,
  onApplyFilters: PropTypes.func,
  onChangeApplyFilters: PropTypes.func,
  onChangeBudgetMax: PropTypes.func,
  onChangeBudgetMin: PropTypes.func,
  onChangeServingCapacityMax: PropTypes.func,
  onChangeServingCapacityMin: PropTypes.func,
  onCloseButtonTouchTap: PropTypes.func,
  onSetKeyVal: PropTypes.func,
  onRemoveServiceProviderStyleId: PropTypes.func,
  onRemoveServiceProviderTypeId: PropTypes.func,
  open: PropTypes.bool,
  queryParams: PropTypes.object,
  servingCapacityMax: PropTypes.string,
  servingCapacityMin: PropTypes.string,
  serviceProviderStyles: PropTypes.object,
  serviceProviderTypes: PropTypes.object,
  styles: PropTypes.object,
  types: PropTypes.object,
  width: PropTypes.object,
};

SearchFilterDrawer.defaultProps = {
  budgetMax: '',
  budgetMin: '',
  onCloseButtonTouchTap: _.noop,
  servingCapacityMax: '',
  servingCapacityMin: '',
  serviceProviderStyles: fromJS([]),
  serviceProviderTypes: fromJS([]),
  styles: fromJS([]),
  types: fromJS([]),
};

const mapStateToProps = createStructuredSelector({
  budgetMax: makeSelectBudgetMax(),
  budgetMin: makeSelectBudgetMin(),
  filterDrawerState: makeSelectSearchFilterDrawer(),
  servingCapacityMax: makeSelectServingCapacityMax(),
  servingCapacityMin: makeSelectServingCapacityMin(),
  serviceProviderStyles: makeSelectServiceProviderStyles(),
  serviceProviderTypes: makeSelectServiceProviderTypes(),
  styles: makeSelectStyles(),
  types: makeSelectTypes(),
});

function mapDispatchToProps(dispatch) {
  return {
    onSetKeyVal: (key, val) => dispatch(setKeyVal(key, val)),
    onAddServiceProviderStyleId: (val) => dispatch(addServiceProviderStyleId(val)),
    onAddServiceProviderTypeId: (val) => dispatch(addServiceProviderTypeId(val)),
    onRemoveServiceProviderStyleId: (val) => dispatch(removeServiceProviderStyleId(val)),
    onRemoveServiceProviderTypeId: (val) => dispatch(removeServiceProviderTypeId(val)),
    onChangeApplyFilters: (val) => dispatch(changeApplyFilters(val)),
    onChangeBudgetMax: (val) => dispatch(changeBudgetMax(val)),
    onChangeBudgetMin: (val) => dispatch(changeBudgetMin(val)),
    onChangeServingCapacityMax: (val) => dispatch(changeServingCapacityMax(val)),
    onChangeServingCapacityMin: (val) => dispatch(changeServingCapacityMin(val)),
    dispatchLoadStyles: () => dispatch(loadStyles()),
    dispatchLoadTypes: () => dispatch(loadTypes()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchFilterDrawer);
