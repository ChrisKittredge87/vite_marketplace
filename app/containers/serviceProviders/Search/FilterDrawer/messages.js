/*
 * SearchFilterDrawer Messages
 *
 * This contains all the text for the SearchFilterDrawer component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  apply: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.apply',
    defaultMessage: 'Apply',
  },
  budget: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.budget',
    defaultMessage: 'Budget',
  },
  clear: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.clear',
    defaultMessage: 'Clear',
  },
  currencySymbol: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.currencySymbol',
    defaultMessage: '$',
  },
  date: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.date',
    defaultMessage: 'Date',
  },
  filters: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.filters',
    defaultMessage: 'Filters',
  },
  guests: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.guests',
    defaultMessage: 'Guests',
  },
  max: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.max',
    defaultMessage: 'max',
  },
  min: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.min',
    defaultMessage: 'min',
  },
  startTime: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.startTime',
    defaultMessage: 'Start Time',
  },
  endTime: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.endTime',
    defaultMessage: 'End Time',
  },
  style: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.style',
    defaultMessage: 'Style',
  },
  type: {
    id: 'app.containers.promoterPortal.serviceProviders.Search.FilterDrawer.type',
    defaultMessage: 'Type',
  },
});
