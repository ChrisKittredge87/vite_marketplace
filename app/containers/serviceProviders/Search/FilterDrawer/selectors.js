import { createSelector } from 'reselect';
import makeSelectEntities from 'entities/selectors';
import { SCHEMA_KEY as STYLE_SCHEMA_KEY, NAME as STYLE_NAME } from 'entities/schemas/serviceProviders/styles/constants';
import { SCHEMA_KEY as TYPE_SCHEMA_KEY, NAME as TYPE_NAME } from 'entities/schemas/serviceProviders/types/constants';
import { DOMAIN } from './reducer';
import {
  APPLY_FILTERS,
  BUDGET_MAX,
  BUDGET_MIN,
  SERVING_CAPACITY_MAX,
  SERVING_CAPACITY_MIN,
  SERVICE_PROVIDER_STYLE_IDS,
  SERVICE_PROVIDER_TYPE_IDS,
  STYLE_IDS,
  TYPE_IDS,
} from './constants';

/**
 * Direct selector to the searchFilterDrawer state domain
 */
const selectSearchFilterDrawerDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by SearchFilterDrawer
 */

const makeSelectSearchFilterDrawer = () => createSelector(
  selectSearchFilterDrawerDomain(),
  (substate) => substate
);

const makeSelectApplyFilters = () => createSelector(
  selectSearchFilterDrawerDomain(),
  (searchFilterDrawerState) => searchFilterDrawerState.get(APPLY_FILTERS)
);

const makeSelectBudgetMax = () => createSelector(
  selectSearchFilterDrawerDomain(),
  (searchFilterDrawerState) => searchFilterDrawerState.get(BUDGET_MAX)
);

const makeSelectBudgetMin = () => createSelector(
  selectSearchFilterDrawerDomain(),
  (searchFilterDrawerState) => searchFilterDrawerState.get(BUDGET_MIN)
);

const makeSelectServingCapacityMax = () => createSelector(
  selectSearchFilterDrawerDomain(),
  (searchFilterDrawerState) => searchFilterDrawerState.get(SERVING_CAPACITY_MAX)
);

const makeSelectServingCapacityMin = () => createSelector(
  selectSearchFilterDrawerDomain(),
  (searchFilterDrawerState) => searchFilterDrawerState.get(SERVING_CAPACITY_MIN)
);

const makeSelectServiceProviderStyles = () => createSelector(
  selectSearchFilterDrawerDomain(),
  (searchFilterDrawerState) => searchFilterDrawerState.get(SERVICE_PROVIDER_STYLE_IDS).toList()
);

const makeSelectServiceProviderTypes = () => createSelector(
  selectSearchFilterDrawerDomain(),
  (searchFilterDrawerState) => searchFilterDrawerState.get(SERVICE_PROVIDER_TYPE_IDS).toList()
);

const makeSelectStyles = () => createSelector(
  selectSearchFilterDrawerDomain(),
  makeSelectEntities(),
  (searchFilterDomain, entitiesDomain) =>
    searchFilterDomain
      .get(STYLE_IDS)
      .map((id) => entitiesDomain.getIn([STYLE_SCHEMA_KEY, `${id}`]))
      .sort((a, b) => a.get(STYLE_NAME) > b.get(STYLE_NAME))
);

const makeSelectTypes = () => createSelector(
  selectSearchFilterDrawerDomain(),
  makeSelectEntities(),
  (searchFilterDomain, entitiesDomain) =>
    searchFilterDomain
      .get(TYPE_IDS)
      .map((id) => entitiesDomain.getIn([TYPE_SCHEMA_KEY, `${id}`]))
      .sort((a, b) => a.get(TYPE_NAME) > b.get(TYPE_NAME))
);

export default makeSelectSearchFilterDrawer;
export {
  selectSearchFilterDrawerDomain,
  makeSelectApplyFilters,
  makeSelectBudgetMax,
  makeSelectBudgetMin,
  makeSelectServingCapacityMax,
  makeSelectServingCapacityMin,
  makeSelectServiceProviderStyles,
  makeSelectServiceProviderTypes,
  makeSelectStyles,
  makeSelectTypes,
};
