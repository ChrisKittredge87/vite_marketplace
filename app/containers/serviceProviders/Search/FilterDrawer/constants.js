/*
 *
 * SearchFilterDrawer constants
 *
 */

/* Actions */
export const ADD_SERVICE_PROVIDER_STYLE_ID = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/ADD_SERVICE_PROVIDER_STYLE_ID';
export const ADD_SERVICE_PROVIDER_TYPE_ID = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/ADD_SERVICE_PROVIDER_TYPE_ID';
export const REMOVE_SERVICE_PROVIDER_STYLE_ID = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/REMOVE_SERVICE_PROVIDER_STYLE_ID';
export const REMOVE_SERVICE_PROVIDER_TYPE_ID = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/REMOVE_SERVICE_PROVIDER_TYPE_ID';
export const CHANGE_APPLY_FILTERS = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/CHANGE_APPLY_FILTERS';
export const CHANGE_BUDGET_MAX = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/CHANGE_BUDGET_MAX';
export const CHANGE_BUDGET_MIN = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/CHANGE_BUDGET_MIN';
export const CHANGE_SERVING_CAPACITY_MAX = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/CHANGE_SERVING_CAPACITY_MAX';
export const CHANGE_SERVING_CAPACITY_MIN = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/CHANGE_SERVING_CAPACITY_MIN';
export const LOAD_STYLES = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/LOAD_STYLES';
export const STYLES_LOADED = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/STYLES_LOADED';
export const STYLES_LOADING_ERROR = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/STYLES_LOADING_ERROR';
export const LOAD_TYPES = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/LOAD_TYPES';
export const TYPES_LOADED = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/TYPES_LOADED';
export const TYPES_LOADING_ERROR = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/TYPES_LOADING_ERROR';
export const SET_KEY_VAL = 'app/promoterPortal/serviceProviders/Search/FilterDrawer/SET_KEY_VAL';

/* State Keys */
export const APPLY_FILTERS = 'applyFilters';
export const BUDGET_MAX = 'budgetMax';
export const BUDGET_MIN = 'budgetMin';
export const DATE = 'date';
export const END_TIME = 'endTime';
export const SERVING_CAPACITY_MAX = 'servingCapacityMax';
export const SERVING_CAPACITY_MIN = 'servingCapacityMin';
export const SERVICE_PROVIDER_STYLE_IDS = 'serviceProviderStyleIds';
export const SERVICE_PROVIDER_TYPE_IDS = 'serviceProviderTypeIds';
export const START_TIME = 'startTime';
export const STYLE_IDS = 'styleIds';
export const TYPE_IDS = 'typeIds';
