/*
 *
 * SearchFilterDrawer reducer
 *
 */

import { fromJS, Set as set } from 'immutable';
import {
  /* Actions */
  ADD_SERVICE_PROVIDER_STYLE_ID, ADD_SERVICE_PROVIDER_TYPE_ID,
  REMOVE_SERVICE_PROVIDER_STYLE_ID, REMOVE_SERVICE_PROVIDER_TYPE_ID,
  CHANGE_APPLY_FILTERS,
  CHANGE_BUDGET_MAX, CHANGE_BUDGET_MIN,
  CHANGE_SERVING_CAPACITY_MAX, CHANGE_SERVING_CAPACITY_MIN,
  STYLES_LOADED, TYPES_LOADED,
  SET_KEY_VAL,
  /* State Keys */
  APPLY_FILTERS,
  BUDGET_MAX, BUDGET_MIN,
  SERVING_CAPACITY_MAX, SERVING_CAPACITY_MIN,
  SERVICE_PROVIDER_STYLE_IDS, SERVICE_PROVIDER_TYPE_IDS,
  STYLE_IDS, TYPE_IDS,
} from './constants';

const initialState = fromJS({
  [APPLY_FILTERS]: false,
  [SERVICE_PROVIDER_STYLE_IDS]: set(),
  [SERVICE_PROVIDER_TYPE_IDS]: set(),
  [STYLE_IDS]: [],
  [TYPE_IDS]: [],
});

function searchFilterDrawerReducer(state = initialState, action) {
  const { key, result, type, val } = action;
  switch (type) {
    case ADD_SERVICE_PROVIDER_STYLE_ID:
      return state.set(SERVICE_PROVIDER_STYLE_IDS, state.get(SERVICE_PROVIDER_STYLE_IDS).add(val));
    case ADD_SERVICE_PROVIDER_TYPE_ID:
      return state.set(SERVICE_PROVIDER_TYPE_IDS, state.get(SERVICE_PROVIDER_TYPE_IDS).add(val));
    case REMOVE_SERVICE_PROVIDER_STYLE_ID:
      return state.set(SERVICE_PROVIDER_STYLE_IDS, state.get(SERVICE_PROVIDER_STYLE_IDS).remove(val));
    case REMOVE_SERVICE_PROVIDER_TYPE_ID:
      return state.set(SERVICE_PROVIDER_TYPE_IDS, state.get(SERVICE_PROVIDER_TYPE_IDS).remove(val));
    case CHANGE_APPLY_FILTERS:
      if (val === false) {
        return initialState
          .set(STYLE_IDS, state.get(STYLE_IDS))
          .set(TYPE_IDS, state.get(TYPE_IDS));
      }
      return state.set(APPLY_FILTERS, val);
    case SET_KEY_VAL:
      return state.set(key, val);
    case CHANGE_BUDGET_MAX:
      return state.set(BUDGET_MAX, val);
    case CHANGE_BUDGET_MIN:
      return state.set(BUDGET_MIN, val);
    case CHANGE_SERVING_CAPACITY_MAX:
      return state.set(SERVING_CAPACITY_MAX, val);
    case CHANGE_SERVING_CAPACITY_MIN:
      return state.set(SERVING_CAPACITY_MIN, val);
    case STYLES_LOADED:
      return state.set(STYLE_IDS, fromJS(result));
    case TYPES_LOADED:
      return state.set(TYPE_IDS, fromJS(result));
    default:
      return state;
  }
}

export default searchFilterDrawerReducer;

export const DOMAIN = 'searchFilterDrawer';
