import styled from 'styled-components';
import { MD, LG, XL } from 'globalConstants';

export const MapContainer = styled.div`
  width: 50%;
  height: calc(100vh - 156px);
  padding-right: 15px;
  padding-bottom: 15px;
  @media (min-width: ${MD}px) {
    width: 35%;
  }
  @media (min-width: ${XL}px) {
    width: 25%;
  }
`;

export const RespDiv = styled.div`
  cursor: pointer;
  margin: 0 1%;
  width: ${(props) => (100 / props.cols) - 3}%;
  @media (min-width: ${MD}px) {
    width: ${(props) => (100 / props.cols) - 2.5}%;
  }
  @media (min-width: ${LG}px) {
    width: ${(props) => (100 / props.cols) - 2}%;
  }
`;

export const SearchResultsContainer = styled.div`
  width: 100%;
  height: calc(100vh - 142px);
  overflow: auto;
  @media (min-width: ${MD}px) {
    width: 65%;
  }
  @media (min-width: ${XL}px) {
    width: 75%;
  }
`;
