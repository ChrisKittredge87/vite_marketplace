import vendorProfileRoutes from 'containers/serviceProviders/Profile/routes';

export const path = 'search';
export const name = 'Search | Vendors and Venues';

export default function (loadModule, errorLoading, injectReducers, injectSagas) {
  return {
    path,
    name,
    getComponent(nextState, cb) {
      const importModules = Promise.all([
        import('containers/serviceProviders/Search/reducer'),
        import('containers/serviceProviders/Search/sagas'),
        import('containers/serviceProviders/Search/FilterDrawer/reducer'),
        import('containers/serviceProviders/Search/FilterDrawer/sagas'),
        import('containers/serviceProviders/Search'),
      ]);

      const renderRoute = loadModule(cb);

      importModules.then(([
        reducer, sagas,
        filterDrawerReducer, filterDrawerSagas,
        component,
      ]) => {
        injectReducers([reducer, filterDrawerReducer]);
        injectSagas([sagas, filterDrawerSagas]);
        renderRoute(component);
      });
      importModules.catch(errorLoading);
    },
  };
}
