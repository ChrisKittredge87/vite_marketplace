import { call, put, select, takeLatest } from 'redux-saga/effects';

import request from 'utils/request';
import { BASE_URL } from 'entities/schemas/serviceProviders/constants';
import { serviceProvidersLoaded, serviceProvidersLoadingError } from './actions';
import { LOAD_SERVICE_PROVIDERS } from './constants';
import { makeSelectSearchText } from './selectors';
import {
  makeSelectApplyFilters,
  makeSelectBudgetMax, makeSelectBudgetMin,
  makeSelectServingCapacityMax, makeSelectServingCapacityMin,
  makeSelectServiceProviderStyles, makeSelectServiceProviderTypes,
} from './FilterDrawer/selectors';

function* buildFilters() {
  const filters = {};
  const searchText = yield select(makeSelectSearchText());
  if (searchText) {
    filters.search_text = searchText;
  }
  const applyFilters = yield select(makeSelectApplyFilters());
  if (applyFilters) {
    const budgetMax = yield select(makeSelectBudgetMax());
    const budgetMin = yield select(makeSelectBudgetMin());
    const servingCapacityMax = yield select(makeSelectServingCapacityMax());
    const servingCapacityMin = yield select(makeSelectServingCapacityMin());
    const serviceProviderStyles = yield select(makeSelectServiceProviderStyles());
    const serviceProviderTypes = yield select(makeSelectServiceProviderTypes());
    if (budgetMax) {
      filters.budget_max = budgetMax;
    }
    if (budgetMin) {
      filters.budget_min = budgetMin;
    }
    if (servingCapacityMax) {
      filters.capacity_max = servingCapacityMax;
    }
    if (servingCapacityMin) {
      filters.capacity_min = servingCapacityMin;
    }
    if (serviceProviderStyles) {
      filters.style = serviceProviderStyles.toJS();
    }
    if (serviceProviderTypes) {
      filters.type = serviceProviderTypes.toJS();
    }
  }
  return filters;
}

// Individual exports for testing
export function* getServiceProviders() {
  try {
    const data = yield buildFilters();
    const serviceProviders = yield call(request.get, BASE_URL, { data });
    yield put(serviceProvidersLoaded(serviceProviders));
  } catch (err) {
    yield put(serviceProvidersLoadingError(err));
  }
}

export function* serviceProviderData() {
  yield takeLatest(LOAD_SERVICE_PROVIDERS, getServiceProviders);
}

// All sagas to be loaded
export default [
  serviceProviderData,
];

export const NAME = 'serviceProviderSearchSagas';
