/*
 *
 * Search reducer
 *
 */

import { fromJS } from 'immutable';
import {
  /* State Keys */
  ERROR,
  FILTER_DRAWER_OPEN,
  HIGHLIGHTED_SERVICE_PROVIDER,
  SEARCH_TEXT,
  SERVICE_PROVIDER_IDS,
  LOADING,
  /* Actions Types */
  CHANGE_FILTER_DRAWER_OPEN,
  CHANGE_HIGHLIGHTED_SERVICE_PROVIDER,
  CHANGE_SEARCH_TEXT,
  LOAD_SERVICE_PROVIDERS,
  SERVICE_PROVIDERS_LOADED,
  SERVICE_PROVIDERS_LOADING_ERROR,
} from './constants';

const initialState = fromJS({
  [LOADING]: false,
  [FILTER_DRAWER_OPEN]: false,
  [SERVICE_PROVIDER_IDS]: [],
  [SEARCH_TEXT]: '',
});

function searchReducer(state = initialState, action) {
  const { error, result, type, val } = action;
  switch (type) {
    case CHANGE_SEARCH_TEXT:
      return state.set(SEARCH_TEXT, val);
    case CHANGE_FILTER_DRAWER_OPEN:
      return state.set(FILTER_DRAWER_OPEN, val);
    case CHANGE_HIGHLIGHTED_SERVICE_PROVIDER:
      return state.set(HIGHLIGHTED_SERVICE_PROVIDER, val);
    case LOAD_SERVICE_PROVIDERS:
      return state.set(LOADING, true);
    case SERVICE_PROVIDERS_LOADED:
      return state
        .set(LOADING, false)
        .set(SERVICE_PROVIDER_IDS, fromJS(result));
    case SERVICE_PROVIDERS_LOADING_ERROR:
      return state
        .set(LOADING, false)
        .set(ERROR, error);
    default:
      return state;
  }
}

export default searchReducer;

export const DOMAIN = 'serviceProvidersSearch';
