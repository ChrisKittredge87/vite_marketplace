import { createSelector } from 'reselect';

import makeSelectEntities from 'entities/selectors';
import { SCHEMA_KEY as SERVICE_PROVIDERS_SCHEMA_KEY } from 'entities/schemas/serviceProviders/constants';
import { DOMAIN } from './reducer';
import {
  FILTER_DRAWER_OPEN,
  HIGHLIGHTED_SERVICE_PROVIDER,
  SEARCH_TEXT,
  SERVICE_PROVIDER_IDS,
} from './constants';

/**
 * Direct selector to the search state domain
 */
const selectSearchDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by Search
 */

const makeSelectHighlightedServiceProvider = () => createSelector(
  selectSearchDomain(),
  (searchState) => searchState.get(HIGHLIGHTED_SERVICE_PROVIDER),
);

const makeSelectFilterDrawerOpen = () => createSelector(
  selectSearchDomain(),
  (searchState) => searchState.get(FILTER_DRAWER_OPEN),
);

const makeSelectSearch = () => createSelector(
  selectSearchDomain(),
  (substate) => substate.toJS()
);

const makeSelectSearchText = () => createSelector(
  selectSearchDomain(),
  (searchState) => searchState.get(SEARCH_TEXT),
);

const makeSelectServiceProviders = () => createSelector(
  selectSearchDomain(),
  makeSelectEntities(),
  (searchState, entities) =>
    searchState
      .get(SERVICE_PROVIDER_IDS)
      .map((id) => entities.getIn([SERVICE_PROVIDERS_SCHEMA_KEY, `${id}`]))
);

export default makeSelectSearch;
export {
  makeSelectFilterDrawerOpen,
  makeSelectHighlightedServiceProvider,
  makeSelectSearchText,
  makeSelectServiceProviders,
};
