import { createSelector } from 'reselect';
import { DOMAIN } from './reducer';

/**
 * Direct selector to the speventList state domain
 */
const selectSpEventListDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by SpEventList
 */

const makeSelectSpEventList = () => createSelector(
  selectSpEventListDomain(),
  (substate) => substate.toJS()
);

export default makeSelectSpEventList;
export {
  selectSpEventListDomain,
};
