
import { fromJS } from 'immutable';
import speventListReducer from '../reducer';

describe('speventListReducer', () => {
  it('returns the initial state', () => {
    expect(speventListReducer(undefined, {})).toEqual(fromJS({}));
  });
});
