/*
 *
 * SpEventList
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { fromJS } from 'immutable';

import EventList from 'components/EventList';

import makeSelectSpEventList from './selectors';

export class SpEventList extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { events } = this.props;
    return (
      <div>
        <EventList events={events} />
      </div>
    );
  }
}

SpEventList.defaultProps = {
  events: fromJS(new Array(100)),
};

SpEventList.propTypes = {
  events: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  SPEventList: makeSelectSpEventList(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SpEventList);
