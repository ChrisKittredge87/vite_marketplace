/*
 *
 * home
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';

import SpEventList from 'containers/serviceProviders/SpEventList';

import makeSelectEphome from './selectors';

export class Ephome extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Helmet
          title="Service Provider - Home"
          meta={[
            { name: 'description', content: 'Description of Service Provider Home' },
          ]}
        />
        <SpEventList />
      </div>
    );
  }
}

Ephome.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  EPHome: makeSelectEphome(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Ephome);
