/*
 *
 * Ephome actions
 *
 */

 // import { createSagaActions } from 'utils/sagas';

import {
  SET_KEY_VAL,
} from './constants';

export function defaultAction(key, val) {
  return { type: SET_KEY_VAL, key, val };
}
