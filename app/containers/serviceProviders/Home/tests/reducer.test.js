
import { fromJS } from 'immutable';
import ephomeReducer from '../reducer';

describe('ephomeReducer', () => {
  it('returns the initial state', () => {
    expect(ephomeReducer(undefined, {})).toEqual(fromJS({}));
  });
});
