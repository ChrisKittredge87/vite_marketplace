import { createSelector } from 'reselect';
import { DOMAIN } from './reducer';

/**
 * Direct selector to the ephome state domain
 */
const selectEphomeDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by Ephome
 */

const makeSelectEphome = () => createSelector(
  selectEphomeDomain(),
  (substate) => substate.toJS()
);

export default makeSelectEphome;
export {
  selectEphomeDomain,
};
