/*
 *
 * Ephome reducer
 *
 */

import { fromJS } from 'immutable';
import {
  SET_KEY_VAL,
} from './constants';

const initialState = fromJS({});

function ephomeReducer(state = initialState, action) {
  const { key, type, val } = action;
  switch (type) {
    case SET_KEY_VAL:
      return state.set(key, val);
    default:
      return state;
  }
}

export default ephomeReducer;

export const DOMAIN = 'serviceProviderHome';
