/*
 * Ephome Messages
 *
 * This contains all the text for the Ephome component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({});
