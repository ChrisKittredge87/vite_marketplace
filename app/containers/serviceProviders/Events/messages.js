/*
 * ServiceProvidersCalendar Messages
 *
 * This contains all the text for the ServiceProvidersCalendar component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.serviceProviders.Calendar.header',
    defaultMessage: 'This is ServiceProvidersCalendar container !',
  },
});
