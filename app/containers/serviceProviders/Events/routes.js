import { SERVICE_PROVIDER } from 'entities/schemas/users/types/constants';

export const path = 'sp-events';
export const name = 'Events';

export default function (loadModule, errorLoading, injectReducers, injectSagas) {
  return {
    guard: ({ accountType }) => accountType === SERVICE_PROVIDER,
    path,
    name,
    getComponent(nextState, cb) {
      const importModules = Promise.all([
        import('containers/serviceProviders/Events/reducer'),
        import('containers/serviceProviders/Events/sagas'),
        import('containers/serviceProviders/Events'),
      ]);

      const renderRoute = loadModule(cb);

      importModules.then(([
        reducer,
        sagas,
        component,
      ]) => {
        injectReducers([reducer]);
        injectSagas([sagas]);
        renderRoute(component);
      });
      importModules.catch(errorLoading);
    },
  };
}
