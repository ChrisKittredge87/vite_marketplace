
import { fromJS } from 'immutable';
import eventScheduleReducer from '../reducer';

describe('eventScheduleReducer', () => {
  it('returns the initial state', () => {
    expect(eventScheduleReducer(undefined, {})).toEqual(fromJS({}));
  });
});
