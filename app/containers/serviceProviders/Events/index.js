/*
 *
 * Calendar
 *
 */

import React, { PropTypes } from 'react';
import BigCalendar from 'react-big-calendar';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import Helmet from 'react-helmet';
import moment from 'moment';

import makeSelectServiceProvidersCalendar from './selectors';
import messages from './messages';

BigCalendar.setLocalizer(
  BigCalendar.momentLocalizer(moment)
);

export class ServiceProvidersCalendar extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Helmet
          title="ServiceProvidersCalendar"
          meta={[
            { name: 'description', content: 'Description of ServiceProvidersCalendar' },
          ]}
        />
        <div style={{ height: 'calc(100vh - 100px)', padding: '30px 120px' }}>
          <BigCalendar
            events={[]}
            startAccessor='startDate'
            endAccessor='endDate'
          />
        </div>
      </div>
    );
  }
}

ServiceProvidersCalendar.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ServiceProvidersCalendar: makeSelectServiceProvidersCalendar(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceProvidersCalendar);
