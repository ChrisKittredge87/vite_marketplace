import { createSelector } from 'reselect';
import { DOMAIN } from './reducer';

/**
 * Direct selector to the ServiceProviders state domain
 */
const selectServiceProvidersDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by ServiceProviders
 */

const makeSelectServiceProviders = () => createSelector(
  selectServiceProvidersDomain(),
  (substate) => substate.toJS()
);

export default makeSelectServiceProviders;
export {
  selectServiceProvidersDomain,
};
