/*
 *
 * ServiceProviderProfile
 *
 */

import React, { PropTypes } from 'react';
import Measure from 'react-measure';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { fromJS } from 'immutable';
import { FormattedMessage } from 'react-intl';
import Sticky from 'react-stickynode';
import { animateScroll, Element, Events, Link as ScrollLink, scrollSpy } from 'react-scroll';
import Helmet from 'react-helmet';
import DatePicker from 'material-ui/DatePicker';
import EditIcon from 'material-ui/svg-icons/image/edit';
import IconButton from 'material-ui/IconButton';
import InputRange from 'react-input-range';
import RaisedButton from 'material-ui/RaisedButton';
import Rater from 'react-rater';
import { Tab, Tabs } from 'material-ui/Tabs';
import TextField from 'material-ui/TextField';
import TimePicker from 'material-ui/TimePicker';
import { Toolbar } from 'material-ui/Toolbar';
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import _ from 'lodash';

import { XS, LG } from 'globalConstants';
import MessageDialog from 'containers/serviceProviders/MessageDialog';
import Reviews from 'containers/serviceProviders/Reviews';
import SPImageUploadDialog from 'containers/serviceProviders/SPImageUploadDialog';
import ImgSlider from 'components/common/ImgSlider';
import CenteredLoadingSpinner from 'components/common/CenteredLoadingSpinner';

import {
  ADDRESS,
  AVG_RATING,
  CAN_EDIT,
  ID,
  IMAGES,
  LOCALITY,
  LAT, LON,
  NAME,
  OVERVIEW,
  REGION,
  SERVICE_PROVIDER_TYPE,
} from 'entities/schemas/serviceProviders/constants';
import { DISPLAY_NAME } from 'entities/schemas/serviceProviders/types/constants';
import { selectEntity as selectServiceProvider } from 'entities/schemas/serviceProviders/selectors';
import {
  changeCurrentScrollElement,
  loadServiceProviders,
  updateServiceProvider,
  saveServiceProvider,
  setCleanState,
} from './actions';
import { LOCATION_SCROLL, OVERVIEW_SCROLL, REVIEWS_SCROLL, SERVICE_PROVIDER_IDS } from './constants';
import messages from './messages';
import selectServiceProviderProfileDomain, {
  makeSelectCurrentScrollElement,
  makeSelectLoading,
} from './selectors';
import {
  CenterColumn,
  FlexBaseline, FlexContainer, FlexColumns, FlexSpaceBetween,
  OuterColumn,
  PaddedDiv,
  Paper75Centered, Paper85Centered,
  PositionMenuItem,
} from './styled';

const STATE_KEYS = {
  EDIT_LOCATION_MODE: 'editLocationMode',
  EDIT_OVERVIEW_MODE: 'editOverviewMode',
  OVERVIEW_TEXT: 'overviewText',
};

const TheGoogleMap = withGoogleMap(({ center, marker }) => (
  <GoogleMap
    options={{
      scrollwheel: false,
    }}
    defaultZoom={12}
    defaultCenter={center}
  >
    <Marker
      {...marker}
    />
  </GoogleMap>
));

export class ServiceProviderDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    getServiceProvider: PropTypes.func,
    loading: PropTypes.bool,
    location: PropTypes.object,
    onLoadServiceProviders: PropTypes.func,
    onUpdateServiceProvider: PropTypes.func,
    onSaveServiceProvider: PropTypes.func,
    onSetCleanState: PropTypes.func,
    serviceProviderProfileDomain: PropTypes.object,
  };

  static defaultProps = {
    serviceProvider: fromJS({}),
  };

  constructor(props) {
    super(props);

    const { EDIT_LOCATION_MODE, EDIT_OVERVIEW_MODE, OVERVIEW_TEXT } = STATE_KEYS;

    this.state = {
      currentScrollElement: OVERVIEW_SCROLL,
      [EDIT_LOCATION_MODE]: false,
      [EDIT_OVERVIEW_MODE]: false,
      [OVERVIEW_TEXT]: '',
      serviceProviderId: null,
    };

    this.handleChangeOverviewText = this.handleChangeOverviewText.bind(this);
    this.handleChangeScrollElement = this.handleChangeScrollElement.bind(this);
    this.handleSaveOverview = this.handleSaveOverview.bind(this);
    this.handleSetServiceProviderId = this.handleSetServiceProviderId.bind(this);
    this.handleToggleOverviewEditMode = this.handleToggleOverviewEditMode.bind(this);
    this.handleToggleStateKeyVal = this.handleToggleStateKeyVal.bind(this);
    this.renderLocation = this.renderLocation.bind(this);
    this.renderOverview = this.renderOverview.bind(this);
    this.renderServiceProviderProfile = this.renderServiceProviderProfile.bind(this);
  }

  /* ----- React Lifecycle ----- */

  componentWillMount() {
    this.props.onLoadServiceProviders();
  }

  componentDidMount() {
    Events.scrollEvent.register('begin', (to) => this.handleChangeScrollElement(to));
    scrollSpy.update();
  }

  componentWillReceiveProps(nextProps) {
    const { location, serviceProviderProfileDomain } = this.props;
    const { serviceProviderProfileDomain: nextServiceProviderProfileDomain } = nextProps;
    const serviceProviderIds = serviceProviderProfileDomain.get(SERVICE_PROVIDER_IDS);
    const nextServiceProviderIds = nextServiceProviderProfileDomain.get(SERVICE_PROVIDER_IDS);
    if (nextServiceProviderIds && nextServiceProviderIds.size > 0 && !nextServiceProviderIds.equals(serviceProviderIds)) {
      const spIdFromQuery = _.parseInt(_.get(location, 'query.serviceProviderId'));
      const validSpId = nextServiceProviderIds.indexOf(spIdFromQuery) > -1 ? spIdFromQuery : nextServiceProviderIds.first();
      this.handleSetServiceProviderId(validSpId);
    }
  }

  componentWillUnmount() {
    Events.scrollEvent.remove('begin');
    const { onSetCleanState } = this.props;
    onSetCleanState();
  }

  /* ----- END React Lifecycle ----- */

  /* ----- Event Binding ----- */

  handleChangeScrollElement(currentScrollElement) {
    this.setState({ currentScrollElement });
  }

  handleChangeOverviewText(evt) {
    const overviewText = evt.target.value;
    this.setState({ overviewText });
  }

  handleSaveOverview() {
    const { onUpdateServiceProvider, onSaveServiceProvider, getServiceProvider } = this.props;
    const serviceProvider = getServiceProvider(this.state.serviceProviderId) || fromJS({});
    const { overviewText } = this.state;
    onUpdateServiceProvider(serviceProvider.set(OVERVIEW, overviewText).toJS());
    onSaveServiceProvider(serviceProvider.get(ID));
    this.setState({ [STATE_KEYS.EDIT_OVERVIEW_MODE]: false });
  }

  handleSetServiceProviderId(serviceProviderId) {
    this.setState({ serviceProviderId, currentScrollElement: OVERVIEW_SCROLL });
    animateScroll.scrollToTop({ duration: 0 });
  }

  handleToggleOverviewEditMode(editOverviewMode) {
    return () => {
      const { getServiceProvider } = this.props;
      const serviceProvider = getServiceProvider(this.state.serviceProviderId) || fromJS({});
      this.setState({ editOverviewMode, overviewText: serviceProvider.get(OVERVIEW) });
    };
  }

  handleToggleStateKeyVal(key, val) {
    return () => { this.setState({ [key]: val }); };
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  renderLoremIpsum() {
    return (<div>
      Lorem ipsum dolor sit amet, at porro facete tibique ius. Per cu mazim voluptua vituperatoribus, est offendit sapientem ut. In vix offendit perfecto interesset. No eius mucius interesset sea, ei nulla dolor mucius duo. Oporteat ullamcorper ut nec.
Everti dolorem patrioque eum ne, vim et minimum posidonium. Erat vivendum at vim. Hendrerit dissentiet ex sea, commune suscipit erroribus vel ea. In odio reque per. Ad omnium iriure urbanitas eos, qui in tollit antiopam voluptatum, omnesque fabellas elaboraret ex quo. Eu vis vocent saperet tincidunt.
Dicta nulla consequat et eam, tale constituto ei pro, mel adhuc option voluptua ex. Clita animal vocibus in eos. Cum et prodesset adolescens adversarium. In nec novum timeam vocibus, mel impetus praesent in. Iuvaret scripserit suscipiantur eam ne, eirmod senserit ut ius.
Ius ex ferri aeque. Falli debet commune ad cum, an minim aliquid disputationi duo, eruditi voluptua erroribus ei ius. Eu sea quot explicari dissentiet. Eam dico nobis suscipit te, wisi putant explicari nam eu. Te everti regione nostrud vix, ex iriure tamquam deserunt vim.
Sed suas reprimique temporibus ex, ad vel quot invenire moderatius. Per et rebum pertinacia referrentur, et mei dolores constituam, duo repudiare assueverit eu. Cum te viris ignota albucius. Et cum eros vocent, ponderum probatus persecuti ut vix. Delenit periculis sadipscing te has, ei eos dicant munere appareat. Mea tantas dissentias repudiandae eu, vim at volumus argumentum, dolor animal ut sit.
    </div>);
  }

  renderLeftContainer() {
    const { currentScrollElement } = this.state;
    return (<OuterColumn>
      <Sticky top={64 + 84}>
        <Paper75Centered>
          <FlexColumns>
            <ScrollLink to={OVERVIEW_SCROLL} smooth offset={-64 - 84} duration={350}>
              <PositionMenuItem selected={currentScrollElement === OVERVIEW_SCROLL}>
                <FormattedMessage {...messages.overview} />
              </PositionMenuItem>
            </ScrollLink>
            <ScrollLink to={REVIEWS_SCROLL} smooth offset={-70} duration={350}>
              <PositionMenuItem selected={currentScrollElement === REVIEWS_SCROLL}>
                <FormattedMessage {...messages.reviews} />
              </PositionMenuItem>
            </ScrollLink>
            <ScrollLink to={LOCATION_SCROLL} smooth offset={-36} duration={350}>
              <PositionMenuItem selected={currentScrollElement === LOCATION_SCROLL} last>
                <FormattedMessage {...messages.location} />
              </PositionMenuItem>
            </ScrollLink>
          </FlexColumns>
        </Paper75Centered>
      </Sticky>
    </OuterColumn>);
  }

  renderRightContainer() {
    return (<OuterColumn>
      <Sticky top={64 + 60}>
        <Paper85Centered style={{ height: 340, marginTop: 20 }}>
          <Toolbar style={{ height: 40 }}></Toolbar>
          <div style={{ margin: '0 15px' }}>
            <DatePicker hintText="mm/dd/yyyy" textFieldStyle={{ width: '100%' }} />
            <TimePicker hintText="start time - end time" textFieldStyle={{ width: '100%' }} />
            <div style={{ display: 'flex', alignItems: 'center', margin: '30px 0' }}>
              <span style={{ marginRight: 25 }}><FormattedMessage {...messages.guests} /></span>
              <div style={{ width: '70%' }}>
                <InputRange maxValue={50} minValue={0} value={25} />
              </div>
            </div>
            <div style={{ display: 'flex', alignItems: 'center', margin: '30px 0' }}>
              <span style={{ marginRight: 25 }}><FormattedMessage {...messages.budget} /></span>
              <div style={{ width: '70%' }}>
                <InputRange maxValue={50} minValue={0} value={25} />
              </div>
            </div>
            <div style={{ margin: '40px 0', textAlign: 'center' }}>
              <RaisedButton secondary label={<FormattedMessage {...messages.requestAQuote} />} />
            </div>
          </div>
        </Paper85Centered>
      </Sticky>
    </OuterColumn>);
  }

  renderOverview(width) {
    const { loading, getServiceProvider } = this.props;
    const serviceProvider = getServiceProvider(this.state.serviceProviderId);
    const { editOverviewMode, overviewText } = this.state;
    return (<div style={{ marginTop: 10 }}>
      {loading || !serviceProvider ?
        <CenteredLoadingSpinner /> :
        <div>
          <div style={{ position: 'relative' }}>
            {
              serviceProvider.get(CAN_EDIT) ?
                <SPImageUploadDialog
                  buttonStyle={{ position: 'absolute ', bottom: 0, right: 0, zIndex: 2 }}
                /> : null
            }
            <ImgSlider
              height={width > XS ? '400px' : '200px'}
              imgSources={serviceProvider.get(IMAGES)}
            />
          </div>
          <FlexSpaceBetween>
            <div>
              <h1 style={{ margin: '5px 0' }}>{serviceProvider.get(NAME)}</h1>
              {width < LG ?
                <RaisedButton
                  label={<FormattedMessage {...messages.requestAQuote} />}
                  secondary
                  style={{ marginTop: 30 }}
                /> : null}
            </div>
            <FlexColumns>
              <FlexBaseline>
                <h3 style={{ margin: '15px 15px 5px 0' }}>
                  {`${serviceProvider.getIn([ADDRESS, LOCALITY])}, ${serviceProvider.getIn([ADDRESS, REGION])}`}
                </h3>
                <Rater style={{ paddingRight: 7 }} total={5} rating={serviceProvider.get(AVG_RATING)} interactive={false} />
                <span>15</span>
              </FlexBaseline>
              <FlexColumns style={{ textAlign: 'right' }}>
                <ScrollLink to={REVIEWS_SCROLL} smooth offset={-36} duration={350}>
                  <span style={{ textDecoration: 'underline', cursor: 'pointer', color: 'blue' }}>
                    <FormattedMessage {...messages.reviews} />
                  </span>
                </ScrollLink>
                <div style={{ paddingTop: 15 }}>
                  <MessageDialog dialogWidth={width < LG ? '95%' : '75%'} />
                </div>
              </FlexColumns>
            </FlexColumns>
          </FlexSpaceBetween>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <h2>Overview</h2>
            {serviceProvider.get(CAN_EDIT) && !editOverviewMode ?
              <IconButton
                tooltip="Edit"
                tooltipPosition="top"
                onClick={this.handleToggleOverviewEditMode(true)}
              >
                <EditIcon />
              </IconButton> : null}
          </div>
          <div>
            {serviceProvider.get(CAN_EDIT) && editOverviewMode ?
              <div>
                <TextField
                  autoFocus
                  fullWidth
                  onChange={this.handleChangeOverviewText}
                  rows={2}
                  rowsMax={15}
                  multiLine
                  value={overviewText}
                />
                <div style={{ textAlign: 'right' }}>
                  <RaisedButton
                    label={'Cancel'}
                    onClick={this.handleToggleOverviewEditMode(false)}
                  />
                  <RaisedButton
                    label={'Save'}
                    onClick={this.handleSaveOverview}
                    primary
                  />
                </div>
              </div> :
              serviceProvider.get(OVERVIEW)}
          </div>
        </div>}
    </div>);
  }

  renderLocation(width) {
    const { loading, getServiceProvider } = this.props;
    const serviceProvider = getServiceProvider(this.state.serviceProviderId) || fromJS({});
    const editLocationMode = this.state[EDIT_LOCATION_MODE];
    const { EDIT_LOCATION_MODE } = STATE_KEYS;
    const lat = parseFloat(serviceProvider.getIn([ADDRESS, LAT]));
    const lng = parseFloat(serviceProvider.getIn([ADDRESS, LON]));
    const center = { lat, lng };
    const marker = { position: center, key: serviceProvider.get(NAME), defaultAnimation: 2 };
    return loading ? <div>Loading... </div> :
      (<div style={{ width: '100%', height: width > XS ? 500 : 300 }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <h2><FormattedMessage {...messages.location} /></h2>
          {serviceProvider.get(CAN_EDIT) && !editLocationMode ?
            <IconButton
              onClick={this.handleToggleStateKeyVal(EDIT_LOCATION_MODE, true)}
              tooltip="Edit"
              tooltipPosition="top"
            >
              <EditIcon />
            </IconButton> : null}
        </div>
        <TheGoogleMap
          containerElement={
            <div style={{ height: '100%', width: '100%' }} />
          }
          mapElement={
            <div style={{ height: '100%', width: '100%' }} />
          }
          center={center}
          marker={marker}
        />
      </div>);
  }

  renderServiceProviderProfile() {
    return (<FlexContainer>
      {window.innerWidth > LG ? this.renderLeftContainer() : null}
      <CenterColumn>
        <Element name={OVERVIEW_SCROLL}>
          {this.renderOverview(window.innerWidth)}
        </Element>
        <Element name={REVIEWS_SCROLL}>
          <Reviews
            dialogWidth={window.innerWidth > LG ? '75%' : '95%'}
            serviceProviderId={this.state.serviceProviderId}
          />
        </Element>
        <Element name={LOCATION_SCROLL}>
          <PaddedDiv>
            {this.renderLocation(window.innerWidth)}
          </PaddedDiv>
        </Element>
      </CenterColumn>
      {window.innerWidth > LG ? this.renderRightContainer() : null}
    </FlexContainer>);
  }

  render() {
    const { getServiceProvider, serviceProviderProfileDomain } = this.props;
    const serviceProviderIds = serviceProviderProfileDomain.get(SERVICE_PROVIDER_IDS);
    return (
      <Measure>
        {() =>
          <div>
            <Helmet
              title="Company Profile"
              meta={[
                { name: 'description', content: 'Description of ServiceProviderDetail' },
              ]}
            />
            <Sticky top={64} innerZ={3}>
              {
                serviceProviderIds.size > 1 ?
                  <Tabs
                    onChange={this.handleSetServiceProviderId}
                    value={this.state.serviceProviderId}
                  >
                    {
                      serviceProviderIds.map((id) => {
                        const sp = getServiceProvider(id) || fromJS({});
                        return (
                          <Tab
                            key={`sp_tab_${id}`}
                            label={sp.getIn([SERVICE_PROVIDER_TYPE, DISPLAY_NAME])}
                            value={id}
                          />
                        );
                      }
                      )
                    }
                  </Tabs> : null
              }
            </Sticky>
            {this.renderServiceProviderProfile()}
          </div>
      }
      </Measure>
    );
  }

  /* ----- END Rendering ----- */
}

const mapStateToProps = createStructuredSelector({
  currentScrollElement: makeSelectCurrentScrollElement(),
  loading: makeSelectLoading(),
  getServiceProvider: (state) => (id) => selectServiceProvider(id)(state),
  serviceProviderProfileDomain: selectServiceProviderProfileDomain(),
});

function mapDispatchToProps(dispatch, ownProps) {
  return {
    onChangeCurrentScrollElement: (val) => dispatch(changeCurrentScrollElement(val)),
    onLoadServiceProviders: () => dispatch(loadServiceProviders(ownProps.params.companyId)),
    onSaveServiceProvider: (id) => dispatch(saveServiceProvider(id)),
    onSetCleanState: () => dispatch(setCleanState()),
    onUpdateServiceProvider: (updatedServiceProvider) => dispatch(updateServiceProvider(updatedServiceProvider)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceProviderDetail);
