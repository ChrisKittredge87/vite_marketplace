export const path = 'companies';
export const name = 'Company Profile';

export default function (loadModule, errorLoading, injectReducers, injectSagas) {
  return {
    path: `${path}/:companyId`,
    name,
    getComponent(nextState, cb) {
      const importModules = Promise.all([
        import('containers/serviceProviders/Profile/reducer'),
        import('containers/serviceProviders/Profile/sagas'),
        import('containers/serviceProviders/Reviews/reducer'),
        import('containers/serviceProviders/Reviews/sagas'),
        import('containers/serviceProviders/Reviews/Dialog/reducer'),
        import('containers/serviceProviders/Reviews/Dialog/sagas'),
        import('containers/serviceProviders/MessageDialog/reducer'),
        import('containers/serviceProviders/MessageDialog/sagas'),
        import('containers/serviceProviders/SPImageUploadDialog/reducer'),
        import('containers/serviceProviders/SPImageUploadDialog/sagas'),
        import('containers/serviceProviders/Profile'),
      ]);

      const renderRoute = loadModule(cb);

      importModules.then(([
        reducer, sagas,
        reviewsReducer, reviewsSagas,
        reviewsDialogReducer, reviewsDialogSagas,
        messagesDialogReducer, messagesDialogSagas,
        SPImageUploadDialogReducer, SPImageUploadDialogSagas,
        component,
      ]) => {
        injectReducers([reducer, reviewsReducer, reviewsDialogReducer, messagesDialogReducer, SPImageUploadDialogReducer]);
        injectSagas([sagas, reviewsSagas, reviewsDialogSagas, messagesDialogSagas, SPImageUploadDialogSagas]);
        renderRoute(component);
      });
      importModules.catch(errorLoading);
    },
  };
}
