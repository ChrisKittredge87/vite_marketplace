/*
 *
 * ServiceProviderDetail constants
 *
 */


/* Actions */
export const CHANGE_CURRENT_SCROLL_ELEMENT = 'app/serviceProviders/Profile/CHANGE_CURRENT_SCROLL_ELEMENT';
export const LOAD_SERVICE_PROVIDERS = 'app/serviceProviders/Profile/LOAD_SERVICE_PROVIDERS';
export const LOAD_SERVICE_PROVIDERS_SUCCESS = 'app/serviceProviders/Profile/LOAD_SERVICE_PROVIDERS_SUCCESS';
export const LOAD_SERVICE_PROVIDERS_ERROR = 'app/serviceProviders/Profile/LOAD_SERVICE_PROVIDERS_ERROR';
export const SAVE_SERVICE_PROVIDER = 'app/serviceProviders/Profile/SAVE_SERVICE_PROVIDER';
export const SAVE_SERVICE_PROVIDER_SUCCESS = 'app/serviceProviders/Profile/SAVE_SERVICE_PROVIDER_SUCCESS';
export const SAVE_SERVICE_PROVIDER_ERROR = 'app/serviceProviders/Profile/SAVE_SERVICE_PROVIDER_ERROR';
export const SET_CLEAN_STATE = 'app/serviceProviders/Profile/SET_CLEAN_STATE';
export const UPDATE_SERVICE_PROVIDER = 'app/serviceProviders/Profile/UPDATE_SERVICE_PROVIDER';

/* State Keys */
export const CURRENT_SCROLL_ELEMENT = 'currentScrollElement';
export const ENTITY = 'entity';
export const ERROR = 'error';
export const LOADING = 'loading';
export const SERVICE_PROVIDER_IDS = 'serviceProviderIds';

/* Scroll Locations */
export const OVERVIEW_SCROLL = 'overview';
export const MENU_SCROLL = 'menu';
export const REVIEWS_SCROLL = 'reviews';
export const LOCATION_SCROLL = 'location';
