import { call, put, select, takeLatest } from 'redux-saga/effects';

import { BASE_URL } from 'entities/schemas/serviceProviders/constants';
import { selectEntity as selectServiceProvider } from 'entities/schemas/serviceProviders/selectors';
import request from 'utils/request';
import {
  serviceProvidersLoaded, serviceProvidersLoadingError,
  saveServiceProviderSuccess, saveServiceProviderError,
} from './actions';
import { LOAD_SERVICE_PROVIDERS, SAVE_SERVICE_PROVIDER } from './constants';

// Individual exports for testing
export function* getServiceProviders({ id }) {
  const requestUrl = `${BASE_URL}?companyId=${id}`;

  try {
    const serviceProviders = yield call(request.get, requestUrl);
    yield put(serviceProvidersLoaded(serviceProviders));
  } catch (err) {
    yield put(serviceProvidersLoadingError(err));
  }
}

export function* serviceProvidersData() {
  yield takeLatest(LOAD_SERVICE_PROVIDERS, getServiceProviders);
}

export function* saveServiceProvider({ id }) {
  const requestUrl = `${BASE_URL}/${id}`;
  const serviceProvider = yield select(selectServiceProvider(id));
  try {
    const serviceProviderUpdated = yield call(request.put, requestUrl, { data: serviceProvider.toJS() });
    yield put(saveServiceProviderSuccess(serviceProviderUpdated));
  } catch (err) {
    yield put(saveServiceProviderError(err));
  }
}

export function* saveServiceProviderData() {
  yield takeLatest(SAVE_SERVICE_PROVIDER, saveServiceProvider);
}

// All sagas to be loaded
export default [
  serviceProvidersData,
  saveServiceProviderData,
];

export const NAME = 'serviceProviderProfileSagas';
