/*
 *
 * ServiceProviderDetail actions
 *
 */

import { createSagaActions } from 'utils/sagas';
import {
  entity as serviceProviderSchema,
  list as serviceProvidersSchema,
} from 'entities/schemas/serviceProviders';
import {
  CHANGE_CURRENT_SCROLL_ELEMENT,
  LOAD_SERVICE_PROVIDERS, LOAD_SERVICE_PROVIDERS_SUCCESS, LOAD_SERVICE_PROVIDERS_ERROR,
  SAVE_SERVICE_PROVIDER, SAVE_SERVICE_PROVIDER_SUCCESS, SAVE_SERVICE_PROVIDER_ERROR,
  SET_CLEAN_STATE,
  UPDATE_SERVICE_PROVIDER,
} from './constants';

export function setCleanState() {
  return { type: SET_CLEAN_STATE };
}

export function changeCurrentScrollElement(val) {
  return { type: CHANGE_CURRENT_SCROLL_ELEMENT, val };
}

export function updateServiceProvider(updatedServiceProvider) {
  return { type: UPDATE_SERVICE_PROVIDER, entity: updatedServiceProvider, schema: serviceProviderSchema };
}

/* ----- Saga Actions ----- */

export const {
  loadEntity: loadServiceProviders,
  entityLoaded: serviceProvidersLoaded,
  entityLoadingError: serviceProvidersLoadingError,
} = createSagaActions({
  loadAction: LOAD_SERVICE_PROVIDERS,
  entityLoadedAction: LOAD_SERVICE_PROVIDERS_SUCCESS,
  entityLoadingErrorAction: LOAD_SERVICE_PROVIDERS_ERROR,
  schema: serviceProvidersSchema,
});

export const {
  loadEntity: saveServiceProvider,
  entityLoaded: saveServiceProviderSuccess,
  entityLoadingError: saveServiceProviderError,
} = createSagaActions({
  loadAction: SAVE_SERVICE_PROVIDER,
  entityLoadedAction: SAVE_SERVICE_PROVIDER_SUCCESS,
  entityLoadingErrorAction: SAVE_SERVICE_PROVIDER_ERROR,
  schema: serviceProviderSchema,
});

/* ----- END Saga Actions ----- */
