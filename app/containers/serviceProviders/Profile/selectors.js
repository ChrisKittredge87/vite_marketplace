import { createSelector } from 'reselect';

import { DOMAIN } from './reducer';
import { CURRENT_SCROLL_ELEMENT, LOADING } from './constants';

/**
 * Direct selector to the serviceProviderDetail state domain
 */
const selectServiceProviderProfileDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by ServiceProviderDetail
 */
const makeSelectCurrentScrollElement = () => createSelector(
  selectServiceProviderProfileDomain(),
  (substate) => substate.get(CURRENT_SCROLL_ELEMENT)
);

const makeSelectLoading = () => createSelector(
  selectServiceProviderProfileDomain(),
  (substate) => substate.get(LOADING),
);

export default selectServiceProviderProfileDomain;
export {
  selectServiceProviderProfileDomain,
  makeSelectCurrentScrollElement,
  makeSelectLoading,
};
