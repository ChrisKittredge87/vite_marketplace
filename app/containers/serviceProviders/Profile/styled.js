import styled from 'styled-components';
import Paper from 'material-ui/Paper';
import { LG } from 'globalConstants';

export const CenterColumn = styled.div`
  width: 55%;
  @media (max-width: ${LG}px) {
    width: 90%;
    margin: 0 5%;
  }
`;

export const FlexBaseline = styled.div`
  align-items: baseline;
  display: flex;
`;

export const FlexContainer = styled.div`
  display: flex;
`;

export const FlexColumns = styled.div`
  display: flex;
  flex-direction: column;
`;

export const FlexSpaceBetween = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const OuterColumn = styled.div`
  width: 20%;
  @media (min-width: ${LG}px) {
    width: 22.5%;
  }
`;

export const PaddedDiv = styled.div`
  padding: 25px 0;
`;

export const Paper75Centered = styled(Paper)`
  margin: 0 12.5%;
  width: 75%;
`;

export const Paper85Centered = styled(Paper)`
  margin: 0 7.5%;
  width: 85%;
`;

export const PositionMenuItem = styled.div`
  border: 1px solid darkgray;
  padding: 10px;
  cursor: pointer;
  border-bottom: ${({ last }) => !last ? 'none' : null};
  background: ${({ selected }) => selected ? 'gray' : 'white'};
  color: ${({ selected }) => selected ? 'white' : 'black'};
  transition: background-color 0.2s ease-in, color 0.2s ease-in;
  &:hover {
    background: ${({ selected }) => selected ? 'gray' : 'rgba(0, 0, 0, 0.09)'};
    color: ${({ selected }) => selected ? 'white' : 'black'};
  }
`;
