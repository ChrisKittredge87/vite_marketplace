/*
 * ServiceProviderDetail Messages
 *
 * This contains all the text for the ServiceProviderDetail component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  budget: {
    id: 'budget',
    defaultMessage: 'Budget',
  },
  guests: {
    id: 'guests',
    defaultMessage: 'Guests',
  },
  loading: {
    id: 'loading',
    defaultMessage: 'Loading',
  },
  location: {
    id: 'location',
    defaultMessage: 'Location',
  },
  overview: {
    id: 'overview',
    defaultMessage: 'Overview',
  },
  requestAQuote: {
    id: 'requestAQuote',
    defaultMessage: 'Request a quote',
  },
  reviews: {
    id: 'reviews',
    defaultMessage: 'Reviews',
  },
});
