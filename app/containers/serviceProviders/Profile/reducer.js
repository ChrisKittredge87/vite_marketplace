/*
 *
 * ServiceProviderDetail reducer
 *
 */

import { fromJS } from 'immutable';
import {
  /* Actions */
  CHANGE_CURRENT_SCROLL_ELEMENT,
  LOAD_SERVICE_PROVIDERS, LOAD_SERVICE_PROVIDERS_SUCCESS, LOAD_SERVICE_PROVIDERS_ERROR,
  SET_CLEAN_STATE,
  /* State Keys */
  CURRENT_SCROLL_ELEMENT,
  ERROR,
  LOADING,
  OVERVIEW_SCROLL,
  SERVICE_PROVIDER_IDS,
} from './constants';

const initialState = fromJS({
  [CURRENT_SCROLL_ELEMENT]: OVERVIEW_SCROLL,
  [LOADING]: false,
  [ERROR]: null,
  [SERVICE_PROVIDER_IDS]: fromJS([]),
});

function serviceProviderDetailReducer(state = initialState, action) {
  const { error, result, type, val } = action;
  switch (type) {
    case CHANGE_CURRENT_SCROLL_ELEMENT:
      return state.set(CURRENT_SCROLL_ELEMENT, val);
    case LOAD_SERVICE_PROVIDERS:
      return state.set(LOADING, true);
    case LOAD_SERVICE_PROVIDERS_SUCCESS:
      return state
        .set(LOADING, false)
        .set(SERVICE_PROVIDER_IDS, fromJS(result));
    case LOAD_SERVICE_PROVIDERS_ERROR:
      return state
        .set(LOADING, false)
        .set(ERROR, error);
    case SET_CLEAN_STATE:
      return initialState;
    default:
      return state;
  }
}

export default serviceProviderDetailReducer;

export const DOMAIN = 'serviceProviderDetail';
