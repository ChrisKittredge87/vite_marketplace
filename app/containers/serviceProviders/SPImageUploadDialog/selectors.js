import { createSelector } from 'reselect';
import { DOMAIN } from './reducer';

/**
 * Direct selector to the spimageUploadDialog state domain
 */
const selectSpimageUploadDialogDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by SpimageUploadDialog
 */

const makeSelectSpimageUploadDialog = () => createSelector(
  selectSpimageUploadDialogDomain(),
  (substate) => substate
);

export default makeSelectSpimageUploadDialog;
export {
  selectSpimageUploadDialogDomain,
};
