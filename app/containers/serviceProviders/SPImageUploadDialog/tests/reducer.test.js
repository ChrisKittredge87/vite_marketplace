
import { fromJS } from 'immutable';
import spimageUploadDialogReducer from '../reducer';

describe('spimageUploadDialogReducer', () => {
  it('returns the initial state', () => {
    expect(spimageUploadDialogReducer(undefined, {})).toEqual(fromJS({}));
  });
});
