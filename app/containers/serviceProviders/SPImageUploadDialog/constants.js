/*
 *
 * SpimageUploadDialog constants
 *
 */

/* Actions */
export const UPLOAD_PHOTOS = 'app/serviceProviders/SpimageUploadDialog/UPLOAD_PHOTOS';
export const UPLOAD_PHOTOS_SUCCESS = 'app/serviceProviders/SpimageUploadDialog/UPLOAD_PHOTOS_SUCCESS';
export const UPLOAD_PHOTOS_ERROR = 'app/serviceProviders/SpimageUploadDialog/UPLOAD_PHOTOS_ERROR';
export const SET_KEY_VAL = 'app/serviceProviders/SpimageUploadDialog/SET_KEY_VAL';

/* State Keys */
export const PHOTOS_UPLOADING = 'photosUploading';
