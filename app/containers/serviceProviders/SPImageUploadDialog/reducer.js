/*
 *
 * SpimageUploadDialog reducer
 *
 */

import { fromJS } from 'immutable';
import {
  /* State Keys */
  PHOTOS_UPLOADING,
  /* Actions */
  SET_KEY_VAL,
  UPLOAD_PHOTOS, UPLOAD_PHOTOS_SUCCESS, UPLOAD_PHOTOS_ERROR,
} from './constants';

const initialState = fromJS({});

function spimageUploadDialogReducer(state = initialState, action) {
  const { key, type, val } = action;
  switch (type) {
    case SET_KEY_VAL:
      return state.set(key, val);
    case UPLOAD_PHOTOS:
      return state.set(PHOTOS_UPLOADING, true);
    case UPLOAD_PHOTOS_SUCCESS:
      return state.set(PHOTOS_UPLOADING, false);
    case UPLOAD_PHOTOS_ERROR:
      return state.set(PHOTOS_UPLOADING, false);
    default:
      return state;
  }
}

export default spimageUploadDialogReducer;

export const DOMAIN = 'spImageUploadDialog';
