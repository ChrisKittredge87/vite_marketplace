/*
 *
 * SpimageUploadDialog actions
 *
 */

import {
  SET_KEY_VAL,
  UPLOAD_PHOTOS, UPLOAD_PHOTOS_SUCCESS, UPLOAD_PHOTOS_ERROR,
} from './constants';

export function defaultAction(key, val) {
  return { type: SET_KEY_VAL, key, val };
}

/* ----- Saga Actions ----- */

export function uploadPhotos(id, photos) { return { type: UPLOAD_PHOTOS, id, photos }; }
export function uploadPhotosSuccess() { return { type: UPLOAD_PHOTOS_SUCCESS }; }
export function uploadPhotosError(err) { return { type: UPLOAD_PHOTOS_ERROR, err }; }

/* ----- END Saga Actions ----- */
