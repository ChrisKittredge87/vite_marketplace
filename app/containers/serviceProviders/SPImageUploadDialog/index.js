/*
 *
 * SpimageUploadDialog
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import ImageUploadDialog from 'components/common/ImageUploadDialog';

import { uploadPhotos } from './actions';
import { PHOTOS_UPLOADING } from './constants';
import makeSelectSpimageUploadDialog from './selectors';

export class SpimageUploadDialog extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.handleUploadPhotos = this.handleUploadPhotos.bind(this);
  }

  handleUploadPhotos(photos) {
    const { onUploadPhotos } = this.props;
    onUploadPhotos(1, photos);
  }

  render() {
    const photosUploading = this.props.SPImageUploadDialog.get(PHOTOS_UPLOADING);
    return (
      <div>
        <ImageUploadDialog
          {...this.props}
          onUploadPhotos={this.handleUploadPhotos}
          photosUploading={photosUploading}
        />
      </div>
    );
  }
}

SpimageUploadDialog.propTypes = {
  SPImageUploadDialog: PropTypes.object,
  onUploadPhotos: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  SPImageUploadDialog: makeSelectSpimageUploadDialog(),

});

function mapDispatchToProps(dispatch) {
  return {
    onUploadPhotos: (id, photos) => dispatch(uploadPhotos(id, photos)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SpimageUploadDialog);
