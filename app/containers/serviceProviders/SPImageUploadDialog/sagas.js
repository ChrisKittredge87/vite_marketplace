import { call, put, takeLatest } from 'redux-saga/effects';
import { BASE_URL } from 'entities/schemas/serviceProviders/constants';
import axios from 'axios';
import _ from 'lodash';

import { uploadPhotosSuccess, uploadPhotosError } from './actions';
import { UPLOAD_PHOTOS } from './constants';

// Individual exports for testing
export function* uploadPhotos({ id, photos }) {
  const requestUrl = `${BASE_URL}/${id}/photos`;
  try {
    const formData = new FormData();
    _.each(photos, (img) => {
      formData.append('photos', img.file);
    });
    yield call(axios.post, requestUrl, formData);
    yield put(uploadPhotosSuccess());
  } catch (err) {
    yield put(uploadPhotosError(err));
  }
}

export function* uploadPhotosData() {
  yield takeLatest(UPLOAD_PHOTOS, uploadPhotos);
}

// All sagas to be loaded
export default [
  uploadPhotosData,
];
