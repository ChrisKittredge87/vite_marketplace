/*
 * Reviews Messages
 *
 * This contains all the text for the Reviews component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  reviews: {
    id: 'reviews',
    defaultMessage: 'Reviews',
  },
  showAll: {
    id: 'showAll',
    defaultMessage: 'Show All',
  },
});
