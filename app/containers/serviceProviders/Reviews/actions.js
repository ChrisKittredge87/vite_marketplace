/*
 *
 * Reviews actions
 *
 */

import { list as reviewsListEntity } from 'entities/schemas/serviceProviders/reviews';
import { createSagaActions } from 'utils/sagas';

import {
  LOAD_TOP_REVIEWS, TOP_REVIEWS_LOADED, TOP_REVIEWS_LOADING_ERROR,
  SET_KEY_VAL,
} from './constants';

export function setKeyVal(key, val) {
  return { type: SET_KEY_VAL, key, val };
}

export const {
  loadEntity: loadTopReviews,
  entityLoaded: topReviewsLoaded,
  entityLoadingError: topReviewsLoadingError,
} = createSagaActions({
  loadAction: LOAD_TOP_REVIEWS,
  entityLoadedAction: TOP_REVIEWS_LOADED,
  entityLoadingErrorAction: TOP_REVIEWS_LOADING_ERROR,
  schema: reviewsListEntity,
});
