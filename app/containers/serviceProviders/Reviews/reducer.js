/*
 *
 * Reviews reducer
 *
 */

import { fromJS } from 'immutable';

import {
  /* Actions */
  LOAD_TOP_REVIEWS, TOP_REVIEWS_LOADED, TOP_REVIEWS_LOADING_ERROR,
  SET_KEY_VAL,
  /* State Keys */
  DIALOG_OPEN,
  ERROR,
  IDS,
  LOADING,
} from './constants';

const initialState = fromJS({
  [DIALOG_OPEN]: false,
  [IDS]: [],
});

function reviewsReducer(state = initialState, action) {
  const { error, key, result, val } = action;
  switch (action.type) {
    case LOAD_TOP_REVIEWS:
      return state.set(LOADING, true);
    case SET_KEY_VAL:
      return state.set(key, val);
    case TOP_REVIEWS_LOADED:
      return state.set(LOADING, false).set(IDS, fromJS(result || []));
    case TOP_REVIEWS_LOADING_ERROR:
      return state.set(LOADING, false).set(ERROR, error);
    default:
      return state;
  }
}

export default reviewsReducer;

export const DOMAIN = 'serviceProviderReviews';
