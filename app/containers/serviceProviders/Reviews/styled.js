import styled from 'styled-components';

export const PaddedDiv = styled.div`
  padding: 25px 0;
`;

export const SpaceBetweenFlex = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
`;
