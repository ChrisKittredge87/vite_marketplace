import { createSelector } from 'reselect';
import makeSelectEntitiesDomain from 'entities/selectors';
import { SCHEMA_KEY } from 'entities/schemas/serviceProviders/reviews/constants';
import { DOMAIN } from './reducer';
import {
  DIALOG_OPEN,
  IDS,
  LOADING,
} from './constants';

/**
 * Direct selector to the reviews state domain
 */
const selectReviewsDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by Reviews
 */

const makeSelectReviewsState = () => createSelector(
  selectReviewsDomain(),
  (substate) => substate
);

const makeSelectDialogOpen = () => createSelector(
  selectReviewsDomain(),
  (substate) => substate.get(DIALOG_OPEN)
);

const makeSelectSPReviewsLoading = () => createSelector(
  selectReviewsDomain(),
  (substate) => substate.get(LOADING)
);


const makeSelectTopSPReviews = () => createSelector(
  selectReviewsDomain(),
  makeSelectEntitiesDomain(),
  (reviewsDomain, entities) =>
    reviewsDomain.get(IDS).map((reviewId) => entities.getIn([SCHEMA_KEY, `${reviewId}`]))
);

const makeSelectTopSPReviewsLoading = () => createSelector(
  selectReviewsDomain(),
  (substate) => substate.get(LOADING)
);

export default makeSelectReviewsState;
export {
  selectReviewsDomain,
  makeSelectDialogOpen,
  makeSelectSPReviewsLoading,
  makeSelectTopSPReviews,
  makeSelectTopSPReviewsLoading,
};
