/*
 *
 * Dialog
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import Pagination from 'rc-pagination';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import CircularProgress from 'material-ui/CircularProgress';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import SearchIcon from 'material-ui/svg-icons/action/search';
import TextField from 'material-ui/TextField';
import _ from 'lodash';

import Review from 'components/common/Review';
import { ID, USER } from 'entities/schemas/serviceProviders/reviews/constants';
import { selectEntity as selectUser } from 'entities/schemas/users/selectors';
import { SpaceBetweenFlex } from 'styled/common';

import { clearState, loadReviews, setKeyVal } from './actions';
import {
  HIGHLIGHT_WORDS,
  LOADING,
  PAGE, PER_PAGE,
  SEARCH_TEXT,
  TOTAL_REVIEWS,
} from './constants';
import messages from './messages';
import makeSelectDialog, { makeSelectReviews } from './selectors';

export class ReviewsDialog extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
    this.renderContent = this.renderContent.bind(this);
  }

  /* ----- React Lifecyle ----- */

  componentWillReceiveProps(nextProps) {
    const { dialogState, onLoadReviews, onClearState, open, serviceProviderId } = this.props;
    const { dialogState: nextDialogState, open: nextOpen } = nextProps;
    if (nextOpen) {
      const nextSearchText = nextDialogState.get(SEARCH_TEXT);
      const nextPage = nextDialogState.get(PAGE);
      if (nextOpen !== open || nextSearchText !== dialogState.get(SEARCH_TEXT) || nextPage !== dialogState.get(PAGE)) {
        onLoadReviews(serviceProviderId, nextSearchText);
      }
    } else if (open && !nextOpen) {
      onClearState();
    }
  }

  /* ----- END React Lifecyle ----- */

  /* ----- Event Binding ----- */

  handlePageChange(page) {
    const { onSetKeyVal } = this.props;
    onSetKeyVal(PAGE, page);
  }

  handleSearchTextChange(event) {
    const { onSetKeyVal } = this.props;
    onSetKeyVal(SEARCH_TEXT, event.target.value);
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  renderLoading() {
    return (<div style={{ height: 200, display: 'flex', alignItems: 'center', jusifyContent: 'center' }}>
      <div style={{ width: '100%' }}>
        <div style={{ textAlign: 'center' }}>
          <CircularProgress size={150} thickness={8} />
        </div>
      </div>
    </div>);
  }

  renderContent() {
    const { dialogState, onSelectUser, reviews } = this.props;
    return (<div>
      <div style={{ position: 'relative' }}>
        <TextField
          fullWidth
          hintText={<FormattedMessage {...messages.searchElipses} />}
          onChange={this.handleSearchTextChange}
          value={dialogState.get(SEARCH_TEXT)}
        />
        <SearchIcon style={{ position: 'absolute', right: 4, top: 10, color: 'gray' }} />
      </div>
      {dialogState.get(LOADING) && reviews.size === 0 ? this.renderLoading() :
        reviews.map((review) =>
          <Review
            key={`review_${review.get(ID)}`}
            review={review}
            user={onSelectUser(review.get(USER))}
            highlightWords={dialogState.get(HIGHLIGHT_WORDS)}
          />)}
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <Pagination
          current={dialogState.get(PAGE)}
          onChange={this.handlePageChange}
          pageSize={dialogState.get(PER_PAGE)}
          total={dialogState.get(TOTAL_REVIEWS)}
        />
      </div>
    </div>);
  }

  render() {
    const { onRequestClose, open, width } = this.props;
    const dialogHeader = (
      <SpaceBetweenFlex style={{ padding: '5px 20px' }}>
        <h3 style={{ margin: 0 }}>
          <FormattedMessage {...messages.reviews} />
        </h3>
        <IconButton onClick={onRequestClose}>
          <CloseIcon />
        </IconButton>
      </SpaceBetweenFlex>
    );
    return (<Dialog
      actions={[<FlatButton label="close" onClick={onRequestClose} />]}
      autoScrollBodyContent
      contentStyle={{ maxWidth: 'none', width }}
      modal
      open={open}
      bodyStyle={{ minHeight: 700 }}
      title={dialogHeader}
    >
      {this.renderContent()}
    </Dialog>);
  }

  /* ----- END Rendering ----- */
}

ReviewsDialog.propTypes = {
  dialogState: PropTypes.object,
  onClearState: PropTypes.func,
  onLoadReviews: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  onSelectUser: PropTypes.func.isRequired,
  onSetKeyVal: PropTypes.func.isRequired,
  open: PropTypes.bool,
  reviews: PropTypes.object,
  serviceProviderId: PropTypes.string.isRequired,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

ReviewsDialog.defaultProps = {
  width: '75%',
};

const mapStateToProps = createStructuredSelector({
  dialogState: makeSelectDialog(),
  onSelectUser: (state) => (id) => selectUser(id)(state),
  reviews: makeSelectReviews(),
});

function mapDispatchToProps(dispatch) {
  return {
    onClearState: () => dispatch(clearState()),
    onLoadReviews: _.debounce((serviceProviderId, search) => dispatch(loadReviews(serviceProviderId, { search })), 250),
    onSetKeyVal: (key, val) => dispatch(setKeyVal(key, val)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ReviewsDialog);
