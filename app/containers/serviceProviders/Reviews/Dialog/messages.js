/*
 * Dialog Messages
 *
 * This contains all the text for the Dialog component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  reviews: {
    id: 'reviews',
    defaultMessage: 'Reviews',
  },
  searchElipses: {
    id: 'searchElipses',
    defaultMessage: 'Search...',
  },
});
