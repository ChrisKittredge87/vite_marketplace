
import { fromJS } from 'immutable';
import dialogReducer from '../reducer';

describe('dialogReducer', () => {
  it('returns the initial state', () => {
    expect(dialogReducer(undefined, {})).toEqual(fromJS({}));
  });
});
