/*
 *
 * Dialog reducer
 *
 */

import { fromJS } from 'immutable';
import _ from 'lodash';

import {
  /* Actions */
  CLEAR_STATE,
  SET_KEY_VAL,
  LOAD_REVIEWS, REVIEWS_LOADED, REVIEWS_LOADING_ERROR,
  /* State Keys */
  ERROR,
  HIGHLIGHT_WORDS,
  IDS,
  LOADING,
  PAGE, PER_PAGE, TOTAL_REVIEWS,
  SEARCH_TEXT,
} from './constants';

const initialState = fromJS({
  [HIGHLIGHT_WORDS]: [],
  [LOADING]: false,
  [PAGE]: 1,
  [PER_PAGE]: 10,
  [IDS]: [],
  [SEARCH_TEXT]: '',
  [TOTAL_REVIEWS]: 0,
});

function dialogReducer(state = initialState, action) {
  const { error, key, result, type, val } = action;
  switch (type) {
    case CLEAR_STATE:
      return initialState;
    case SET_KEY_VAL:
      return state.set(key, val);
    case LOAD_REVIEWS:
      return state.set(LOADING, true);
    case REVIEWS_LOADED:
      return state
        .set(LOADING, false)
        .set(TOTAL_REVIEWS, 10)
        .set(HIGHLIGHT_WORDS, _.filter((state.get(SEARCH_TEXT) || '').split(/\s/), (word) => word))
        .set(IDS, fromJS(result));
    case REVIEWS_LOADING_ERROR:
      return state.set(LOADING, false).set(ERROR, error);
    default:
      return state;
  }
}

export default dialogReducer;

export const DOMAIN = 'reviewsDialog';
