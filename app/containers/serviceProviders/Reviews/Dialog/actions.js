/*
 *
 * Dialog actions
 *
 */

import { list as reviewsListEntity } from 'entities/schemas/serviceProviders/reviews';
import { createSagaActions } from 'utils/sagas';

import {
  CLEAR_STATE,
  SET_KEY_VAL,
  LOAD_REVIEWS, REVIEWS_LOADED, REVIEWS_LOADING_ERROR,
} from './constants';

export function clearState() {
  return { type: CLEAR_STATE };
}

export function setKeyVal(key, val) {
  return { type: SET_KEY_VAL, key, val };
}

export const {
  loadEntity: loadReviews,
  entityLoaded: reviewsLoaded,
  entityLoadingError: reviewsLoadingError,
} = createSagaActions({
  loadAction: LOAD_REVIEWS,
  entityLoadedAction: REVIEWS_LOADED,
  entityLoadingErrorAction: REVIEWS_LOADING_ERROR,
  schema: reviewsListEntity,
});
