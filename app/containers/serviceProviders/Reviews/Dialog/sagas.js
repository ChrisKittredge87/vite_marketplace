import { call, select, takeLatest, put } from 'redux-saga/effects';

import { BASE_URL } from 'entities/schemas/serviceProviders/constants';
import request from 'utils/request';
import { reviewsLoaded, reviewsLoadingError } from './actions';
import { LOAD_REVIEWS, PAGE, perPage } from './constants';
import makeSelectReviewsState from './selectors';

export function* getReviews({ id, options }) {
  try {
    const reviewsState = yield select(makeSelectReviewsState());
    const page = reviewsState.get(PAGE);
    const opt = { data: { page, per_page: perPage } };
    if (options.search) {
      opt.data.search = options.search;
    }
    const requestUrl = `${BASE_URL}/${id}/reviews`;
    const reviews = yield call(request.get, requestUrl, opt);
    yield put(reviewsLoaded(reviews));
  } catch (err) {
    yield put(reviewsLoadingError(err));
  }
}

export function* reviewsData() {
  yield takeLatest(LOAD_REVIEWS, getReviews);
}

// All sagas to be loaded
export default [
  reviewsData,
];
