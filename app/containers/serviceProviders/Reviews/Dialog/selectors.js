import { createSelector } from 'reselect';
import makeSelectEntitiesDomain from 'entities/selectors';
import { SCHEMA_KEY } from 'entities/schemas/serviceProviders/reviews/constants';

import { IDS } from './constants';
import { DOMAIN } from './reducer';

/**
 * Direct selector to the dialog state domain
 */
const selectDialogDomain = () => (state) => state.get(DOMAIN);


const makeSelectDialog = () => createSelector(
  selectDialogDomain(),
  (substate) => substate
);

const makeSelectReviews = () => createSelector(
  selectDialogDomain(),
  makeSelectEntitiesDomain(),
  (dialogDomain, entities) =>
    dialogDomain.get(IDS).map((reviewId) =>
      entities.getIn([SCHEMA_KEY, `${reviewId}`]))
);

export default makeSelectDialog;
export {
  selectDialogDomain,
  makeSelectReviews,
};
