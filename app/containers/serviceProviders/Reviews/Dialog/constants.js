/*
 *
 * Dialog constants
 *
 */

/* Actions */
export const CLEAR_STATE = 'app/containers/promoterPortal/serviceProviders/Reviews/CLEAR_STATE';
export const SET_KEY_VAL = 'app/containers/promoterPortal/serviceProviders/Reviews/SET_KEY_VAL';
export const LOAD_REVIEWS = 'app/containers/promoterPortal/serviceProviders/Reviews/Dialog/LOAD_REVIEWS';
export const REVIEWS_LOADED = 'app/containers/promoterPortal/serviceProviders/Reviews/Dialog/REVIEWS_LOADED';
export const REVIEWS_LOADING_ERROR = 'app/containers/promoterPortal/serviceProviders/Reviews/Dialog/REVIEWS_LOADING_ERROR';

/* State Keys */
export const ERROR = 'error';
export const HIGHLIGHT_WORDS = 'highlightWords';
export const LOADING = 'loading';
export const PAGE = 'page';
export const PAGE_COUNT = 'pageCount';
export const PER_PAGE = 'pergPage';
export const IDS = 'ids';
export const SEARCH_TEXT = 'searchText';
export const TOTAL_REVIEWS = 'totalReviews';

/* Defaults */
export const perPage = 10;
