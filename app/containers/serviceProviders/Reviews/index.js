/*
 *
 * Reviews
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import CircularProgress from 'material-ui/CircularProgress';
import FlatButton from 'material-ui/FlatButton';

import Review from 'components/common/Review';
import { ID, USER } from 'entities/schemas/serviceProviders/reviews/constants';
import { selectEntity as selectUser } from 'entities/schemas/users/selectors';

import ReviewsDialog from './Dialog';
import {
  loadTopReviews,
  setKeyVal,
} from './actions';
import { DIALOG_OPEN } from './constants';
import { PaddedDiv, SpaceBetweenFlex } from './styled';
import makeSelectReviewsState, {
  makeSelectDialogOpen,
  makeSelectTopSPReviews,
  makeSelectTopSPReviewsLoading,
} from './selectors';
import messages from './messages';

export class Reviews extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.toggleDialogOpen = this.toggleDialogOpen.bind(this);
  }

  /* ------ React Lifecycle ----- */

  componentWillMount() {
    const { onLoadTopReviews, serviceProviderId } = this.props;
    if (!!serviceProviderId) { onLoadTopReviews(serviceProviderId); }
  }

  componentWillReceiveProps(nextProps) {
    const { onLoadTopReviews, serviceProviderId } = this.props;
    const { serviceProviderId: nextServiceProviderId } = nextProps;
    if (nextServiceProviderId !== serviceProviderId) {
      onLoadTopReviews(nextServiceProviderId);
    }
  }

  /* ------ END React Lifecycle ----- */

  /* ------ Event Binding ----- */

  toggleDialogOpen() {
    const { dialogOpen, onSetKeyVal } = this.props;
    onSetKeyVal(DIALOG_OPEN, !dialogOpen);
  }

  /* ----- Rendering ----- */

  renderLoading() {
    return (<div style={{ height: 200, display: 'flex', alignItems: 'center', jusifyContent: 'center' }}>
      <div style={{ width: '100%' }}>
        <div style={{ textAlign: 'center' }}>
          <CircularProgress size={150} thickness={8} />
        </div>
      </div>
    </div>);
  }

  render() {
    const { dialogOpen, dialogWidth, onSelectUser, serviceProviderId, topReviews, topReviewsLoading } = this.props;
    return (
      <div>
        <PaddedDiv>
          <SpaceBetweenFlex>
            <h2><FormattedMessage {...messages.reviews} /></h2>
            <FlatButton
              label={<FormattedMessage {...messages.showAll} />}
              onClick={this.toggleDialogOpen}
            />
          </SpaceBetweenFlex>
          {topReviewsLoading ? this.renderLoading() :
            topReviews.map((review) => <Review key={`review_${review.get(ID)}`} review={review} user={onSelectUser(review.get(USER))} />)}
        </PaddedDiv>
        <ReviewsDialog
          open={dialogOpen}
          onRequestClose={this.toggleDialogOpen}
          serviceProviderId={serviceProviderId}
          width={dialogWidth}
        />
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

Reviews.propTypes = {
  dialogOpen: PropTypes.bool,
  dialogWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onLoadTopReviews: PropTypes.func,
  onSelectUser: PropTypes.func,
  onSetKeyVal: PropTypes.func,
  serviceProviderId: PropTypes.string.isRequired,
  topReviews: PropTypes.object,
  topReviewsLoading: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  dialogOpen: makeSelectDialogOpen(),
  onSelectUser: (state) => (id) => selectUser(id)(state),
  reviewsState: makeSelectReviewsState(),
  topReviews: makeSelectTopSPReviews(),
  topReviewsLoading: makeSelectTopSPReviewsLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    onLoadTopReviews: (serviceProviderId) => dispatch(loadTopReviews(serviceProviderId)),
    onSetKeyVal: (key, val) => dispatch(setKeyVal(key, val)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Reviews);
