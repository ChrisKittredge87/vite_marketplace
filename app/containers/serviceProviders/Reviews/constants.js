/*
 *
 * Reviews constants
 *
 */

/* Actions */
export const SET_KEY_VAL = 'app/containers/promoterPortal/serviceProviders/Reviews/SET_KEY_VAL';
export const LOAD_TOP_REVIEWS = 'app/containers/promoterPortal/serviceProviders/Reviews/LOAD_TOP_REVIEWS';
export const TOP_REVIEWS_LOADED = 'app/containers/promoterPortal/serviceProviders/Reviews/TOP_REVIEWS_LOADED';
export const TOP_REVIEWS_LOADING_ERROR = 'app/containers/promoterPortal/serviceProviders/Reviews/TOP_REVIEWS_LOADING_ERROR';

/* State Keys */
export const DIALOG_OPEN = 'dialogOpen';
export const ERROR = 'error';
export const IDS = 'ids';
export const LOADING = 'loading';

/* Defaults */
export const perPage = 3;
