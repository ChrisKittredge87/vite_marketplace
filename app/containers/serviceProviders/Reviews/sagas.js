import { getEntitiesSaga as createGetReviewsSagas } from 'entities/schemas/serviceProviders/reviews/sagas';

import { topReviewsLoaded, topReviewsLoadingError } from './actions';
import { LOAD_TOP_REVIEWS } from './constants';

const reviewsData = createGetReviewsSagas({
  actionKey: LOAD_TOP_REVIEWS,
  successAction: topReviewsLoaded,
  errorAction: topReviewsLoadingError,
});

// All sagas to be loaded
export default [
  reviewsData,
];

export const NAME = 'serviceProviderReviewSagas';
