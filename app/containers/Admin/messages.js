/*
 * Users Messages
 *
 * This contains all the text for the Users component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  adminPortal: {
    id: 'adminPortal',
    defaultMessage: 'Admin Portal',
  },
  companies: {
    id: 'companies',
    defaultMessage: 'Companies',
  },
  users: {
    id: 'app.containers.serviceProviders.admin.Users.users',
    defaultMessage: 'Users',
  },
});
