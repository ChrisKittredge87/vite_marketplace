/*
 *
 * Users actions
 *
 */

import { createSagaActions } from 'utils/sagas';
import { list as usersSchema, entity as userSchema } from 'entities/schemas/users';

import {
  CREATE_USER, CREATE_USER_SUCCESS, CREATE_USER_ERROR,
  LOAD_USERS, LOAD_USERS_SUCCESS, LOAD_USERS_ERROR,
  UPDATE_USER, UPDATE_USER_SUCCESS, UPDATE_USER_ERROR,
  DELETE_USER, DELETE_USER_SUCCESS, DELETE_USER_ERROR,
  SET_KEY_VAL,
} from './constants';

export function defaultAction(key, val) {
  return { type: SET_KEY_VAL, key, val };
}

export const {
  loadEntity: loadUsers,
  entityLoaded: loadUsersSuccess,
  entityLoadingError: loadUsersError,
} = createSagaActions({
  loadAction: LOAD_USERS,
  entityLoadedAction: LOAD_USERS_SUCCESS,
  entityLoadingErrorAction: LOAD_USERS_ERROR,
  schema: usersSchema,
});

export const {
  loadEntity: createUser,
  entityLoaded: createUserSuccess,
  entityLoadingError: createUserError,
} = createSagaActions({
  loadAction: CREATE_USER,
  entityLoadedAction: CREATE_USER_SUCCESS,
  entityLoadingErrorAction: CREATE_USER_ERROR,
  schema: userSchema,
});

export const {
  loadEntity: updateUser,
  entityLoaded: updateUserSuccess,
  entityLoadingError: updateUserError,
} = createSagaActions({
  loadAction: UPDATE_USER,
  entityLoadedAction: UPDATE_USER_SUCCESS,
  entityLoadingErrorAction: UPDATE_USER_ERROR,
  schema: userSchema,
});

export const {
  loadEntity: deleteUser,
  entityLoaded: deleteUserSuccess,
  entityLoadingError: deleteUserError,
} = createSagaActions({
  loadAction: DELETE_USER,
  entityLoadedAction: DELETE_USER_SUCCESS,
  entityLoadingErrorAction: DELETE_USER_ERROR,
});
