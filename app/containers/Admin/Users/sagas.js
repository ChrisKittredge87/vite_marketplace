import { takeLatest, call, put } from 'redux-saga/effects';

import { BASE_URL } from 'entities/schemas/users/constants';
import request from 'utils/request';

import {
  createUserSuccess, createUserError,
  deleteUserSuccess, deleteUserError,
  loadUsersSuccess, loadUsersError,
  updateUserSuccess, updateUserError,
} from './actions';
import { CREATE_USER, LOAD_USERS, UPDATE_USER, DELETE_USER } from './constants';

// Individual exports for testing
export function* getUsers() {
  const requestUrl = `${BASE_URL}`;
  try {
    const users = yield call(request.get, requestUrl);
    yield put(loadUsersSuccess(users));
  } catch (err) {
    yield put(loadUsersError(err));
  }
}

export function* getUsersData() {
  yield takeLatest(LOAD_USERS, getUsers);
}

export function* createUser({ options: { user } }) {
  const requestUrl = `${BASE_URL}`;
  try {
    const newUser = yield call(request.post, requestUrl, { data: { user } });
    yield put(createUserSuccess(newUser));
  } catch (err) {
    yield put(createUserError(err));
  }
}

export function* createUserData() {
  yield takeLatest(CREATE_USER, createUser);
}

export function* updateUser({ id, options: { user } }) {
  const requestUrl = `${BASE_URL}/${id}`;
  try {
    const updatedUser = yield call(request.put, requestUrl, { data: { user } });
    yield put(updateUserSuccess(updatedUser));
  } catch (err) {
    yield put(updateUserError(err));
  }
}

export function* updateUserData() {
  yield takeLatest(UPDATE_USER, updateUser);
}

export function* deleteUser({ id }) {
  const requestUrl = `${BASE_URL}/${id}`;
  try {
    yield call(request.delete, requestUrl);
    yield put(deleteUserSuccess({ id }));
  } catch (err) {
    yield put(deleteUserError(err));
  }
}

export function* deleteUserData() {
  yield takeLatest(DELETE_USER, deleteUser);
}

// All sagas to be loaded
export default [
  getUsersData,
  createUserData,
  updateUserData,
  deleteUserData,
];
