import { createSelector } from 'reselect';

import makeSelectEntitiesDomain from 'entities/selectors';
import { SCHEMA_KEY as USERS } from 'entities/schemas/users/constants';

import { USER_IDS } from './constants';
import { DOMAIN } from './reducer';

/**
 * Direct selector to the users state domain
 */
const selectUsersDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by Users
 */

const makeSelectUsers = () => createSelector(
  selectUsersDomain(),
  makeSelectEntitiesDomain(),
  (usersDomain, entities) =>
    usersDomain.get(USER_IDS).map((id) => entities.getIn([USERS, `${id}`]))
);

export default makeSelectUsers;
export {
  selectUsersDomain,
};
