/*
 * Users Messages
 *
 * This contains all the text for the Users component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  admin: {
    id: 'admin',
    defaultMessage: 'Admin',
  },
  cancel: {
    id: 'cancel',
    defaultMessage: 'Cancel',
  },
  delete: {
    id: 'delete',
    defaultMessage: 'Delete',
  },
  edit: {
    id: 'edit',
    defaultMessage: 'Edit',
  },
  email: {
    id: 'email',
    defaultMessage: 'Email',
  },
  firstName: {
    id: 'firstName',
    defaultMessage: 'First Name',
  },
  lastName: {
    id: 'lastName',
    defaultMessage: 'Last Name',
  },
  managedUsers: {
    id: 'managedUsers',
    defaultMessage: 'Managed Users',
  },
  manager: {
    id: 'manager',
    defaultMessage: 'Manager',
  },
  save: {
    id: 'save',
    defaultMessage: 'Save',
  },
});
