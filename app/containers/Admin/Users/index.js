/*
 *
 * Users
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import Avatar from 'material-ui/Avatar';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import PersonOutline from 'material-ui/svg-icons/social/person-outline';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';
import EditIcon from 'material-ui/svg-icons/image/edit';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import IconButton from 'material-ui/IconButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Subheader from 'material-ui/Subheader';
import { fromJS } from 'immutable';
import _ from 'lodash';

import { makeSelectCurrentUser } from 'containers/Auth/selectors';
import {
  ID,
  EMAIL,
  FIRST_NAME,
  LAST_NAME,
  IS_ADMIN,
  MANAGED_USERS,
  MANAGER_ID,
} from 'entities/schemas/users/constants';

// TODO: figure out why relative imports don't work for this file
import { createUser, loadUsers, updateUser, deleteUser } from './actions';
import messages from './messages';
import makeSelectUsers from './selectors';

export class Users extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = {
      userDialogOpen: false,
      selectedUser: null,
      formFirstName: '',
      formLastName: '',
      formEmail: '',
      formManagerId: null,
      formIsAdmin: false,
    };

    this.handleCloseConfirmDialog = this.handleCloseConfirmDialog.bind(this);
    this.handleCloseUserDialog = this.handleCloseUserDialog.bind(this);
    this.handleDeleteUser = this.handleDeleteUser.bind(this);
    this.handleOpenConfirmDialog = this.handleOpenConfirmDialog.bind(this);
    this.handleOpenUserDialog = this.handleOpenUserDialog.bind(this);
    this.handleSaveUser = this.handleSaveUser.bind(this);
    this.handleSetManagerId = this.handleSetManagerId.bind(this);
    this.renderConfirmDialog = this.renderConfirmDialog.bind(this);
    this.renderUserDialog = this.renderUserDialog.bind(this);
    this.renderUsersList = this.renderUsersList.bind(this);
    this.renderUserListItems = this.renderUserListItems.bind(this);
  }

  /* ----- Lifecycle ----- */

  componentWillMount() {
    const { onLoadUsers } = this.props;
    onLoadUsers();
  }

  componentWillReceiveProps(nextProps) {
    const { currentUser } = this.props;
    const { currentUser: nextCurrentUser } = nextProps;
    if (nextCurrentUser && !nextCurrentUser.equals(currentUser)) {
      this.setState({ formManagerId: nextCurrentUser.get(ID) });
    }
  }

  /* ----- END Lifecycle ----- */

  /* ----- Event Binding ----- */

  handleChangeFormData(key) {
    return ({ target: { value } }, isToggled, newVal) => {
      let val = value;
      if (_.isBoolean(isToggled)) {
        val = isToggled;
      } else if (_.isNumber(isToggled)) {
        val = newVal;
      }
      this.setState({ [key]: val });
    };
  }

  handleCloseConfirmDialog() {
    this.setState({ confirmDialogOpen: false, selectedUser: null });
  }

  handleCloseUserDialog() {
    this.setState({
      userDialogOpen: false,
      formFirstName: '',
      formLastName: '',
      formEmail: '',
      formManagerId: null,
      formIsAdmin: false,
    });
  }

  handleDeleteUser() {
    const { onDeleteUser } = this.props;
    const { selectedUser } = this.state;
    onDeleteUser(selectedUser.get(ID));
    this.handleCloseConfirmDialog();
  }

  handleOpenConfirmDialog(selectedUser) {
    return () => this.setState({ confirmDialogOpen: true, selectedUser });
  }

  handleOpenUserDialog(selectedUser) {
    return () => this.setState({
      userDialogOpen: true,
      selectedUser,
      formFirstName: selectedUser ? selectedUser.get(FIRST_NAME) : '',
      formLastName: selectedUser ? selectedUser.get(LAST_NAME) : '',
      formEmail: selectedUser ? selectedUser.get(EMAIL) : '',
      formManagerId: selectedUser ? selectedUser.get(MANAGER_ID) : this.props.currentUser.get(ID),
      formIsAdmin: selectedUser ? selectedUser.get(IS_ADMIN) : false,
    });
  }

  handleSaveUser() {
    const { onCreateUser, onUpdateUser } = this.props;
    const { formFirstName, formLastName, formEmail, formManagerId, formIsAdmin, selectedUser } = this.state;
    const user = {
      firstName: formFirstName,
      lastName: formLastName,
      email: formEmail,
      managerId: formManagerId,
      isAdmin: formIsAdmin,
    };
    if (selectedUser) {
      onUpdateUser(selectedUser.get(ID), user);
    } else {
      onCreateUser(user);
    }
    this.handleCloseUserDialog();
  }

  handleSetManagerId(event, idx, managerId) {
    const { currentUser } = this.props;
    this.setState({ formManagerId: managerId || currentUser.get(ID) });
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  renderConfirmDialog() {
    const { selectedUser } = this.state;
    const actions = [
      <FlatButton
        key="confirm_modal_cancel_btn"
        label={<FormattedMessage {...messages.cancel} />}
        onTouchTap={this.handleCloseConfirmDialog}
        secondary
      />,
      <FlatButton
        key="confirm_modal_save_btn"
        label={<FormattedMessage {...messages.delete} />}
        onTouchTap={this.handleDeleteUser}
        primary
      />,
    ];

    return (<Dialog
      title={'Confirm'}
      actions={actions}
      modal
      open={!!this.state.confirmDialogOpen}
    >
      <div>{
          selectedUser ?
            `Are you sure you want to delete ${selectedUser.get(FIRST_NAME)} ${selectedUser.get(LAST_NAME)} (${selectedUser.get(EMAIL)})?` :
            'Are you sure?'
          }
      </div>
    </Dialog>);
  }

  renderUserDialog() {
    const { users } = this.props;
    const { formFirstName, formLastName, formEmail, formManagerId, formIsAdmin } = this.state;
    const selectedUser = this.state.selectedUser || fromJS({});
    const userHasManagedUsers = users.filter((user) => user.get(MANAGER_ID) === selectedUser.get(ID)).size > 0;
    const actions = [
      <FlatButton
        key="user_modal_cancel_btn"
        label={<FormattedMessage {...messages.cancel} />}
        onTouchTap={this.handleCloseUserDialog}
        secondary
      />,
      <FlatButton
        key="user_modal_save_btn"
        label={<FormattedMessage {...messages.save} />}
        onTouchTap={this.handleSaveUser}
        primary
      />,
    ];

    return (<Dialog
      title={'User'}
      actions={actions}
      modal
      open={this.state.userDialogOpen}
    >
      <div>
        <div style={{ display: 'flex', alignItems: 'flex-start' }}>
          <TextField
            floatingLabelText={<FormattedMessage {...messages.firstName} />}
            onChange={this.handleChangeFormData('formFirstName')}
            style={{ marginRight: 20, width: '50%' }}
            value={formFirstName}
          />
          <TextField
            floatingLabelText={<FormattedMessage {...messages.lastName} />}
            onChange={this.handleChangeFormData('formLastName')}
            style={{ marginRight: 20, width: '50%' }}
            value={formLastName}
          />
        </div>
        <div style={{ display: 'flex', alignItems: 'flex-start', marginBottom: 20 }}>
          <TextField
            floatingLabelText={<FormattedMessage {...messages.email} />}
            onChange={this.handleChangeFormData('formEmail')}
            style={{ marginRight: 20, width: '50%' }}
            value={formEmail}
          />
          <SelectField
            floatingLabelText={<FormattedMessage {...messages.manager} />}
            onChange={this.handleSetManagerId}
            style={{ marginRight: 20, width: '50%' }}
            value={formManagerId}
          >
            {users.reduce((memo, user) =>
              user.get(IS_ADMIN) && user.get(ID) !== selectedUser.get(ID) && user.get(MANAGER_ID) !== selectedUser.get(ID) ?
                memo.concat(
                  <MenuItem
                    key={user.get(ID)}
                    primaryText={`${user.get(FIRST_NAME)} ${user.get(LAST_NAME)} (${user.get(EMAIL)})`}
                    value={user.get(ID)}
                  />) : memo
            , [])}
          </SelectField>
        </div>
        <Toggle
          disabled={userHasManagedUsers}
          toggled={formIsAdmin}
          label={<FormattedMessage {...messages.admin} />}
          labelPosition={'right'}
          onToggle={this.handleChangeFormData('formIsAdmin')}
        />
      </div>
    </Dialog>);
  }

  renderUserListItems(user) {
    let nestLevel = 0;
    const managedUserItems = (u) => {
      nestLevel += 1;
      const managedUsers = u.get(MANAGED_USERS, fromJS([]));
      return managedUsers.size ?
        [<Subheader
          key={`user_${u.get(ID)}_manage_users_subheader`}
          style={{ marginLeft: 20 * nestLevel }}
        >
          <FormattedMessage {...messages.managedUsers} />
        </Subheader>]
        .concat(managedUsers.map((managedUser) =>
          (<ListItem
            key={managedUser.get(ID)}
            leftAvatar={<Avatar size={40} icon={<PersonOutline />} />}
            primaryText={`${managedUser.get(FIRST_NAME)} ${managedUser.get(LAST_NAME)} (${managedUser.get(EMAIL)})`}
            secondaryText={managedUser.get(IS_ADMIN) ?
              <span style={{ fontSize: 14, color: 'darkgray' }}>
                <FormattedMessage {...messages.admin} />
              </span> : ''}
            rightIconButton={<div>
              <IconButton
                tooltip={<FormattedMessage {...messages.edit} />}
                tooltipPosition="top-left"
                onClick={this.handleOpenUserDialog(managedUser)}
              >
                <EditIcon />
              </IconButton>
              <IconButton
                disabled={managedUser.get(MANAGED_USERS) && managedUser.get(MANAGED_USERS).size}
                tooltip={<FormattedMessage {...messages.delete} />}
                tooltipPosition="top-left"
                onClick={this.handleOpenConfirmDialog(managedUser)}
              >
                <DeleteIcon />
              </IconButton>
            </div>}
            primaryTogglesNestedList
            initiallyOpen
            nestedItems={managedUserItems(managedUser)}
          />)
        ).toJS()) : [];
    };
    return (<ListItem
      leftAvatar={<Avatar size={40} icon={<PersonOutline />} />}
      primaryText={`${user.get(FIRST_NAME)} ${user.get(LAST_NAME)} (${user.get(EMAIL)})`}
      secondaryText={<span style={{ fontSize: 14, color: 'darkgray' }}><FormattedMessage {...messages.admin} /></span>}
      primaryTogglesNestedList
      initiallyOpen
      nestedItems={managedUserItems(user)}
    />);
  }

  renderUsersList() {
    const { currentUser, users } = this.props;
    function buildUserHierarchy(user) {
      return user.merge(fromJS({
        [MANAGED_USERS]: users.reduce((memo, u) =>
          u.get(MANAGER_ID) === user.get(ID) ? memo.push(buildUserHierarchy(u)) : memo, fromJS([])),
      }));
    }
    const currentUserAsHierarchy = currentUser ? buildUserHierarchy(currentUser) : fromJS({});
    return (
      <div>
        <List>
          {this.renderUserListItems(currentUserAsHierarchy)}
        </List>
      </div>
    );
  }

  render() {
    const { active } = this.props;
    return (
      <div>
        {this.renderUsersList()}
        {this.renderUserDialog()}
        {this.renderConfirmDialog()}
        {
          active ?
            <FloatingActionButton
              onClick={this.handleOpenUserDialog()}
              secondary
              style={{ position: 'fixed', bottom: 20, right: 20 }}
            >
              <ContentAdd />
            </FloatingActionButton> : null
        }
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

Users.defaultProps = {
  currentUser: fromJS({}),
};

Users.propTypes = {
  active: PropTypes.bool,
  currentUser: PropTypes.object,
  onCreateUser: PropTypes.func,
  onDeleteUser: PropTypes.func,
  onLoadUsers: PropTypes.func,
  onUpdateUser: PropTypes.func,
  users: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
  users: makeSelectUsers(),
});

function mapDispatchToProps(dispatch) {
  return {
    onCreateUser: (user) => dispatch(createUser(null, { user })),
    onDeleteUser: (id) => dispatch(deleteUser(id)),
    onLoadUsers: () => dispatch(loadUsers()),
    onUpdateUser: (id, user) => dispatch(updateUser(id, { user })),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);
