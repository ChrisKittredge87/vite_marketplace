/*
 *
 * Users reducer
 *
 */

import { fromJS } from 'immutable';
import {
  /* Actions */
  CREATE_USER_SUCCESS, CREATE_USER_ERROR,
  DELETE_USER_SUCCESS, DELETE_USER_ERROR,
  LOAD_USERS, LOAD_USERS_SUCCESS, LOAD_USERS_ERROR,
  SET_KEY_VAL,
  /* State Keys */
  ERROR,
  LOADING,
  USER_IDS,
} from './constants';

const initialState = fromJS({
  [LOADING]: false,
  [USER_IDS]: fromJS([]),
});

function usersReducer(state = initialState, action) {
  const { entity, error, key, result, type, val } = action;
  switch (type) {
    case CREATE_USER_SUCCESS:
      return state.set(USER_IDS, state.get(USER_IDS).push(result));
    case CREATE_USER_ERROR:
      return state.set(ERROR, error);
    case DELETE_USER_SUCCESS:
      return state.set(USER_IDS, state.get(USER_IDS).filter((userId) => userId !== entity.id));
    case DELETE_USER_ERROR:
      return state.set(ERROR, error);
    case LOAD_USERS:
      return state.set(LOADING, true);
    case LOAD_USERS_SUCCESS:
      return state
        .set(LOADING, false)
        .set(USER_IDS, fromJS(result));
    case LOAD_USERS_ERROR:
      return state
        .set(LOADING, false)
        .set(ERROR, error);
    case SET_KEY_VAL:
      return state.set(key, val);
    default:
      return state;
  }
}

export default usersReducer;

export const DOMAIN = 'serviceProviderUsersAdmin';
