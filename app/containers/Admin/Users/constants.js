/*
 *
 * Users constants
 *
 */

 /* Actions */
export const CREATE_USER = 'app/serviceProviders/admin/Users/CREATE_USER';
export const CREATE_USER_SUCCESS = 'app/serviceProviders/admin/Users/CREATE_USER_SUCCESS';
export const CREATE_USER_ERROR = 'app/serviceProviders/admin/Users/CREATE_USER_ERROR';
export const DELETE_USER = 'app/serviceProviders/admin/Users/DELETE_USER';
export const DELETE_USER_SUCCESS = 'app/serviceProviders/admin/Users/DELETE_USER_SUCCESS';
export const DELETE_USER_ERROR = 'app/serviceProviders/admin/Users/DELETE_USER_ERROR';
export const LOADING = 'app/serviceProviders/admin/Users/LOADING';
export const LOAD_USERS = 'app/serviceProviders/admin/Users/LOAD_USERS';
export const LOAD_USERS_SUCCESS = 'app/serviceProviders/admin/Users/LOAD_USERS_SUCCESS';
export const LOAD_USERS_ERROR = 'app/serviceProviders/admin/Users/LOAD_USERS_ERROR';
export const UPDATE_USER = 'app/serviceProviders/admin/Users/UPDATE_USER';
export const UPDATE_USER_SUCCESS = 'app/serviceProviders/admin/Users/UPDATE_USER_SUCCESS';
export const UPDATE_USER_ERROR = 'app/serviceProviders/admin/Users/UPDATE_USER_ERROR';
export const SET_KEY_VAL = 'app/serviceProviders/admin/Users/SET_KEY_VAL';

/* State Keys */
export const ERROR = 'error';
export const USER_IDS = 'userIds';
