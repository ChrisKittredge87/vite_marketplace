import { takeLatest, all, call, put } from 'redux-saga/effects';

import {
  createEntitySaga as createCompanySaga,
  getEntitiesSaga as getCompaniesSaga,
} from 'entities/schemas/companies/sagas';
import { getEntitiesSaga as createSPTypeGetSagas } from 'entities/schemas/serviceProviders/types/sagas';
import {
  createEntitySaga as createSPSaga,
  updateEntitySaga as updateSPSaga,
  deleteEntitySaga as deleteSPSaga,
  getEntitiesSaga as getSPSaga,
} from 'entities/schemas/serviceProviders/sagas';
import { BASE_URL as COMPANIES_BASE_URL } from 'entities/schemas/companies/constants';
import { BASE_URL as COMPANY_TYPES_BASE_URL } from 'entities/schemas/companies/types/constants';
import { BASE_URL as COMPANY_MEMBERSHIPS_BASE_URL, ID as COMPANY_MEMBERSHIP_ID } from 'entities/schemas/companyMemberships/constants';
import { BASE_URL as ROLES_BASE_URL } from 'entities/schemas/roles/constants';
import { BASE_URL as USERS_BASE_URL } from 'entities/schemas/users/constants';
import request from 'utils/request';

import {
  createCompanySuccess, createCompanyError,
  createServiceProviderSuccess, createServiceProviderError,
  deleteCompanySuccess, deleteCompanyError,
  deleteServiceProviderSuccess, deleteServiceProviderError,
  loadCompaniesSuccess, loadCompaniesError,
  loadCompanyMembershipsSuccess, loadCompanyMembershipsError,
  loadCompanyTypesSuccess, loadCompanyTypesError,
  loadManagedServiceProvidersSuccess, loadManagedServiceProvidersError,
  loadRolesSuccess, loadRolesError,
  loadServiceProviderTypesSuccess, loadServiceProviderTypesError,
  loadUsersSuccess, loadUsersError,
  updateCompanySuccess, updateCompanyError,
  updateServiceProviderSuccess, updateServiceProviderError,
  upsertCompanyMembershipsSuccess, upsertCompanyMembershipsError,
} from './actions';
import {
  CREATE_COMPANY, LOAD_COMPANIES, UPDATE_COMPANY, DELETE_COMPANY,
  CREATE_SERVICE_PROVIDER, UPDATE_SERVICE_PROVIDER, DELETE_SERVICE_PROVIDER,
  LOAD_COMPANY_MEMBERSHIPS,
  LOAD_COMPANY_TYPES,
  LOAD_MANAGED_SERVICE_PROVIDERS,
  LOAD_ROLES,
  LOAD_SERVICE_PROVIDER_TYPES,
  LOAD_USERS,
  UPSERT_COMPANY_MEMBERSHIPS,
} from './constants';

// Individual exports for testing

const getCompaniesData = getCompaniesSaga({
  actionKey: LOAD_COMPANIES,
  successAction: loadCompaniesSuccess,
  errorAction: loadCompaniesError,
});

const createCompanyData = createCompanySaga({
  actionKey: CREATE_COMPANY,
  successAction: createCompanySuccess,
  errorAction: createCompanyError,
});

export function* updateCompany({ id, entity }) {
  const requestUrl = `${COMPANIES_BASE_URL}/${id}`;
  try {
    const updatedCompany = yield call(request.put, requestUrl, { data: entity });
    yield put(updateCompanySuccess(updatedCompany));
  } catch (err) {
    yield put(updateCompanyError(err));
  }
}

export function* updateCompanyData() {
  yield takeLatest(UPDATE_COMPANY, updateCompany);
}

export function* deleteCompany({ id }) {
  const requestUrl = `${COMPANIES_BASE_URL}/${id}`;
  try {
    yield call(request.delete, requestUrl);
    yield put(deleteCompanySuccess({ id }));
  } catch (err) {
    yield put(deleteCompanyError(err));
  }
}

export function* deleteCompanyData() {
  yield takeLatest(DELETE_COMPANY, deleteCompany);
}

export function* loadCompanyMemberships() {
  const requestUrl = `${COMPANY_MEMBERSHIPS_BASE_URL}`;
  try {
    yield call(request.get, requestUrl);
    const companyMemberships = yield call(request.get, requestUrl);
    yield put(loadCompanyMembershipsSuccess(companyMemberships));
  } catch (err) {
    yield put(loadCompanyMembershipsError(err));
  }
}

export function* loadCompanyMembershipsData() {
  yield takeLatest(LOAD_COMPANY_MEMBERSHIPS, loadCompanyMemberships);
}

export function* upsertCompanyMemberships({ created, updated, deletedIds }) {
  const createRequestUrl = `${COMPANY_MEMBERSHIPS_BASE_URL}`;
  const updateDeleteRequestUrl = (id) => `${COMPANY_MEMBERSHIPS_BASE_URL}/${id}`;
  try {
    const [createdResp, updatedResp, deletedResp] = yield all(
      [
        all(created.map((cm) =>
          call(request.post, createRequestUrl, { data: { companyMembership: cm } }))),
        all(updated.map((cm) =>
          call(request.put, updateDeleteRequestUrl(cm[COMPANY_MEMBERSHIP_ID]), { data: { companyMembership: cm } }))),
        all(deletedIds.map((id) => call(request.delete, updateDeleteRequestUrl(id)))),
      ],
    );
    const upsertActions = upsertCompanyMembershipsSuccess({
      entitiesToMerge: [].concat(createdResp).concat(updatedResp),
      entityIdsToDelete: deletedIds,
    });
    if ((createdResp && createdResp.length) || (updatedResp && updatedResp.length)) {
      yield put(upsertActions.merge());
    }
    if ((deletedResp && deletedResp.length)) {
      yield put(upsertActions.delete());
    }
  } catch (err) {
    yield put(upsertCompanyMembershipsError(err));
  }
}

export function* upsertCompanyMembershipsData() {
  yield takeLatest(UPSERT_COMPANY_MEMBERSHIPS, upsertCompanyMemberships);
}

export function* loadCompanyTypes() {
  const requestUrl = `${COMPANY_TYPES_BASE_URL}`;
  try {
    yield call(request.get, requestUrl);
    const companyTypes = yield call(request.get, requestUrl);
    yield put(loadCompanyTypesSuccess(companyTypes));
  } catch (err) {
    yield put(loadCompanyTypesError(err));
  }
}

export function* loadCompanyTypesData() {
  yield takeLatest(LOAD_COMPANY_TYPES, loadCompanyTypes);
}

const managedServiceProvidersData = getSPSaga({
  actionKey: LOAD_MANAGED_SERVICE_PROVIDERS,
  successAction: loadManagedServiceProvidersSuccess,
  errorAction: loadManagedServiceProvidersError,
  query: { managed: true },
});

export function* getRoles() {
  const requestUrl = `${ROLES_BASE_URL}`;
  try {
    const users = yield call(request.get, requestUrl);
    yield put(loadRolesSuccess(users));
  } catch (err) {
    yield put(loadRolesError(err));
  }
}

export function* getRolesData() {
  yield takeLatest(LOAD_ROLES, getRoles);
}

export function* getUsers() {
  const requestUrl = `${USERS_BASE_URL}`;
  try {
    const users = yield call(request.get, requestUrl);
    yield put(loadUsersSuccess(users));
  } catch (err) {
    yield put(loadUsersError(err));
  }
}

export function* getUsersData() {
  yield takeLatest(LOAD_USERS, getUsers);
}

const serviceProviderTypesData = createSPTypeGetSagas({
  actionKey: LOAD_SERVICE_PROVIDER_TYPES,
  successAction: loadServiceProviderTypesSuccess,
  errorAction: loadServiceProviderTypesError,
});

const createServiceProviderData = createSPSaga({
  actionKey: CREATE_SERVICE_PROVIDER,
  successAction: createServiceProviderSuccess,
  errorAction: createServiceProviderError,
});

const updateServiceProviderData = updateSPSaga({
  actionKey: UPDATE_SERVICE_PROVIDER,
  successAction: updateServiceProviderSuccess,
  errorAction: updateServiceProviderError,
});

const deleteServiceProviderData = deleteSPSaga({
  actionKey: DELETE_SERVICE_PROVIDER,
  successAction: deleteServiceProviderSuccess,
  errorAction: deleteServiceProviderError,
});

// All sagas to be loaded
export default [
  getCompaniesData,
  createCompanyData,
  updateCompanyData,
  deleteCompanyData,
  loadCompanyMembershipsData,
  upsertCompanyMembershipsData,
  loadCompanyTypesData,
  getRolesData,
  getUsersData,
  managedServiceProvidersData,
  serviceProviderTypesData,
  createServiceProviderData,
  updateServiceProviderData,
  deleteServiceProviderData,
];

export const NAME = 'companiesSagas';
