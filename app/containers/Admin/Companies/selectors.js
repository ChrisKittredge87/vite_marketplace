import { createSelector } from 'reselect';

import makeSelectEntitiesDomain from 'entities/selectors';
import { selectList as selectServiceProviders } from 'entities/schemas/serviceProviders/selectors';
import { selectList as selectServiceProviderTypes } from 'entities/schemas/serviceProviders/types/selectors';
import { SCHEMA_KEY as COMPANIES } from 'entities/schemas/companies/constants';
import { SCHEMA_KEY as COMPANY_MEMBERSHIPS } from 'entities/schemas/companyMemberships/constants';
import { SCHEMA_KEY as COMPANY_TYPES } from 'entities/schemas/companies/types/constants';
import { SCHEMA_KEY as ROLES } from 'entities/schemas/roles/constants';
import { SCHEMA_KEY as USERS } from 'entities/schemas/users/constants';

import {
  COMPANY_IDS,
  COMPANY_MEMBERSHIP_IDS,
  COMPANY_TYPE_IDS,
  MANAGED_SERVICE_PROVIDER_IDS,
  ROLE_IDS,
  SERVICE_PROVIDER_TYPE_IDS,
  USER_IDS,
} from './constants';
import { DOMAIN } from './reducer';

/**
 * Direct selector to the companies state domain
 */
const selectCompaniesDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by Companies
 */

const makeSelectCompanies = () => createSelector(
  selectCompaniesDomain(),
  makeSelectEntitiesDomain(),
  (companiesDomain, entities) =>
    companiesDomain.get(COMPANY_IDS).map((id) => entities.getIn([COMPANIES, `${id}`]))
);

const makeSelectCompanyMemberships = () => createSelector(
  selectCompaniesDomain(),
  makeSelectEntitiesDomain(),
  (companiesDomain, entities) =>
    companiesDomain.get(COMPANY_MEMBERSHIP_IDS).map((id) => entities.getIn([COMPANY_MEMBERSHIPS, `${id}`]))
);

const makeSelectCompanyTypes = () => createSelector(
  selectCompaniesDomain(),
  makeSelectEntitiesDomain(),
  (companiesDomain, entities) =>
    companiesDomain.get(COMPANY_TYPE_IDS).map((id) => entities.getIn([COMPANY_TYPES, `${id}`]))
);

const makeSelectManagedServiceProviders = () => createSelector(
  (state) => state,
  createSelector(selectCompaniesDomain(), (domain) => domain.get(MANAGED_SERVICE_PROVIDER_IDS)),
  (state, ids) => selectServiceProviders(ids)(state)
);

const makeSelectRoles = () => createSelector(
  selectCompaniesDomain(),
  makeSelectEntitiesDomain(),
  (companiesDomain, entities) =>
    companiesDomain.get(ROLE_IDS).map((id) => entities.getIn([ROLES, `${id}`]))
);

const makeSelectUsers = () => createSelector(
  selectCompaniesDomain(),
  makeSelectEntitiesDomain(),
  (companiesDomain, entities) =>
    companiesDomain.get(USER_IDS).map((id) => entities.getIn([USERS, `${id}`]))
);

const makeSelectServiceProviderTypes = () => createSelector(
  (state) => state,
  createSelector(selectCompaniesDomain(), (domain) => domain.get(SERVICE_PROVIDER_TYPE_IDS)),
  (state, ids) => selectServiceProviderTypes(ids)(state)
);

export default makeSelectCompanies;
export {
  makeSelectCompanyMemberships,
  makeSelectCompanyTypes,
  makeSelectManagedServiceProviders,
  makeSelectRoles,
  makeSelectServiceProviderTypes,
  makeSelectUsers,
  selectCompaniesDomain,
};
