/*
 *
 * Companies
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage, injectIntl } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { fromJS } from 'immutable';
import { error, success } from 'react-notification-system-redux';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Menu from 'material-ui/Menu';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import MenuItem from 'material-ui/MenuItem';
import Popover from 'material-ui/Popover';
import _ from 'lodash';

import CompanyDialog from 'components/common/admin/CompanyDialog';
import CompanyList, { COMPANY, SERVICE_PROVIDER } from 'components/common/admin/CompanyList';
import CompanyMembershipDialog from 'components/serviceProviders/admin/CompanyMembershipDialog';
import ConfirmDialog from 'components/common/ConfirmDialog';
import ServiceProviderDialog from 'components/serviceProviders/admin/ServiceProviderDialog';
import { ID, NAME, PARENT_ID } from 'entities/schemas/companies/constants';
import { ID as COMPANY_MEMBERSHIP_ID } from 'entities/schemas/companyMemberships/constants';
import { ID as SERVICE_PROVIDER_ID, COMPANY_ID as SP_COMPANY_ID } from 'entities/schemas/serviceProviders/constants';
import { makeSelectCurrentUser } from 'containers/Auth/selectors';

import {
  clearErrors,
  createCompany, loadCompanies, updateCompany, deleteCompany,
  loadServiceProviderTypes, createServiceProvider, updateServiceProvider,
  loadCompanyTypes,
  loadCompanyMemberships, upsertCompanyMemberships,
  loadManagedServiceProviders,
  loadRoles,
  loadUsers,
} from './actions';
import {
  COMPANIES_LOADING,
  COMPANY_MEMBERSHIPS_LOADING,
  COMPANY_TYPES_LOADING,
  ERRORS,
  ROLES_LOADING,
  SAVE_COMPANY_LOADING,
  SERVICE_PROVIDER_TYPES_LOADING,
  UPDATE_SERVICE_PROVIDER_LOADING,
  UPSERT_COMPANY_MEMBERSHIPS_LOADING,
  USERS_LOADING,
} from './constants';
import messages from './messages';
import makeSelectCompanies, {
  makeSelectCompanyMemberships,
  makeSelectCompanyTypes,
  makeSelectManagedServiceProviders,
  makeSelectRoles,
  makeSelectServiceProviderTypes,
  makeSelectUsers,
  selectCompaniesDomain,
} from './selectors';

export class Companies extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = {
      companyDialogOpen: false,
      companyMembershipsDialogOpen: false,
      floatingActionsPopoverOpen: false,
      selectedCompany: null,
      selectedServiceProvider: null,
      serviceProviderDialogOpen: false,
    };

    this.handleAddServiceProvider = this.handleAddServiceProvier.bind(this);
    this.handleAddSubsidiary = this.handleAddSubsidiary.bind(this);
    this.handleCloseConfirmDialog = this.handleCloseConfirmDialog.bind(this);
    this.handleCloseCompanyDialog = this.handleCloseCompanyDialog.bind(this);
    this.handleCloseCompanyMembershipsDialog = this.handleCloseCompanyMembershipsDialog.bind(this);
    this.handleCloseServiceProviderDialog = this.handleCloseServiceProviderDialog.bind(this);
    this.handleDeleteCompany = this.handleDeleteCompany.bind(this);
    this.handleFloatingActionButtonClick = this.handleFloatingActionButtonClick.bind(this);
    this.handleOpenConfirmDialog = this.handleOpenConfirmDialog.bind(this);
    this.handleOpenCompanyDialog = this.handleOpenCompanyDialog.bind(this);
    this.handleOpenCompanyMembershipsDialog = this.handleOpenCompanyMembershipsDialog.bind(this);
    this.handleOpenEditDialog = this.handleOpenEditDialog.bind(this);
    this.handleOpenServiceProvierDialog = this.handleOpenServiceProvierDialog.bind(this);
    this.handleNewCompanyButtonClick = this.handleNewCompanyButtonClick.bind(this);
    this.handleNewServiceProviderClick = this.handleNewServiceProviderClick.bind(this);
    this.handleRequestCloseFloatingActionButtonPopover = this.handleRequestCloseFloatingActionButtonPopover.bind(this);
    this.handleSaveCompany = this.handleSaveCompany.bind(this);
    this.handleSaveCompanyMemberships = this.handleSaveCompanyMemberships.bind(this);
    this.handleSaveServiceProvider = this.handleSaveServiceProvider.bind(this);
  }

  /* ----- Lifecycle ----- */

  componentWillMount() {
    const {
      onLoadCompanies,
      onLoadCompanyMemberships,
      onLoadCompanyTypes,
      onLoadManagedServiceProviders,
      onLoadRoles,
      onLoadServiceProviderTypes,
      onLoadUsers,
    } = this.props;
    onLoadCompanies();
    onLoadCompanyMemberships();
    onLoadCompanyTypes();
    onLoadManagedServiceProviders();
    onLoadRoles();
    onLoadServiceProviderTypes();
    onLoadUsers();
  }

  componentWillReceiveProps(nextProps) {
    const { domain, intl, onClearErrors, onLoadCompanyMemberships, onError, onSuccess } = this.props;
    const { domain: nextDomain } = nextProps;
    // On completion of company CRUD
    if (domain.get(SAVE_COMPANY_LOADING) && !nextDomain.get(SAVE_COMPANY_LOADING)) {
      onSuccess(
        intl.formatMessage(messages.success),
        intl.formatMessage(messages.entitySavedSuccessfully, { entity: intl.formatMessage(messages.company) })
      );
      this.handleCloseCompanyDialog();
      onLoadCompanyMemberships();
    }
    // On completion of service provider update
    if (domain.get(UPDATE_SERVICE_PROVIDER_LOADING) && !nextDomain.get(UPDATE_SERVICE_PROVIDER_LOADING)) {
      onSuccess(
        intl.formatMessage(messages.success),
        intl.formatMessage(messages.entitySavedSuccessfully, { entity: intl.formatMessage(messages.serviceProvider) })
      );
      this.handleCloseServiceProviderDialog();
    }
    // On completion of company memberships CRUD
    if (domain.get(UPSERT_COMPANY_MEMBERSHIPS_LOADING) && !nextDomain.get(UPSERT_COMPANY_MEMBERSHIPS_LOADING)) {
      onSuccess(
        intl.formatMessage(messages.success),
        intl.formatMessage(messages.entitySavedSuccessfully, { entity: intl.formatMessage(messages.companyMemberships) })
      );
      this.handleCloseCompanyMembershipsDialog();
    }
    const errors = nextDomain.get(ERRORS);
    if (errors.size) {
      errors.map((e) => onError('Error!', e));
      onClearErrors();
    }
  }

  /* ----- END Lifecycle ----- */

  /* ----- Event Binding ----- */

  handleAddServiceProvier(selectedCompany) {
    this.setState({
      serviceProviderDialogOpen: true,
      selectedServiceProvider: fromJS({ [SP_COMPANY_ID]: selectedCompany.get(ID) }),
    });
  }

  handleAddSubsidiary(parentCompany) {
    this.setState({ companyDialogOpen: true, selectedCompany: fromJS({ [PARENT_ID]: parentCompany.get(ID) }) });
  }

  handleCloseConfirmDialog() {
    this.setState({ confirmDialogOpen: false, selectedCompany: null });
  }

  handleCloseCompanyDialog() {
    this.setState({ companyDialogOpen: false, selectedCompany: null });
  }

  handleCloseCompanyMembershipsDialog() {
    this.setState({ companyMembershipsDialogOpen: false, selectedCompany: null });
  }

  handleDeleteCompany() {
    const { onDeleteCompany } = this.props;
    const { selectedCompany } = this.state;
    onDeleteCompany(selectedCompany.get(ID));
    this.handleCloseConfirmDialog();
  }

  handleFloatingActionButtonClick(event) {
    event.preventDefault();
    this.setState({ floatingActionsPopoverOpen: true, anchorEl: event.currentTarget });
  }

  handleOpenConfirmDialog(selectedCompany) {
    this.setState({ confirmDialogOpen: true, selectedCompany });
  }

  handleOpenCompanyDialog(selectedCompany) {
    this.setState({ companyDialogOpen: true, selectedCompany });
  }

  handleOpenCompanyMembershipsDialog(selectedCompany) {
    this.setState({ companyMembershipsDialogOpen: true, selectedCompany });
  }

  handleOpenEditDialog(type, entity) {
    if (type === COMPANY) {
      this.handleOpenCompanyDialog(entity);
    } else if (type === SERVICE_PROVIDER) {
      this.handleOpenServiceProvierDialog(entity);
    }
  }

  handleOpenServiceProvierDialog(selectedServiceProvider) {
    this.setState({ selectedServiceProvider, serviceProviderDialogOpen: true });
  }

  handleNewCompanyButtonClick() {
    this.handleOpenCompanyDialog(fromJS({}));
    this.setState({ floatingActionsPopoverOpen: false });
  }

  handleNewServiceProviderClick() {
    this.handleOpenServiceProvierDialog(fromJS({}));
    this.setState({ floatingActionsPopoverOpen: false });
  }

  handleCloseServiceProviderDialog() {
    this.setState({ serviceProviderDialogOpen: false });
  }

  handleRequestCloseFloatingActionButtonPopover = () => {
    this.setState({ floatingActionsPopoverOpen: false });
  };

  handleSaveCompany(company) {
    const { onCreateCompany, onUpdateCompany } = this.props;
    if (company.get(ID)) {
      onUpdateCompany(company.get(ID), company.toJS());
    } else {
      onCreateCompany(company.toJS());
    }
  }

  handleSaveServiceProvider(serviceProvider) {
    const { onCreateServiceProvider, onUpdateServiceProvider } = this.props;
    if (serviceProvider.get(SERVICE_PROVIDER_ID)) {
      onUpdateServiceProvider(serviceProvider.get(SERVICE_PROVIDER_ID), serviceProvider.toJS());
    } else {
      onCreateServiceProvider(serviceProvider.toJS());
    }
  }

  handleSaveCompanyMemberships({ created, updated, deleted }) {
    const { onUpsertCompanyMemberships } = this.props;
    const deletedIds = deleted.map((m) => m.get(COMPANY_MEMBERSHIP_ID)).toJS();
    onUpsertCompanyMemberships({ created: created.toJS(), updated: updated.toJS(), deletedIds });
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  render() {
    function isLoading(domain = fromJS({}), loadingKeys = []) {
      return _.find(loadingKeys, (loadingKey) => domain.get(loadingKey)) != null;
    }
    const {
      active,
      companies,
      companyMemberships,
      companyTypes,
      managedServiceProviders,
      domain,
      roles,
      serviceProviderTypes,
      users,
    } = this.props;
    const {
      companyDialogOpen,
      companyMembershipsDialogOpen,
      confirmDialogOpen,
      selectedCompany,
      selectedParentCompanyId,
      selectedServiceProvider,
      serviceProviderDialogOpen,
    } = this.state;
    return (
      <div>
        <CompanyList
          companies={companies}
          serviceProviders={managedServiceProviders}
          onAddSubsidiaryIconClick={this.handleAddSubsidiary}
          onAddServiceProviderIconClick={this.handleAddServiceProvider}
          onDeleteIconClick={this.handleOpenConfirmDialog}
          onEditIconClick={this.handleOpenEditDialog}
          onCompanyMembershipIconClick={this.handleOpenCompanyMembershipsDialog}
        />
        <CompanyMembershipDialog
          company={selectedCompany || fromJS({})}
          companyMemberships={companyMemberships}
          loading={
            isLoading(domain, [
              COMPANIES_LOADING,
              COMPANY_MEMBERSHIPS_LOADING,
              COMPANY_TYPES_LOADING,
              SAVE_COMPANY_LOADING,
              ROLES_LOADING,
              UPSERT_COMPANY_MEMBERSHIPS_LOADING,
              USERS_LOADING,
            ])
          }
          onCancel={this.handleCloseCompanyMembershipsDialog}
          onSave={this.handleSaveCompanyMemberships}
          open={companyMembershipsDialogOpen}
          roles={roles}
          users={users}
        />
        <CompanyDialog
          companies={companies}
          companyTypes={companyTypes}
          loading={
            isLoading(domain, [
              COMPANIES_LOADING,
              COMPANY_TYPES_LOADING,
              SAVE_COMPANY_LOADING,
            ])
          }
          onCloseDialog={this.handleCloseCompanyDialog}
          onSaveCompany={this.handleSaveCompany}
          open={companyDialogOpen}
          selectedCompany={selectedCompany}
          selectedParentCompanyId={selectedParentCompanyId}
        />
        <ServiceProviderDialog
          companies={companies}
          company={selectedCompany}
          loading={isLoading(domain, [
            SERVICE_PROVIDER_TYPES_LOADING,
            UPDATE_SERVICE_PROVIDER_LOADING,
          ])}
          onCloseDialog={this.handleCloseServiceProviderDialog}
          onSaveServiceProvider={this.handleSaveServiceProvider}
          open={serviceProviderDialogOpen}
          serviceProvider={selectedServiceProvider}
          serviceProviderTypes={serviceProviderTypes}
        />
        <ConfirmDialog
          confirmBtnText={<FormattedMessage {...messages.confirm} />}
          message={
            <FormattedMessage
              {...messages.confirmDeleteMessage}
              values={{ companyName: (selectedCompany || fromJS({})).get(NAME) }}
            />
          }
          modal
          onCancel={this.handleCloseConfirmDialog}
          onConfirm={this.handleDeleteCompany}
          open={confirmDialogOpen}
          title={<FormattedMessage {...messages.confirm} />}
        />
        {
          active ?
            <FloatingActionButton
              onClick={this.handleFloatingActionButtonClick}
              secondary
              style={{ position: 'fixed', bottom: 20, right: 20, zIndex: 3 }}
            >
              <MenuIcon />
              <Popover
                anchorEl={this.state.anchorEl}
                anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                onRequestClose={this.handleRequestCloseFloatingActionButtonPopover}
                open={this.state.floatingActionsPopoverOpen}
                style={{ height: 112 }}
                targetOrigin={{ horizontal: 'left', vertical: 'bottom' }}
              >
                <Menu>
                  <MenuItem
                    onClick={this.handleNewCompanyButtonClick}
                    primaryText={<FormattedMessage {...messages.createCompany} />}
                    style={{ background: 'rgba(0,0,0,0)' }}
                  />
                  <MenuItem
                    onClick={this.handleNewServiceProviderClick}
                    primaryText={<FormattedMessage {...messages.createServiceProvider} />}
                    style={{ background: 'rgba(0,0,0,0)' }}
                  />
                </Menu>
              </Popover>
            </FloatingActionButton> : null
        }
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

Companies.propTypes = {
  active: PropTypes.bool,
  companies: PropTypes.object,
  companyMemberships: PropTypes.object,
  companyTypes: PropTypes.object,
  domain: PropTypes.object,
  intl: PropTypes.object,
  managedServiceProviders: PropTypes.object,
  onClearErrors: PropTypes.func,
  onCreateCompany: PropTypes.func,
  onCreateServiceProvider: PropTypes.func,
  onDeleteCompany: PropTypes.func,
  onError: PropTypes.func,
  onLoadCompanies: PropTypes.func,
  onLoadCompanyMemberships: PropTypes.func,
  onLoadCompanyTypes: PropTypes.func,
  onLoadManagedServiceProviders: PropTypes.func,
  onLoadRoles: PropTypes.func,
  onLoadServiceProviderTypes: PropTypes.func,
  onLoadUsers: PropTypes.func,
  onSuccess: PropTypes.func,
  onUpdateCompany: PropTypes.func,
  onUpsertCompanyMemberships: PropTypes.func,
  onUpdateServiceProvider: PropTypes.func,
  roles: PropTypes.object,
  serviceProviderTypes: PropTypes.object,
  users: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  companies: makeSelectCompanies(),
  companyMemberships: makeSelectCompanyMemberships(),
  companyTypes: makeSelectCompanyTypes(),
  currentUser: makeSelectCurrentUser(),
  domain: selectCompaniesDomain(),
  managedServiceProviders: makeSelectManagedServiceProviders(),
  roles: makeSelectRoles(),
  serviceProviderTypes: makeSelectServiceProviderTypes(),
  users: makeSelectUsers(),
});

function mapDispatchToProps(dispatch) {
  return {
    onCreateCompany: (company) => dispatch(createCompany(null, null, { company })),
    onCreateServiceProvider: (serviceProvider) => dispatch(createServiceProvider(null, null, { serviceProvider })),
    onDeleteCompany: (id) => dispatch(deleteCompany(id)),
    onError: (title, message) => dispatch(error({ title, message, position: 'tc' })),
    onClearErrors: () => dispatch(clearErrors()),
    onLoadCompanies: () => dispatch(loadCompanies()),
    onLoadCompanyMemberships: () => dispatch(loadCompanyMemberships()),
    onLoadCompanyTypes: () => dispatch(loadCompanyTypes()),
    onLoadManagedServiceProviders: () => dispatch(loadManagedServiceProviders()),
    onLoadRoles: () => dispatch(loadRoles()),
    onLoadServiceProviderTypes: () => dispatch(loadServiceProviderTypes()),
    onLoadUsers: () => dispatch(loadUsers()),
    onSuccess: (title, message) => dispatch(success({ title, message, position: 'tc', autoDismiss: 5 })),
    onUpdateCompany: (id, company) => dispatch(updateCompany(id, null, { company })),
    onUpsertCompanyMemberships: ({ created, updated, deletedIds }) =>
      dispatch(upsertCompanyMemberships({ created, updated, deletedIds })),
    onUpdateServiceProvider: (id, serviceProvider) => dispatch(updateServiceProvider(id, null, { serviceProvider })),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Companies));
