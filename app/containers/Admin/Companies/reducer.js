/*
 *
 * Users reducer
 *
 */

import { fromJS } from 'immutable';

import { errorState, successState } from 'utils/reducer';

import {
  /* Actions */
  CLEAR_ERRORS,
  CREATE_COMPANY, CREATE_COMPANY_SUCCESS, CREATE_COMPANY_ERROR,
  DELETE_COMPANY, DELETE_COMPANY_SUCCESS, DELETE_COMPANY_ERROR,
  LOAD_COMPANIES, LOAD_COMPANIES_SUCCESS, LOAD_COMPANIES_ERROR,
  LOAD_COMPANY_MEMBERSHIPS, LOAD_COMPANY_MEMBERSHIPS_SUCCESS, LOAD_COMPANY_MEMBERSHIPS_ERROR,
  LOAD_MANAGED_SERVICE_PROVIDERS, LOAD_MANAGED_SERVICE_PROVIDERS_SUCCESS, LOAD_MANAGED_SERVICE_PROVIDERS_ERROR,
  LOAD_SERVICE_PROVIDER_TYPES, LOAD_SERVICE_PROVIDER_TYPES_SUCCESS, LOAD_SERVICE_PROVIDER_TYPES_ERROR,
  UPDATE_SERVICE_PROVIDER, UPDATE_SERVICE_PROVIDER_SUCCESS, UPDATE_SERVICE_PROVIDER_ERROR,
  UPSERT_COMPANY_MEMBERSHIPS,
  UPSERT_COMPANY_MEMBERSHIPS_SUCCESS_MERGE,
  UPSERT_COMPANY_MEMBERSHIPS_SUCCESS_DELETE,
  UPSERT_COMPANY_MEMBERSHIPS_ERROR,
  LOAD_COMPANY_TYPES, LOAD_COMPANY_TYPES_SUCCESS, LOAD_COMPANY_TYPES_ERROR,
  UPDATE_COMPANY, UPDATE_COMPANY_SUCCESS, UPDATE_COMPANY_ERROR,
  LOAD_ROLES, LOAD_ROLES_SUCCESS, LOAD_ROLES_ERROR,
  LOAD_USERS, LOAD_USERS_SUCCESS, LOAD_USERS_ERROR,
  SET_KEY_VAL,
  /* State Keys */
  COMPANIES_LOADING,
  COMPANY_IDS,
  COMPANY_MEMBERSHIPS_LOADING,
  COMPANY_MEMBERSHIP_IDS,
  COMPANY_TYPE_IDS,
  COMPANY_TYPES_LOADING,
  CREATE_SERVICE_PROVIDER_LOADING,
  DELETE_SERVICE_PROVIDER_LOADING,
  ERRORS,
  MANAGED_SERVICE_PROVIDER_IDS,
  MANAGED_SERVICE_PROVIDERS_LOADING,
  SAVE_COMPANY_LOADING,
  SERVICE_PROVIDER_TYPE_IDS,
  SERVICE_PROVIDER_TYPES_LOADING,
  ROLES_LOADING, ROLE_IDS,
  UPDATE_SERVICE_PROVIDER_LOADING,
  USERS_LOADING, USER_IDS,
  UPSERT_COMPANY_MEMBERSHIPS_LOADING,
} from './constants';

const initialState = fromJS({
  [COMPANIES_LOADING]: false,
  [COMPANY_IDS]: fromJS([]),
  [COMPANY_MEMBERSHIP_IDS]: fromJS([]),
  [COMPANY_MEMBERSHIPS_LOADING]: false,
  [COMPANY_TYPE_IDS]: fromJS([]),
  [COMPANY_TYPES_LOADING]: false,
  [CREATE_SERVICE_PROVIDER_LOADING]: false,
  [DELETE_SERVICE_PROVIDER_LOADING]: false,
  [ERRORS]: fromJS([]),
  [MANAGED_SERVICE_PROVIDER_IDS]: fromJS([]),
  [MANAGED_SERVICE_PROVIDERS_LOADING]: false,
  [ROLE_IDS]: fromJS([]),
  [SAVE_COMPANY_LOADING]: false,
  [SERVICE_PROVIDER_TYPE_IDS]: fromJS([]),
  [SERVICE_PROVIDER_TYPES_LOADING]: false,
  [UPDATE_SERVICE_PROVIDER_LOADING]: false,
  [USER_IDS]: fromJS([]),
  [UPSERT_COMPANY_MEMBERSHIPS_LOADING]: false,
});

function companiesReducer(state = initialState, action) {
  const { entity, error, key, result, type, val } = action;
  const setErrorState = errorState.bind(null, state, error);
  const setSuccessState = successState.bind(null, state);
  switch (type) {
    case CLEAR_ERRORS:
      return state.set(ERRORS, fromJS([]));
    case CREATE_COMPANY:
      return state.set(SAVE_COMPANY_LOADING, true);
    case CREATE_COMPANY_SUCCESS:
      return setSuccessState(result, {
        loadingKey: SAVE_COMPANY_LOADING, resultsKey: COMPANY_IDS, concat: true,
      });
    case CREATE_COMPANY_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: SAVE_COMPANY_LOADING });
    case DELETE_COMPANY:
      return state.set(SAVE_COMPANY_LOADING, true);
    case DELETE_COMPANY_SUCCESS:
      return setSuccessState(
        entity.id,
        {
          loadingKey: SAVE_COMPANY_LOADING,
          resultsKey: COMPANY_IDS,
          result: entity.id,
          concat: false,
          removeEntity: true,
        }
      );
    case DELETE_COMPANY_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: SAVE_COMPANY_LOADING });
    case LOAD_COMPANIES:
      return state.set(COMPANIES_LOADING, true);
    case LOAD_COMPANIES_SUCCESS:
      return setSuccessState(result, { loadingKey: COMPANIES_LOADING, resultsKey: COMPANY_IDS });
    case LOAD_COMPANIES_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: COMPANIES_LOADING });
    case LOAD_COMPANY_MEMBERSHIPS:
      return state.set(COMPANY_MEMBERSHIPS_LOADING, true);
    case LOAD_COMPANY_MEMBERSHIPS_SUCCESS:
      return setSuccessState(result, { loadingKey: COMPANY_MEMBERSHIPS_LOADING, resultsKey: COMPANY_MEMBERSHIP_IDS });
    case LOAD_COMPANY_MEMBERSHIPS_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: COMPANY_MEMBERSHIPS_LOADING });
    case LOAD_COMPANY_TYPES:
      return state.set(COMPANY_TYPES_LOADING, true);
    case LOAD_COMPANY_TYPES_SUCCESS:
      return setSuccessState(result, { loadingKey: COMPANY_TYPES_LOADING, resultsKey: COMPANY_TYPE_IDS });
    case LOAD_COMPANY_TYPES_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: COMPANY_TYPES_LOADING });
    case LOAD_MANAGED_SERVICE_PROVIDERS:
      return state.set(MANAGED_SERVICE_PROVIDERS_LOADING, true);
    case LOAD_MANAGED_SERVICE_PROVIDERS_SUCCESS:
      return setSuccessState(result, { loadingKey: MANAGED_SERVICE_PROVIDERS_LOADING, resultsKey: MANAGED_SERVICE_PROVIDER_IDS });
    case LOAD_MANAGED_SERVICE_PROVIDERS_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: MANAGED_SERVICE_PROVIDERS_LOADING });
    case LOAD_ROLES:
      return state.set(ROLES_LOADING, true);
    case LOAD_ROLES_SUCCESS:
      return setSuccessState(result, { loadingKey: ROLES_LOADING, resultsKey: ROLE_IDS });
    case LOAD_ROLES_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: ROLES_LOADING });
    case LOAD_SERVICE_PROVIDER_TYPES:
      return state.set(SERVICE_PROVIDER_TYPES_LOADING, true);
    case LOAD_SERVICE_PROVIDER_TYPES_SUCCESS:
      return setSuccessState(result,{ loadingKey: SERVICE_PROVIDER_TYPES_LOADING, resultsKey: SERVICE_PROVIDER_TYPE_IDS });
    case LOAD_SERVICE_PROVIDER_TYPES_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: SERVICE_PROVIDER_TYPES_LOADING });
    case LOAD_USERS:
      return state.set(USERS_LOADING, true);
    case LOAD_USERS_SUCCESS:
      return setSuccessState(result, {loadingKey: USERS_LOADING, resultsKey: USER_IDS });
    case LOAD_USERS_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: USERS_LOADING });
    case UPDATE_COMPANY:
      return state.set(SAVE_COMPANY_LOADING, true);
    case UPDATE_COMPANY_SUCCESS:
      return state.set(SAVE_COMPANY_LOADING, false);
    case UPDATE_COMPANY_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: SAVE_COMPANY_LOADING });
    case UPDATE_SERVICE_PROVIDER:
      return state.set(UPDATE_SERVICE_PROVIDER_LOADING, true);
    case UPDATE_SERVICE_PROVIDER_SUCCESS:
      return state.set(UPDATE_SERVICE_PROVIDER_LOADING, false);
    case UPDATE_SERVICE_PROVIDER_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: UPDATE_SERVICE_PROVIDER_LOADING });
    case UPSERT_COMPANY_MEMBERSHIPS:
      return state.set(UPSERT_COMPANY_MEMBERSHIPS_LOADING, true);
    case UPSERT_COMPANY_MEMBERSHIPS_SUCCESS_MERGE:
      return setSuccessState(result,
        {
          loadingKey: UPSERT_COMPANY_MEMBERSHIPS_LOADING,
          resultsKey: COMPANY_MEMBERSHIP_IDS,
          concat: true,
        }
      );
    case UPSERT_COMPANY_MEMBERSHIPS_SUCCESS_DELETE:
      return setSuccessState(result, {
        loadingKey: UPSERT_COMPANY_MEMBERSHIPS_LOADING,
        resultsKey: COMPANY_MEMBERSHIP_IDS,
        concat: false,
        removeEntity: true,
      });
    case UPSERT_COMPANY_MEMBERSHIPS_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: UPSERT_COMPANY_MEMBERSHIPS_LOADING });
    case SET_KEY_VAL:
      return state.set(key, val);
    default:
      return state;
  }
}

export default companiesReducer;

export const DOMAIN = 'serviceProviderCompaniesAdmin';
