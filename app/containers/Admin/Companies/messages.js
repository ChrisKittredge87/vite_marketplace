/*
 * Companies Messages
 *
 * This contains all the text for the Companies component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  company: {
    id: 'company',
    defaultMessage: 'Company',
  },
  companyMemberships: {
    id: 'companyMemberships',
    defaultMessage: 'Company Memberships',
  },
  confirm: {
    id: 'app.containers.serviceProviders.admin.Companies.confirm',
    defaultMessage: 'Confirm',
  },
  confirmDeleteMessage: {
    id: 'app.containers.serviceProviders.admin.Companies.confirmDeleteMessage',
    defaultMessage: 'Are you sure you want to delete { companyName }',
  },
  createCompany: {
    id: 'createCompany',
    defaultMessage: 'Create Company',
  },
  createServiceProvider: {
    id: 'createServiceProvider',
    defaultMessage: 'Create Service Provider',
  },
  entitySavedSuccessfully: {
    id: 'entitySavedSuccessfully',
    defaultMessage: '{ entity } saved successfully!',
  },
  success: {
    id: 'success',
    defaultMessage: 'Success!',
  },
});
