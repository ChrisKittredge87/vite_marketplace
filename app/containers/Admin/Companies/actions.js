/*
 *
 * Companies actions
 *
 */

import { list as companiesSchema, entity as companySchema } from 'entities/schemas/companies';
import { list as rolesSchema } from 'entities/schemas/roles';
import { list as companyMembershipsSchema } from 'entities/schemas/companyMemberships';
import {
  entity as serviceProviderSchema,
  list as serviceProvidersSchema,
} from 'entities/schemas/serviceProviders';
import { list as serviceProviderTypesSchema } from 'entities/schemas/serviceProviders/types';
import { list as companyTypesSchema } from 'entities/schemas/companies/types';
import { list as usersSchema } from 'entities/schemas/users';
import { createSagaActions, createUpsertSagaActions } from 'utils/sagas';

import {
  CLEAR_ERRORS,
  CREATE_COMPANY, CREATE_COMPANY_SUCCESS, CREATE_COMPANY_ERROR,
  CREATE_SERVICE_PROVIDER, CREATE_SERVICE_PROVIDER_SUCCESS, CREATE_SERVICE_PROVIDER_ERROR,
  LOAD_COMPANIES, LOAD_COMPANIES_SUCCESS, LOAD_COMPANIES_ERROR,
  DELETE_COMPANY, DELETE_COMPANY_SUCCESS, DELETE_COMPANY_ERROR,
  DELETE_SERVICE_PROVIDER, DELETE_SERVICE_PROVIDER_SUCCESS, DELETE_SERVICE_PROVIDER_ERROR,
  LOAD_COMPANY_MEMBERSHIPS, LOAD_COMPANY_MEMBERSHIPS_SUCCESS, LOAD_COMPANY_MEMBERSHIPS_ERROR,
  LOAD_COMPANY_TYPES, LOAD_COMPANY_TYPES_SUCCESS, LOAD_COMPANY_TYPES_ERROR,
  LOAD_MANAGED_SERVICE_PROVIDERS, LOAD_MANAGED_SERVICE_PROVIDERS_SUCCESS, LOAD_MANAGED_SERVICE_PROVIDERS_ERROR,
  LOAD_ROLES, LOAD_ROLES_SUCCESS, LOAD_ROLES_ERROR,
  LOAD_SERVICE_PROVIDER_TYPES, LOAD_SERVICE_PROVIDER_TYPES_SUCCESS, LOAD_SERVICE_PROVIDER_TYPES_ERROR,
  LOAD_USERS, LOAD_USERS_SUCCESS, LOAD_USERS_ERROR,
  SET_KEY_VAL,
  UPDATE_COMPANY, UPDATE_COMPANY_SUCCESS, UPDATE_COMPANY_ERROR,
  UPDATE_SERVICE_PROVIDER, UPDATE_SERVICE_PROVIDER_SUCCESS, UPDATE_SERVICE_PROVIDER_ERROR,
  UPSERT_COMPANY_MEMBERSHIPS,
  UPSERT_COMPANY_MEMBERSHIPS_SUCCESS_MERGE,
  UPSERT_COMPANY_MEMBERSHIPS_SUCCESS_DELETE,
  UPSERT_COMPANY_MEMBERSHIPS_ERROR,
} from './constants';

export function clearErrors() {
  return { type: CLEAR_ERRORS };
}

export function defaultAction(key, val) {
  return { type: SET_KEY_VAL, key, val };
}

export const {
  loadEntity: loadCompanies,
  entityLoaded: loadCompaniesSuccess,
  entityLoadingError: loadCompaniesError,
} = createSagaActions({
  loadAction: LOAD_COMPANIES,
  entityLoadedAction: LOAD_COMPANIES_SUCCESS,
  entityLoadingErrorAction: LOAD_COMPANIES_ERROR,
  schema: companiesSchema,
});

export const {
  loadEntity: createCompany,
  entityLoaded: createCompanySuccess,
  entityLoadingError: createCompanyError,
} = createSagaActions({
  loadAction: CREATE_COMPANY,
  entityLoadedAction: CREATE_COMPANY_SUCCESS,
  entityLoadingErrorAction: CREATE_COMPANY_ERROR,
  schema: companySchema,
});

export const {
  loadEntity: updateCompany,
  entityLoaded: updateCompanySuccess,
  entityLoadingError: updateCompanyError,
} = createSagaActions({
  loadAction: UPDATE_COMPANY,
  entityLoadedAction: UPDATE_COMPANY_SUCCESS,
  entityLoadingErrorAction: UPDATE_COMPANY_ERROR,
  schema: companySchema,
});

export const {
  loadEntity: deleteCompany,
  entityLoaded: deleteCompanySuccess,
  entityLoadingError: deleteCompanyError,
} = createSagaActions({
  loadAction: DELETE_COMPANY,
  entityLoadedAction: DELETE_COMPANY_SUCCESS,
  entityLoadingErrorAction: DELETE_COMPANY_ERROR,
});

export const {
  loadEntity: loadCompanyMemberships,
  entityLoaded: loadCompanyMembershipsSuccess,
  entityLoadingError: loadCompanyMembershipsError,
} = createSagaActions({
  loadAction: LOAD_COMPANY_MEMBERSHIPS,
  entityLoadedAction: LOAD_COMPANY_MEMBERSHIPS_SUCCESS,
  entityLoadingErrorAction: LOAD_COMPANY_MEMBERSHIPS_ERROR,
  schema: companyMembershipsSchema,
});

export const {
  loadEntity: upsertCompanyMemberships,
  entityLoaded: upsertCompanyMembershipsSuccess,
  entityLoadingError: upsertCompanyMembershipsError,
} = createUpsertSagaActions({
  loadAction: UPSERT_COMPANY_MEMBERSHIPS,
  entityLoadedMergeAction: UPSERT_COMPANY_MEMBERSHIPS_SUCCESS_MERGE,
  entityLoadedDeleteAction: UPSERT_COMPANY_MEMBERSHIPS_SUCCESS_DELETE,
  entityLoadingErrorAction: UPSERT_COMPANY_MEMBERSHIPS_ERROR,
  schema: companyMembershipsSchema,
});

export const {
  loadEntity: loadCompanyTypes,
  entityLoaded: loadCompanyTypesSuccess,
  entityLoadingError: loadCompanyTypesError,
} = createSagaActions({
  loadAction: LOAD_COMPANY_TYPES,
  entityLoadedAction: LOAD_COMPANY_TYPES_SUCCESS,
  entityLoadingErrorAction: LOAD_COMPANY_TYPES_ERROR,
  schema: companyTypesSchema,
});

export const {
  loadEntity: loadRoles,
  entityLoaded: loadRolesSuccess,
  entityLoadingError: loadRolesError,
} = createSagaActions({
  loadAction: LOAD_ROLES,
  entityLoadedAction: LOAD_ROLES_SUCCESS,
  entityLoadingErrorAction: LOAD_ROLES_ERROR,
  schema: rolesSchema,
});

export const {
  loadEntity: loadServiceProviderTypes,
  entityLoaded: loadServiceProviderTypesSuccess,
  entityLoadingError: loadServiceProviderTypesError,
} = createSagaActions({
  loadAction: LOAD_SERVICE_PROVIDER_TYPES,
  entityLoadedAction: LOAD_SERVICE_PROVIDER_TYPES_SUCCESS,
  entityLoadingErrorAction: LOAD_SERVICE_PROVIDER_TYPES_ERROR,
  schema: serviceProviderTypesSchema,
});

export const {
  loadEntity: loadUsers,
  entityLoaded: loadUsersSuccess,
  entityLoadingError: loadUsersError,
} = createSagaActions({
  loadAction: LOAD_USERS,
  entityLoadedAction: LOAD_USERS_SUCCESS,
  entityLoadingErrorAction: LOAD_USERS_ERROR,
  schema: usersSchema,
});

export const {
  loadEntity: loadManagedServiceProviders,
  entityLoaded: loadManagedServiceProvidersSuccess,
  entityLoadingError: loadManagedServiceProvidersError,
} = createSagaActions({
  loadAction: LOAD_MANAGED_SERVICE_PROVIDERS,
  entityLoadedAction: LOAD_MANAGED_SERVICE_PROVIDERS_SUCCESS,
  entityLoadingErrorAction: LOAD_MANAGED_SERVICE_PROVIDERS_ERROR,
  schema: serviceProvidersSchema,
});

export const {
  loadEntity: createServiceProvider,
  entityLoaded: createServiceProviderSuccess,
  entityLoadingError: createServiceProviderError,
} = createSagaActions({
  loadAction: CREATE_SERVICE_PROVIDER,
  entityLoadedAction: CREATE_SERVICE_PROVIDER_SUCCESS,
  entityLoadingErrorAction: CREATE_SERVICE_PROVIDER_ERROR,
  schema: serviceProviderSchema,
});

export const {
  loadEntity: updateServiceProvider,
  entityLoaded: updateServiceProviderSuccess,
  entityLoadingError: updateServiceProviderError,
} = createSagaActions({
  loadAction: UPDATE_SERVICE_PROVIDER,
  entityLoadedAction: UPDATE_SERVICE_PROVIDER_SUCCESS,
  entityLoadingErrorAction: UPDATE_SERVICE_PROVIDER_ERROR,
  schema: serviceProviderSchema,
});

export const {
  loadEntity: deleteServiceProvider,
  entityLoaded: deleteServiceProviderSuccess,
  entityLoadingError: deleteServiceProviderError,
} = createSagaActions({
  loadAction: DELETE_SERVICE_PROVIDER,
  entityLoadedAction: DELETE_SERVICE_PROVIDER_SUCCESS,
  entityLoadingErrorAction: DELETE_SERVICE_PROVIDER_ERROR,
  schema: serviceProviderSchema,
});
