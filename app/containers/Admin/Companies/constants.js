/*
 *
 * Companies constants
 *
 */

 /* Actions */
export const CLEAR_ERRORS = 'app/serviceProviders/admin/Companies/CLEAR_ERRORS';
export const CREATE_COMPANY = 'app/serviceProviders/admin/Companies/CREATE_COMPANY';
export const CREATE_COMPANY_SUCCESS = 'app/serviceProviders/admin/Companies/CREATE_COMPANY_SUCCESS';
export const CREATE_COMPANY_ERROR = 'app/serviceProviders/admin/Companies/CREATE_COMPANY_ERROR';
export const CREATE_SERVICE_PROVIDER = 'app/serviceProviders/admin/Companies/CREATE_SERVICE_PROVIDER';
export const CREATE_SERVICE_PROVIDER_SUCCESS = 'app/serviceProviders/admin/Companies/CREATE_SERVICE_PROVIDER_SUCCESS';
export const CREATE_SERVICE_PROVIDER_ERROR = 'app/serviceProviders/admin/Companies/CREATE_SERVICE_PROVIDER_ERROR';
export const DELETE_COMPANY = 'app/serviceProviders/admin/Companies/DELETE_COMPANY';
export const DELETE_COMPANY_SUCCESS = 'app/serviceProviders/admin/Companies/DELETE_COMPANY_SUCCESS';
export const DELETE_COMPANY_ERROR = 'app/serviceProviders/admin/Companies/DELETE_COMPANY_ERROR';
export const DELETE_SERVICE_PROVIDER = 'app/serviceProviders/admin/Companies/DELETE_SERVICE_PROVIDER';
export const DELETE_SERVICE_PROVIDER_SUCCESS = 'app/serviceProviders/admin/Companies/DELETE_SERVICE_PROVIDER_SUCCESS';
export const DELETE_SERVICE_PROVIDER_ERROR = 'app/serviceProviders/admin/Companies/DELETE_SERVICE_PROVIDER_ERROR';
export const LOAD_COMPANIES = 'app/serviceProviders/admin/Companies/LOAD_COMPANIES';
export const LOAD_COMPANIES_SUCCESS = 'app/serviceProviders/admin/Companies/LOAD_COMPANIES_SUCCESS';
export const LOAD_COMPANIES_ERROR = 'app/serviceProviders/admin/Companies/LOAD_COMPANIES_ERROR';
export const LOAD_COMPANY_MEMBERSHIPS = 'app/serviceProviders/admin/Companies/LOAD_COMPANY_MEMBERSHIPS';
export const LOAD_COMPANY_MEMBERSHIPS_SUCCESS = 'app/serviceProviders/admin/Companies/LOAD_COMPANY_MEMBERSHIPS_SUCCESS';
export const LOAD_COMPANY_MEMBERSHIPS_ERROR = 'app/serviceProviders/admin/Companies/LOAD_COMPANY_MEMBERSHIPS_ERROR';
export const LOAD_COMPANY_TYPES = 'app/serviceProviders/admin/Companies/LOAD_COMPANY_TYPES';
export const LOAD_COMPANY_TYPES_SUCCESS = 'app/serviceProviders/admin/Companies/LOAD_COMPANY_TYPES_SUCCESS';
export const LOAD_COMPANY_TYPES_ERROR = 'app/serviceProviders/admin/Companies/LOAD_COMPANY_TYPES_ERROR';
export const LOAD_ROLES = 'app/serviceProviders/admin/Companies/LOAD_ROLES';
export const LOAD_ROLES_SUCCESS = 'app/serviceProviders/admin/Companies/LOAD_ROLES_SUCCESS';
export const LOAD_ROLES_ERROR = 'app/serviceProviders/admin/Companies/LOAD_ROLES_ERROR';
export const LOAD_MANAGED_SERVICE_PROVIDERS = 'app/serviceProviders/admin/Companies/LOAD_MANAGED_SERVICE_PROVIDERS';
export const LOAD_MANAGED_SERVICE_PROVIDERS_SUCCESS = 'app/serviceProviders/admin/Companies/LOAD_MANAGED_SERVICE_PROVIDERS_SUCCESS';
export const LOAD_MANAGED_SERVICE_PROVIDERS_ERROR = 'app/serviceProviders/admin/Companies/LOAD_MANAGED_SERVICE_PROVIDERS_ERROR';
export const LOAD_SERVICE_PROVIDER_TYPES = 'app/serviceProviders/admin/Companies/LOAD_SERVICE_PROVIDER_TYPES';
export const LOAD_SERVICE_PROVIDER_TYPES_SUCCESS = 'app/serviceProviders/admin/Companies/LOAD_SERVICE_PROVIDER_TYPES_SUCCESS';
export const LOAD_SERVICE_PROVIDER_TYPES_ERROR = 'app/serviceProviders/admin/Companies/LOAD_SERVICE_PROVIDER_TYPES_ERROR';
export const LOAD_USERS = 'app/serviceProviders/admin/Companies/LOAD_USERS';
export const LOAD_USERS_SUCCESS = 'app/serviceProviders/admin/Companies/LOAD_USERS_SUCCESS';
export const LOAD_USERS_ERROR = 'app/serviceProviders/admin/Companies/LOAD_USERS_ERROR';
export const SET_KEY_VAL = 'app/serviceProviders/admin/Companies/SET_KEY_VAL';
export const UPDATE_COMPANY = 'app/serviceProviders/admin/Companies/UPDATE_COMPANY';
export const UPDATE_COMPANY_SUCCESS = 'app/serviceProviders/admin/Companies/UPDATE_COMPANY_SUCCESS';
export const UPDATE_COMPANY_ERROR = 'app/serviceProviders/admin/Companies/UPDATE_COMPANY_ERROR';
export const UPDATE_SERVICE_PROVIDER = 'app/serviceProviders/admin/Companies/UPDATE_SERVICE_PROVIDER';
export const UPDATE_SERVICE_PROVIDER_SUCCESS = 'app/serviceProviders/admin/Companies/UPDATE_SERVICE_PROVIDER_SUCCESS';
export const UPDATE_SERVICE_PROVIDER_ERROR = 'app/serviceProviders/admin/Companies/UPDATE_SERVICE_PROVIDER_ERROR';
export const UPSERT_COMPANY_MEMBERSHIPS = 'app/serviceProviders/admin/Companies/UPSERT_COMPANY_MEMBERSHIPS';
export const UPSERT_COMPANY_MEMBERSHIPS_SUCCESS_MERGE = 'app/serviceProviders/admin/Companies/UPSERT_COMPANY_MEMBERSHIPS_SUCCESS_MERGE';
export const UPSERT_COMPANY_MEMBERSHIPS_SUCCESS_DELETE = 'app/serviceProviders/admin/Companies/UPSERT_COMPANY_MEMBERSHIPS_SUCCESS_DELETE';
export const UPSERT_COMPANY_MEMBERSHIPS_ERROR = 'app/serviceProviders/admin/Companies/UPSERT_COMPANY_MEMBERSHIPS_ERROR';

/* State Keys */
export const COMPANIES_LOADING = 'companiesLoading';
export const COMPANY_IDS = 'companyIds';
export const COMPANY_MEMBERSHIP_IDS = 'companyMembershipIds';
export const COMPANY_MEMBERSHIPS_LOADING = 'companyMembershipsLoading';
export const COMPANY_TYPE_IDS = 'companyTypeIds';
export const COMPANY_TYPES_LOADING = 'companyTypesLoading';
export const CREATE_SERVICE_PROVIDER_LOADING = 'createServiceProviderLoading';
export const DELETE_SERVICE_PROVIDER_LOADING = 'deleteServiceProviderLoading';
export const ERRORS = 'errors';
export const MANAGED_SERVICE_PROVIDER_IDS = 'managedServiceProviderIds';
export const MANAGED_SERVICE_PROVIDERS_LOADING = 'managedServiceProvidersLoading';
export const ROLE_IDS = 'roleIds';
export const ROLES_LOADING = 'roleLoading';
export const SAVE_COMPANY_LOADING = 'saveCompanyLoading';
export const SERVICE_PROVIDER_TYPE_IDS = 'serviceProviderTypeIds';
export const SERVICE_PROVIDER_TYPES_LOADING = 'serviceProviderTypesLoading';
export const UPDATE_SERVICE_PROVIDER_LOADING = 'updateServiceProviderLoading';
export const USER_IDS = 'userIds';
export const USERS_LOADING = 'usersLoading';
export const UPSERT_COMPANY_MEMBERSHIPS_LOADING = 'upsertCompanyMembershipsLoading';
