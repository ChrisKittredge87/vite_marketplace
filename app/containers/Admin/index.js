/*
 *
 * Users
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { Tabs, Tab } from 'material-ui/Tabs';
import SettingsIcon from 'material-ui/svg-icons/action/settings';

import CompaniesAdmin from 'containers/Admin/Companies';
import UsersAdmin from 'containers/Admin/Users';

import messages from './messages';

const COMPANIES = 'companies';
const USERS = 'users';

export class Users extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = {
      activeTab: USERS,
      hasLoadedUsersTab: false,
      hasLoadedCompaniesTab: false,
      hasLoadedMembershipsTab: false,
      initialSelectedIndex: 0,
    };

    this.handleActiveTabChange = this.handleActiveTabChange.bind(this);
  }

  /* ----- Lifecycle ----- */

  componentWillMount() {
    const startingTab = (this.props.params.tab || '').trim().toLowerCase();
    const startingIndex = [USERS, COMPANIES].indexOf(startingTab);
    if (startingIndex > -1) {
      this.setState({ initialSelectedIndex: startingIndex });
      this.handleActiveTabChange(startingTab)();
    }
  }

  /* ----- END Lifecycle ----- */

  /* ----- Event Binding ----- */

  handleActiveTabChange(tab) {
    return () => {
      const newRoute = `/admin/${tab}`;
      if (this.props.location.pathname !== newRoute) {
        browserHistory.replace(newRoute);
      }
      return this.setState({
        activeTab: tab,
        hasLoadedUsersTab: tab === USERS || this.state.hasLoadedUsersTab,
        hasLoadedCompaniesTab: tab === COMPANIES || this.state.hasLoadedCompaniesTab,
      });
    };
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  render() {
    return (
      <div>
        <Helmet
          title="Users"
          meta={[
            { name: 'description', content: 'Description of Users' },
          ]}
        />
        <h2 style={{ margin: '10px 25px' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <SettingsIcon style={{ marginRight: 5 }} /><FormattedMessage {...messages.adminPortal} />
          </div>
        </h2>
        <Tabs initialSelectedIndex={this.state.initialSelectedIndex}>
          <Tab
            label={<FormattedMessage {...messages.users} />}
            onActive={this.handleActiveTabChange(USERS)}
            style={{ height: '100%' }}
          >
            <UsersAdmin style={{ minHeight: '100%' }} active={this.state.activeTab === USERS} />
          </Tab>
          <Tab
            label={<FormattedMessage {...messages.companies} />}
            onActive={this.handleActiveTabChange(COMPANIES)}
          >
            {this.state.activeTab === COMPANIES || this.state.hasLoadedCompaniesTab ? <CompaniesAdmin style={{ minHeight: '100%' }} active={this.state.activeTab === COMPANIES} /> : null}
          </Tab>
        </Tabs>
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

Users.propTypes = {
  location: PropTypes.object,
  params: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);
