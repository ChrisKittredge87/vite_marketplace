import { IS_ADMIN } from 'entities/schemas/users/constants';

export const path = 'admin';
export const name = 'Admin';

export default function (loadModule, errorLoading, injectReducers, injectSagas) {
  return {
    guard: ({ currentUser }) => !!currentUser && currentUser.get(IS_ADMIN),
    path: `${path}/:tab`,
    name,
    getComponent(nextState, cb) {
      const importModules = Promise.all([
        import('containers/Admin/Companies/reducer'),
        import('containers/Admin/Companies/sagas'),
        import('containers/Admin/Users/reducer'),
        import('containers/Admin/Users/sagas'),
        import('containers/Admin'),
      ]);

      const renderRoute = loadModule(cb);

      importModules.then(([companiesReducer, companiesSagas, usersReducer, usersSagas, component]) => {
        injectReducers([companiesReducer, usersReducer]);
        injectSagas([companiesSagas, usersSagas]);
        renderRoute(component);
      });
      importModules.catch(errorLoading);
    },
  };
}
