import { getEntitiesSaga as get } from 'utils/sagas';

import { BASE_URL, TTL } from './constants';

export const getEntitiesSaga = ({ actionKey, successAction, errorAction }) =>
  get({ url: BASE_URL, ttl: TTL, actionKey, successAction, errorAction });
