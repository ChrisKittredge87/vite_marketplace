import createSelectors from 'utils/selectors';

import { SCHEMA_KEY } from './constants';

const { selectEntity, selectList } = createSelectors(SCHEMA_KEY);

export { selectEntity, selectList };
