export const path = '*';
export const name = 'notFound';

export default function (loadModule, errorLoading, injectReducers, injectSagas) {
  return {
    path,
    name,
    getComponent(nextState, cb) {
      import('containers/NotFoundPage')
        .then(loadModule(cb))
        .catch(errorLoading);
    },
  };
}
