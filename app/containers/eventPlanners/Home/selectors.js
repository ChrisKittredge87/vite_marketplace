import { createSelector } from 'reselect';

import { SCHEMA_KEY as serviceProvidersSchemaKey } from 'entities/schemas/serviceProviders/constants';
import makeSelectEntities from 'entities/selectors';

import { TOP_SERVICE_PROVIDER_IDS } from './constants';
import { DOMAIN } from './reducer';

/**
 * Direct selector to the home state domain
 */
const selectHomeDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by Home
 */

const makeSelectHome = () => createSelector(
  selectHomeDomain(),
  (substate) => substate
);

const makeSelectTopServiceProviders = () => createSelector(
  selectHomeDomain(),
  makeSelectEntities(),
  (homeDomain, entities) =>
    homeDomain
      .get(TOP_SERVICE_PROVIDER_IDS)
      .map((id) => entities.getIn([serviceProvidersSchemaKey, `${id}`]))
);

export default makeSelectHome;
export {
  selectHomeDomain,
  makeSelectTopServiceProviders,
};
