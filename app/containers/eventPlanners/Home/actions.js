/*
 *
 * Home actions
 *
 */
import { createSagaActions } from 'utils/sagas';
import { list as schema } from 'entities/schemas/serviceProviders';

import {
  LOAD_TOP_SERVICE_PROVIDERS, TOP_SERVICE_PROVIDERS_LOADED, TOP_SERVICE_PROVIDERS_LOADING_ERROR,
  SET_KEY_VAL,
} from './constants';

export function setKeyVal(key, val) {
  return { type: SET_KEY_VAL, key, val };
}

export const {
  loadEntity: loadTopServiceProviders,
  entityLoaded: topServiceProvidersLoaded,
  entityLoadingError: topsServiceProvidersLoadingError,
} = createSagaActions({
  loadAction: LOAD_TOP_SERVICE_PROVIDERS,
  entityLoadedAction: TOP_SERVICE_PROVIDERS_LOADED,
  entityLoadingErrorAction: TOP_SERVICE_PROVIDERS_LOADING_ERROR,
  schema,
});
