/*
 *
 * Home reducer
 *
 */

import { fromJS } from 'immutable';
import {
  /* Actions */
  LOAD_TOP_SERVICE_PROVIDERS, TOP_SERVICE_PROVIDERS_LOADED, TOP_SERVICE_PROVIDERS_LOADING_ERROR,
  SET_KEY_VAL,
  /* State Keys */
  GUESTS_POPOVER_OPEN,
  LOADING,
  TOP_SERVICE_PROVIDER_IDS,
} from './constants';

const initialState = fromJS({
  [GUESTS_POPOVER_OPEN]: false,
  [LOADING]: false,
  [TOP_SERVICE_PROVIDER_IDS]: [],
});

function homeReducer(state = initialState, action) {
  const { key, result, type, val } = action;
  switch (type) {
    case LOAD_TOP_SERVICE_PROVIDERS:
      return state.set(LOADING, true);
    case TOP_SERVICE_PROVIDERS_LOADED:
      return state
        .set(LOADING, false)
        .set(TOP_SERVICE_PROVIDER_IDS, fromJS(result));
    case TOP_SERVICE_PROVIDERS_LOADING_ERROR:
      return state.set(LOADING, false);
    case SET_KEY_VAL:
      return state.set(key, val);
    default:
      return state;
  }
}

export default homeReducer;

export const DOMAIN = 'eventPlannersHome';
