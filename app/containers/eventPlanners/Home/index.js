/*
 *
 * Home
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Rater from 'react-rater';
import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import Slider from 'react-slick';
import DatePicker from 'material-ui/DatePicker';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import StarsIcon from 'material-ui/svg-icons/action/stars';
import TextField from 'material-ui/TextField';

import { XXS, XS, SM, MD, LG, XL } from 'globalConstants';
import { path as searchPath } from 'containers/serviceProviders/Search/routes';
import { path as serviceProviderProfilePath } from 'containers/serviceProviders/Profile/routes';
import GuestsRangeDropdown from 'components/common/GuestsRangeDropdown';
import TimeRangeDropdown from 'components/common/TimeRangeDropdown';
import {
  AVG_RATING,
  ADDRESS,
  COMPANY_ID,
  ID,
  IMAGES,
  LOCALITY,
  NAME,
  REGION,
} from 'entities/schemas/serviceProviders/constants';
import { Crop, DynamicImg, SpaceBetweenFlex } from 'styled/common';

import { loadTopServiceProviders, setKeyVal } from './actions';
import {
  SEARCH_MIN_GUESTS, SEARCH_MAX_GUESTS,
} from './constants';
import messages from './messages';
import makeSelectHome, { makeSelectTopServiceProviders } from './selectors';

export class Home extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = {
      guestsPopoverOpen: false,
      guestsRangeValue: null,
      timeRangeValue: null,
    };

    this.handleChangeMinMaxServingCapacity = this.handleChangeMinMaxServingCapacity.bind(this);
    this.handleChangeGuestsRangeValue = this.handleChangeGuestsRangeValue.bind(this);
    this.handleChangeTimeRangeValue = this.handleChangeTimeRangeValue.bind(this);
    this.handleSetKeyVal = this.handleSetKeyVal.bind(this);
    this.handleToggleGuestsPopoverOpen = this.handleToggleGuestsPopoverOpen.bind(this);
    this.renderSearchBar = this.renderSearchBar.bind(this);
    this.renderTopProviders = this.renderTopProviders.bind(this);
  }

  /* ----- React Lifecycle ----- */

  componentWillMount() {
    const { onLoadTopServiceProviders } = this.props;
    onLoadTopServiceProviders();
  }

  /* ----- END React Lifecycle ----- */

  /* ----- Event Binding ----- */

  handleChangeMinMaxServingCapacity({ min, max }) {
    const { homeState, onSetKeyVal } = this.props;
    const servingCapacityMin = homeState.get(SEARCH_MIN_GUESTS);
    const servingCapacityMax = homeState.get(SEARCH_MAX_GUESTS);
    if (min !== servingCapacityMin) {
      onSetKeyVal(SEARCH_MIN_GUESTS, min);
    }
    if (max !== servingCapacityMax) {
      onSetKeyVal(SEARCH_MIN_GUESTS, max);
    }
  }

  handleChangeTimeRangeValue(evt, key, timeRangeValue) {
    this.setState({ timeRangeValue });
  }

  handleChangeGuestsRangeValue(evt, key, guestsRangeValue) {
    this.setState({ guestsRangeValue });
  }

  handleSetKeyVal(key) {
    const { onSetKeyVal } = this.props;
    return (val) => onSetKeyVal(key, val);
  }

  handleToggleGuestsPopoverOpen() {
    this.setState({
      guestsPopoverOpen: !this.state.guestsPopoverOpen,
    });
  }

  /* ----- END Event Binding ----- */

  /* ----- Rendering ----- */

  renderSearchBar() {
    const { guestsRangeValue, timeRangeValue } = this.state;
    return (<div style={{ border: '1px solid lightgray', display: 'flex', alignItems: 'flex-end', justifyContent: 'center', paddingBottom: 10 }}>
      <TextField
        floatingLabelText={<FormattedMessage {...messages.where} />}
        value="Atlanta, GA" disabled style={{ margin: '0 10px 0 0' }}
      />
      <DatePicker
        autoOk
        locale="en-US"
        floatingLabelText={<FormattedMessage {...messages.when} />}
        textFieldStyle={{ margin: '0 10px' }}
      />
      <TimeRangeDropdown
        onChange={this.handleChangeTimeRangeValue}
        style={{ padding: '0 10px' }}
        value={timeRangeValue}
      />
      <GuestsRangeDropdown
        onChange={this.handleChangeGuestsRangeValue}
        style={{ padding: '0 10px' }}
        value={guestsRangeValue}
      />
      <Link to={searchPath}>
        <RaisedButton label="Search" primary style={{ margin: '10px 0' }} />
      </Link>
    </div>);
  }

  renderTopProviders(serviceProviders, header) {
    const { topServiceProviders } = this.props;
    const isMobile = window.innerWidth < SM;
    const sliderSettings = {
      dots: true,
      infinite: true,
      pauseOnHover: true,
      speed: 500,
      slidesToShow: isMobile ? 1 : 3,
      slidesToScroll: 1,
    };
    return (<div>
      <h2 style={{ display: 'flex', alignItems: 'center' }}>{header}</h2>
      <Divider />
      <br />
      {
        topServiceProviders.size ?
          <Slider
            {...sliderSettings}
          >
            {topServiceProviders.map((sp) =>
                (<div key={`${sp.get(ID)}`}>
                  <div style={{ margin: '0 10px' }}>
                    <Link
                      to={`/${serviceProviderProfilePath}/${sp.get(COMPANY_ID)}?serviceProviderId=${sp.get(ID)}`}
                    >
                      <Crop style={{ height: isMobile ? 200 : 400 }}>
                        <DynamicImg src={sp.getIn([IMAGES, 0])} />
                      </Crop>
                    </Link>
                    <SpaceBetweenFlex>
                      <h3>{sp.get(NAME)}</h3>
                      <div style={{ display: 'flex', alignItems: 'baseline' }}>
                        <h3 style={{ marginRight: 15 }}>
                          {`${sp.getIn([ADDRESS, LOCALITY])}, ${sp.getIn([ADDRESS, REGION])}`}
                        </h3>
                        <Rater style={{ paddingRight: 7 }} total={5} rating={sp.get(AVG_RATING)} interactive={false} />
                      </div>
                    </SpaceBetweenFlex>
                  </div>
                </div>)
            )}
        </Slider> : null
    }
    </div>);
  }

  render() {
    const { topServiceProviders } = this.props;
    const topServiceProvidersHeader = (<span style={{ display: 'flex', alignItems: 'center' }}>
      <StarsIcon style={{ margin: '0 7px' }} />
      <FormattedMessage {...messages.topServiceProviders} />
    </span>);
    const cateringHeader = (<span style={{ display: 'flex', alignItems: 'center' }}>
      <StarsIcon style={{ margin: '0 7px' }} />
      <FormattedMessage {...messages.catering} />
    </span>);
    const foodTrucksHeader = (<span style={{ display: 'flex', alignItems: 'center' }}>
      <StarsIcon style={{ margin: '0 7px' }} />
      <FormattedMessage {...messages.foodTrucks} />
    </span>);
    const venuesHeader = (<span style={{ display: 'flex', alignItems: 'center' }}>
      <StarsIcon style={{ margin: '0 7px' }} />
      <FormattedMessage {...messages.venues} />
    </span>);
    return (
      <div>
        <Helmet
          title="Home"
          meta={[
            { name: 'description', content: 'Description of Home' },
          ]}
        />

        <div style={{ margin: '10px 10px' }}>
          {this.renderSearchBar()}
          {this.renderTopProviders(topServiceProviders, topServiceProvidersHeader)}
          {this.renderTopProviders(topServiceProviders, cateringHeader)}
          {this.renderTopProviders(topServiceProviders, foodTrucksHeader)}
          {this.renderTopProviders(topServiceProviders, venuesHeader)}
        </div>
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

Home.propTypes = {
  homeState: PropTypes.object,
  onLoadTopServiceProviders: PropTypes.func.isRequired,
  onSetKeyVal: PropTypes.func.isRequired,
  topServiceProviders: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  homeState: makeSelectHome(),
  topServiceProviders: makeSelectTopServiceProviders(),
});

function mapDispatchToProps(dispatch) {
  return {
    onLoadTopServiceProviders: () => dispatch(loadTopServiceProviders()),
    onSetKeyVal: (key, val) => dispatch(setKeyVal(key, val)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
