/*
 * Home Messages
 *
 * This contains all the text for the Home component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  catering: {
    id: 'catering',
    defaultMessage: 'Catering',
  },
  foodTrucks: {
    id: 'foodTrucks',
    defaultMessage: 'Food Trucks',
  },
  topServiceProviders: {
    id: 'topServiceProviders',
    defaultMessage: 'Top Service Providers',
  },
  venues: {
    id: 'venues',
    defaultMessage: 'Venues',
  },
  when: {
    id: 'when',
    defaultMessage: 'When',
  },
  where: {
    id: 'where',
    defaultMessage: 'Where',
  },
});
