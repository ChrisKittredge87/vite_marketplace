import { call, put, takeLatest } from 'redux-saga/effects';

import request from 'utils/request';
import { BASE_URL } from 'entities/schemas/serviceProviders/constants';
import { topServiceProvidersLoaded, topsServiceProvidersLoadingError } from './actions';
import { LOAD_TOP_SERVICE_PROVIDERS } from './constants';

// Individual exports for testing
export function* getTopServiceProviders() {
  try {
    const serviceProviders = yield call(request.get, `${BASE_URL}`);
    yield put(topServiceProvidersLoaded(serviceProviders));
  } catch (err) {
    yield put(topsServiceProvidersLoadingError(err));
  }
}

export function* serviceProviderData() {
  yield takeLatest(LOAD_TOP_SERVICE_PROVIDERS, getTopServiceProviders);
}

// All sagas to be loaded
export default [
  serviceProviderData,
];

export const NAME = 'homeSagas';
