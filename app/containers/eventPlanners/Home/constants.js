/*
 *
 * Home constants
 *
 */

 /* Actions */
export const LOAD_TOP_SERVICE_PROVIDERS = 'app/Home/LOAD_TOP_SERVICE_PROVIDERS';
export const TOP_SERVICE_PROVIDERS_LOADED = 'app/Home/TOP_SERVICE_PROVIDERS_LOADED';
export const TOP_SERVICE_PROVIDERS_LOADING_ERROR = 'app/Home/TOP_SERVICE_PROVIDERS_LOADING_ERROR';
export const SET_KEY_VAL = 'app/Home/SET_KEY_VAL';

/* State Keys */
export const GUESTS_POPOVER_OPEN = 'guestsPopoverOpen';
export const LOADING = 'loading';
export const SEARCH_DATE = 'searchDate';
export const SEARCH_MIN_GUESTS = 'searchMinGuests';
export const SEARCH_MAX_GUESTS = 'searchMaxGuests';
export const TOP_SERVICE_PROVIDER_IDS = 'topServiceProviderIds';

/* Default */
export const SEARCH_MIN_GUESTS_DEFAULT = 0;
export const SEARCH_MAX_GUESTS_DEFAULT = 500;
