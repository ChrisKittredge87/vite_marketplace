// import { take, call, put, select } from 'redux-saga/effects';

import { getEntitiesSaga as createGetEventTemplatesSagas } from 'entities/schemas/eventTemplates/sagas';

import {
  eventTemplatesLoaded,
  eventTemplatesLoadingError,
} from './actions';
import { LOAD_EVENT_TEMPLATES } from './constants';

const eventTemplatesData = createGetEventTemplatesSagas({
  actionKey: LOAD_EVENT_TEMPLATES,
  successAction: eventTemplatesLoaded,
  errorAction: eventTemplatesLoadingError,
});

// All sagas to be loaded
export default [
  eventTemplatesData,
];

export const NAME = 'EventsSagas';
