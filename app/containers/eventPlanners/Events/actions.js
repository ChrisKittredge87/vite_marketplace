/*
 *
 * Events actions
 *
 */

 import { createSagaActions } from 'utils/sagas';
 import { list as eventTemplateList } from 'entities/schemas/eventTemplates';

import {
  LOAD_EVENT_TEMPLATES, EVENT_TEMPLATES_LOADED, EVENT_TEMPLATES_LOADING_ERROR,
  SET_KEY_VAL,
} from './constants';

export function defaultAction(key, val) {
  return { type: SET_KEY_VAL, key, val };
}

export const {
  loadEntity: loadEventTemplates,
  entityLoaded: eventTemplatesLoaded,
  entityLoadingError: eventTemplatesLoadingError,
} = createSagaActions({
  loadAction: LOAD_EVENT_TEMPLATES,
  entityLoadedAction: EVENT_TEMPLATES_LOADED,
  entityLoadingErrorAction: EVENT_TEMPLATES_LOADING_ERROR,
  schema: eventTemplateList,
});
