/*
 *
 * Events reducer
 *
 */

import { fromJS } from 'immutable';

import { successState, errorState } from 'utils/reducer';

import {
   // Actions
  SET_KEY_VAL,
  LOAD_EVENT_TEMPLATES, EVENT_TEMPLATES_LOADED, EVENT_TEMPLATES_LOADING_ERROR,
  // State Keys
  ERRORS,
  EVENT_TEMPLATE_IDS,
  LOADING_EVENT_TEMPLATES,
} from './constants';

const initialState = fromJS({
  [ERRORS]: [],
  [EVENT_TEMPLATE_IDS]: [],
  [LOADING_EVENT_TEMPLATES]: false,
});

function eventsReducer(state = initialState, action) {
  const { error, key, result, type, val } = action;
  const setErrorState = errorState.bind(null, state, error);
  const setSuccessState = successState.bind(null, state);
  switch (type) {
    case LOAD_EVENT_TEMPLATES:
      return state.set(LOADING_EVENT_TEMPLATES, true);
    case EVENT_TEMPLATES_LOADED:
      return setSuccessState(result,
        { loadingKey: LOADING_EVENT_TEMPLATES, resultsKey: EVENT_TEMPLATE_IDS }
      );
    case EVENT_TEMPLATES_LOADING_ERROR:
      return setErrorState({ errorKey: ERRORS, loadingKey: LOADING_EVENT_TEMPLATES });
    case SET_KEY_VAL:
      return state.set(key, val);
    default:
      return state;
  }
}

export default eventsReducer;

export const DOMAIN = 'events';
