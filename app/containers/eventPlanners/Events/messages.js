/*
 * Events Messages
 *
 * This contains all the text for the Events component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  calendar: {
    id: 'calendar',
    defaultMessage: 'Calendar',
  },
  eventTemplates: {
    id: 'eventTemplates',
    defaultMessage: 'Event Templates',
  },
});
