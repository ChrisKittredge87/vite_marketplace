/*
 *
 * RfpStepper reducer
 *
 */

import { fromJS } from 'immutable';
import {
  // Actions
  SET_KEY_VAL,
  LOAD_VENUE_EXPERIENCES, VENUE_EXPERIENCES_LOADED, VENUE_EXPERIENCES_LOADING_ERROR,
  LOAD_CUISINE_TYPES, CUISINE_TYPES_LOADED, CUISINE_TYPES_LOADING_ERROR,
  // State Keys
  CUISINE_TYPE_IDS,
  VENUE_EXPERIENCE_IDS,
} from './constants';

const initialState = fromJS({
  [CUISINE_TYPE_IDS]: [],
  [VENUE_EXPERIENCE_IDS]: [],
});

function EventPlannerWizardReducer(state = initialState, action) {
  const { key, result, type, val } = action;
  switch (type) {
    case LOAD_CUISINE_TYPES:
      return state;
    case CUISINE_TYPES_LOADED:
      return state
        .set(CUISINE_TYPE_IDS, fromJS(result));
    case CUISINE_TYPES_LOADING_ERROR:
      return state;
    case LOAD_VENUE_EXPERIENCES:
      return state;
    case VENUE_EXPERIENCES_LOADED:
      return state
        .set(VENUE_EXPERIENCE_IDS, fromJS(result));
    case VENUE_EXPERIENCES_LOADING_ERROR:
      return state;
    case SET_KEY_VAL:
      return state.set(key, val);
    default:
      return state;
  }
}

export default EventPlannerWizardReducer;

export const DOMAIN = 'EventPlannerWizard';
