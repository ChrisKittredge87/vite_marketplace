
import { fromJS } from 'immutable';
import EventPlannerWizardReducer from '../reducer';

describe('EventPlannerWizardReducer', () => {
  it('returns the initial state', () => {
    expect(EventPlannerWizardReducer(undefined, {})).toEqual(fromJS({}));
  });
});
