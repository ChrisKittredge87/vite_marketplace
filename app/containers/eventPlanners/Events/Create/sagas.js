// import { take, call, put, select } from 'redux-saga/effects';

import {
  cuisineTypesLoaded, cuisineTypesLoadingError,
  eventTemplateCreated, eventTemplateCreationError,
  venueExperiencesLoaded, venueExperiencesLoadingError,
} from './actions';
import {
  CREATE_EVENT_TEMPLATE,
  LOAD_CUISINE_TYPES,
  LOAD_VENUE_EXPERIENCES,
} from './constants';

import { createEntitySaga as createEventTemplateSaga } from 'entities/schemas/eventTemplates/sagas';
import { getEntitiesSaga as createCuisineTypesGetSagas } from 'entities/schemas/serviceProviders/cuisineTypes/sagas';
import { getEntitiesSaga as createVenueExperiencesGetSagas } from 'entities/schemas/serviceProviders/experiences/sagas';

const createEventTemplateData = createEventTemplateSaga({
  actionKey: CREATE_EVENT_TEMPLATE,
  successAction: eventTemplateCreated,
  errorAction: eventTemplateCreationError,
});

const cuisineTypesData = createCuisineTypesGetSagas({
  actionKey: LOAD_CUISINE_TYPES,
  successAction: cuisineTypesLoaded,
  errorAction: cuisineTypesLoadingError,
});

const venueExperiencesData = createVenueExperiencesGetSagas({
  actionKey: LOAD_VENUE_EXPERIENCES,
  successAction: venueExperiencesLoaded,
  errorAction: venueExperiencesLoadingError,
});

// All sagas to be loaded
export default [
  createEventTemplateData,
  cuisineTypesData,
  venueExperiencesData,
];

export const NAME = 'CreateEventsSagas';
