/*
 *
 * RfpStepper actions
 *
 */

 import { createSagaActions } from 'utils/sagas';
 import { list as cuisineTypeList } from 'entities/schemas/serviceProviders/cuisineTypes';
 import { entity as eventTemplate } from 'entities/schemas/eventTemplates';
 import { list as venueExperienceList } from 'entities/schemas/serviceProviders/experiences';

import {
  SET_KEY_VAL,
  LOAD_VENUE_EXPERIENCES, VENUE_EXPERIENCES_LOADED, VENUE_EXPERIENCES_LOADING_ERROR,
  LOAD_CUISINE_TYPES, CUISINE_TYPES_LOADED, CUISINE_TYPES_LOADING_ERROR,
  CREATE_EVENT_TEMPLATE, EVENT_TEMPLATE_CREATED, EVENT_TEMPLATE_CREATION_ERROR,
} from './constants';

export function defaultAction(key, val) {
  return { type: SET_KEY_VAL, key, val };
}

export const {
  loadEntity: createEventTemplate,
  entityLoaded: eventTemplateCreated,
  entityLoadingError: eventTemplateCreationError,
} = createSagaActions({
  loadAction: CREATE_EVENT_TEMPLATE,
  entityLoadedAction: EVENT_TEMPLATE_CREATED,
  entityLoadingErrorAction: EVENT_TEMPLATE_CREATION_ERROR,
  schema: eventTemplate,
});

export const {
  loadEntity: loadCuisineTypes,
  entityLoaded: cuisineTypesLoaded,
  entityLoadingError: cuisineTypesLoadingError,
} = createSagaActions({
  loadAction: LOAD_CUISINE_TYPES,
  entityLoadedAction: CUISINE_TYPES_LOADED,
  entityLoadingErrorAction: CUISINE_TYPES_LOADING_ERROR,
  schema: cuisineTypeList,
});

export const {
  loadEntity: loadVenueExperiences,
  entityLoaded: venueExperiencesLoaded,
  entityLoadingError: venueExperiencesLoadingError,
} = createSagaActions({
  loadAction: LOAD_VENUE_EXPERIENCES,
  entityLoadedAction: VENUE_EXPERIENCES_LOADED,
  entityLoadingErrorAction: VENUE_EXPERIENCES_LOADING_ERROR,
  schema: venueExperienceList,
});
