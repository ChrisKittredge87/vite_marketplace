export const path = 'create';
export const name = 'Create Your Event';

export default function (loadModule, errorLoading, injectReducers, injectSagas) {
  return {
    path,
    name,
    getComponent(nextState, cb) {
      const importModules = Promise.all([
        import('containers/eventPlanners/Events/Create/reducer'),
        import('containers/eventPlanners/Events/Create/sagas'),
        import('containers/eventPlanners/Events/Create'),
      ]);

      const renderRoute = loadModule(cb);

      importModules.then(([reducer, sagas, component]) => {
        injectReducers([reducer]);
        injectSagas([sagas]);
        renderRoute(component);
      });
      importModules.catch(errorLoading);
    },
  };
}
