/*
 *
 * CreateEvent
 *
 */

import React, { PropTypes } from 'react';

import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import Helmet from 'react-helmet';

import RfpStepper from 'components/eventPlanners/EventPlannerWizard';

import {
  createEventTemplate,
  loadCuisineTypes,
  loadVenueExperiences,
} from './actions';
import messages from './messages';
import {
  makeSelectCuisineTypes,
  makeSelectVenueExperiences,
} from './selectors';

export class CreateEvent extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.handleSaveEventTemplate = this.handleSaveEventTemplate.bind(this);
  }

  componentWillMount() {
    const { onLoadCuisineTypes, onLoadVenueExperiences } = this.props;
    onLoadCuisineTypes();
    onLoadVenueExperiences();
  }

  // Event Binding

  handleSaveEventTemplate(eventTemplate) {
    const { onCreateEventTemplate } = this.props;
    onCreateEventTemplate(eventTemplate);
  }

  render() {
    const { cuisineTypes, venueExperiences } = this.props;
    return (
      <div>
        <Helmet
          title="CreateEvent"
          meta={[
            { name: 'description', content: 'Description of CreateEvent' },
          ]}
        />
        <RfpStepper
          cuisineTypeOptions={cuisineTypes}
          onSave={this.handleSaveEventTemplate}
          venueExperienceOptions={venueExperiences}
        />
      </div>
    );
  }
}

CreateEvent.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  cuisineTypes: makeSelectCuisineTypes(),
  venueExperiences: makeSelectVenueExperiences()
});

function mapDispatchToProps(dispatch) {
  return {
    onCreateEventTemplate: (eventTemplate) =>
      dispatch(createEventTemplate(null, null, { eventTemplate })),
    onLoadCuisineTypes: () => dispatch(loadCuisineTypes()),
    onLoadVenueExperiences: () => dispatch(loadVenueExperiences()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateEvent);
