/*
 *
 * RfpStepper constants
 *
 */

/* Actions */
export const SET_KEY_VAL = 'app.RfpStepper.SET_KEY_VAL';
export const LOAD_CUISINE_TYPES = 'eventPlanners.RfpStepper.LOAD_CUISINE_TYPES';
export const CUISINE_TYPES_LOADED = 'eventPlanners.RfpStepper.CUISINE_TYPES_LOADED';
export const CUISINE_TYPES_LOADING_ERROR = 'eventPlanners.RfpStepper.CUISINE_TYPES_LOADING_ERROR';
export const LOAD_VENUE_EXPERIENCES = 'eventPlanners.RfpStepper.LOAD_VENUE_EXPERIENCES';
export const VENUE_EXPERIENCES_LOADED = 'eventPlanners.RfpStepper.VENUE_EXPERIENCES_LOADED';
export const VENUE_EXPERIENCES_LOADING_ERROR = 'eventPlanners.RfpStepper.VENUE_EXPERIENCES_LOADING_ERROR';
export const CREATE_EVENT_TEMPLATE = 'eventPlanners.RfpStepper.CREATE_EVENT_TEMPLATE';
export const EVENT_TEMPLATE_CREATED = 'eventPlanners.RfpStepper.EVENT_TEMPLATE_CREATED';
export const EVENT_TEMPLATE_CREATION_ERROR = 'eventPlanners.RfpStepper.EVENT_TEMPLATE_CREATION_ERROR';

/* State Keys */
export const CUISINE_TYPE_IDS = 'cuisineTypeIds';
export const VENUE_EXPERIENCE_IDS = 'venueExperienceIds';
