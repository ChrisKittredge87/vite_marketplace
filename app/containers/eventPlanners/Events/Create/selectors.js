import { createSelector } from 'reselect';

import makeSelectEntities from 'entities/selectors';

import { CUISINE_TYPE_IDS, VENUE_EXPERIENCE_IDS } from './constants';
import { DOMAIN } from './reducer';
import { SCHEMA_KEY as CUISINE_TYPE_SCHEMA_KEY } from 'entities/schemas/serviceProviders/cuisineTypes/constants';
import { SCHEMA_KEY as VENUE_EXPERIENCE_SCHEMA_KEY } from 'entities/schemas/serviceProviders/experiences/constants';

/**
 * Direct selector to the EventPlannerWizard state domain
 */
const selectRfpStepperDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by RfpStepper
 */

const makeSelectRfpStepper = () => createSelector(
  selectRfpStepperDomain(),
  (substate) => substate.toJS()
);

const makeSelectCuisineTypes = () => createSelector(
  selectRfpStepperDomain(),
  makeSelectEntities(),
  (state, entities) => {
    return state
      .get(CUISINE_TYPE_IDS)
      .map((id) => entities.getIn([CUISINE_TYPE_SCHEMA_KEY, `${id}`]));
  }
);

const makeSelectVenueExperiences = () => createSelector(
  selectRfpStepperDomain(),
  makeSelectEntities(),
  (state, entities) => {
    return state
      .get(VENUE_EXPERIENCE_IDS)
      .map((id) => entities.getIn([VENUE_EXPERIENCE_SCHEMA_KEY, `${id}`]));
  }
);

export default makeSelectRfpStepper;
export {
  makeSelectCuisineTypes,
  makeSelectVenueExperiences,
  selectRfpStepperDomain,
};
