/*
 * RfpStepper Messages
 *
 * This contains all the text for the RfpStepper component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.RfpStepper.header',
    defaultMessage: 'This is RfpStepper container !',
  },
});
