/*
 *
 * Events
 *
 */

import React, { PropTypes } from 'react';
import AddIcon from 'material-ui/svg-icons/content/add';
import BigCalendar from 'react-big-calendar';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import { FormattedMessage } from 'react-intl';
import { fromJS } from 'immutable';
import Helmet from 'react-helmet';
import { Link } from 'react-router';
import makeSelectEvents from './selectors';
import messages from './messages';
import moment from 'moment';
import { Tabs, Tab } from 'material-ui/Tabs';
import _ from 'lodash';

import { XXS, XS, SM, MD, LG, XL } from 'globalConstants';
import EventTemplateList from 'components/eventPlanners/EventTemplateList';
import EventSummaryCard from 'components/eventPlanners/EventSummaryCard';
import { path as createEventPath } from 'containers/eventPlanners/Events/Create/routes';
import {
  ADDRESS,
  DONT_SET_BUDGET,
  END_DATE,
  INCLUDE_IN_HOUSE_VENDORS,
  INCLUDE_OUTSIDE_VENDORS,
  NAME,
  NEEDS_VENUE,
  NUMBER_OF_EXPECTED_GUESTS,
  OVERALL_BUDGET,
  RADIUS,
  START_DATE,
  VENDOR_BUDGET,
  VENUE_BUDGET,
} from 'entities/schemas/eventTemplates/constants';

import { loadEventTemplates } from './actions';
import { path as eventsPath } from './routes';
import { selectEventTemplates } from './selectors';

BigCalendar.setLocalizer(
  BigCalendar.momentLocalizer(moment)
);

export class Events extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = {
      selectedTab: 'eventTemplates',
    };

    this.formatEventTemplatesForCalendar = this.formatEventTemplatesForCalendar.bind(this);
    this.handleChangeTab = this.handleChangeTab.bind(this);
    this.renderCalendarTab = this.renderCalendarTab.bind(this);
  }

  /* ----- Lifecycle ----- */

  componentWillMount() {
    const { onLoadEventTemplates } = this.props;
    onLoadEventTemplates();
  }

  /* ----- END Lifecycle ----- */

  /* ----- Event Binding ----- */

  handleChangeTab(selectedTab) {
    this.setState({ selectedTab });
  }

  /* ----- END Event Binding ----- */

  /* ----- Util Methods ----- */

  formatEventTemplatesForCalendar() {
    const { eventTemplates } = this.props;
    return eventTemplates.map((template) => {
      return {
        title: template.get(NAME),
        start: moment(template.get(START_DATE)).toDate(),
        end: moment(template.get(END_DATE)).toDate(),
      }
    }).toJS();
  }

  /* ----- END Util Methods ----- */

  /* ----- Rendering ----- */

  renderCalendarTab() {
    const { events, eventTemplates } = this.props;
    const mobile = window.innerWidth <= SM;
    return <div
        style={{ height: 'calc(100vh - 112px)', padding: mobile ? 8 : 30 }}
      >
      <BigCalendar
        defaultDate={new Date()}
        events={this.formatEventTemplatesForCalendar()}
        popup
        selectable
        step={60}
        onSelectEvent={event => alert(event.title)}
        onSelectSlot={(slotInfo) => alert(
          `selected slot: \n\nstart ${slotInfo.start.toLocaleString()} ` +
          `\nend: ${slotInfo.end.toLocaleString()}` +
          `\naction: ${slotInfo.action}`
        )}
      />
    </div>
  }

  render() {
    const { children, eventTemplates } = this.props;
    const { selectedTab } = this.state;
    if (!!children) {
      return children;
    }
    return (
      <div>
        <Helmet
          title="Events"
          meta={[
            { name: 'description', content: 'Description of Events' },
          ]}
        />
        <Tabs
          onChange={this.handleChangeTab}
          value={selectedTab}
        >
          <Tab
            label={<FormattedMessage {...messages.eventTemplates} />}
            value={'eventTemplates'}
          >
            <EventTemplateList eventTemplates={eventTemplates} />
          </Tab>
          <Tab
            label={<FormattedMessage {...messages.calendar} />}
            value={'calendar'}
          >
            {this.renderCalendarTab()}
          </Tab>
        </Tabs>
        <FloatingActionButton
          containerElement={<Link to={`${eventsPath}/${createEventPath}`}>.</Link>}
          mini={window.innerWidth < SM}
          primary
          style={{ position: 'fixed', bottom: 12, right: 12, zIndex: 3 }}
        >
          <AddIcon />
        </FloatingActionButton>
      </div>
    );
  }

  /* ----- END Rendering ----- */
}

Events.propTypes = {
  events: PropTypes.object,
  eventTemplates: PropTypes.object,
};

Events.defaultProps = {
  events: fromJS([
    {
      'title': 'All Day Event very long title',
      'allDay': true,
      'start': new Date(2017, 11, 0),
      'end': new Date(2017, 11, 1)
    },
    {
      'title': 'Long Event',
      'start': new Date(2017, 11, 7),
      'end': new Date(2017, 11, 10)
    },
    {
      'title': 'DTS STARTS',
      'start': new Date(2018, 2, 13, 0, 0, 0),
      'end': new Date(2018, 2, 20, 0, 0, 0)
    },

    {
      'title': 'DTS ENDS',
      'start': new Date(2018, 10, 6, 0, 0, 0),
      'end': new Date(2018, 10, 13, 0, 0, 0)
    },

    {
      'title': 'Some Event',
      'start': new Date(2017, 11, 9, 0, 0, 0),
      'end': new Date(2017, 11, 9, 0, 0, 0)
    },
    {
      'title': 'Conference',
      'start': new Date(2017, 11, 11),
      'end': new Date(2017, 11, 13),
      desc: 'Big conference for important people'
    },
    {
      'title': 'Meeting',
      'start': new Date(2017, 11, 12, 10, 30, 0, 0),
      'end': new Date(2017, 11, 12, 12, 30, 0, 0),
      desc: 'Pre-meeting meeting, to prepare for the meeting'
    },
    {
      'title': 'Lunch',
      'start':new Date(2017, 11, 12, 12, 0, 0, 0),
      'end': new Date(2017, 11, 12, 13, 0, 0, 0),
      desc: 'Power lunch'
    },
    {
      'title': 'Meeting',
      'start':new Date(2017, 11, 12,14, 0, 0, 0),
      'end': new Date(2017, 11, 12,15, 0, 0, 0)
    },
    {
      'title': 'Happy Hour',
      'start':new Date(2017, 11, 12, 17, 0, 0, 0),
      'end': new Date(2017, 11, 12, 17, 30, 0, 0),
      desc: 'Most important meal of the day'
    },
    {
      'title': 'Dinner',
      'start':new Date(2017, 11, 12, 20, 0, 0, 0),
      'end': new Date(2017, 11, 12, 21, 0, 0, 0)
    },
    {
      'title': 'Birthday Party',
      'start':new Date(2017, 11, 13, 7, 0, 0),
      'end': new Date(2017, 11, 13, 10, 30, 0)
    },
    {
      'title': 'Birthday Party 2',
      'start':new Date(2017, 11, 13, 7, 0, 0),
      'end': new Date(2017, 11, 13, 10, 30, 0)
    },
    {
      'title': 'Birthday Party 3',
      'start':new Date(2017, 11, 13, 7, 0, 0),
      'end': new Date(2017, 11, 13, 10, 30, 0)
    },
    {
      'title': 'Late Night Event',
      'start':new Date(2017, 11, 17, 19, 30, 0),
      'end': new Date(2017, 11, 18, 2, 0, 0)
    },
    {
      'title': 'Multi-day Event',
      'start':new Date(2017, 11, 20, 19, 30, 0),
      'end': new Date(2017, 11, 22, 2, 0, 0)
    }
  ]),
  eventTemplates: fromJS([]),
};

const mapStateToProps = createStructuredSelector({
  eventTemplates: selectEventTemplates(),
});

function mapDispatchToProps(dispatch) {
  return {
    onLoadEventTemplates: () => dispatch(loadEventTemplates()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Events);
