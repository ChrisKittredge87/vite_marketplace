import { createSelector } from 'reselect';

import { ID } from 'entities/schemas/eventTemplates/constants';
import { selectList as eventTemplatesSelector } from 'entities/schemas/eventTemplates/selectors';

import { EVENT_TEMPLATE_IDS } from './constants';
import { DOMAIN } from './reducer';

/**
 * Direct selector to the events state domain
 */
const selectEventsDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by Events
 */

const selectEventTemplates = () => createSelector(
  (state) => state,
  selectEventsDomain(),
  (state, domain) => eventTemplatesSelector(domain.get(EVENT_TEMPLATE_IDS))(state)
);

const makeSelectEvents = () => createSelector(
  selectEventsDomain(),
  (substate) => substate.toJS()
);

export default makeSelectEvents;
export {
  selectEventsDomain,
  selectEventTemplates,
};
