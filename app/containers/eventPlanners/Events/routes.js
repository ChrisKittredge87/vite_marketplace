import { EVENT_PLANNER } from 'entities/schemas/users/types/constants';
import createEventRoutes from './Create/routes'

export const path = 'ep-events';
export const name = 'Events';

export default function (loadModule, errorLoading, injectReducers, injectSagas) {
  return {
    guard: ({ accountType }) => accountType === EVENT_PLANNER,
    path,
    name,
    getComponent(nextState, cb) {
      const importModules = Promise.all([
        import('containers/eventPlanners/Events/reducer'),
        import('containers/eventPlanners/Events/sagas'),
        import('containers/eventPlanners/Events'),
      ]);

      const renderRoute = loadModule(cb);

      importModules.then(([reducer, sagas, component]) => {
        injectReducers([reducer]);
        injectSagas([sagas]);
        renderRoute(component);
      });
      importModules.catch(errorLoading);
    },
    childRoutes: [
      createEventRoutes(loadModule, errorLoading, injectReducers, injectSagas),
    ],
  };
}
