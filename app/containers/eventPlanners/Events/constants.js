/*
 *
 * Events constants
 *
 */

/* Actions */
export const LOAD_EVENT_TEMPLATES = 'app.eventPlanners.Events.LOAD_EVENT_TEMPATES';
export const EVENT_TEMPLATES_LOADED = 'app.eventPlanners.Events.EVENT_TEMPLATES_LOADED';
export const EVENT_TEMPLATES_LOADING_ERROR = 'app.eventPlanners.Events.EVENT_TEMPLATES_LOADING_ERROR';
export const SET_KEY_VAL = 'app.eventPlanners.Events.SET_KEY_VAL';

/* State Keys */
export const ERRORS = 'errors';
export const EVENT_TEMPLATE_IDS = 'eventTemplateIds';
export const LOADING_EVENT_TEMPLATES = 'loadingEventTemplates';
