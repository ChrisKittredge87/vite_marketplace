import { createSelector } from 'reselect';
import { DOMAIN } from './reducer';

/**
 * Direct selector to the home state domain
 */
const selectHomeDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by Home
 */

const makeSelectHome = () => createSelector(
  selectHomeDomain(),
  (substate) => substate.toJS()
);

export default makeSelectHome;
export {
  selectHomeDomain,
};
