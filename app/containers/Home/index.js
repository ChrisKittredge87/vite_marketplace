/*
 *
 * Home
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { fromJS } from 'immutable';
import Helmet from 'react-helmet';


import RfpStepper from 'components/eventPlanners/EventPlannerWizard';
import EventPlannerHome from 'containers/eventPlanners/Home';
import ServiceProviderHome from 'containers/serviceProviders/Home';
import { makeSelectCurrentUser } from 'containers/Auth/selectors';
import LoadingSpinner from 'components/common/CenteredLoadingSpinner';
import { ACCOUNT_TYPE } from 'entities/schemas/users/constants';
import {
  NAME as ACCOUNT_TYPE_NAME,
  SERVICE_PROVIDER,
} from 'entities/schemas/users/types/constants';
import { selectEntity as selectAccountType } from 'entities/schemas/users/types/selectors';

import makeSelectHome from './selectors';

export class Home extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { currentUser = fromJS({}), getAccountType } = this.props;
    const getCorrectHomeContainer = () => {
      const userAccountType = getAccountType(currentUser.get(ACCOUNT_TYPE)) || fromJS({});
      return userAccountType.get(ACCOUNT_TYPE_NAME) === SERVICE_PROVIDER ?
        <ServiceProviderHome /> : <EventPlannerHome />;
    };
    return (
      <div>
        <Helmet
          title="Radavoo Marketpalce"
          meta={[
            { name: 'description', content: 'Randavoo Marketplace Home' },
          ]}
        />
        {getCorrectHomeContainer()}
      </div>
    );
  }
}

Home.propTypes = {
  currentUser: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
  getAccountType: (state) => (id) => selectAccountType(id)(state),
  Home: makeSelectHome(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
