export const path = '/';
export const name = 'Home';

export default function (loadModule, errorLoading, injectReducers, injectSagas) {
  return {
    path,
    name,
    getComponent(nextState, cb) {
      const importModules = Promise.all([
        import('containers/Home/reducer'),
        import('containers/eventPlanners/Home/reducer'),
        import('containers/serviceProviders/Home/reducer'),
        import('containers/serviceProviders/SpEventList/reducer'),
        import('containers/Home/sagas'),
        import('containers/eventPlanners/Home/sagas'),
        import('containers/serviceProviders/Home/sagas'),
        import('containers/serviceProviders/SpEventList/sagas'),
        import('containers/Home'),
      ]);

      const renderRoute = loadModule(cb);

      importModules.then(([
        homeReducer,
        epHomeReducer,
        spHomeReducer,
        spEventListReducer,
        homeSagas,
        epHomeSagas,
        spHomeSagas,
        spEventListSagas,
        component,
      ]) => {
        injectReducers([homeReducer, epHomeReducer, spHomeReducer, spEventListReducer]);
        injectSagas([homeSagas, epHomeSagas, spHomeSagas, spEventListSagas]);
        renderRoute(component);
      });
      importModules.catch(errorLoading);
    },
  };
}
