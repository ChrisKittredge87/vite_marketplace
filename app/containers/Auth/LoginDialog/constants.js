/*
 *
 * LoginModal constants
 *
 */

/* Actions */
export const CLEAR_LOGIN_FORM = 'app/Auth/LoginModal/CLEAR_LOGIN_FORM';
export const SET_FORM_VAL = 'app/Auth/LoginModal/SET_FORM_VAL';

/* State Keys */
export const ERROR = 'error';
export const USERNAME = 'username';
export const PASSWORD = 'password';
export const PROVIDER = 'provider';
export const STATUS = 'status';
