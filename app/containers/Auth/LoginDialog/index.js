/*
 *
 * LoginDialog
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import Dialog from 'material-ui/Dialog';
import Divider from 'material-ui/Divider';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import LockIcon from 'material-ui/svg-icons/action/lock-outline';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import fbLogo from 'img/facebook_white_logo.png';
import googleLogo from 'img/google_g_logo.png';
import {
  FACEBOOK_BLUE, FB_LOGIN_STATUS_CONNECTED, OAUTH_ACCESS_TOKEN, SM,
} from 'globalConstants';
import {
  FACEBOOK_PROVIDER,
  GOOGLE_PROVIDER,
  LOCAL_PROVIDER,
} from 'entities/constants';

import { login } from 'containers/Auth/actions';
import { clearLoginForm, setFormVal } from './actions';
import { ERROR, USERNAME, PASSWORD } from './constants';
import makeSelectLoginDialog from './selectors';
import messages from './messages';

export class LoginDialog extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.handleAttachGAuth2 = this.handleAttachGAuth2.bind(this);
    this.handleChangeFormVal = this.handleChangeFormVal.bind(this);
    this.handleFBLogin = this.handleFBLogin.bind(this);
    this.handleGoogleLogin = this.handleGoogleLogin.bind(this);
    this.handleSubmitLogin = this.handleSubmitLogin.bind(this);
    this.renderLocalLoginForm = this.renderLocalLoginForm.bind(this);
    this.renderSocialLoginButtons = this.renderSocialLoginButtons.bind(this);
    this.renderRegistrationSection = this.renderRegistrationSection.bind(this);
  }

  /* ------ React Lifecycle ----- */

  componentWillReceiveProps(nextProps) {
    const { onClearLoginForm, onSwitchToRegister, open } = this.props;
    const { loginDialog, open: nextOpen } = nextProps;
    const error = loginDialog.get(ERROR);
    if (open && !nextOpen) {
      onClearLoginForm();
    }
    if (error) {
      onSwitchToRegister();
    }
  }

  /* ------ END React Lifecycle ----- */

  /* ------ Event Binding ----- */

  handleAttachGAuth2(btn) {
    if (!btn) { return; }
    if (window.gapi) {
      window.gapi.auth2
        .getAuthInstance()
        .attachClickHandler(btn.refs.overlay, {}, this.handleGoogleLogin, this.handleGoogleLogin);
    }
  }

  handleChangeFormVal(key) {
    const { onSetFormVal } = this.props;
    return (event) => onSetFormVal(key, event.target.value);
  }

  handleFBLogin() {
    const { onLogin } = this.props;
    window.FB.login((resp) => {
      if (resp.status === FB_LOGIN_STATUS_CONNECTED) {
        localStorage.setItem(OAUTH_ACCESS_TOKEN, resp.authResponse.accessToken);
        onLogin({ provider: FACEBOOK_PROVIDER });
      }
    }, { scope: 'public_profile,email' });
  }

  handleGoogleLogin(user) {
    const { onLogin } = this.props;
    localStorage.setItem(OAUTH_ACCESS_TOKEN, user.getAuthResponse().id_token);
    onLogin({ provider: GOOGLE_PROVIDER });
  }

  handleSubmitLogin(event) {
    event.preventDefault();
    const { loginDialog, onLogin } = this.props;
    onLogin({ username: loginDialog.get(USERNAME), password: loginDialog.get(PASSWORD), provider: LOCAL_PROVIDER });
  }

  /* ------ END Event Binding ----- */

  /* ------ Rendering ----- */

  renderLocalLoginForm() {
    const { loginDialog } = this.props;
    return (
      <form style={{ margin: '10px 0' }} onSubmit={this.handleSubmitLogin}>
        <div>
          <TextField
            fullWidth
            hintText={<FormattedMessage {...messages.emailAddress} />}
            onChange={this.handleChangeFormVal(USERNAME)}
            value={loginDialog.get(USERNAME)}
          />
          <EmailIcon style={{ color: 'gray', position: 'absolute', right: 30, marginTop: 7 }} />
        </div>
        <div>
          <TextField
            fullWidth
            hintText={<FormattedMessage {...messages.password} />}
            onChange={this.handleChangeFormVal(PASSWORD)}
            type="password"
            value={loginDialog.get(PASSWORD)}
          />
          <LockIcon style={{ color: 'gray', position: 'absolute', right: 30, marginTop: 7 }} />
        </div>
        <div style={{ width: '100%', textAlign: 'center', margin: '10px 0' }}>
          <RaisedButton
            label={<FormattedMessage {...messages.logIn} />}
            primary
            style={{ height: 50, width: '100%' }}
            type="submit"
          />
        </div>
      </form>
    );
  }

  renderSocialLoginButtons() {
    return (
      <div style={{ margin: '10px 0' }}>
        <div style={{ width: '100%' }}>
          <RaisedButton
            backgroundColor={FACEBOOK_BLUE}
            labelStyle={{ color: 'white' }}
            style={{ height: 50, width: '100%' }}
            onClick={this.handleFBLogin}
          >
            <div style={{ color: 'white' }}>
              <img
                alt="facebook_logo"
                src={fbLogo}
                height="30"
                style={{ position: 'absolute', left: 10, top: 10 }}
              />
              <FormattedMessage {...messages.logInWithFacebook} />
            </div>
          </RaisedButton>
        </div>
        <div style={{ width: '100%', margin: '10px 0' }}>
          <RaisedButton
            id="g-signin2"
            ref={this.handleAttachGAuth2}
            style={{ height: 50, width: '100%', borderColor: 'darkgray', borderRadius: 5 }}
          >
            <div>
              <img
                alt="google_logo"
                src={googleLogo}
                height="30"
                style={{ position: 'absolute', left: 10, top: 10 }}
              />
              <FormattedMessage {...messages.logInWithGoogle} />
            </div>
          </RaisedButton>
        </div>
      </div>
    );
  }

  renderRegistrationSection() {
    const { onSwitchToRegister } = this.props;
    return (
      <div>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%', marginTop: 25 }}>
          <FormattedMessage {...messages.createANewAccount} />
          <RaisedButton
            label={<FormattedMessage {...messages.createAccount} />}
            onClick={onSwitchToRegister}
            secondary
          />
        </div>
      </div>
    );
  }

  render() {
    const { onRequestClose, open, showSocialLoginButtons } = this.props;
    const contentStyle = window.innerWidth < SM ? { maxWidth: 'none', width: '96%' } : {};
    return (
      <div>
        <Dialog
          contentStyle={contentStyle}
          onRequestClose={onRequestClose}
          open={!!open}
        >
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <h2 style={{ margin: '7px 0', textAlign: 'center' }}>
              <FormattedMessage {...messages.randavooMarketplace} />
            </h2>
            <Divider />
            {showSocialLoginButtons ? this.renderSocialLoginButtons() : null}
            {showSocialLoginButtons ? <Divider /> : null}
            {this.renderLocalLoginForm()}
            <Divider />
            {this.renderRegistrationSection()}
          </div>
        </Dialog>
      </div>
    );
  }

  /* ------ END Rendering ----- */
}

LoginDialog.propTypes = {
  loginDialog: PropTypes.object,
  open: PropTypes.bool,
  showSocialLoginButtons: PropTypes.bool,
  // callbacks
  onClearLoginForm: PropTypes.func.isRequired,
  onLogin: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  onSetFormVal: PropTypes.func.isRequired,
  onSwitchToRegister: PropTypes.func.isRequired,
};

LoginDialog.defaultProps = {
  showSocialLoginButtons: false,
};

const mapStateToProps = createStructuredSelector({
  loginDialog: makeSelectLoginDialog(),
});

function mapDispatchToProps(dispatch) {
  return {
    onClearLoginForm: () => dispatch(clearLoginForm()),
    onLogin: (options) => dispatch(login(null, options)),
    onSetFormVal: (key, val) => dispatch(setFormVal(key, val)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginDialog);
