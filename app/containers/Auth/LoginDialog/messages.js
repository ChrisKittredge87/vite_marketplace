/*
 * LoginDialog Messages
 *
 * This contains all the text for the LoginDialog component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  createAccount: {
    id: 'createAccount',
    defaultMessage: 'Create Account',
  },
  createANewAccount: {
    id: 'createANewAccount',
    defaultMessage: 'Create A New Account',
  },
  emailAddress: {
    id: 'emailAddress',
    defaultMessage: 'Email Address',
  },
  logIn: {
    id: 'app.containers.Auth.LoginDialog.logIn',
    defaultMessage: 'Log In',
  },
  logInWithFacebook: {
    id: 'loginWithFacebook',
    defaultMessage: 'Log In in with Facebook',
  },
  logInWithGoogle: {
    id: 'logInWithGoogle',
    defaultMessage: 'Login in with Google',
  },
  password: {
    id: 'password',
    defaultMessage: 'Password',
  },
  randavooMarketplace: {
    id: 'randavooMarketplace',
    defaultMessage: 'Randavoo | Events Marketplace',
  },
});
