/*
 *
 * LoginDialog reducer
 *
 */

import { fromJS } from 'immutable';

import { LOGIN_SUCCESS, LOGIN_ERROR } from 'containers/Auth/constants';
import {
  /* Actions */
  CLEAR_LOGIN_FORM,
  SET_FORM_VAL,
  /* State Keys */
  ERROR,
  USERNAME,
  PASSWORD,
  PROVIDER, STATUS,
} from './constants';

const initialState = fromJS({
  [USERNAME]: '',
  [PASSWORD]: '',
});

function loginDialogReducer(state = initialState, action) {
  const {
    key, type, val,
    error = { response: { url: '', status: -1 } },
  } = action;
  switch (type) {
    case CLEAR_LOGIN_FORM:
      return initialState;
    case LOGIN_SUCCESS:
      return initialState;
    case LOGIN_ERROR:
      return state.set(ERROR, fromJS({
        [PROVIDER]: (error.response.url.split('auth/') || [])[1],
        [STATUS]: error.response.status,
      }));
    case SET_FORM_VAL:
      return state.set(key, val);
    default:
      return state;
  }
}

export default loginDialogReducer;

export const DOMAIN = 'loginDialog';
