/*
 *
 * LoginModal actions
 *
 */

import {
  CLEAR_LOGIN_FORM,
  SET_FORM_VAL,
} from './constants';

export function clearLoginForm() {
  return { type: CLEAR_LOGIN_FORM };
}

export function setFormVal(key, val) {
  return { type: SET_FORM_VAL, key, val };
}
