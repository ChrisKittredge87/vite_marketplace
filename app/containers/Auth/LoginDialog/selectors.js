import { createSelector } from 'reselect';

import { DOMAIN } from './reducer';

/**
 * Direct selector to the loginModal state domain
 */
const selectLoginDialogDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by LoginModal
 */

const makeSelectLoginDialog = () => createSelector(
  selectLoginDialogDomain(),
  (substate) => substate
);

export default makeSelectLoginDialog;
export {
  selectLoginDialogDomain,
};
