/*
 *
 * Auth constants
 *
 */

 /* Actions */
 export const CLEAR_USER = 'app/Auth/CLEAR_USER';
 export const AUTH = 'app/Auth/AUTH';
 export const AUTH_SUCCESS = 'app/Auth/AUTH_SUCCESS';
 export const AUTH_ERROR = 'app/Auth/AUTH_ERROR';
 export const EXISTING_SESSION = 'app/Auth/EXISITING_SESSION';
 export const EXISTING_SESSION_SUCCESS = 'app/Auth/EXISITING_SESSION_SUCCESS';
 export const EXISTING_SESSION_ERROR = 'app/Auth/EXISITING_SESSION_ERROR';
 export const LOGIN = 'app/Auth/LOGIN';
 export const LOGIN_SUCCESS = 'app/Auth/LOGIN_SUCCESS';
 export const LOGIN_ERROR = 'app/Auth/LOGIN_ERROR';
 export const LOGOUT = 'app/Auth/LOGOUT';
 export const LOGOUT_SUCCESS = 'app/Auth/LOGOUT_SUCCESS';
 export const LOGOUT_ERROR = 'app/Auth/LOGOUT_ERROR';
 export const REGISTER = 'app/Auth/RegisterDialog/REGISTER';
 export const REGISTER_SUCCESS = 'app/Auth/RegisterDialog/REGISTER_SUCCESS';
 export const REGISTER_ERROR = 'app/Auth/RegisterDialog/REGISTER_ERROR';
 export const USER_EXISTS = 'app/Auth/RegisterDialog/USER_EXISTS';
 export const USER_EXISTS_SUCCESS = 'app/Auth/RegisterDialog/USER_EXISTS_SUCCESS';
 export const USER_EXISTS_ERROR = 'app/Auth/RegisterDialog/USER_EXISTS_ERROR';

 /* State Keys */
 export const ERRORS = 'errors';
 export const LOADING_AUTH = 'loadingAuth';
 export const USER_ID = 'userId';
