/*
 *
 * Authorize
 *
 */

import React, { PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { fromJS } from 'immutable';
import _ from 'lodash';

import Spinner from 'components/common/CenteredLoadingSpinner';
import { path as homePath } from 'containers/Home/routes';
import { NAME } from 'entities/schemas/users/types/constants';

import { auth } from './actions';
import { LOADING_AUTH } from './constants';
import makeSelectAuthorize,
  {
    makeSelectCurrentUserAccountType,
    makeSelectCurrentUser,
  } from './selectors';

export class Auth extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
  }

  /* ----- Lifecycle ----- */

  componentWillMount() {
    const { currentUser } = this.props;
    if (!currentUser) {
      this.props.onAuth();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { authState, currentUser, currentAccountType = fromJS({}) } = nextProps;
    const guardFn = this.getGuardFn(nextProps);
    if (authState.get(LOADING_AUTH) || !guardFn) {
      return;
    }
    const accountType = currentAccountType.get(NAME);
    if (location.pathname !== homePath && !guardFn({ accountType, currentUser })) {
      browserHistory.replace(homePath);
    }
  }

  /* ----- END Lifecycle ----- */

  /* ----- Helpers ----- */

  getGuardFn(props) {
    const { children } = props;
    return _.get(children, 'props.route.guard');
  }

  /* ----- END Helpers ----- */

  /* ----- Rendering ----- */

  render() {
    const { children, currentUser, currentAccountType } = this.props;
    const guardFn = this.getGuardFn(this.props);
    if (!guardFn) {
      return this.props.children;
    }
    if (!currentAccountType) {
      return <Spinner containerHeight={'calc(100vh - 64px)'} />;
    }
    const accountType = currentAccountType.get(NAME);
    return guardFn({ accountType, currentUser }) ?
      children : <Spinner containerHeight={'calc(100vh - 64px)'} />;
  }

  /* ----- END Rendering ----- */
}

Auth.propTypes = {
  authState: PropTypes.object,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]),
  currentAccountType: PropTypes.object,
  currentUser: PropTypes.object,
  onAuth: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  authState: makeSelectAuthorize(),
  currentAccountType: makeSelectCurrentUserAccountType(),
  currentUser: makeSelectCurrentUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    onAuth: () => dispatch(auth()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
