/*
 *
 * RegisterDialog constants
 *
 */

/* Actions */
export const CLEAR_REGISTER_FORM = 'app/Auth/RegisterDialog/CLEAR_REGISTER_FORM';
export const LOAD_USER_TYPES = 'app/Auth/RegisterDialog/LOAD_USER_TYPES';
export const LOAD_USER_TYPES_SUCCESS = 'app/Auth/RegisterDialog/LOAD_USER_TYPES_SUCCESS';
export const LOAD_USER_TYPES_ERROR = 'app/Auth/RegisterDialog/LOAD_USER_TYPES_ERROR';
export const SET_FORM_VAL = 'app/Auth/RegisterDialog/SET_FORM_VAL';
export const SET_KEY_VAL = 'app/Auth/RegisterDialog/SET_KEY_VAL';
export const SET_SELECTED_PROVIDER_TYPE = 'app/Auth/RegisterDialog/SET_SELECTED_PROVIDER_TYPE';

/* State Keys */
export const AUTH_ID = 'authId';
export const CONFIRM_PASSWORD = 'confirmPassword';
export const FIRST_NAME = 'firstName';
export const HAS_CHECKED_FOR_EXISTING_USER = 'hasCheckedForExistingUser';
export const LAST_NAME = 'lastName';
export const LOADING = 'loading';
export const PASSWORD = 'password';
export const SELECTED_PROVIDER_TYPE = 'selectedProviderType';
export const SELECTED_USER_TYPE = 'selectedUserType';
export const USERNAME = 'username';
export const USER_EXISTS = 'userExists';
export const USER_TYPE_IDS = 'userTypeIds';
