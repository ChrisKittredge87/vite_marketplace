import { call, put, takeLatest } from 'redux-saga/effects';

import { BASE_URL } from 'entities/schemas/users/types/constants';
import request from 'utils/request';

import {
  loadUserTypesSuccess,
  loadUserTypesError,
} from './actions';
import { LOAD_USER_TYPES } from './constants';

// Individual exports for testing
export function* getUserTypes() {
  try {
    const userTypes = yield call(request.get, BASE_URL);
    yield put(loadUserTypesSuccess(userTypes));
  } catch (err) {
    yield put(loadUserTypesError(err));
  }
}

export function* userTypesData() {
  yield takeLatest(LOAD_USER_TYPES, getUserTypes);
}

// All sagas to be loaded
export default [
  userTypesData,
];

export const NAME = 'registerDialogSagas';
