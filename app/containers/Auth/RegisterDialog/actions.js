/*
 *
 * RegisterDialog actions
 *
 */

import { createSagaActions } from 'utils/sagas';
import { list as userTypesSchema } from 'entities/schemas/users/types';
import {
  CLEAR_REGISTER_FORM,
  LOAD_USER_TYPES, LOAD_USER_TYPES_SUCCESS, LOAD_USER_TYPES_ERROR,
  SET_KEY_VAL, SET_SELECTED_PROVIDER_TYPE,
} from './constants';

export function clearRegisterForm() {
  return { type: CLEAR_REGISTER_FORM };
}

export function setKeyVal(key, val) {
  return { type: SET_KEY_VAL, key, val };
}

export function setSelectedProviderType(val) {
  return { type: SET_SELECTED_PROVIDER_TYPE, val };
}

export const {
  loadEntity: loadUserTypes,
  entityLoaded: loadUserTypesSuccess,
  entityLoadingError: loadUserTypesError,
} = createSagaActions({
  loadAction: LOAD_USER_TYPES,
  entityLoadedAction: LOAD_USER_TYPES_SUCCESS,
  entityLoadingErrorAction: LOAD_USER_TYPES_ERROR,
  schema: userTypesSchema,
  notifyOnError: (error) => `Error loading user types! ${error}`,
});
