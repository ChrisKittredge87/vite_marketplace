import { createSelector } from 'reselect';

import makeSelectEntities from 'entities/selectors';
import { SCHEMA_KEY } from 'entities/schemas/users/types/constants';

import { USER_TYPE_IDS } from './constants';
import { DOMAIN } from './reducer';

/**
 * Direct selector to the registerModal state domain
 */
const selectRegisterDialogDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by RegisterModal
 */

const makeSelectRegisterDialog = () => createSelector(
  selectRegisterDialogDomain(),
  (substate) => substate
);

const makeSelectUserTypes = () => createSelector(
  selectRegisterDialogDomain(),
  makeSelectEntities(),
  (dialogState, entities) =>
    dialogState.get(USER_TYPE_IDS).map((id) => entities.getIn([SCHEMA_KEY, `${id}`]))
);


export default makeSelectRegisterDialog;
export {
  makeSelectUserTypes,
  selectRegisterDialogDomain,
};
