/*
 *
 * LoginModal reducer
 *
 */

import { fromJS } from 'immutable';

import { LOGIN_SUCCESS, USER_EXISTS_SUCCESS } from 'containers/Auth/constants';
import { LOCAL_PROVIDER } from 'entities/constants';
import {
  /* Actions */
  CLEAR_REGISTER_FORM,
  LOADING,
  LOAD_USER_TYPES, LOAD_USER_TYPES_SUCCESS, LOAD_USER_TYPES_ERROR,
  SET_KEY_VAL, SET_SELECTED_PROVIDER_TYPE,
  /* State Keys */
  FIRST_NAME, LAST_NAME,
  USERNAME, PASSWORD, CONFIRM_PASSWORD,
  SELECTED_USER_TYPE,
  SELECTED_PROVIDER_TYPE,
  USER_EXISTS,
  USER_TYPE_IDS,
} from './constants';

const setInitialState = (state) =>
  state
    .set(FIRST_NAME, '')
    .set(LAST_NAME, '')
    .set(USERNAME, '')
    .set(PASSWORD, '')
    .set(CONFIRM_PASSWORD, '')
    .set(SELECTED_PROVIDER_TYPE, false)
    .set(SELECTED_USER_TYPE, false)
    .set(USER_EXISTS, false);

const initialState = setInitialState(fromJS({ [USER_TYPE_IDS]: [] }));

function registerDialogReducer(state = initialState, action) {
  const { key, result, type, val } = action;
  switch (type) {
    case CLEAR_REGISTER_FORM:
      return setInitialState(state);
    case LOAD_USER_TYPES:
      return state.set(LOADING, true);
    case LOAD_USER_TYPES_SUCCESS:
      return state
        .set(LOADING, false)
        .set(USER_TYPE_IDS, fromJS(result));
    case LOAD_USER_TYPES_ERROR:
      return state.set(LOADING, false);
    case LOGIN_SUCCESS:
      return setInitialState(state);
    case SET_KEY_VAL:
      return state.set(key, val);
    case SET_SELECTED_PROVIDER_TYPE:
      return state.set(SELECTED_PROVIDER_TYPE, val)
        .set(FIRST_NAME, '').set(LAST_NAME, '')
        .set(USERNAME, '').set(PASSWORD, '').set(CONFIRM_PASSWORD, '')
        .set(SELECTED_USER_TYPE, false)
        .set(USER_EXISTS, false);
    case USER_EXISTS_SUCCESS:
      return state
        .set(SELECTED_PROVIDER_TYPE, !val || action.providerType === LOCAL_PROVIDER ? action.providerType : false)
        .set(USER_EXISTS, val);
    default:
      return state;
  }
}

export default registerDialogReducer;

export const DOMAIN = 'registerDialog';
