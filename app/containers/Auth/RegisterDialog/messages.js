/*
 * RegisterDialog Messages
 *
 * This contains all the text for the RegisterDialog component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  alreadyHaveAnAccount: {
    id: 'alreadyHaveAnAccount',
    defaultMessage: 'Already have an account?',
  },
  completeRegistrationWith: {
    id: 'app.containers.Auth.RegisterDialog.completeRegistrationWith',
    defaultMessage: 'Complete registration with ',
  },
  confirmPassword: {
    id: 'confirmPassword',
    defaultMessage: 'Confirm Password',
  },
  createAccount: {
    id: 'createAccount',
    defaultMessage: 'Create Account',
  },
  emailAddress: {
    id: 'emailAddress',
    defaultMessage: 'Email Address',
  },
  facebook: {
    id: 'facebook',
    defaultMessage: 'Facebook',
  },
  firstName: {
    id: 'firstName',
    defaultMessage: 'First Name',
  },
  google: {
    id: 'google',
    defaultMessage: 'Google',
  },
  lastName: {
    id: 'lastName',
    defaultMessage: 'Last Name',
  },
  logIn: {
    id: 'logIn',
    defaultMessage: 'Log In',
  },
  logInQuestion: {
    id: 'logInQuestion',
    defaultMessage: 'Log In?',
  },
  password: {
    id: 'password',
    defaultMessage: 'Password',
  },
  randavooMarketplace: {
    id: 'randavooMarketplace',
    defaultMessage: 'Randavoo | Events Marketplace',
  },
  selectAUserType: {
    id: 'selectAUserType',
    defaultMessage: 'Select a user type...',
  },
  signUpWithEmail: {
    id: 'signUpWithEmail',
    defaultMessage: 'Sign up in with Email',
  },
  signUpWithFacebook: {
    id: 'signUpWithFacebook',
    defaultMessage: 'Sign up in with Facebook',
  },
  signUpWithGoogle: {
    id: 'signUpWithGoogle',
    defaultMessage: 'Sign Up in with Google',
  },
  userAlreadyExistsForThisAccount: {
    id: 'userAlreadyExistsForThisAccount',
    defaultMessage: 'User already exists for this account',
  },
});
