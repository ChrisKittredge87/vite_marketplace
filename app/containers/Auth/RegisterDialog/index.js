/*
 *
 * RegisterDialog
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import Dialog from 'material-ui/Dialog';
import Divider from 'material-ui/Divider';
import DropDownMenu from 'material-ui/DropDownMenu';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import FlatButton from 'material-ui/FlatButton';
import LockIcon from 'material-ui/svg-icons/action/lock-outline';
import MenuItem from 'material-ui/MenuItem';
import PersonIcon from 'material-ui/svg-icons/social/person';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import fbLogo from 'img/facebook_white_logo.png';
import googleLogo from 'img/google_g_logo.png';
import {
  FACEBOOK_BLUE, FB_LOGIN_STATUS_CONNECTED, OAUTH_ACCESS_TOKEN, SM,
} from 'globalConstants';
import { FACEBOOK_PROVIDER, GOOGLE_PROVIDER, LOCAL_PROVIDER } from 'entities/constants';
import { ID, DISPLAY_NAME } from 'entities/schemas/users/types/constants';

import { register, userExists } from 'containers/Auth/actions';
import { clearRegisterForm, loadUserTypes, setKeyVal, setSelectedProviderType } from './actions';
import {
  LOADING,
  AUTH_ID,
  FIRST_NAME, LAST_NAME,
  USERNAME, PASSWORD, CONFIRM_PASSWORD,
  SELECTED_PROVIDER_TYPE,
  SELECTED_USER_TYPE,
  USER_EXISTS,
} from './constants';
import makeSelectRegisterDialog, { makeSelectUserTypes } from './selectors';
import messages from './messages';

export class RegisterDialog extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.handleAttachGAuth2 = this.handleAttachGAuth2.bind(this);
    this.handleChangeFormVal = this.handleChangeFormVal.bind(this);
    this.handleCheckLocalUserExists = this.handleCheckLocalUserExists.bind(this);
    this.handleFBLogin = this.handleFBLogin.bind(this);
    this.handleGoogleLogin = this.handleGoogleLogin.bind(this);
    this.handleRegistration = this.handleRegistration.bind(this);
    this.handleToggleRegisterFormProvider = this.handleToggleRegisterFormProvider.bind(this);
    this.renderLoginSection = this.renderLoginSection.bind(this);
    this.renderLocalRegisterFormContent = this.renderLocalRegisterFormContent.bind(this);
    this.renderRegisterForm = this.renderRegisterForm.bind(this);
    this.renderTopRegisterSection = this.renderTopRegisterSection.bind(this);
    this.renderUserAlreadyExistsSection = this.renderUserAlreadyExistsSection.bind(this);
  }

  /* ------ React Lifecycle ----- */

  componentWillMount() {
    const { onLoadUserTypes } = this.props;
    onLoadUserTypes();
  }

  componentWillReceiveProps(nextProps) {
    const {
      onClearRegisterForm,
      onLoadUserTypes,
      open,
      registerDialog,
    } = this.props;
    const {
      open: nextOpen,
      registerDialog: nextRegisterDialog,
      userTypes: nextUserTypes,
    } = nextProps;
    if (!nextRegisterDialog.get(LOADING) && nextUserTypes.size === 0) {
      onLoadUserTypes();
    }
    if (nextOpen && nextOpen !== open) {
      // if (window.gapi) {
      //   const existingGoogleUser = window.gapi.auth2
      //     .getAuthInstance().currentUser.get();
      //   if (existingGoogleUser && existingGoogleUser.isSignedIn()) {
      //     this.handleGoogleLogin(existingGoogleUser);
      //   }
      // }
      // if (window.FB) {
      //   const existingFBAccessToken = window.FB.getAccessToken();
      //   if (existingFBAccessToken) {
      //     this.handleFBLogin(existingFBAccessToken);
      //   }
      // }
    } else if (open && !nextOpen) {
      onClearRegisterForm();
    }
  }

  /* ------ END React Lifecycle ----- */

  /* ------ Event Binding ----- */

  handleAttachGAuth2(btn) {
    if (!btn || !window.gapi) { return; }
    window.gapi.auth2
      .getAuthInstance()
      .attachClickHandler(btn.refs.overlay, {}, this.handleGoogleLogin, this.handleGoogleLogin);
  }

  handleChangeFormVal(key) {
    const { onSetKeyVal } = this.props;
    return (event, index, value) => onSetKeyVal(key, value != null ? value : event.target.value);
  }

  handleCheckLocalUserExists(event) {
    const { onCheckUserExists } = this.props;
    onCheckUserExists(LOCAL_PROVIDER, null, event.target.value);
  }

  handleFBLogin(fbAccessToken) {
    const { onCheckUserExists, onSetKeyVal } = this.props;
    const getUserDetails = (loginResp) => {
      const url = '/me?fields=first_name,last_name,email';
      window.FB.api(url, (response) => {
        onCheckUserExists(FACEBOOK_PROVIDER, response.id);
        onSetKeyVal(AUTH_ID, response.id);
        onSetKeyVal(FIRST_NAME, response.first_name);
        onSetKeyVal(LAST_NAME, response.last_name);
        onSetKeyVal(USERNAME, response.email);
      });
      localStorage.setItem(OAUTH_ACCESS_TOKEN, loginResp ? loginResp.authResponse.accessToken : fbAccessToken);
    };
    if (fbAccessToken) {
      getUserDetails();
    }
    window.FB.login((loginResp) => {
      if (loginResp.status === FB_LOGIN_STATUS_CONNECTED) {
        getUserDetails(loginResp);
      }
    }, { scope: 'public_profile,email' });
  }

  handleGoogleLogin(user) {
    const { onCheckUserExists, onSetKeyVal } = this.props;
    const googleProfile = user.getBasicProfile();
    onCheckUserExists(GOOGLE_PROVIDER, googleProfile.getId());
    onSetKeyVal(AUTH_ID, googleProfile.getId());
    onSetKeyVal(FIRST_NAME, googleProfile.getGivenName());
    onSetKeyVal(LAST_NAME, googleProfile.getFamilyName());
    onSetKeyVal(USERNAME, googleProfile.getEmail());
    localStorage.setItem(OAUTH_ACCESS_TOKEN, user.getAuthResponse().id_token);
  }

  handleRegistration(event) {
    event.preventDefault();
    const { onRegister } = this.props;
    onRegister();
  }

  handleToggleRegisterFormProvider(registrationType) {
    return () => {
      const { onSetSelectedProviderType } = this.props;
      onSetSelectedProviderType(registrationType);
    };
  }

  /* ------ END Event Binding ----- */

  /* ------ Rendering ----- */

  renderBottomRegisterSection() {
    return (
      <div style={{ width: '100%', textAlign: 'center', margin: '10px 0' }}>
        {this.renderRegisterButton(LOCAL_PROVIDER)}
      </div>
    );
  }

  renderLocalRegisterFormContent() {
    const { registerDialog } = this.props;
    return (<div>
      <div>
        <TextField
          autoComplete={'off'}
          fullWidth
          hintText={<FormattedMessage {...messages.emailAddress} />}
          onChange={this.handleChangeFormVal(USERNAME)}
          // onBlur={this.handleCheckLocalUserExists}
          value={registerDialog.get(USERNAME)}
        />
        <EmailIcon style={{ color: 'gray', position: 'absolute', right: 30, marginTop: 7 }} />
      </div>
      <div>
        <TextField
          autoComplete={'off'}
          fullWidth
          hintText={<FormattedMessage {...messages.password} />}
          onChange={this.handleChangeFormVal(PASSWORD)}
          type="password"
          value={registerDialog.get(PASSWORD)}
        />
        <LockIcon style={{ color: 'gray', position: 'absolute', right: 30, marginTop: 7 }} />
      </div>
      <div>
        <TextField
          autoComplete={'off'}
          fullWidth
          hintText={<FormattedMessage {...messages.confirmPassword} />}
          onChange={this.handleChangeFormVal(CONFIRM_PASSWORD)}
          type="password"
          value={registerDialog.get(CONFIRM_PASSWORD)}
        />
        <LockIcon style={{ color: 'gray', position: 'absolute', right: 30, marginTop: 7 }} />
      </div>
      <div>
        <TextField
          autoComplete={'off'}
          fullWidth
          hintText={<FormattedMessage {...messages.firstName} />}
          onChange={this.handleChangeFormVal(FIRST_NAME)}
          value={registerDialog.get(FIRST_NAME)}
        />
        <PersonIcon style={{ color: 'gray', position: 'absolute', right: 30, marginTop: 7 }} />
      </div>
      <div>
        <TextField
          autoComplete={'off'}
          fullWidth
          hintText={<FormattedMessage {...messages.lastName} />}
          onChange={this.handleChangeFormVal(LAST_NAME)}
          value={registerDialog.get(LAST_NAME)}
        />
        <PersonIcon style={{ color: 'gray', position: 'absolute', right: 30, marginTop: 7 }} />
      </div>
    </div>);
  }

  renderLoginSection() {
    const { onSwitchToLogin } = this.props;
    return (
      <div>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%', marginTop: 25 }}>
          <FormattedMessage {...messages.alreadyHaveAnAccount} />
          <RaisedButton
            label={<FormattedMessage {...messages.logIn} />}
            onClick={onSwitchToLogin}
            secondary
          />
        </div>
      </div>
    );
  }

  renderRegisterButton(providerType) {
    const isMobile = window.innerWidth < SM;
    const localBtn = (<RaisedButton
      key={`local_register_btn${Math.random()}`}
      labelStyle={{ color: 'white' }}
      onClick={this.handleToggleRegisterFormProvider(LOCAL_PROVIDER)}
      primary
      style={{ height: 50, width: '100%' }}
    >
      <div style={{ color: 'white' }}>
        <EmailIcon
          style={{ color: 'white', position: 'absolute', left: 10, top: 12, fontSize: '6px' }}
        />
        <FormattedMessage {...messages.signUpWithEmail} />
      </div>
    </RaisedButton>);
    const fbBtn = (<RaisedButton
      backgroundColor={FACEBOOK_BLUE}
      key={`fb_register_btn${Math.random()}`}
      labelStyle={{ color: 'white' }}
      style={{ height: 50, width: '100%' }}
      onClick={this.handleFBLogin}
    >
      <div style={{ color: 'white' }}>
        <img
          alt="facebook_logo"
          src={fbLogo}
          height="30"
          style={{ position: 'absolute', left: 10, top: 10 }}
        />
        <FormattedMessage {...messages[isMobile ? 'facebook' : 'signUpWithFacebook']} />
      </div>
    </RaisedButton>);
    const googleBtn = (<RaisedButton
      key={`google_register_btn${Math.random()}`}
      id="g-signin2"
      ref={this.handleAttachGAuth2}
      style={{ height: 50, width: '100%', borderColor: 'darkgray', borderRadius: 5 }}
    >
      <div>
        <img
          alt="google_logo"
          src={googleLogo}
          height="30"
          style={{ position: 'absolute', left: 10, top: 10 }}
        />
        <FormattedMessage {...messages[isMobile ? 'google' : 'signUpWithGoogle']} />
      </div>
    </RaisedButton>);
    switch (providerType) {
      case LOCAL_PROVIDER:
        return localBtn;
      case FACEBOOK_PROVIDER:
        return fbBtn;
      case GOOGLE_PROVIDER:
        return googleBtn;
      default:
        return localBtn;
    }
  }

  renderRegisterForm() {
    const { registerDialog, userTypes } = this.props;
    const selectedProviderType = registerDialog.get(SELECTED_PROVIDER_TYPE);
    const shouldDisable = () => {
      if (registerDialog.get(SELECTED_PROVIDER_TYPE) === LOCAL_PROVIDER) {
        return registerDialog.get(USER_EXISTS) || !registerDialog.get(SELECTED_USER_TYPE) ||
          !registerDialog.get(USERNAME) || !registerDialog.get(PASSWORD) || !registerDialog.get(CONFIRM_PASSWORD) ||
          !registerDialog.get(FIRST_NAME) || !registerDialog.get(LAST_NAME);
      }
      return registerDialog.get(USER_EXISTS) || !registerDialog.get(SELECTED_USER_TYPE);
    };
    return (<form style={{ margin: '10px 0' }}>
      {selectedProviderType === LOCAL_PROVIDER ?
        this.renderLocalRegisterFormContent() :
        <div style={{ textAlign: 'right', color: 'green' }}>
          <FormattedMessage
            {...messages.completeRegistrationWith}
            values={{
              provider: selectedProviderType === FACEBOOK_PROVIDER ?
                <FormattedMessage {...messages.facebook} /> :
                <FormattedMessage {...messages.google} />,
            }}
          />
        </div>
      }
      <div>
        <DropDownMenu
          autoWidth={false}
          iconStyle={{ right: 0 }}
          labelStyle={{ padding: 0 }}
          onChange={this.handleChangeFormVal(SELECTED_USER_TYPE)}
          style={{ padding: 0, width: '100%' }}
          underlineStyle={{ margin: 0 }}
          value={registerDialog.get(SELECTED_USER_TYPE)}
        >
          <MenuItem
            key={'user_type_default'}
            primaryText={<FormattedMessage {...messages.selectAUserType} />}
            value={false}
          />
          {
            userTypes.map((userType) =>
              (<MenuItem
                key={`user_type_${userType.get(ID)}`}
                primaryText={userType.get(DISPLAY_NAME)}
                style={{ width: '100%' }}
                value={userType.get(ID)}
              />))
          }
        </DropDownMenu>
      </div>
      <div style={{ width: '100%', textAlign: 'center', margin: '10px 0' }}>
        <RaisedButton
          disabled={shouldDisable()}
          label={<FormattedMessage {...messages.createAccount} />}
          onClick={this.handleRegistration}
          primary
          style={{ height: 50, width: '100%' }}
          type="submit"
        />
      </div>
    </form>);
  }

  renderTopRegisterSection() {
    const { registerDialog } = this.props;
    const selectedProviderType = registerDialog.get(SELECTED_PROVIDER_TYPE);
    let leftProvider;
    let rightProvider;
    switch (selectedProviderType) {
      case LOCAL_PROVIDER:
        leftProvider = FACEBOOK_PROVIDER;
        rightProvider = GOOGLE_PROVIDER;
        break;
      case FACEBOOK_PROVIDER:
        leftProvider = GOOGLE_PROVIDER;
        rightProvider = LOCAL_PROVIDER;
        break;
      case GOOGLE_PROVIDER:
        leftProvider = FACEBOOK_PROVIDER;
        rightProvider = LOCAL_PROVIDER;
        break;
      default:
        leftProvider = FACEBOOK_PROVIDER;
        rightProvider = GOOGLE_PROVIDER;
    }
    return (
      <div style={{ display: selectedProviderType ? 'flex' : null, margin: '10px 0' }}>
        <div
          style={{
            width: selectedProviderType ? '50%' : '100%',
            marginRight: selectedProviderType ? 5 : null,
          }}
        >
          {this.renderRegisterButton(leftProvider)}
        </div>
        <div
          style={{
            width: selectedProviderType ? '50%' : '100%',
            margin: selectedProviderType ? '0 0 0 5px' : '10px 0',
          }}
        >
          {this.renderRegisterButton(rightProvider)}
        </div>
      </div>
    );
  }

  renderUserAlreadyExistsSection() {
    const { onSwitchToLogin } = this.props;
    return (<div style={{ paddingBottom: 7 }}>
      <span><FormattedMessage {...messages.userAlreadyExistsForThisAccount} />
        <FlatButton label={<FormattedMessage {...messages.logInQuestion} />} onClick={onSwitchToLogin} />
      </span>
    </div>);
  }

  render() {
    const { onRequestClose, open, registerDialog, showSocialRegisterButtons } = this.props;
    const selectedProviderType = registerDialog.get(SELECTED_PROVIDER_TYPE);
    const userAlreadyExists = registerDialog.get(USER_EXISTS);
    const contentStyle = window.innerWidth < SM ? { maxWidth: 'none', width: '96%' } : {};
    return (
      <div>
        <Dialog
          autoScrollBodyContent
          contentStyle={contentStyle}
          onRequestClose={onRequestClose}
          open={!!open}
        >
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <h2 style={{ margin: '7px 0', textAlign: 'center' }}>
              <FormattedMessage {...messages.randavooMarketplace} />
            </h2>
            <Divider />
            {userAlreadyExists ? this.renderUserAlreadyExistsSection() : null}
            {showSocialRegisterButtons ? this.renderTopRegisterSection() : null}
            {showSocialRegisterButtons ? <Divider /> : null }
            {selectedProviderType ? this.renderRegisterForm() : this.renderBottomRegisterSection()}
            <Divider />
            {this.renderLoginSection()}
          </div>
        </Dialog>
      </div>
    );
  }

  /* ------ END Rendering ----- */
}

RegisterDialog.propTypes = {
  open: PropTypes.bool,
  registerDialog: PropTypes.object,
  userTypes: PropTypes.object,
  showSocialRegisterButtons: PropTypes.bool,
  // callbacks
  onCheckUserExists: PropTypes.func.isRequired,
  onClearRegisterForm: PropTypes.func.isRequired,
  onLoadUserTypes: PropTypes.func.isRequired,
  onRegister: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  onSetKeyVal: PropTypes.func.isRequired,
  onSetSelectedProviderType: PropTypes.func.isRequired,
  onSwitchToLogin: PropTypes.func.isRequired,
};

RegisterDialog.defaultProps = {
  showSocialRegisterButtons: false,
};

const mapStateToProps = createStructuredSelector({
  registerDialog: makeSelectRegisterDialog(),
  userTypes: makeSelectUserTypes(),
});

function mapDispatchToProps(dispatch) {
  return {
    onCheckUserExists: (providerType, authId, email) => dispatch(userExists(providerType, authId, email)),
    onClearRegisterForm: () => dispatch(clearRegisterForm()),
    onLoadUserTypes: () => dispatch(loadUserTypes()),
    onRegister: () => dispatch(register()),
    onSetKeyVal: (key, val) => dispatch(setKeyVal(key, val)),
    onSetSelectedProviderType: (providerType) => dispatch(setSelectedProviderType(providerType)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterDialog);
