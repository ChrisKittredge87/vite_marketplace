/*
 *
 * Auth reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CLEAR_USER,
  AUTH, AUTH_SUCCESS, AUTH_ERROR,
  EXISTING_SESSION_SUCCESS, EXISTING_SESSION_ERROR,
  LOGIN_SUCCESS, LOGIN_ERROR,
  LOGOUT_SUCCESS, LOGOUT_ERROR,
  REGISTER_SUCCESS, REGISTER_ERROR,
  /* State Keys */
  ERRORS,
  LOADING_AUTH,
  USER_ID,
} from './constants';

const initialState = fromJS({
  [LOADING_AUTH]: false,
});

function authReducer(state = initialState, action) {
  const { error, type, result } = action;
  switch (type) {
    case CLEAR_USER:
      return state.delete(USER_ID);
    case AUTH:
      return state.set(LOADING_AUTH, true);
    case AUTH_SUCCESS:
      return state
        .set(LOADING_AUTH, false)
        .set(USER_ID, result);
    case AUTH_ERROR:
      return state
        .set(LOADING_AUTH, false)
        .set(ERRORS, error);
    case EXISTING_SESSION_SUCCESS:
      return state.set(USER_ID, result);
    case EXISTING_SESSION_ERROR:
      return state.set(ERRORS, error);
    case LOGIN_SUCCESS:
      return state.set(USER_ID, result);
    case LOGIN_ERROR:
      return state.set(ERRORS, error);
    case LOGOUT_SUCCESS:
      return state.delete(USER_ID);
    case LOGOUT_ERROR:
      return state.set(ERRORS, error);
    case REGISTER_SUCCESS:
      return state.set(USER_ID, result);
    case REGISTER_ERROR:
      return state.set(ERRORS, error);
    default:
      return state;
  }
}

export default authReducer;

export const DOMAIN = 'auth';
