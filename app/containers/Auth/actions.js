/*
 *
 * Auth actions
 *
 */
import { createSagaActions } from 'utils/sagas';
import { entity as schema } from 'entities/schemas/users';

import {
  CLEAR_USER,
  AUTH, AUTH_SUCCESS, AUTH_ERROR,
  EXISTING_SESSION, EXISTING_SESSION_SUCCESS, EXISTING_SESSION_ERROR,
  LOGIN, LOGIN_SUCCESS, LOGIN_ERROR,
  LOGOUT, LOGOUT_SUCCESS, LOGOUT_ERROR,
  REGISTER, REGISTER_SUCCESS, REGISTER_ERROR,
  USER_EXISTS, USER_EXISTS_SUCCESS, USER_EXISTS_ERROR,
} from './constants';

export function clearUser() {
  return {
    type: CLEAR_USER,
  };
}

export const {
  loadEntity: auth,
  entityLoaded: authSuccess,
  entityLoadingError: authError,
} = createSagaActions({
  loadAction: AUTH,
  entityLoadedAction: AUTH_SUCCESS,
  entityLoadingErrorAction: AUTH_ERROR,
  schema,
});

export const {
  loadEntity: existingSession,
  entityLoaded: existingSessionSuccess,
  entityLoadingError: existingSessionError,
} = createSagaActions({
  loadAction: EXISTING_SESSION,
  entityLoadedAction: EXISTING_SESSION_SUCCESS,
  entityLoadingErrorAction: EXISTING_SESSION_ERROR,
  schema,
});

export const {
  loadEntity: login,
  entityLoaded: loginSuccess,
  entityLoadingError: loginError,
} = createSagaActions({
  loadAction: LOGIN,
  entityLoadedAction: LOGIN_SUCCESS,
  entityLoadingErrorAction: LOGIN_ERROR,
  schema,
});

export const {
  loadEntity: register,
  entityLoaded: registerSuccess,
  entityLoadingError: registerError,
} = createSagaActions({
  loadAction: REGISTER,
  entityLoadedAction: REGISTER_SUCCESS,
  entityLoadingErrorAction: REGISTER_ERROR,
  schema,
  notifyOnSuccess: () => 'Account created successfully!',
});

export function userExists(providerType, authId, email) {
  return { type: USER_EXISTS, authId, email, providerType };
}
export function userExistsSuccess(val, providerType) {
  return { type: USER_EXISTS_SUCCESS, val, providerType };
}
export function userExistsError() {
  return { type: USER_EXISTS_ERROR };
}

export function logout() {
  return { type: LOGOUT };
}
export function logoutSuccess() {
  return { type: LOGOUT_SUCCESS };
}
export function logoutError() {
  return { type: LOGOUT_ERROR };
}
