
import { fromJS } from 'immutable';
import authorizeReducer from '../reducer';

describe('authorizeReducer', () => {
  it('returns the initial state', () => {
    expect(authorizeReducer(undefined, {})).toEqual(fromJS({}));
  });
});
