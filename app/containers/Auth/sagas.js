import { call, put, select, takeLatest } from 'redux-saga/effects';

import { selectRegisterDialogDomain } from 'containers/Auth/RegisterDialog/selectors';
import {
  AUTH_ID,
  CONFIRM_PASSWORD,
  FIRST_NAME,
  LAST_NAME,
  SELECTED_PROVIDER_TYPE,
  SELECTED_USER_TYPE,
  PASSWORD,
  USERNAME,
} from 'containers/Auth/RegisterDialog/constants';
import {
  AUTH_URL, LOGOUT_URL, getAuthUrl,
  LOCAL_PROVIDER, FACEBOOK_PROVIDER, GOOGLE_PROVIDER,
} from 'entities/constants';
import { BASE_URL as USERS_URL } from 'entities/schemas/users/constants';
import request from 'utils/request';

import {
  authSuccess, authError,
  loginSuccess, loginError,
  logoutSuccess, logoutError,
  registerSuccess, registerError,
  userExistsSuccess, userExistsError,
} from './actions';
import { AUTH, LOGIN, LOGOUT, REGISTER, USER_EXISTS } from './constants';

// Individual exports for testing
export function* auth() {
  try {
    const user = yield call(request.get, AUTH_URL);
    yield put(authSuccess(user));
  } catch (err) {
    yield put(authError(err));
  }
}

export function* authData() {
  yield takeLatest(AUTH, auth);
}

export function* login({ options = {} }) {
  try {
    const { username, password, provider } = options;
    const user = yield call(request.post, getAuthUrl(provider), { data: { username, password } });
    yield put(loginSuccess(user));
  } catch (err) {
    yield put(loginError(err));
  }
}

export function* loginData() {
  yield takeLatest(LOGIN, login);
}

export function* logout() {
  try {
    const response = yield call(request.post, LOGOUT_URL);
    yield put(logoutSuccess(response));
  } catch (err) {
    yield put(logoutError(err));
  }
}

export function* logoutData() {
  yield takeLatest(LOGOUT, logout);
}

export function* register() {
  try {
    const registerDialog = yield select(selectRegisterDialogDomain());
    const providerType = registerDialog.get(SELECTED_PROVIDER_TYPE);
    const user = {
      email: registerDialog.get(USERNAME),
      firstName: registerDialog.get(FIRST_NAME),
      lastName: registerDialog.get(LAST_NAME),
      accountTypeId: registerDialog.get(SELECTED_USER_TYPE),
    };
    if ([FACEBOOK_PROVIDER, GOOGLE_PROVIDER].indexOf(providerType) > -1) {
      user.authId = registerDialog.get(AUTH_ID);
    } else if (providerType === LOCAL_PROVIDER) {
      user.password = registerDialog.get(PASSWORD);
      user.passwordConfirmation = registerDialog.get(CONFIRM_PASSWORD);
    } else {
      throw new Error('Invalid Provider Type!');
    }
    const response = yield call(request.post, '/register', { data: { user } });
    yield put(registerSuccess(response));
  } catch (err) {
    yield put(registerError(err.message));
  }
}

export function* registerData() {
  yield takeLatest(REGISTER, register);
}

export function* userExists({ authId, email, providerType }) {
  try {
    const data = {};
    if (authId) { data.auth_id = authId; }
    if (email) { data.email = email; }
    const response = yield call(request.get, `${USERS_URL}`, { data });
    yield put(userExistsSuccess(response.length > 0, providerType));
  } catch (err) {
    yield put(userExistsError(err.message));
  }
}

export function* existingUserData() {
  yield takeLatest(USER_EXISTS, userExists);
}

// All sagas to be loaded
export default [
  authData,
  loginData,
  logoutData,
  registerData,
  existingUserData,
];

export const NAME = 'authSagas';
