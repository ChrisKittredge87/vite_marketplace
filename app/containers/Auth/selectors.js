import { createSelector } from 'reselect';

import { selectEntity as selectUser } from 'entities/schemas/users/selectors';
import { selectEntity as selectAccountType } from 'entities/schemas/users/types/selectors';
import { ACCOUNT_TYPE } from 'entities/schemas/users/constants';

import { USER_ID } from './constants';
import { DOMAIN } from './reducer';

/**
 * Direct selector to the authorize state domain
 */
const selectAuthorizeDomain = () => (state) => state.get(DOMAIN);

/**
 * Other specific selectors
 */


/**
 * Default selector used by Authorize
 */

const makeSelectAuthorize = () => createSelector(
  selectAuthorizeDomain(),
  (substate) => substate
);

const makeSelectCurrentUserId = () => createSelector(
  selectAuthorizeDomain(),
  (substate) => substate.get(USER_ID)
);

const makeSelectCurrentUser = () => createSelector(
  (state) => state,
  makeSelectCurrentUserId(),
  (state, id) => selectUser(id)(state)
);

const makeSelectCurrentUserAccountType = () => createSelector(
  (state) => state,
  makeSelectCurrentUser(),
  (state, currentUser) => selectAccountType(currentUser && currentUser.get(ACCOUNT_TYPE))(state)
);

export default makeSelectAuthorize;
export {
  selectAuthorizeDomain,
  makeSelectCurrentUserId,
  makeSelectCurrentUser,
  makeSelectCurrentUserAccountType,
};
