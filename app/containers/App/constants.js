/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

/* ACTIONS */
export const CHANGE_NAV_DRAWER_OPEN = 'app/App/CHANGE_NAV_DRAWER_OPEN';
export const SET_KEY_VAL = 'app/App/SET_KEY_VAL';

/* STATE KEYS */

export const LOGIN_DIALOG_OPEN = 'loginDialogOpen';
export const NAV_DRAWER_OPEN = 'navDrawerOpen';
export const REGISTER_DIALOG_OPEN = 'registerDialogOpen';

/* DEFAULTS */
export const DEFAULT_LOCALE = 'en';
