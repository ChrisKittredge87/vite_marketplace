/*
 *
 * App actions
 *
 */

import {
  CHANGE_NAV_DRAWER_OPEN,
  NAV_DRAWER_OPEN,
  SET_KEY_VAL,
} from './constants';

export function changeNavDrawerOpen(navDrawerOpen) {
  return { type: CHANGE_NAV_DRAWER_OPEN, [NAV_DRAWER_OPEN]: navDrawerOpen };
}

export function setKeyVal(key, val) {
  return { type: SET_KEY_VAL, key, val };
}
