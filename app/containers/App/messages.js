/*
 * App Messages
 *
 * This contains all the text for the Home component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  admin: {
    id: 'admin',
    defaultMessage: 'Admin',
  },
  companies: {
    id: 'companies',
    defaultMessage: 'Companies',
  },
  home: {
    id: 'home',
    defaultMessage: 'Home',
  },
  logIn: {
    id: 'logIn',
    defaultMessage: 'Log In',
  },
  logOut: {
    id: 'logOut',
    defaultMessage: 'Log Out',
  },
  randavooMarketplace: {
    id: 'randavooMarketplace',
    defaultMessage: 'Randavoo Marketplace',
  },
  search: {
    id: 'search',
    defaultMessage: 'Search',
  },
  signUp: {
    id: 'signUp',
    defaultMessage: 'Sign Up',
  },
  users: {
    id: 'users',
    defaultMessage: 'Users',
  },
});
