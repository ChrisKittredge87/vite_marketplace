/*
 *
 * LanguageProvider reducer
 *
 */

import { fromJS } from 'immutable';

import { LOGIN_SUCCESS, REGISTER_SUCCESS } from 'containers/Auth/constants';
import {
  /* Actions */
  CHANGE_NAV_DRAWER_OPEN,
  SET_KEY_VAL,
  /* State Keys */
  LOGIN_DIALOG_OPEN,
  NAV_DRAWER_OPEN,
  REGISTER_DIALOG_OPEN,
} from './constants';

const initialState = fromJS({
  [LOGIN_DIALOG_OPEN]: false,
  [NAV_DRAWER_OPEN]: false,
  [REGISTER_DIALOG_OPEN]: false,
});

function appReducer(state = initialState, action) {
  const { key, type, val } = action;
  switch (type) {
    case CHANGE_NAV_DRAWER_OPEN:
      return state
        .set(NAV_DRAWER_OPEN, action[NAV_DRAWER_OPEN]);
    case LOGIN_SUCCESS:
      return state.set(LOGIN_DIALOG_OPEN, false);
    case REGISTER_SUCCESS:
      return state.set(REGISTER_DIALOG_OPEN, false);
    case SET_KEY_VAL:
      return state.set(key, val);
    default:
      return state;
  }
}

export default appReducer;

export const DOMAIN = 'app';
