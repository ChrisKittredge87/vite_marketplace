import { createSelector } from 'reselect';
import { ROUTE_DOMAIN } from 'reducers';
import { DOMAIN } from './reducer';
import {
  NAV_DRAWER_OPEN,
} from './constants';

// makeSelectLocationState expects a plain JS object for the routing state
const makeSelectLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get(ROUTE_DOMAIN); // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};

const makeSelectNotifications = () => createSelector(
  (state) => state.get('notifications'),
  (notifications) => notifications
);

/**
 * Direct selector to the app state domain
 */
const selectAppDomain = () => (state) => state.get(DOMAIN);

const makeSelectApp = () => createSelector(
  selectAppDomain(),
  (substate) => substate
);

const makeSelectNavDrawerOpen = () => createSelector(
  selectAppDomain(),
  (appState) => appState.get(NAV_DRAWER_OPEN)
);

export default makeSelectApp;
export {
  makeSelectLocationState,
  makeSelectNavDrawerOpen,
  makeSelectNotifications,
};
