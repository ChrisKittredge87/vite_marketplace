/**
 *
 * App.react.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Link, browserHistory } from 'react-router';
import { FormattedMessage } from 'react-intl';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';
import Divider from 'material-ui/Divider';
import FlatButton from 'material-ui/FlatButton';
import { fromJS } from 'immutable';
import HomeIcon from 'material-ui/svg-icons/action/home';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Notifications from 'react-notification-system-redux';
import { StickyContainer, Sticky } from 'react-sticky';
import LogoutIcon from 'material-ui/svg-icons/action/exit-to-app';
import SearchIcon from 'material-ui/svg-icons/action/search';
import SettingsIcon from 'material-ui/svg-icons/action/settings';

import 'react-big-calendar/lib/css/react-big-calendar.css';
import 'react-virtualized/styles.css'; // only needs to be imported once
import 'react-input-range/lib/css/index.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import randavooLogo from 'img/Randavoo_Final_W.png';
import EventPlannerAppMenu from 'components/eventPlanners/AppMenu';
import ServiceProviderAppMenu from 'components/serviceProviders/AppMenu';
import { path as homePath } from 'containers/Home/routes';
import { auth, logout } from 'containers/Auth/actions';
import { makeSelectCurrentUser } from 'containers/Auth/selectors';
import { selectEntity as selectAccountType } from 'entities/schemas/users/types/selectors';
import LoginDialog from 'containers/Auth/LoginDialog';
import RegisterDialog from 'containers/Auth/RegisterDialog';
import { ACCOUNT_TYPE, AUTH_TOKEN, IS_ADMIN } from 'entities/schemas/users/constants';
import {
  EVENT_PLANNER,
  NAME,
  SERVICE_PROVIDER,
} from 'entities/schemas/users/types/constants';

import { changeNavDrawerOpen, setKeyVal } from './actions';
import { LOGIN_DIALOG_OPEN, REGISTER_DIALOG_OPEN } from './constants';
import messages from './messages';
import makeSelectAppState, { makeSelectNavDrawerOpen, makeSelectNotifications } from './selectors';
import theme from './theme';

injectTapEventPlugin();

class App extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    appState: PropTypes.object,
    children: PropTypes.node,
    currentUser: PropTypes.object,
    notifications: PropTypes.array,
    onLogout: PropTypes.func,
    onSetKeyVal: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.handleLogoutUser = this.handleLogoutUser.bind(this);
    this.handleNavHome = this.handleNavHome.bind(this);
    this.handleSwitchDialogs = this.handleSwitchDialogs.bind(this);
    this.handleToggleDialog = this.handleToggleDialog.bind(this);
  }

  /* ----- Lifecycle ----- */

  componentWillMount() {
    // const { onExisitingSession } = this.props;
    // const authToken = window.sessionStorage.getItem(AUTH_TOKEN);
    // if (authToken) {
    //   onExisitingSession(authToken);
    // }
  }

  componentWillReceiveProps(nextProps) {
    const { onCheckAuth } = this.props;
    const { currentUser } = nextProps;
    if (!currentUser) {
      window.localStorage.removeItem(AUTH_TOKEN);
    } else if (this.props.currentUser !== currentUser) {
      window.localStorage.setItem(AUTH_TOKEN, currentUser.get(AUTH_TOKEN));
      onCheckAuth();
    }
  }

  /* ----- END Lifecycle ----- */

  /* ----- Handlers ----- */

  handleLogoutUser() {
    this.props.onLogout();
    this.handleNavHome();
  }

  handleNavHome() {
    browserHistory.push(homePath);
  }

  handleSwitchDialogs(dialogToClose, dialogToOpen) {
    const { onSetKeyVal } = this.props;
    return () => {
      onSetKeyVal(dialogToClose, false);
      onSetKeyVal(dialogToOpen, true);
    };
  }

  handleToggleDialog(dialogOpenKey, val) {
    const { onSetKeyVal } = this.props;
    return () => onSetKeyVal(dialogOpenKey, val);
  }

  /* ----- END Handlers ----- */

  /* ----- Rendering ----- */

  renderLoginButton() {
    return (<div style={{ marginTop: 6 }}>
      <FlatButton
        label={<FormattedMessage {...messages.signUp} />}
        onClick={this.handleToggleDialog(REGISTER_DIALOG_OPEN, true)}
        style={{ color: 'white' }}
      />
      <FlatButton
        label={<FormattedMessage {...messages.logIn} />}
        onClick={this.handleToggleDialog(LOGIN_DIALOG_OPEN, true)}
        style={{ color: 'white' }}
      />
    </div>);
  }

  renderDropdownMenu() {
    const { currentUser, getAccountType } = this.props;
    const appMenuProps = {
      isAdmin: currentUser.get(IS_ADMIN),
      onLogoutUser: this.handleLogoutUser,
    };
    const userAccountType = getAccountType(currentUser.get(ACCOUNT_TYPE)) || fromJS({});
    switch(userAccountType.get(NAME)) {
      case EVENT_PLANNER:
        return <EventPlannerAppMenu {...appMenuProps} />;
      case SERVICE_PROVIDER:
        return <ServiceProviderAppMenu {...appMenuProps} />;
      default:
        return this.renderLoginButton();
    }
  }

  render() {
    const { appState, notifications, currentUser } = this.props;
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(theme)}>
        <StickyContainer>
          <Sticky style={{ position: 'relative', zIndex: 4 }}>
            <AppBar
              iconElementLeft={
                <Link
                  style={{ color: 'white' }}
                  to={homePath}
                >
                  <img
                    alt="randavoo_logo"
                    src={randavooLogo}
                    height="40"
                    style={{ position: 'absolute', left: 15, top: 10 }}
                  />
              </Link>
              }
              iconElementRight={currentUser ? this.renderDropdownMenu() : this.renderLoginButton()}
            />
          </Sticky>
          {React.Children.toArray(this.props.children)}
          <Notifications notifications={notifications} />
          <LoginDialog
            open={appState.get(LOGIN_DIALOG_OPEN)}
            onRequestClose={this.handleToggleDialog(LOGIN_DIALOG_OPEN, false)}
            onSwitchToRegister={this.handleSwitchDialogs(LOGIN_DIALOG_OPEN, REGISTER_DIALOG_OPEN)}
          />
          <RegisterDialog
            open={appState.get(REGISTER_DIALOG_OPEN)}
            onRequestClose={this.handleToggleDialog(REGISTER_DIALOG_OPEN, false)}
            onSwitchToLogin={this.handleSwitchDialogs(REGISTER_DIALOG_OPEN, LOGIN_DIALOG_OPEN)}
          />
        </StickyContainer>
      </MuiThemeProvider>
    );
  }

  /* ----- END Rendering ----- */
}

const mapStateToProps = createStructuredSelector({
  appState: makeSelectAppState(),
  currentUser: makeSelectCurrentUser(),
  getAccountType: (state) => (id) => selectAccountType(id)(state),
  navDrawerOpen: makeSelectNavDrawerOpen(),
  notifications: makeSelectNotifications(),
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeNavDrawerOpen: (navDrawerOpen) => dispatch(changeNavDrawerOpen(navDrawerOpen)),
    onCheckAuth: () => dispatch(auth()),
    onLogout: () => dispatch(logout()),
    onSetKeyVal: (key, val) => dispatch(setKeyVal(key, val)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
