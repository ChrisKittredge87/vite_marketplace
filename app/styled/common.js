import styled from 'styled-components';
import { MD, LG } from 'globalConstants';
import _ from 'lodash';

export const Crop = styled.div`
  height: ${({ height }) => _.isNumber(height) ? `${height}px` : height};
  overflow: hidden;
`;

export const DynamicImg = styled.div`
  background: url(${({ src }) => src}) 0% 50% / cover no-repeat;
  background-position: center;
  height: 100%;
  width: 100%;
`;

export const RespDiv = styled.div`
  cursor: pointer;
  margin: 0 0.5%;
  width: ${(props) => (100 / props.cols) - 3}%;
  @media (min-width: ${MD}px) {
    width: ${(props) => (100 / props.cols) - 2.5}%;
  }
  @media (min-width: ${LG}px) {
    width: ${(props) => (100 / props.cols) - 2}%;
  }
`;

export const SpaceBetweenFlex = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
`;

export const SelectableMenuItem = styled.div`
  border: 1px solid darkgray;
  padding: 10px;
  cursor: pointer;
  border-bottom: ${({ last }) => !last ? 'none' : null};
  background: ${({ selected }) => selected ? 'gray' : 'white'};
  color: ${({ selected }) => selected ? 'white' : 'black'};
  transition: background-color 0.2s ease-in, color 0.2s ease-in;
  &:hover {
    background: ${({ selected }) => selected ? 'gray' : 'rgba(0, 0, 0, 0.09)'};
    color: ${({ selected }) => selected ? 'white' : 'black'};
  }
`;
