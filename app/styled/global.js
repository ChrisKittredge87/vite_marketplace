/* eslint no-unused-expressions: 0 */
import { injectGlobal } from 'styled-components';

injectGlobal`
  * {
    font-family: 'Roboto', serif;
  }
  .react-rater {
    font-size: 16px;
    a:hover {
      color: #ffd88a;
    }
    .is-active, .will-be-active {
      color: #ffcc66;
    }
  }
  .rbc-btn-group {
    button {
      cursor: pointer;
    }
  }
  .rbc-toolbar {
    .rbc-toolbar-label {
      font-size: 20px;
      font-weight: bold;
      padding: 0 10px 0 96px !important;
    }
  }
  .rbc-month-row, .rbc-time-view {
    cursor: pointer;
  }
`;
