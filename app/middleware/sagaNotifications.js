/*
** Saga Notifications Middleware
** Intercepts actions with notifyOnSuccess or notifyOnError
** Dispatches an action to show a notification
*/

import Notifications from 'react-notification-system-redux';
import _ from 'lodash';

const sagaNotificationsMiddleware = (store) => (next) => (action) => {
  if (_.isFunction(action.notifyOnSuccess) && action.type.toLowerCase().indexOf('success') > -1) {
    store.dispatch(Notifications.success({
      title: 'Success!',
      message: action.notifyOnSuccess(action.message),
      position: 'bc',
      autoDismiss: 2000,
    }));
  } else if (_.isFunction(action.notifyOnError) && action.type.toLowerCase().indexOf('error') > -1) {
    store.dispatch(Notifications.error({
      title: 'Error!',
      message: action.notifyOnError(action.error),
      position: 'bc',
      autoDismiss: 0,
    }));
  }
  next(action);
};

export default sagaNotificationsMiddleware;
