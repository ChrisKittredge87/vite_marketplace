/**
 * Combine all reducers in this file and export the combined reducers.
 * If we were to do this in store.js, reducers wouldn't be hot reloadable.
 */

import { combineReducers } from 'redux-immutable';
import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import { reducer as notifications } from 'react-notification-system-redux';

import appReducer, { DOMAIN as APP_DOMAIN } from 'containers/App/reducer';
import authReducer, { DOMAIN as AUTH_DOMAIN } from 'containers/Auth/reducer';
import { LOGOUT } from 'containers/Auth/constants';
import loginDialogReducer, { DOMAIN as LOGIN_DIALOG_DOMAIN } from 'containers/Auth/LoginDialog/reducer';
import { DOMAIN as LANGUAGE_PROVIDER_DOMAIN } from 'containers/LanguageProvider/constants';
import languageProviderReducer from 'containers/LanguageProvider/reducer';
import registerDialogReducer, { DOMAIN as REGISTER_DIALOG_DOMAIN } from 'containers/Auth/RegisterDialog/reducer';
import entitiesReducer, { DOMAIN as ENTITIES_DOMAIN } from 'entities/reducer';

/*
 * routeReducer
 *
 * The reducer merges route location changes into our immutable state.
 * The change is necessitated by moving to react-router-redux@4
 *
 */

// Initial routing state
const routeInitialState = fromJS({
  locationBeforeTransitions: null,
});

/**
 * Merge route into the global application state
 */
export const ROUTE_DOMAIN = 'route';

function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
    /* istanbul ignore next */
    case LOCATION_CHANGE:
      return state.merge({
        locationBeforeTransitions: action.payload,
      });
    default:
      return state;
  }
}

/**
 * Creates the main reducer with the asynchronously loaded ones
 */
export default function createReducer(asyncReducers) {
  function rootReducer(state, action) {
    const reducers = {
      [APP_DOMAIN]: appReducer,
      [AUTH_DOMAIN]: authReducer,
      [ENTITIES_DOMAIN]: entitiesReducer,
      [LANGUAGE_PROVIDER_DOMAIN]: languageProviderReducer,
      [LOGIN_DIALOG_DOMAIN]: loginDialogReducer,
      [REGISTER_DIALOG_DOMAIN]: registerDialogReducer,
      [ROUTE_DOMAIN]: routeReducer,
      notifications,
      ...asyncReducers,
    };
    if (action.type === LOGOUT) {
      return combineReducers(reducers)(fromJS({}), action);
    }
    return combineReducers(reducers)(state, action);
  }

  return rootReducer;
}
