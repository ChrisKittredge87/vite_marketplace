import { createSelector } from 'reselect';
import { DOMAIN } from './reducer';

const selectEntitiesDomain = () => (state) => state.get(DOMAIN);

export default createSelector(selectEntitiesDomain);
