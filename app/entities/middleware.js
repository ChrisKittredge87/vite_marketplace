/*
** Entities Middleware
** Intercepts entites that come in with a schema
** Transforms and returns a result that includes
** entities and result (id/ids)
*/

import { normalize } from 'normalizr';
import _ from 'lodash';

import { addEntity, deleteEntity } from './actions';
import { ENTITY_IDS_TO_DELETE, ENTITY, ID } from './constants';

const entitiesMiddleware = (store) => (next) => (action) => {
  if (action.schema) {
    if (action[ENTITY]) {
      if (action.deleteOnSuccess) {
        const schemaKey = action.schema.schema ? action.schema.schema._key : action.schema._key;
        store.dispatch(deleteEntity(schemaKey, [].concat(action[ENTITY][ID])));
        next(_.assign({}, action, { result: action[ENTITY_IDS_TO_DELETE] }));
      } else {
        const normalizedEntity = normalize(action[ENTITY], action.schema);
        store.dispatch(addEntity(normalizedEntity));
        next(_.assign({}, action, normalizedEntity));
      }
    } else if (action[ENTITY_IDS_TO_DELETE]) {
      const schemaKey = action.schema.schema ? action.schema.schema._key : action.schema._key;
      store.dispatch(deleteEntity(schemaKey, action[ENTITY_IDS_TO_DELETE]));
      next(_.assign({}, action, { result: action[ENTITY_IDS_TO_DELETE] }));
    }
  } else {
    next(action);
  }
};

export default entitiesMiddleware;
