/*
 *
 * ServiceProviderDetail reducer
 *
 */

import { fromJS } from 'immutable';
import { LOGOUT_SUCCESS } from 'containers/Auth/constants';
import { SCHEMA_KEY as USERS } from 'entities/schemas/users/constants';
import {
  /* Actions */
  ADD_ENTITY, DELETE_ENTITY,
} from './constants';

export default function (state = fromJS({}), action) {
  const { entities, ids = [], schemaKey, type } = action;
  let newState = state;
  switch (type) {
    case ADD_ENTITY:
      return state.mergeDeep(entities);
    case DELETE_ENTITY:
      ids.forEach((id) => { newState = newState.deleteIn([schemaKey, `${id}`]); });
      return newState;
    case LOGOUT_SUCCESS:
      return state.delete(USERS);
    default:
      return state;
  }
}

export const DOMAIN = 'entities';
