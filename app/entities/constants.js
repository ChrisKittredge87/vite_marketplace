
/* Domains */
export const DOMAIN = 'entities';

/* Actions */
export const ADD_ENTITY = 'app/entities/ADD_ENTITY';
export const DELETE_ENTITY = 'app/entities/DELETE_ENTITY';

/* State Keys */
export const DELETE = 'delete';
export const ENTITY = 'entity';
export const ENTITY_IDS_TO_DELETE = 'entityIdsToDelete';
export const ID = 'id';

/* URLS */
export const API_BASE_URL = '/api';
export const AUTH_URL = '/auth';
export const LOCAL_PROVIDER = 'local';
export const FACEBOOK_PROVIDER = 'facebook';
export const GOOGLE_PROVIDER = 'google';
export const LOGOUT_URL = '/logout';

export const getAuthUrl = (provider) => `/auth/${provider}`;
