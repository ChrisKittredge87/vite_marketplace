import { ADD_ENTITY, DELETE_ENTITY } from './constants';

export function addEntity({ entities }) { return { type: ADD_ENTITY, entities }; }
export function deleteEntity(schemaKey, ids) { return { type: DELETE_ENTITY, schemaKey, ids }; }
