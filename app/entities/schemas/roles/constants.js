import { API_BASE_URL } from 'entities/constants';

export const SCHEMA_KEY = 'roles';

export const ID = 'id';
export const NAME = 'name';
export const DESCRIPTION = 'description';
export const DISPLAY_NAME = 'displayName';

export const BASE_URL = `${API_BASE_URL}/roles`;
