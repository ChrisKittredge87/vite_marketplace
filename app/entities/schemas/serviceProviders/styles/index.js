import { schema } from 'normalizr';
import { SCHEMA_KEY } from './constants';

export const entity = new schema.Entity(SCHEMA_KEY);
export const list = new schema.Array(entity);
