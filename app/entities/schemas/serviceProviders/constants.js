import { API_BASE_URL } from 'entities/constants';

export const SCHEMA_KEY = 'serviceProviders';

export const ADDRESS = 'address';
export const AVG_RATING = 'averageRating';
export const CAN_CONTRIBUTE = 'canContribute';
export const CAN_EDIT = 'canEdit';
export const COMPANY_ID = 'companyId';
export const ID = 'id';
export const IMAGES = 'images';
export const LAT = 'lat';
export const LOCALITY = 'locality';
export const LON = 'lon';
export const NAME = 'name';
export const OVERVIEW = 'overview';
export const REGION = 'region';
export const SERVICE_PROVIDER_TYPE_ID = 'serviceProviderTypeId';
export const SERVICE_PROVIDER_TYPE = 'serviceProviderType';

export const BASE_URL = `${API_BASE_URL}/service_providers`;
export const TTL = 60 * 15; // 15 minutes
