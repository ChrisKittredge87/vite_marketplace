import { API_BASE_URL } from 'entities/constants';

export const SCHEMA_KEY = 'cuisineTypes';

export const DESCRIPTION = 'description';
export const DISPLAY_NAME = 'displayName';
export const ID = 'id';
export const NAME = 'name';

export const BASE_URL = `${API_BASE_URL}/cuisine_types`;
export const TTL = 60 * 15; // 15 minutes
