import { BASE_URL as SP_BASE_URL } from 'entities/schemas/serviceProviders/constants';

export const SCHEMA_KEY = 'serviceProviderReviews';

export const ID = 'id';
export const DATE = 'createdAt';
export const HEADER = 'header';
export const NAME = 'name';
export const RATING = 'rating';
export const REVIEWER_ID = 'reviewerId';
export const REVIEWER_NAME = 'reviewerName';
export const TEXT = 'text';
export const USER = 'user';

export const BASE_URL = `${SP_BASE_URL}/{id}/reviews`;
export const TTL = 60 * 5; // 5 minutes
