import { schema } from 'normalizr';
import { SCHEMA_KEY } from './constants';
import { entity as accountType } from './types'

export const entity = new schema.Entity(SCHEMA_KEY, { accountType });
export const list = new schema.Array(entity);
