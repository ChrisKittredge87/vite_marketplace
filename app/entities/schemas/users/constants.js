import { API_BASE_URL } from 'entities/constants';

export const SCHEMA_KEY = 'users';

export const ACCOUNT_TYPE = 'accountType';
export const AUTH_TOKEN = 'authToken';
export const EMAIL = 'email';
export const FIRST_NAME = 'firstName';
export const ID = 'id';
export const IS_ADMIN = 'isAdmin';
export const LAST_NAME = 'lastName';
export const MANAGER_ID = 'managerId';
export const MANAGED_USERS = 'managedUsers';

export const BASE_URL = `${API_BASE_URL}/users`;
