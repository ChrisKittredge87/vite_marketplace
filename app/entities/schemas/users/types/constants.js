import { API_BASE_URL } from 'entities/constants';

export const SCHEMA_KEY = 'userTypes';

export const DESCRIPTION = 'description';
export const DISPLAY_NAME = 'displayName';
export const ID = 'id';
export const NAME = 'name';

// TYPES
export const EVENT_PLANNER = 'event_planner';
export const SERVICE_PROVIDER = 'service_provider';

export const BASE_URL = `${API_BASE_URL}/account_types`;
