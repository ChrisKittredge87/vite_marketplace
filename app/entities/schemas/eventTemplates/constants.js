import { API_BASE_URL } from 'entities/constants';

export const SCHEMA_KEY = 'eventTemplates';

// DATE TIME BUDGET
export const DONT_SET_BUDGET = 'dontSetBudget';
export const START_DATE = 'startDate';
export const END_DATE = 'endDate';
export const NUMBER_OF_EXPECTED_GUESTS = 'numberOfExpectedGuests';
export const OVERALL_BUDGET = 'overallBudget';
export const ADDRESS = 'address';

// Venue
export const LON = 'lon';
export const LAT = 'lat';
export const INCLUDE_IN_HOUSE_VENDORS = 'includeInHouseVendors';
export const INCLUDE_OUTSIDE_VENDORS = 'includeOutsideVendors';
export const RADIUS = 'radius';
export const NEEDS_VENUE = 'needsVenue';
export const VENUE_BUDGET = 'venueBudget';
export const VENUE_EXPERIENCE_ID = 'venueExperienceId';

// Vendors
export const VENDOR_BUDGET = 'vendorBudget';
export const CATERER_QUANITIY = 'catererQuantity';
export const BUDGET_PER_CATERER = 'budgetPerCaterer';
export const FOOD_TRUCK_QUANTITY = 'foodTruckQuantity';
export const BUDGET_PER_FOOD_TRUCK = 'budgetPerFoodTruck';
export const CUISINE_TYPE_IDS = 'cuisineTypeIds';

// Name and Description
export const NAME = 'name';
export const DESCRIPTION = 'description';

export const BASE_URL = `${API_BASE_URL}/event_templates`;
export const TTL = 60 * 15; // 15 minutes
