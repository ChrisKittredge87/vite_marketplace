import {
  createEntitySaga as create,
  updateEntitySaga as update,
  deleteEntitySaga as deleteSaga,
  getEntitiesSaga as get,
} from 'utils/sagas';

import { BASE_URL } from './constants';

export const createEntitySaga = ({ actionKey, successAction, errorAction }) =>
  create({ url: BASE_URL, actionKey, successAction, errorAction });

export const updateEntitySaga = ({ actionKey, successAction, errorAction }) =>
  update({ url: `${BASE_URL}/{id}`, actionKey, successAction, errorAction });

export const deleteEntitySaga = ({ actionKey, successAction, errorAction }) =>
  deleteSaga({ url: `${BASE_URL}/{id}`, actionKey, successAction, errorAction });

export const getEntitiesSaga = ({ actionKey, successAction, errorAction, query }) =>
  get({ url: BASE_URL, actionKey, successAction, errorAction, query });
