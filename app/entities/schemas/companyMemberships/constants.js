import { API_BASE_URL } from 'entities/constants';

export const SCHEMA_KEY = 'companyMemberships';

export const CAN_EDIT = 'canEdit';
export const COMPANY_ID = 'companyId';
export const ID = 'id';
export const ROLE_ID = 'roleId';
export const USER_ID = 'userId';

export const BASE_URL = `${API_BASE_URL}/company_memberships`;
