import { API_BASE_URL } from 'entities/constants';

export const SCHEMA_KEY = 'companies';

export const ADDRESS = 'address';
export const CAN_CONTRIBUTE = 'canContribute';
export const CAN_EDIT = 'canEdit';
export const COMPANY_TYPE = 'companyType';
export const COMPANY_TYPE_ID = 'companyTypeId';
export const ID = 'id';
export const NAME = 'name';
export const PARENT_ID = 'parentId';

export const BASE_URL = `${API_BASE_URL}/companies`;
export const TTL = 60 * 15; // 15 minutes
