import { API_BASE_URL } from 'entities/constants';

export const SCHEMA_KEY = 'companyTypes';

export const ID = 'id';
export const DESCRIPTION = 'description';
export const DISPLAY_NAME = 'displayName';
export const NAME = 'name';

export const BASE_URL = `${API_BASE_URL}/company_types`;
