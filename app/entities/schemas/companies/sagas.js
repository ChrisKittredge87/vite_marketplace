import {
  createEntitySaga as create,
  getEntitiesSaga as get,
} from 'utils/sagas';

import { BASE_URL, TTL } from './constants';

export const createEntitySaga = ({ actionKey, successAction, errorAction }) =>
  create({ url: BASE_URL, actionKey, successAction, errorAction });

export const getEntitiesSaga = ({ actionKey, successAction, errorAction, query }) =>
  get({ url: BASE_URL, ttl: TTL, actionKey, successAction, errorAction, query });
