// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';
import _ from 'lodash';

import Auth from 'containers/Auth';
import adminRoutes from 'containers/Admin/routes';
import spEventsRoutes from 'containers/serviceProviders/Events/routes';
import epEventsRoutes from 'containers/eventPlanners/Events/routes';
import homeRoutes from 'containers/Home/routes';
import serviceProvidersSearchRoutes from 'containers/serviceProviders/Search/routes';
import serviceProviderProfileRoutes from 'containers/serviceProviders/Profile/routes';
import notFoundRoutes from 'containers/NotFoundPage/routes';

const loadModule = (cb) =>
  (componentModule) => cb(null, componentModule.default);

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  const injectMultipleReducers = (reducers) =>
    _.map(reducers, (reducer) => injectReducer(reducer.DOMAIN, reducer.default));

  const injectMultipleSagas = (sagas) =>
    _.map(sagas, (saga) => injectSagas(saga.NAME, saga.default));

  const args = [loadModule, errorLoading, injectMultipleReducers, injectMultipleSagas];

  const admin = adminRoutes(...args);
  const epEvents = epEventsRoutes(...args);
  const home = homeRoutes(...args);
  const spEvents = spEventsRoutes(...args);
  const spProfile = serviceProviderProfileRoutes(...args);
  const spSearch = serviceProvidersSearchRoutes(...args);
  const notFound = notFoundRoutes(...args);

  const auth = {
    component: Auth,
    childRoutes: [
      admin,
      home,
      epEvents,
      spEvents,
      spProfile,
      spSearch,
      notFound,
    ],
  };

  return [auth];
}
