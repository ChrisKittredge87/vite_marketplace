/* Reducer Utils */

import { fromJS } from 'immutable';

export function errorState(state, error, { loadingKey, errorKey }) {
  return state
    .set(loadingKey, false)
    .set(errorKey, state.get(errorKey).concat(error.message || error.exception));
}

export function successState(state, result, { loadingKey, resultsKey, concat = false, removeEntity = false }) {
  const newState = state.set(loadingKey, false);
  if (concat) {
    return newState.set(resultsKey, state.get(resultsKey).concat(result));
  } else if (removeEntity) {
    return newState
      .set(resultsKey, state
        .get(resultsKey)
        .filter((id) => [].concat(result).indexOf(id) === -1));
  }
  return newState.set(resultsKey, fromJS(result));
}
