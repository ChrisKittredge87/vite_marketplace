import { createSelector } from 'reselect';
import { fromJS } from 'immutable';

import entitiesDomain from 'entities/selectors';

export default function createSelectors(schemaKey) {
  const selectEntity = (id) => createSelector(
    entitiesDomain(),
    (entities) => entities.getIn([schemaKey, `${id}`])
  );

  const selectList = (ids = fromJS([])) => createSelector(
    entitiesDomain(),
    (entities) => ids.map((id) => entities.getIn([schemaKey, `${id}`]))
  );

  return { selectEntity, selectList };
}
