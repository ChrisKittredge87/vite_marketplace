import 'whatwg-fetch';
import _ from 'lodash';

// import { OAUTH_ACCESS_TOKEN } from 'globalConstants';

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  return response.json();
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

function getQueryString(params) {
  const esc = encodeURIComponent;
  return _.chain(params)
    .keys()
    .map((k) => {
      if (_.isArray(params[k])) {
        return _.map(params[k], (x) => `${esc(k)}=${esc(x)}`);
      }
      return `${esc(k)}=${esc(params[k])}`;
    })
    .flatten()
    .join('&')
    .value();
}

function request(url, params) {
  const method = params.method || 'GET';
  let qs = '';
  let body;
  const credentials = 'include';
  const authorization = window.localStorage.getItem('authToken');
  const headers = params.headers || {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: authorization,
  };
  // const accessToken = localStorage.getItem(OAUTH_ACCESS_TOKEN);
  let data = params.data; // _.assign({}, params.data, accessToken ? { access_token: accessToken } : null);
  if (params.data instanceof FormData) {
    data = params.data;
    // data.append('access_token', accessToken);
  }
  if (['GET', 'DELETE'].indexOf(method) > -1) {
    const qsData = getQueryString(data);
    qs = qsData.length ? `?${qsData}` : '';
  } else { // POST or PUT
    body = data instanceof FormData ? data : JSON.stringify(data);
  }

  return fetch(`${url}${qs}`, { method, headers, body, credentials })
    .then(checkStatus)
    .then(parseJSON);
}

export default {
  get: (url, params) => request(url, Object.assign({ method: 'GET' }, params)),
  post: (url, params) => request(url, Object.assign({ method: 'POST' }, params)),
  put: (url, params) => request(url, Object.assign({ method: 'PUT' }, params)),
  delete: (url, params) => request(url, Object.assign({ method: 'DELETE' }, params)),
};
