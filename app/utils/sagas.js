import { call, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import murl from 'murl';
import _ from 'lodash';

import { AUTH_TOKEN } from 'entities/schemas/users/constants';

// Add a request interceptor
axios.interceptors.request.use((config) => {
  // Do something before request is sent
  const authorization = window.localStorage.getItem(AUTH_TOKEN);
  return _.assign({}, config, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: authorization,
    },
  });
}, (error) => Promise.reject(error));

export const createSagaActions = ({
  loadAction, entityLoadedAction, entityLoadingErrorAction,
  schema, defaultOptions,
  notifyOnSuccess, notifyOnError,
}) => {
  const loadEntity = (id, options, entity) => ({
    type: loadAction,
    id,
    options: _.assign({}, defaultOptions, options),
    entity,
  });
  const entityLoaded = (entity, pagination) => ({
    type: entityLoadedAction,
    entity,
    pagination,
    notifyOnSuccess,
    schema,
  });
  const entityLoadingError = (error) => ({
    type: entityLoadingErrorAction,
    error,
    notifyOnError,
  });
  return { loadEntity, entityLoaded, entityLoadingError };
};

export const createUpsertSagaActions = ({
  loadAction, entityLoadedMergeAction, entityLoadedDeleteAction, entityLoadingErrorAction,
  schema,
  notifyOnSuccess, notifyOnError,
}) => {
  const loadEntity = ({ created, updated, deletedIds }) => ({
    type: loadAction,
    created,
    updated,
    deletedIds,
  });
  const entityLoaded = ({ entitiesToMerge = [], entityIdsToDelete = [] }) => ({
    merge: () => ({
      type: entityLoadedMergeAction,
      entity: entitiesToMerge,
      notifyOnSuccess,
      schema,
    }),
    delete: () => ({
      type: entityLoadedDeleteAction,
      entityIdsToDelete,
      schema,
    }),
  });
  const entityLoadingError = (error) => ({
    type: entityLoadingErrorAction,
    error,
    notifyOnError,
  });
  return { loadEntity, entityLoaded, entityLoadingError };
};

function buildUrl(url, id, query) {
  const esc = encodeURIComponent;
  const queryString = _.chain(query)
    .keys()
    .map((k) => {
      if (_.isArray(query[k])) {
        return _.map(query[k], (x) => `${esc(k)}=${esc(x)}`);
      }
      return `${esc(k)}=${esc(query[k])}`;
    })
    .flatten()
    .join('&')
    .value();
  const urlMatched = murl(url)(_.assign({}, query, { id }));
  if (!!id) {
    return (queryString.length ? `${urlMatched}/?${queryString}` : urlMatched).replace('{id}', id);
  }
  return queryString.length ? `${urlMatched}?${queryString}` : urlMatched;
}

export function getEntitiesSaga({ url, actionKey, successAction, errorAction, query: initQuery }) {
  function* get({ id, query: callQuery }) {
    try {
      const entities = yield call(
        axios.get.bind(axios, buildUrl(url, id, _.assign({}, initQuery, callQuery)))
      );
      yield put(successAction(entities.data));
    } catch (err) {
      yield put(errorAction(err));
    }
  }

  function* data() {
    yield takeLatest(actionKey, get);
  }
  // This is used to ensure that saga is only ever inserted once during asynchronous saga loading
  Object.defineProperty(data, 'name', { value: `${actionKey}_saga` });
  return data;
}

function cudEntitySaga(action, { url, actionKey, successAction, errorAction }) {
  function* create({ id, entity }) {
    try {
      const { data: persistedEntity } = yield call(action, url.replace('{id}', id), entity);
      yield put(successAction(persistedEntity));
    } catch (err) {
      yield put(errorAction(err));
    }
  }

  function* data() {
    yield takeLatest(actionKey, create);
  }
  Object.defineProperty(data, 'name', { value: `${actionKey}_saga` });
  return data;
}

export const createEntitySaga = (options) => cudEntitySaga(axios.post, options);
export const updateEntitySaga = (options) => cudEntitySaga(axios.put, options);
export const deleteEntitySaga = (options) => cudEntitySaga(axios.delete, options);
