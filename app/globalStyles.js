import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  * {
    min-height: 0;
    min-width: 0;
    font-family: 'Roboto', serif;
  }
  html,
  body {
    height: 100%;
    width: 100%;
  }

  a {
    text-decoration: none;
    color: inherit;
    &:link {
      color: inherit;
    }
    &:visited {
      color: inherit;
    }
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }

  .slick-arrow {
    position: absolute;
    z-index: 2;
    &.slick-prev {
      left: 20px;
    }
    &.slick-next {
      right: 20px;
    }
  }
`;
